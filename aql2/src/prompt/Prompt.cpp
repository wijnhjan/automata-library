/*
 * Prompt.cpp
 *
 *  Created on: 20. 7. 2017
 *	  Author: Jan Travnicek
 */

#include "Prompt.h"

Prompt::Prompt ( cli::Environment environment ) : m_environment ( std::move ( environment ) ) {
}

cli::CommandResult Prompt::run ( ) {
	cli::CommandResult res = cli::CommandResult::OK;

	while ( ! m_lineInterfaces.empty ( ) ) {

		cli::CommandResult state = getEnvironment ( ).execute ( m_lineInterfaces.front ( ) );

		if ( state == cli::CommandResult::QUIT )
			return state;
		if ( state == cli::CommandResult::RETURN )
			res = cli::CommandResult::RETURN;

		m_lineInterfaces.pop_front ( );
	}

	return res;
}
