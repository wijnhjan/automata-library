#include <catch2/catch.hpp>
#include <alib/vector>

#include "testing/TimeoutAqlTest.hpp"
#include "testing/TestFiles.hpp"

TEST_CASE ( "Determinization", "[integration]" ) {
	auto definition = GENERATE ( as < std::tuple < std::string, std::string, std::string > > ( ),
			std::make_tuple ( "automaton::determinize::Determinize - | automaton::simplify::Trim -",      TestFiles::GetOne ( "/automaton/NFSM1.xml$" ),   TestFiles::GetOne ( "/automaton/NFSM1.DET.xml$" ) ),
			std::make_tuple ( "automaton::determinize::Determinize - | automaton::simplify::Trim -",      TestFiles::GetOne ( "/automaton/NFSM2.xml$" ),   TestFiles::GetOne ( "/automaton/NFSM2.DET.xml$" ) ),
			std::make_tuple ( "automaton::determinize::Determinize - | automaton::simplify::Trim -",      TestFiles::GetOne ( "/automaton/NFSM3.xml$" ),   TestFiles::GetOne ( "/automaton/NFSM3.DET.xml$" ) ),
			std::make_tuple ( "automaton::determinize::Determinize - | automaton::simplify::Trim -",      TestFiles::GetOne ( "/automaton/NFSM4.xml$" ),   TestFiles::GetOne ( "/automaton/NFSM4.DET.xml$" ) ),
			std::make_tuple ( "automaton::determinize::Determinize - | automaton::simplify::Trim -",      TestFiles::GetOne ( "/automaton/NFSM5.xml$" ),   TestFiles::GetOne ( "/automaton/NFSM5.DET.xml$" ) ),
			std::make_tuple ( "automaton::simplify::EpsilonRemoverIncoming - | automaton::determinize::Determinize -", TestFiles::GetOne ( "/automaton/ENFSM2.xml$" ),  TestFiles::GetOne ( "/automaton/ENFSM2.DET.xml$" ) ),
			std::make_tuple ( "automaton::determinize::Determinize -                              ",      TestFiles::GetOne ( "/automaton/NFTA.xml$" ),    TestFiles::GetOne ( "/automaton/NFTA.DET.xml$" ) ),
			std::make_tuple ( "automaton::determinize::Determinize -                              ",      TestFiles::GetOne ( "/automaton/NIDPDA0.xml$" ), TestFiles::GetOne ( "/automaton/NIDPDA0.DET.xml$" ) ),
			std::make_tuple ( "automaton::determinize::Determinize -                              ",      TestFiles::GetOne ( "/automaton/NIDPDA1.xml$" ), TestFiles::GetOne ( "/automaton/NIDPDA1.DET.xml$" ) ),
			std::make_tuple ( "automaton::determinize::Determinize -                              ",      TestFiles::GetOne ( "/automaton/NIDPDA2.xml$" ), TestFiles::GetOne ( "/automaton/NIDPDA2.DET.xml$" ) ),
			std::make_tuple ( "automaton::determinize::Determinize -                              ",      TestFiles::GetOne ( "/automaton/NIDPDA3.xml$" ), TestFiles::GetOne ( "/automaton/NIDPDA3.DET.xml$" ) ),
			std::make_tuple ( "automaton::determinize::Determinize - | automaton::simplify::Normalize -", TestFiles::GetOne ( "/automaton/NPDA1.xml$" ),   TestFiles::GetOne ( "/automaton/NPDA1.DET.xml$" ) ) );

	ext::vector < std::string > qs = {
		"execute < " + std::get < 1 > ( definition ) + " > $input",
		"execute < " + std::get < 2 > ( definition ) + " > $expected",
		"quit compare::AutomatonCompare <( $input | " + std::get < 0 > ( definition ) + " ) $expected"
	};

	TimeoutAqlTest ( 2s, qs );
}
