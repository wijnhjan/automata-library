/*
 * Segfault.h
 *
 *  Created on: 18. 3. 2019
 *	  Author: Tomas Pecka
 */

#ifndef SEGFAULT_H_
#define SEGFAULT_H_

namespace debug {

class Segfault {
	static int * NULL_VALUE;

public:
	static int segfault ( );
};

} /* namespace debug */

#endif /* SEGFAULT_H_ */
