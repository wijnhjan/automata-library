/*
 * UndefinedBehaviour.h
 *
 *  Created on: 4. 4. 2020
 *	  Author: Tomas Pecka
 */

#include "UndefinedBehaviour.h"
#include <registration/AlgoRegistration.hpp>

namespace debug {

int UndefinedBehaviour::undefined_behaviour ( ) {
	int a = 5;
	int* b = ( int* ) ( ( ( size_t ) &a ) + 1 );

	return * b;
}

} /* namespace debug */

namespace {

auto UndefinedBehaviourInt = registration::AbstractRegister < debug::UndefinedBehaviour, int > ( debug::UndefinedBehaviour::undefined_behaviour );

} /* namespace */
