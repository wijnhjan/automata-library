#ifndef _TIMEOUT_AQL_TEST_HPP__
#define _TIMEOUT_AQL_TEST_HPP__

#include <catch2/catch.hpp>
#include <chrono>
#include <alib/vector>

using namespace std::chrono_literals;

void _TimeoutAqlTest ( const std::chrono::microseconds & timeout, const ext::vector < std::string > & queries );

template < class D >
void TimeoutAqlTest ( const D & timeout, const ext::vector < std::string > & queries ) {
	_TimeoutAqlTest ( std::chrono::duration_cast < std::chrono::microseconds > ( timeout ), queries );
}

#endif /* _TIMEOUT_AQL_TEST_HPP__ */
