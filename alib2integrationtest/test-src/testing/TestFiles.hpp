#ifndef _TEST_FILES_
#define _TEST_FILES_

#include <vector>
#include <string>
#include <regex>

class TestFiles {
	public:
		static std::vector < std::string > Get ( const std::regex& regex );
		static std::vector < std::string > Get ( const std::string& regex );
		static std::string GetOne ( const std::string& regex );
		static std::string GetOne ( const std::regex& regex );
};

#endif /* _TEST_FILES_ */
