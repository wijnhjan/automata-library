#include "TimeoutAqlTest.hpp"
#include <cstring>
#include <exception>
#include <parser/Parser.h>

#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>
#include <string.h>

#include <global/GlobalData.h>
#include <alib/exception>
#include <alib/random>

#include <readline/StringLineInterface.h>

#include <common/ResultInterpret.h>

#define PIPE_RD 0
#define PIPE_WR 1

#define FD_STDOUT 1
#define FD_STDERR 2

/* Communication between signal handler and the rest of the program */
int g_Wakeup [ 2 ];  // pipe for wakeup
int g_RecvSignal; // signalled flag

std::string formatQueries ( const ext::vector < std::string > & queries ) {
	std::string formated;
	for ( const std::string & query : queries )
		formated += "  " + query + "\n";
	return formated;
}

int waitSignalTimeout ( int timeout ) {
	struct timeval tv;
	fd_set rd;

	tv.tv_sec = 0;
	tv.tv_usec = timeout;
	FD_ZERO ( &rd );
	FD_SET ( g_Wakeup [ PIPE_RD ], &rd );

	select ( g_Wakeup [ PIPE_RD ] + 1, &rd, nullptr, nullptr, &tv );
	return g_RecvSignal;
}


std::string readFromFD ( int fd ) {
	static const size_t BUFSIZE = 64;

	std::string res;
	int rd;
	char buf [ BUFSIZE ];

	while ( ( rd = read ( fd, &buf, BUFSIZE - 1 ) ) > 0 ) {
		res.append ( buf, rd );
	}

	return res;
}

void writeToFD ( int fd, const std::string & message, const std::string & errorDesc ) {
	if ( write ( fd, message.c_str ( ), message.length ( ) ) != ( ssize_t ) message.length ( ) )
		throw std::runtime_error ( "TimeoutAqlTest: write() to pipe failed (" + errorDesc + ")" );
}


void newSigChild ( int ) {
	g_RecvSignal = 1;

	// write into the pipe so select can read something, this effectively means that SIGCHILD was raised
	writeToFD ( g_Wakeup [ PIPE_WR ], " ", "wakeup signalling" );
}

int aqlTest ( int fd_aql, const ext::vector < std::string > & queries, unsigned seed ) {
	try {
		cli::Environment environment;

		std::stringstream ss;
		common::Streams::out = ext::reference_wrapper < std::ostream > ( ss );
		common::Streams::err = ext::reference_wrapper < std::ostream > ( ss );

		// seed cli
		cli::CharSequence sequence { cli::StringLineInterface ( "set seed " + ext::to_string ( seed ) ) };
		cli::Parser ( cli::Lexer ( std::move ( sequence ) ) ).parse ( ) -> run ( environment );

		// run queries
		for ( const std::string & q : queries ) {
			cli::CharSequence querySequence { cli::StringLineInterface ( q ) };
			cli::Parser ( cli::Lexer ( std::move ( querySequence ) ) ).parse ( ) -> run ( environment );

			writeToFD ( fd_aql, ss.str ( ), "writing aql output" );
		}

		return cli::ResultInterpret::cli ( environment.getResult ( ) ); /* 0 = OK */
	} catch ( const std::exception & ) {
		std::ostringstream oss;
		alib::ExceptionHandler::handle ( oss );
		writeToFD ( fd_aql, oss.str ( ), "writing aql output" );
		return -1;
	}
}

void _TimeoutAqlTest ( const std::chrono::microseconds & timeout, const ext::vector < std::string > & queries ) {
	/* Register SIGCHLD handler */
	struct sigaction act;
	memset ( &act, 0, sizeof ( act ) );
	act . sa_handler = newSigChild;
	sigaction ( SIGCHLD, &act, nullptr );

	int pipeAqlOutput [ 2 ]; /* parent-child communication ( aql output ) */
	int pipeStdout [ 2 ];    /* parent-child communication ( child stdout ) */
	int pipeStderr [ 2 ];    /* parent-child communication ( child stderr ) */

	if ( pipe ( pipeAqlOutput ) != 0 )
		throw std::runtime_error ( "TimeoutAqlTest: Failed to initialize pipe (aql output)" );

	if ( pipe ( pipeStdout ) != 0 )
		throw std::runtime_error ( "TimeoutAqlTest: Failed to initialize pipe (child stdout)" );

	if ( pipe ( pipeStderr ) != 0 )
		throw std::runtime_error ( "TimeoutAqlTest: Failed to initialize pipe (child stderr)" );

	/* SIGCHLD was not yet raised, initialize communication pipe */
	g_RecvSignal = 0;
	if ( pipe ( g_Wakeup ) )
		throw std::runtime_error ( "TimeoutAqlTest: Failed to initialize pipe (wakeup signalling)" );

	/* random seed for aql */
	unsigned seed = ext::random_devices::random ( );

	/* do the forking */
	pid_t pid = fork ();
	REQUIRE ( pid >= 0 );

	if ( pid == 0 ) { /* child, run the test here */
		act . sa_handler = SIG_DFL;
		sigaction ( SIGCHLD, &act, nullptr );

		/* close unused ends of pipes in child */
		close ( g_Wakeup [ PIPE_RD ] );
		close ( g_Wakeup [ PIPE_WR ] );
		close ( pipeAqlOutput [ PIPE_RD ] );

		close ( pipeStdout [ PIPE_RD ] );
		close ( pipeStderr [ PIPE_RD ] );

		/* redirect stderr and stdout to pipe */
		dup2 ( pipeStdout [ PIPE_WR ], FD_STDOUT );
		dup2 ( pipeStderr [ PIPE_WR ], FD_STDERR );

		/* run test */
		exit ( aqlTest ( pipeAqlOutput [ PIPE_WR ], queries, seed ) );
	}

	/* close unused ends of pipes in parent */
	close ( pipeAqlOutput [ PIPE_WR ] );
	close ( pipeStdout [ PIPE_WR ] );
	close ( pipeStderr [ PIPE_WR ] );

	/* lets wait the specified time of microseconds, maybe the child will terminate on its own */
	if ( ! waitSignalTimeout ( timeout.count ( ) ) ) {
		/* ... and in case it did not ... */
		kill ( pid, SIGTERM );
		while ( ! waitSignalTimeout ( 250000 ) ) /* 1/4 second */
			kill ( pid, SIGKILL );
	}

	/* child termination confirmed */
	int status;
	waitpid ( pid, &status, 0 );
	close ( g_Wakeup [ PIPE_RD ] );
	close ( g_Wakeup [ PIPE_WR ] );

	/* read child outputs */
	std::string childOutput [ 3 ] = {
		readFromFD ( pipeAqlOutput [ PIPE_RD ] ),
		readFromFD ( pipeStdout [ PIPE_RD ] ),
		readFromFD ( pipeStderr [ PIPE_RD ] )
	};

	/* communication is done */
	close ( pipeAqlOutput [ PIPE_RD ] );
	close ( pipeStdout [ PIPE_RD ] );
	close ( pipeStderr [ PIPE_RD ] );

	/* determine test status */
	if ( WIFEXITED ( status ) ) {
		INFO ( "AqlTest failure. Trying to execute: " << "\n" << formatQueries ( queries ) );
		INFO ( "Seed was: " << seed );
		INFO ( "Child aqlout was: >" << childOutput [ 0 ] << "<" );
		INFO ( "Child stdout was: >" << childOutput [ 1 ] << "<" );
		INFO ( "Child stderr was: >" << childOutput [ 2 ] << "<" );
		REQUIRE ( WEXITSTATUS ( status ) == 0 );
	} else if ( WIFSIGNALED ( status ) ) {
		if ( WTERMSIG ( status ) == SIGTERM || WTERMSIG ( status ) == SIGKILL ) { /* killed by timeout control */
			WARN ( "AqlTest timeout (" << timeout.count ( ) << " us) reached. Trying to execute:\n" << formatQueries ( queries ) <<
			       "Seed was: " << seed << "\n" <<
			       "Child aqlout was: >" << childOutput [ 0 ] << "<\n" <<
			       "Child stdout was: >" << childOutput [ 1 ] << "<\n" <<
			       "Child stderr was: >" << childOutput [ 2 ] << "<\n" );
			CHECK_NOFAIL ( "timeout warning" == nullptr );
		} else {
			INFO ( "AqlTest failure. Trying to execute: " << formatQueries ( queries ) );
			INFO ( "Seed was: " << seed );
			INFO ( "Child aqlout was: >" << childOutput [ 0 ] << "<" );
			INFO ( "Child stdout was: >" << childOutput [ 1 ] << "<" );
			INFO ( "Child stderr was: >" << childOutput [ 2 ] << "<" );
			INFO ( "Child process signaled, signal " << WTERMSIG ( status ) << " (" << strsignal ( WTERMSIG ( status ) ) << ")" );
			FAIL ( );
		}
	}
}
