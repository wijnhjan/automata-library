#include "TestFiles.hpp"
#include "testfiles_basedir.hpp"

#include <exception/CommonException.h>
#if __has_include(<filesystem>)
  #include <filesystem>
#else
  #include <experimental/filesystem>
  #define filesystem experimental::filesystem
#endif

std::vector < std::string > TestFiles::Get ( const std::string& regex ) {
	return Get ( std::regex ( regex ) );
}

std::vector < std::string > TestFiles::Get ( const std::regex& regex ) {
	std::vector < std::string > res;

	try {
		for ( const std::filesystem::directory_entry & entry: std::filesystem::recursive_directory_iterator ( TEST_FILES_BASEDIR ) ) {
			std::smatch match;
			const std::string path = entry.path ( );

			if ( std::regex_search ( path, match, regex ) ) {
				res.push_back ( path );
			}
		}
	} catch ( const std::filesystem::filesystem_error & e ) {
		throw;
	}

	return res;
}

std::string TestFiles::GetOne ( const std::string& regex ) {
	return GetOne ( std::regex ( regex ) );
}

std::string TestFiles::GetOne ( const std::regex& regex ) {
	std::vector < std::string > res = Get ( regex );

	if ( res.size ( ) > 1 )
		throw std::invalid_argument ( std::string ( "Multiple files found for TestFiles::GetOne. TestFilesDir is " ) + TEST_FILES_BASEDIR );

	if ( res.size ( ) == 0 )
		throw std::invalid_argument ( std::string ( "No files found for TestFiles::GetOne. TestFilesDir is " ) + TEST_FILES_BASEDIR );

	return res.at ( 0 );
}
