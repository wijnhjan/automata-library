/*
 * Author: Radovan Cerveny
 */

#ifndef MEASUREMENT_ENGINE_HPP_
#define MEASUREMENT_ENGINE_HPP_

#include "MeasurementTypes.hpp"
#include "MeasurementFrame.hpp"
#include "MeasurementResults.hpp"

namespace measurements {

class MeasurementEngine {
	measurements::stealth_vector < unsigned > frameIdxStack;
	measurements::stealth_vector < MeasurementFrame > frames;

	MeasurementEngine ( );

public:
	void pushMeasurementFrame ( measurements::stealth_string, measurements::Type );
	void popMeasurementFrame ( );
	void resetMeasurements ( );
	MeasurementResults getResults ( ) const;

	template < typename Hint >
	void hint ( Hint );

	~MeasurementEngine ( );

	static MeasurementEngine INSTANCE;
	static bool OPERATIONAL;
};

}

#endif /* MEASUREMENT_ENGINE_HPP_ */
