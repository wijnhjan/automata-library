/*
 * Author: Radovan Cerveny
 */

#ifndef MEASUREMENT_NEW_HPP_
#define MEASUREMENT_NEW_HPP_

void * operator new( std::size_t n, bool measure );

void operator delete( void * ptr, bool measure ) noexcept;

#endif /* MEASUREMENT_NEW_HPP_ */
