/*
 * Author: Radovan Cerveny
 */

#ifndef MEASUREMENT_RESULTS_HPP_
#define MEASUREMENT_RESULTS_HPP_

#include <ostream>
#include "MeasurementTypes.hpp"
#include "MeasurementFrame.hpp"

namespace measurements {

enum class MeasurementFormat {
	LIST = 1, TREE = 2
};

struct MeasurementXalloc {
	static const int FORMAT;
};

struct MeasurementResults {
	measurements::stealth_vector < MeasurementFrame > frames;

	MeasurementResults ( );
	MeasurementResults ( const measurements::stealth_vector < MeasurementFrame > & );

	void printAsList ( std::ostream & ) const;
	void printAsTree ( std::ostream & ) const;

	static MeasurementResults aggregate ( const std::vector < MeasurementResults > & );

private:
	void printAsList ( std::ostream &, unsigned ) const;
	void printAsTree ( std::ostream &, unsigned, std::string &, bool ) const;
};

std::ostream & operator <<( std::ostream &, const MeasurementResults & );

template < typename _CharT, typename _Traits >
inline std::basic_ostream < _CharT, _Traits > & operator <<( std::basic_ostream < _CharT, _Traits > & x, MeasurementFormat f ) {
	x.iword ( MeasurementXalloc::FORMAT ) = static_cast < int > ( f );
	return x;
}

}

#endif /* MEASUREMENT_RESULTS_HPP_ */
