/*
 * Author: Radovan Cerveny
 */
#include "MeasurementTypes.hpp"
#include <stdexcept>

namespace measurements {

std::string to_string ( Type t ) {
	switch ( t ) {
	case Type::ROOT:
		return "ROOT";

	case Type::OVERALL:
		return "OVERALL";

	case Type::INIT:
		return "INIT";

	case Type::FINALIZE:
		return "FINALIZE";

	case Type::MAIN:
		return "MAIN";

	case Type::AUXILIARY:
		return "AUXILIARY";

	case Type::PREPROCESS:
		return "PREPROCESS";

	case Type::ALGORITHM:
		return "ALGORITHM";
	}

	throw std::invalid_argument ( "Missing case." );
}

Type measurementTypeFromString ( const std::string & ts ) {
	if ( ts == "ROOT" ) return Type::ROOT;

	if ( ts == "OVERALL" ) return Type::OVERALL;

	if ( ts == "INIT" ) return Type::INIT;

	if ( ts == "FINALIZE" ) return Type::FINALIZE;

	if ( ts == "MAIN" ) return Type::MAIN;

	if ( ts == "AUXILIARY" ) return Type::AUXILIARY;

	if ( ts == "PREPROCESS" ) return Type::PREPROCESS;

	if ( ts == "ALGORITHM" ) return Type::ALGORITHM;

	throw  std::invalid_argument ( "measurementTypeFromString failed, unknown measurement type: " + ts );
}

std::ostream & operator <<( std::ostream & os, Type t ) {
	os << to_string ( t );
	return os;
}

}
