/*
 * Author: Radovan Cerveny
 */

#ifndef MEASUREMENT_TYPES_HPP_
#define MEASUREMENT_TYPES_HPP_

#include <memory>
#include <vector>
#include <map>
#include "MeasurementNew.hpp"
#include <allocator/StealthTypes.hpp>

namespace measurements {

enum class Type : unsigned {
	ROOT = 1, OVERALL = 2, INIT = 4, FINALIZE = 8, MAIN = 16, AUXILIARY = 32, PREPROCESS = 64, ALGORITHM = 128
};

std::string to_string ( Type );
Type measurementTypeFromString ( const std::string & );
std::ostream & operator <<( std::ostream &, Type );
}

#endif /* MEASUREMENT_TYPES_HPP_ */
