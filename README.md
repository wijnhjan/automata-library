Algorithms Library Toolkit
=====================================

![automaton][logo]

For documentation and quickstart see our [website][7].

What is it?
-----------
The Algorithms library toolkit (ALT) is a collection of implementations
of various types of automata, grammars, and other common structires along
with mathematical implementation of algorithms over these structures.
Originaly developed at Faculty of Information Technology of Czech Technical
University in Prague. It supposed to be a reference implementation of formal
algorithms as-are in papers. It's interface allows practical reuse of existing
algorithms in form of by pipe comunicating, partially using a query language in command
line, and partially using a GUI interface.


The Latest Version
------------------
Packages and docker images with latest stable and nightly versions can be found visiting at [website][7].


Requirements
------------
Project is pure C++ builded by `cmake`. Some tests are handled by
shell scripts.

Build tools:
 * `cmake >= 3.7`
 * `make >= 3.9`
 * `g++ >= 8.3` or `clang++ >= 8.0` (compilation in C++17)

Libraries:
 * `tclap`
 * `libxml2`
 * `readline`

For GUI, you will need also the following libraries:
 * `Qt5-qtbase >= 5.7`
 * `jsoncpp`
 * `graphviz`

Optional dependencies:
 * `doxygen` (for documentation generation)
 * `graphviz` (for documentation generation and automata visualizations)



Installation
------------
 1. Clone the repo: `git clone` or download sources of [stable release][3]
and extract it.
 2. Change directory and either just run `build.sh` script or create a build folder: `cd algorithms-library && mkdir build`.
 3. Generate `CMakeLists`:
   - `../CMake/generate.py -wm` if you want to build whole project
   - or `../CMake/generate.py -wm -g cli` if you want to build cli only
   - or `../CMake/generate.py -wm -p alib2str` if you want to build only `alib2str` and its dependencies
 4. Run `cmake` and build: `cmake .. && make`
 5. Install: `make install` or `make DESTDIR=./install install` for local install.
 6. Run :)

* *Optional step:* Generate documentation by executing `cd doc/userGuide; make`.
* *Optional step:* Run tests with `make test` inside the build folder.


Documentation
-------------
Developer documentation can be generated using `make doc` command (requires
`doxygen`) and will be attached in release packages.


Contributing
------------
Feel free to [fork][4] this project and send your merge requests there.


Developers
----------
The formatting of the code in ALT is defined by *uncrustify* configuration file
and *clang-format* configuration file in cooperation (both had some
side-effects). Every developer contributing to the project shall format the
code before making the commit. Pre-commit hook and both configuration
files are present in the repository.

Please install `uncrustify` and `clang-format`. Then move/copy `.pre-commit`
file to `.git/hooks/pre-commit`.

Editor configuration for this project is provided by [editorconfig][6].


Licensing
---------
Please see LICENSE file.


Contacts
--------
 * If you want to be informed about new code releases, bug fixes,
security fixes, general news and information about the ALT
subscribe to the ?? mailing list as described under ??
 * If you want freely available support for running ALT please see the
resources at ??
 * If you have a concrete bug report for ALT please see the instructions
for bug reporting at ??
 * If you want to participate in actively developing ALT please
subscribe to the ?? mailing list as described at ??

Authors
-------
 * Ing. Jan Trávníček,
 * Ing. Tomáš Pecka,
 * Ing. Štěpán Plachý,
 * et. al.


[1]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library
[3]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library/tags
[4]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library/forks/new
[5]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library/container_registry
[6]: https://editorconfig.org
[7]: https://alt.fit.cvut.cz
[logo]: docs/logo.png
