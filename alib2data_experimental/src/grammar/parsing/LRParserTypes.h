/*
 * LRParserTypes.h
 *
 *  Created on: 10. 5. 2016
 *	  Author: Martin Kocicka
 */

#ifndef LR_PARSER_TYPES_H_
#define LR_PARSER_TYPES_H_

#include <common/DefaultSymbolType.h>
#include <common/DefaultStateType.h>

#include <alib/map>
#include <alib/set>
#include <alib/variant>
#include <alib/vector>

namespace grammar {

namespace parsing {

enum class LRAction {
	Shift,
	Reduce,
	Accept
};

typedef ext::map < DefaultSymbolType, ext::set < ext::pair < unsigned, ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > > > LR0Items;
typedef ext::map < ext::pair < LR0Items, DefaultSymbolType >, ext::pair < LRAction, ext::variant < LR0Items, ext::pair < DefaultSymbolType, ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > > > > > LRActionTable;
typedef ext::map < ext::pair < LR0Items, DefaultSymbolType >, LR0Items > LRGotoTable;

} /* namespace parsing */

} /* namespace grammar */

#endif /* LR_PARSER_TYPES_H_ */
