#include <alib/typeinfo>

#include <registration/InputFileTypeRegistration.hpp>

#include <registry/RawReaderRegistry.hpp>

#include <abstraction/WrapperAbstraction.hpp>
#include <abstraction/PackingAbstraction.hpp>

#include <registry/Registry.h>
#include <common/AlgorithmCategories.hpp>

namespace {

	std::shared_ptr < abstraction::OperationAbstraction > dummy4 ( const std::string & typehint, const ext::vector < std::string > & ) {
		ext::vector < std::shared_ptr < abstraction::OperationAbstraction > > abstractions;

		ext::vector < std::string > templateParams;
		ext::vector < std::string > paramTypes { ext::to_string < std::string > ( ) };
		abstraction::AlgorithmCategories::AlgorithmCategory category = abstraction::AlgorithmCategories::AlgorithmCategory::NONE;
		ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > paramTypeQualifiers { abstraction::TypeQualifiers::typeQualifiers < const std::string & > ( ) };

		abstractions.push_back ( abstraction::Registry::getAlgorithmAbstraction ( "cli::builtin::ReadFile", templateParams, paramTypes, paramTypeQualifiers, category ) );

		auto rawParserAbstractionFinder = [ = ] ( const std::string & ) {
			return abstraction::RawReaderRegistry::getAbstraction ( typehint );
		};

		abstractions.push_back ( std::make_shared < abstraction::WrapperAbstraction < std::string && > > ( rawParserAbstractionFinder ) );

		std::shared_ptr < abstraction::PackingAbstraction < 1 > > res = std::make_shared < abstraction::PackingAbstraction < 1 > > ( std::move ( abstractions ), 1 );
		res->setInnerConnection ( 0, 1, 0 );
		res->setOuterConnection ( 0, 0, 0 );

		return res;
	}

auto rawInputFileHandler = registration::InputFileRegister ( "raw", dummy4 );

}
