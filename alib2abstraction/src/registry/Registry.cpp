/*
 * Registry.cpp
 *
 *  Created on: 10. 7. 2017
 *	  Author: Jan Travnicek
 */

#include <registry/Registry.h>

#include <registry/AlgorithmRegistry.hpp>
#include <registry/ValuePrinterRegistry.hpp>
#include <registry/CastRegistry.hpp>
#include <registry/NormalizeRegistry.hpp>
#include <registry/ContainerRegistry.hpp>
#include <registry/OperatorRegistry.hpp>

namespace abstraction {

ext::set < ext::pair < std::string, ext::vector < std::string > > > Registry::listAlgorithmGroup ( const std::string & group ) {
	return AlgorithmRegistry::listGroup ( group );
}

ext::set < ext::pair < std::string, ext::vector < std::string > > > Registry::listAlgorithms ( ) {
	return AlgorithmRegistry::list ( );
}

ext::list < ext::pair < std::string, bool > > Registry::listCastsFrom ( const std::string & type ) {
	return CastRegistry::listFrom ( type );
}

ext::list < ext::pair < std::string, bool > > Registry::listCastsTo ( const std::string & type ) {
	return CastRegistry::listTo ( type );
}

ext::list < ext::tuple < std::string, std::string, bool > > Registry::listCasts ( ) {
	return CastRegistry::list ( );
}

ext::list < ext::tuple < AlgorithmFullInfo, std::string > > Registry::listOverloads ( const std::string & algorithm, const ext::vector < std::string > & templateParams ) {
	return AlgorithmRegistry::listOverloads ( algorithm, templateParams );
}

ext::list < ext::pair < Operators::BinaryOperators, AlgorithmFullInfo > > Registry::listBinaryOperators ( ) {
	return OperatorRegistry::listBinaryOverloads ( );
}

ext::list < ext::pair < Operators::PrefixOperators, AlgorithmFullInfo > > Registry::listPrefixOperators ( ) {
	return OperatorRegistry::listPrefixOverloads ( );
}

ext::list < ext::pair < Operators::PostfixOperators, AlgorithmFullInfo > > Registry::listPostfixOperators ( ) {
	return OperatorRegistry::listPostfixOverloads ( );
}

std::shared_ptr < abstraction::OperationAbstraction > Registry::getContainerAbstraction ( const std::string & container, const std::string & type ) {
	return ContainerRegistry::getAbstraction ( container, type );
}

std::shared_ptr < abstraction::OperationAbstraction > Registry::getAlgorithmAbstraction ( const std::string & name, const ext::vector < std::string > & templateParams, const ext::vector < std::string > & paramTypes, const ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > & typeQualifiers, AlgorithmCategories::AlgorithmCategory category ) {
	return AlgorithmRegistry::getAbstraction ( name, templateParams, paramTypes, typeQualifiers, category );
}

std::shared_ptr < abstraction::OperationAbstraction > Registry::getBinaryOperatorAbstraction ( Operators::BinaryOperators type, const ext::vector < std::string > & paramTypes, const ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > & typeQualifiers, AlgorithmCategories::AlgorithmCategory category ) {
	return OperatorRegistry::getBinaryAbstraction ( type, paramTypes, typeQualifiers, category );
}

std::shared_ptr < abstraction::OperationAbstraction > Registry::getPrefixOperatorAbstraction ( Operators::PrefixOperators type, const ext::vector < std::string > & paramTypes, const ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > & typeQualifiers, AlgorithmCategories::AlgorithmCategory category ) {
	return OperatorRegistry::getPrefixAbstraction ( type, paramTypes, typeQualifiers, category );
}

std::shared_ptr < abstraction::OperationAbstraction > Registry::getPostfixOperatorAbstraction ( Operators::PostfixOperators type, const ext::vector < std::string > & paramTypes, const ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > & typeQualifiers, AlgorithmCategories::AlgorithmCategory category ) {
	return OperatorRegistry::getPostfixAbstraction ( type, paramTypes, typeQualifiers, category );
}

std::shared_ptr < abstraction::OperationAbstraction > Registry::getCastAbstraction ( const std::string & target, const std::string & param ) {
	return CastRegistry::getAbstraction ( target, param );
}

bool Registry::isCastNoOp ( const std::string & target, const std::string & param ) {
	return CastRegistry::isNoOp ( target, param );
}

std::shared_ptr < abstraction::OperationAbstraction > Registry::getNormalizeAbstraction ( const std::string & param ) {
	return NormalizeRegistry::getAbstraction ( param );
}

bool Registry::hasNormalize ( const std::string & param ) {
	return NormalizeRegistry::hasNormalize ( param );
}

std::shared_ptr < abstraction::OperationAbstraction > Registry::getValuePrinterAbstraction ( const std::string & param ) {
	return ValuePrinterRegistry::getAbstraction ( param );
}

} /* namespace abstraction */
