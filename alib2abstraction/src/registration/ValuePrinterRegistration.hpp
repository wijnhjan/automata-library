#ifndef _VALUE_PRINTER_REGISTRATION_HPP_
#define _VALUE_PRINTER_REGISTRATION_HPP_

#include <registry/ValuePrinterRegistry.hpp>

namespace registration {

template < class Type >
class ValuePrinterRegister {
public:
	ValuePrinterRegister ( ) {
		abstraction::ValuePrinterRegistry::registerValuePrinter < Type > ( );
	}

	~ValuePrinterRegister ( ) {
		abstraction::ValuePrinterRegistry::unregisterValuePrinter < Type > ( );
	}
};

} /* namespace registration */

#endif // _VALUE_PRINTER_REGISTRATION_HPP_
