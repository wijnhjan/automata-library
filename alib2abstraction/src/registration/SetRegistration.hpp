#ifndef _SET_REGISTRATION_HPP_
#define _SET_REGISTRATION_HPP_

#include <registry/ContainerRegistry.hpp>

#include <registration/NormalizationRegistration.hpp>

namespace registration {

template < class Param >
class SetRegister {
public:
	SetRegister ( ) {
		abstraction::ContainerRegistry::registerSet < Param > ( );
	}

	~SetRegister ( ) {
		abstraction::ContainerRegistry::unregisterSet < Param > ( );
	}
};

} /* namespace registration */

#endif // _SET_REGISTRATION_HPP_
