#ifndef _CAST_REGISTRATION_HPP_
#define _CAST_REGISTRATION_HPP_

#include <registry/CastRegistry.hpp>

#include <registration/NormalizationRegistration.hpp>

namespace registration {

template < class To, class From >
class CastRegister {
	registration::NormalizationRegister < To > normalize;

public:
	CastRegister ( ) {
		abstraction::CastRegistry::registerCast < To, From > ( );
	}

	CastRegister ( To ( * castFunction ) ( const From & ) ) {
		abstraction::CastRegistry::registerCastAlgorithm < To, From > ( castFunction );
	}

	~CastRegister ( ) {
		abstraction::CastRegistry::unregisterCast < To, From > ( );
	}
};

} /* namespace registration */

#endif // _CAST_REGISTRATION_HPP_
