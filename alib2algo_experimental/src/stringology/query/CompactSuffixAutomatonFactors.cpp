#include "CompactSuffixAutomatonFactors.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto CompactSuffixAutomatonQueryLinearString = registration::AbstractRegister < stringology::query::CompactSuffixAutomatonFactors, ext::set < unsigned >, const indexes::stringology::CompactSuffixAutomatonTerminatingSymbol < > &, const string::LinearString < > & > ( stringology::query::CompactSuffixAutomatonFactors::query );

} /* namespace */
