/*
 * First.cpp
 *
 *  Created on: 9. 6. 2015
 *	  Author: Tomas Pecka
 */

#include "First.h"

#include <grammar/ContextFree/CFG.h>
#include <grammar/ContextFree/EpsilonFreeCFG.h>
#include <grammar/ContextFree/CNF.h>
#include <grammar/ContextFree/GNF.h>
#include <grammar/ContextFree/LG.h>
#include <grammar/Regular/LeftLG.h>
#include <grammar/Regular/LeftRG.h>
#include <grammar/Regular/RightLG.h>
#include <grammar/Regular/RightRG.h>
#include <exception/CommonException.h>
#include <registration/AlgoRegistration.hpp>

namespace {

auto FirstCFG = registration::AbstractRegister < grammar::parsing::First, ext::map < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > >, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::CFG < > & > ( grammar::parsing::First::first );
auto FirstEpsilonFreeCFG = registration::AbstractRegister < grammar::parsing::First, ext::map < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > >, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::EpsilonFreeCFG < > & > ( grammar::parsing::First::first );
auto FirstGNF = registration::AbstractRegister < grammar::parsing::First, ext::map < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > >, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::GNF < > & > ( grammar::parsing::First::first );
auto FirstCNF = registration::AbstractRegister < grammar::parsing::First, ext::map < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > >, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::CNF < > & > ( grammar::parsing::First::first );
auto FirstLG = registration::AbstractRegister < grammar::parsing::First, ext::map < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > >, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::LG < > & > ( grammar::parsing::First::first );
auto FirstLeftLG = registration::AbstractRegister < grammar::parsing::First, ext::map < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > >, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::LeftLG < > & > ( grammar::parsing::First::first );
auto FirstLeftRG = registration::AbstractRegister < grammar::parsing::First, ext::map < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > >, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::LeftRG < > & > ( grammar::parsing::First::first );
auto FirstRightLG = registration::AbstractRegister < grammar::parsing::First, ext::map < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > >, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::RightLG < > & > ( grammar::parsing::First::first );
auto FirstRightRG = registration::AbstractRegister < grammar::parsing::First, ext::map < ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > >, ext::set < ext::vector < DefaultSymbolType > > >, const grammar::RightRG < > & > ( grammar::parsing::First::first );

auto FirstCFG2 = registration::AbstractRegister < grammar::parsing::First, ext::set < ext::vector < DefaultSymbolType > >, const grammar::CFG < > &, const ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > & > ( grammar::parsing::First::first );
auto FirstEpsilonFreeCFG2 = registration::AbstractRegister < grammar::parsing::First, ext::set < ext::vector < DefaultSymbolType > >, const grammar::EpsilonFreeCFG < > &, const ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > & > ( grammar::parsing::First::first );
auto FirstGNF2 = registration::AbstractRegister < grammar::parsing::First, ext::set < ext::vector < DefaultSymbolType > >, const grammar::GNF < > &, const ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > & > ( grammar::parsing::First::first );
auto FirstCNF2 = registration::AbstractRegister < grammar::parsing::First, ext::set < ext::vector < DefaultSymbolType > >, const grammar::CNF < > &, const ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > & > ( grammar::parsing::First::first );
auto FirstLG2 = registration::AbstractRegister < grammar::parsing::First, ext::set < ext::vector < DefaultSymbolType > >, const grammar::LG < > &, const ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > & > ( grammar::parsing::First::first );
auto FirstLeftLG2 = registration::AbstractRegister < grammar::parsing::First, ext::set < ext::vector < DefaultSymbolType > >, const grammar::LeftLG < > &, const ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > & > ( grammar::parsing::First::first );
auto FirstLeftRG2 = registration::AbstractRegister < grammar::parsing::First, ext::set < ext::vector < DefaultSymbolType > >, const grammar::LeftRG < > &, const ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > & > ( grammar::parsing::First::first );
auto FirstRightLG2 = registration::AbstractRegister < grammar::parsing::First, ext::set < ext::vector < DefaultSymbolType > >, const grammar::RightLG < > &, const ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > & > ( grammar::parsing::First::first );
auto FirstRightRG2 = registration::AbstractRegister < grammar::parsing::First, ext::set < ext::vector < DefaultSymbolType > >, const grammar::RightRG < > &, const ext::vector < ext::variant < DefaultSymbolType, DefaultSymbolType > > & > ( grammar::parsing::First::first );

} /* namespace */
