/*
 * SLR1ParseTable.h
 *
 *  Created on: 4. 5. 2016
 *	  Author: Martin Kocicka
 */

#ifndef SLR_1_PARSE_TABLE_H_
#define SLR_1_PARSE_TABLE_H_

#include "LRParser.h"

#include <grammar/ContextFree/CFG.h>
#include <grammar/parsing/LRParserTypes.h>

namespace grammar {

namespace parsing {

class SLR1ParseTable {
	static void insertToActionTable ( LRActionTable & actionTable, LRActionTable::key_type key, LRActionTable::mapped_type value );

public:
	static LRActionTable getActionTable ( const grammar::CFG < > & originalGrammar );

	static LRGotoTable getGotoTable ( const grammar::CFG < > & originalGrammar );
};

} /* namespace parsing */

} /* namespace grammar */

#endif /* SLR_1_PARSE_TABLE_H_ */
