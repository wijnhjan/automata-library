/*
 * AbsorbTerminalSymbol.h
 *
 *  Created on: 9. 6. 2015
 *	  Author: Jan Travnicek
 */

#ifndef ABSORB_TERMINAL_SYMBOL_H_
#define ABSORB_TERMINAL_SYMBOL_H_

#include <grammar/ContextFree/CFG.h>
#include <alib/set>
#include <alib/map>

namespace grammar {

namespace parsing {

class AbsorbTerminalSymbol {
public:
	static void handleAbsobtion ( const grammar::CFG < > & orig, grammar::CFG < > & res, const DefaultSymbolType & terminal, const ext::set < DefaultSymbolType > & nonterminals, const ext::map < DefaultSymbolType, DefaultSymbolType > & nonterminalsPrimed );

	static void absorbTerminalSymbol ( grammar::CFG < > & grammar, const DefaultSymbolType & terminal, const ext::set < DefaultSymbolType > & nonterminals );
};

} /* namespace parsing */

} /* namespace grammar */

#endif /* ABSORB_TERMINAL_SYMBOL_H_ */
