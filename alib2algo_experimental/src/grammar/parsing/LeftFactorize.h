/*
 * LeftFactorize.h
 *
 *  Created on: 9. 6. 2015
 *	  Author: Jan Travnicek
 */

#ifndef LEFT_FACTORIZE_H_
#define LEFT_FACTORIZE_H_

#include <grammar/ContextFree/CFG.h>

namespace grammar {

namespace parsing {

class LeftFactorize {
public:
	static void leftFactorize ( grammar::CFG < > & grammar, const DefaultSymbolType & terminal, const DefaultSymbolType & nonterminal );

};

} /* namespace parsing */

} /* namespace grammar */

#endif /* LEFT_FACTORIZE_H_ */
