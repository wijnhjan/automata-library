declare -A arr

# arr["arbology,tree"]="Arbology and tree algorithms"
arr["automaton"]="Automata algorithms"
arr["cli"]="Builtin algorithms"
# arr["compare"]="Compare algorithms"
arr["convert"]="Convert algorithms"
# arr["graph"]="Graph algorithms"
arr["grammar"]="Grammar algorithms"
arr["regexp"]="Regular expression algorithms"
arr["rte"]="Regular tree expression algorithms"
# arr["dataAccess,raw,sax,stats,xml"]="Datastructure parsing and auxiliary algorithms"
arr["string"]="String and stringology algorithms"

echo '\lstset{'
echo '	numbers=none'
echo '}'
echo
echo '\chapter{Available algorithms}'

for key in ${!arr[@]}; do
	echo '\section{'${arr[${key}]}'}'
	echo "$key" | tr ',' '\n' | while read group
	do
		../../debug/aql2/aql2 -c 'introspect algorithms '"$group"'::' | sed 's/<object::Object>//g' | while read algorithm
		do
			echo "$algorithm" | sed 's/_/\\_/g'
			echo
			echo '\begin{lstlisting}'
			../../debug/aql2/aql2 -c 'introspect overloads '"$algorithm" | sed 's/ >/>/g' | sed 's/std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char>>/std::string/g' | sed 's/, ext::less<void>//g' | sed 's/, std::allocator<object::Object>//g' | sed 's/, std::allocator<ext::pair<object::Object, object::Object>>//g' | sed 's/, std::allocator<unsigned int>//g' | sed 's/, std::allocator<unsigned long>//g' | sed 's/, std::allocator<int>//g' | sed 's/, std::allocator<std::pair<object::Object const, unsigned long>>//g' | sed 's/, std::allocator<common::ranked_symbol<object::Object, unsigned int>>//g' | sed 's/, std::allocator<std::pair<object::Object const, object::Object>>//g' | sed 's/, std::allocator<std::pair<common::ranked_symbol<object::Object, unsigned int> const, unsigned long>>//g' | sed 's/, std::allocator<std::pair<node::Node const, int>>//g' | sed 's/, std::allocator<ext::pair<long, long>>//g' | sed 's/, std::allocator<std::pair<object::Object const, double>>//g' | sed 's/, std::allocator<std::pair<ext::pair<long, long> const, double>>//g' | sed 's/, std::allocator<std::pair<object::Object const, ext::map<object::Object, double>>>//g' | sed 's/, std::allocator<std::pair<node::Node const, ext::unordered_map<node::Node, int, std::hash<node::Node>, std::equal_to<node::Node>>>>//g' | sed 's/, std::allocator<std::pair<node::Node, node::Node>>//g' | sed 's/, std::allocator<std::pair<ext::pair<long, long> const, ext::map<ext::pair<long, long>, double>>>//g' | sed 's/, std::allocator<ext::variant<object::Object, object::Object>>//g' | sed 's/, std::allocator<ext::variant<object::Object, object::Object>>//g' | sed 's/, std::allocator<ext::vector<ext::variant<object::Object, object::Object>>>//g' | sed 's/, std::allocator<sax::Token>//g' | sed 's/, std::allocator<std::pair<std::pair<object::Object, object::Object> const, ext::map<object::Object, object::Object>>>//g' | sed 's/, std::allocator<std::pair<object::Object const, ext::set<ext::vector<ext::variant<object::Object, object::Object>>>>>//g' | sed 's/, std::allocator<ext::set<object::Object>>//g' | sed 's/, std::allocator<ext::map<std::pair<object::Object, object::Object>, ext::map<object::Object, object::Object>>>//g' | sed 's/, std::allocator<ext::vector<ext::set<object::Object>>>//g' | sed 's/, std::hash<node::Node>, std::equal_to<node::Node>//g' | sed 's/, std::allocator<string::LinearString<object::Object>>//g' | sed 's/, std::hash<std::pair<node::Node, node::Node>>, std::equal_to<std::pair<node::Node, node::Node>>//g'
			echo '\end{lstlisting}'
		done
	done
done
