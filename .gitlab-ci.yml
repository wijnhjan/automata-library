image: alpine:3.11

stages:
 - build
 - test
 - notify

.template:only:merge: &only-merge
  only:
    - merge_requests

.template:only:master: &only-master
  only:
    refs:
      - master

.template:only:tag: &only-tag
  only:
    - /^v.*$/
  except:
    - branches

#######################################################################################################################
# build + test

.template:build:
  dependencies: []
  stage: build
  tags:
    - altbuilder
  script:
    - extra/scripts/build.sh -d build -m Release -n -j8
  artifacts:
    paths:
      - build/
    expire_in: 1 day

.template:test:
  dependencies: []
  stage: test
  tags:
    - altbuilder
  script:
    - cd build
    - ctest . --output-on-failure -j $(grep -c processor /proc/cpuinfo)

# -----------------------------------------------------------------------------
# .config:alpine: &distro_alpine
#   <<: *only-merge
#   image: alpine:3.11
#
# build:alpine:
#   <<: *distro_alpine
#   extends: .template:build
#   before_script:
#     - apk add --no-cache bash build-base cmake ninja python3 libexecinfo-dev libxml2-dev tclap-dev readline-dev qt5-qtbase-dev graphviz-dev jsoncpp-dev
#
# test:alpine:
#   <<: *distro_alpine
#   extends: .template:test
#   dependencies:
#     - build:alpine
#   before_script:
#     - apk add --no-cache bash bc coreutils python3 cmake libexecinfo libxml2 tclap readline qt5-qtbase qt5-qtbase-x11 graphviz jsoncpp
# -----------------------------------------------------------------------------

.config:builder: &distro_builder
  image: ${CI_REGISTRY}/algorithms-library-toolkit/ci-docker-images/alt-builder:latest

.build:builder:
  <<: *distro_builder
  extends: .template:build

.test:builder:
  <<: *distro_builder
  extends: .template:test

build:builder:gcc:
  extends: .build:builder

build:builder:clang-sanitizers:
  extends: .build:builder
  variables:
    CXX: clang++
    CXXFLAGS: "-fsanitize=address -fsanitize=undefined -fno-sanitize-recover=all" # -stdlib=libc++

test:builder:gcc:
  extends: .test:builder
  dependencies:
    - build:builder:gcc

test:builder:clang-sanitizers:
  extends: .test:builder
  dependencies:
    - build:builder:clang-sanitizers

#######################################################################################################################
# doc

build:doc:
  stage: build
  before_script:
    - apk add --no-cache doxygen graphviz cmake ninja python3
  script:
    - mkdir -p build && cd build
    - ../CMake/generate.py -wm
    - cmake -DBUILD_TYPE=Snapshot -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DDOXYGEN_ONLY=1 -GNinja ..
    - ninja doxygen
  artifacts:
    paths:
      - apidoc/
    expire_in: 1 day

#######################################################################################################################
# static analysis

.template:static-analysis:
  stage: build
  image: ${CI_REGISTRY}/algorithms-library-toolkit/ci-docker-images/alt-builder:latest
  before_script:
    - mkdir -p build && pushd build
    - ../CMake/generate.py -wm
    - cmake -DBUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .. -GNinja
    - popd
  allow_failure: true # TODO: Remove
  dependencies: []

# -----------------------------------------------------------------------------

static-analysis:cppcheck:
  extends: .template:static-analysis
  script:
    - cppcheck -q --enable=all --project=build/compile_commands.json --suppress="*:*/contrib/*" --error-exitcode=1


static-analysis:clang-tidy:
  extends: .template:static-analysis
  script:
    - jq ".[].file" build/compile_commands.json | tr -d "\"" | grep -v "test-src" | xargs -n1 -P$(grep -c processor /proc/cpuinfo) clang-tidy -p build/

#######################################################################################################################
# notify

.template:notify:
  stage: notify
  before_script:
    - apk add --no-cache curl
  dependencies: []
  script:
    - curl -X POST -F token="$TOKEN" -F ref=master $PARAMS https://gitlab.fit.cvut.cz/api/v4/projects/$PROJECT_ID/trigger/pipeline

# -----------------------------------------------------------------------------

notify:pyalib:
  <<: *only-master
  extends: .template:notify
  variables:
    TOKEN: "$TOKEN_TRIGGER_PYALIB"
    PROJECT_ID: "11497"

notify:release:stable:
  <<: *only-tag
  extends: .template:notify
  variables:
    TOKEN: "$TOKEN_TRIGGER_RELEASE"
    PROJECT_ID: "17683"
    PARAMS: "-F variables[RELEASE_REF]=$CI_COMMIT_TAG -F variables[RELEASE_MODE]=stable"

notify:release:snapshot:
  <<: *only-master
  extends: .template:notify
  variables:
    TOKEN: "$TOKEN_TRIGGER_RELEASE"
    PROJECT_ID: "17683"
    PARAMS: "-F variables[RELEASE_REF]=$CI_COMMIT_SHA -F variables[RELEASE_MODE]=snapshot"
