#!/usr/bin/env python3

import os
import sys
import configparser
import argparse
import itertools


# ----------------------------------------------------------------------------------------------------------------------

class SmartOpen:
    SEPARATOR_SIZE = 20

    def __init__(self, filename, mode='r', *, directory='.', encoding=None, dry_run=False):
        self._dry_run = dry_run
        self._filename = filename
        self._mode = mode
        self._fd = sys.stdout if self._dry_run else open(os.path.join(directory, filename), mode=mode, encoding=encoding)

    def read(self):
        return self._fd.read()

    def __enter__(self):
        if self._dry_run and 'w' in self._mode:
            self._fd.write('{}\n'.format(self._filename) + '-' * self.SEPARATOR_SIZE + '\n')

        return self._fd

    def __exit__(self, d_type, value, traceback):
        if self._dry_run and 'w' in self._mode:
            self._fd.write('-' * self.SEPARATOR_SIZE + '\n')

        if self._fd != sys.stdout:
            self._fd.close()


# ----------------------------------------------------------------------------------------------------------------------

class Helpers:
    @staticmethod
    def join(lst, sep='\n        '):
        return sep.join(lst)

    @staticmethod
    def path(*args):
        return os.path.realpath(os.path.join(*args))

    @staticmethod
    def find_dirs_with_file(directory, filename):
        return [e.name for e in os.scandir(directory) if e.is_dir() and os.path.isfile(Helpers.path(e.path, filename))]


# ----------------------------------------------------------------------------------------------------------------------

class ConfigException(Exception):
    pass


class Config:
    @staticmethod
    def get(c, section, key, fallback=None):
        try:
            return c.get(section, key)
        except (configparser.NoOptionError, configparser.NoSectionError):
            if fallback is not None:
                return fallback
            raise ConfigException("[{}][{}] not defined".format(section, key))

    @staticmethod
    def get_array(c, section, key, fallback=None):
        try:
            return c.get(section, key).strip().split()
        except (configparser.NoOptionError, configparser.NoSectionError):
            if fallback is not None:
                return fallback
            raise ConfigException("[{}][{}] not defined".format(section, key))

    @staticmethod
    def get_section(c, section, fallback=None):
        try:
            return {k: v for k, v in c[section].items()}
        except (configparser.NoSectionError, KeyError):
            if fallback is not None:
                return fallback
            raise ConfigException("[{}] not defined".format(section))


# ----------------------------------------------------------------------------------------------------------------------

class ProjectException(Exception):
    pass


class Project:
    def __init__(self, name, conf):
        self.name = name
        self.path = Helpers.path(ALIB_DIRECTORY, self.name)
        self._config = configparser.ConfigParser(allow_no_value=True)
        self._config.read(Helpers.path(self.path, conf))

    @property
    def groups(self):
        return Config.get_array(self._config, 'General', 'groups', fallback=[])

    @property
    def category(self):
        ctg = Config.get(self._config, 'General', 'category')
        if ctg not in CATEGORIES.keys():
            raise ProjectException("Invalid category ({})".format(ctg))
        return ctg

    @property
    def cmake_additional_set(self):
        return Config.get_section(self._config, 'CMake', fallback={}).items()

    @property
    def dependencies(self):
        return Config.get_array(self._config, 'Dependencies', 'project', fallback=[])

    @property
    def dependencies_test(self):
        return Config.get_array(self._config, 'TestDependencies', 'project', fallback=[])

    @property
    def system_deps(self):
        return Config.get_array(self._config, 'Dependencies', 'system', fallback=[])

    @property
    def system_deps_test(self):
        return Config.get_array(self._config, 'TestDependencies', 'system', fallback=[])

    def find_sources(self):
        return self._find_sources(
            Config.get(CONFIG, 'CMake:Sources', 'SourcesDir'),
            Config.get_array(CONFIG, 'CMake:Sources', 'ExcludeSources'))

    def find_sources_test(self):
        return self._find_sources(
            Config.get(CONFIG, 'CMake:Sources', 'TestSourcesDir'),
            Config.get_array(CONFIG, 'CMake:Sources', 'ExcludeSources'))

    def find_install_sources(self):
        return self._find_sources(
            Config.get(CONFIG, 'CMake:Sources', 'SourcesDir'),
            [],
            Config.get_array(CONFIG, 'CMake:Sources', 'InstallSources'))

    def _find_sources(self, directory, exclude=None, include=None):
        # Recursively find all source file(s) and directories, return their names relatively to alib/{project}/

        if exclude is None:
            exclude = list()
        if include is None:
            include = list()

        exclude = tuple(exclude)
        include = tuple(include)

        def excluded(filename): return len(exclude) != 0 and filename.endswith(tuple(exclude))
        def included(filename): return len(include) == 0 or (len(include) > 0 and filename.endswith(tuple(include))) or ('EMPTY' in include and '.' not in filename)

        source_files = []
        for dp, dn, fn in os.walk(Helpers.path(self.path, directory)):
            for file in fn:
                if not excluded(file) and included(file):
                    source_files.append(os.path.relpath(Helpers.path(dp, file), self.path))
        return source_files

    def generate(self, dry_run):
        return Generator().generate(self, dry_run)

    def recursive_dependencies(self, no_test_dependencies):
        visited = {self.name}
        stack = [self.name]

        while len(stack) > 0:
            c = stack.pop()

            deps = PROJECTS[c].dependencies

            if no_test_dependencies is False:
                deps += PROJECTS[c].dependencies_test

            for dep in deps:
                if dep not in visited:
                    visited.add(dep)
                    stack.append(dep)
        return visited

    def __repr__(self):
        return '<Project: {}>'.format(self.name)

# ----------------------------------------------------------------------------------------------------------------------

class Generator:
    @classmethod
    def generate(cls, project, dry_run):
        foo = getattr(cls, 'generate_{}'.format(project.category))
        return foo(project, dry_run)

    @classmethod
    def generate_root(cls, project, dry_run, projects):
        with SmartOpen("CMakeLists.txt", 'w', directory=ALIB_DIRECTORY, dry_run=dry_run) as f:
            f.write(CATEGORIES['root'].format(
                alib_modules=Helpers.join([p.name for p in projects]),
                alib_version=ALIB_VERSION,
            ))

    @classmethod
    def generate_library(cls, project, dry_run):
        sys_includes, sys_libs, finds = cls._handle_system_deps(project.system_deps)
        test_sys_includes, test_sys_libs, test_finds = cls._handle_system_deps(project.system_deps_test)

        with SmartOpen("CMakeLists.txt", 'w', directory=project.path, dry_run=dry_run) as f:
            f.write(CATEGORIES[project.category].format(
                project_name=project.name,
                target_libs=Helpers.join(project.dependencies + sys_libs, ' '),
                target_test_libs=Helpers.join(project.dependencies_test + test_sys_libs, ' '),
                include_paths=Helpers.join(sys_includes, ' '),
                include_test_paths=Helpers.join(test_sys_includes, ' '),
                cmake_options=Helpers.join(["set({} {})".format(k.upper(), v) for k, v in project.cmake_additional_set], sep='\n'),
                source_files=Helpers.join(project.find_sources()),
                source_files_test=Helpers.join(project.find_sources_test()),
                include_files=Helpers.join(project.find_install_sources()),
                find_packages=Helpers.join(map(lambda p: "find_package({})".format(p), finds), "\n"),
                find_packages_tests=Helpers.join(map(lambda p: "find_package({})".format(p), test_finds), "\n"),
                alib_version=ALIB_VERSION,
            ))

    @classmethod
    def generate_testing(cls, project, dry_run):
        test_sys_includes, test_sys_libs, test_finds = cls._handle_system_deps(project.system_deps_test)

        with SmartOpen("CMakeLists.txt", 'w', directory=project.path, dry_run=dry_run) as f:
            f.write(CATEGORIES[project.category].format(
                project_name=project.name,
                target_test_libs=Helpers.join(project.dependencies_test + test_sys_libs, ' '),
                include_test_paths=Helpers.join(test_sys_includes, ' '),
                cmake_options=Helpers.join(["set({} {})".format(k.upper(), v) for k, v in project.cmake_additional_set], sep='\n'),
                source_files_test=Helpers.join(project.find_sources_test()),
                find_packages_tests=Helpers.join(map(lambda p: "find_package({})".format(p), test_finds), "\n"),
                alib_version=ALIB_VERSION,
            ))

    @classmethod
    def generate_executable(cls, project, dry_run):
        sys_includes, sys_libs, finds = cls._handle_system_deps(project.system_deps)

        with SmartOpen("CMakeLists.txt", 'w', directory=project.path, dry_run=dry_run) as f:
            f.write(CATEGORIES[project.category].format(
                project_name=project.name,
                target_libs=Helpers.join(project.dependencies + sys_libs, ' '),
                include_paths=Helpers.join([project.name] + sys_includes, ' '),
                cmake_options=Helpers.join(["set({} {})".format(k.upper(), v) for k, v in project.cmake_additional_set], sep='\n'),
                source_files=Helpers.join(project.find_sources()),
                find_packages=Helpers.join(map(lambda p: "find_package({})".format(p), finds), "\n"),
                alib_version=ALIB_VERSION,
            ))

    @staticmethod
    def _handle_system_deps(system_deps):
        libs, incl, find = [], [], []

        for dep in system_deps:
            try:
                incl += Config.get_array(CONFIG, 'CMake:Deps:{}'.format(dep), 'include', fallback=[])
                libs += Config.get_array(CONFIG, 'CMake:Deps:{}'.format(dep), 'link', fallback=[])
                f = Config.get(CONFIG, 'CMake:Deps:{}'.format(dep), 'find', fallback="")
                if f != "":
                    find.append(f)
            except ConfigException:
                print("Warning: System dependency '{}' is not configured".format(dep), file=sys.stderr)

        return incl, libs, find


# ----------------------------------------------------------------------------------------------------------------------

SCRIPT_DIRECTORY = Helpers.path(os.path.dirname(__file__))
CONFIG = configparser.ConfigParser(allow_no_value=True)
CONFIG.read(Helpers.path(SCRIPT_DIRECTORY, 'generate.conf'))
ALIB_DIRECTORY = Helpers.path(SCRIPT_DIRECTORY, Config.get(CONFIG, 'General', 'PathToSources'))
ALIB_VERSION = "{}.{}.{}".format(Config.get(CONFIG, 'Versioning', 'major'), Config.get(CONFIG, 'Versioning', 'minor'),
                                 Config.get(CONFIG, 'Versioning', 'patch'))
PROJECTS = {
    project: Project(project, Config.get(CONFIG, 'General', 'ProjectConfFile'))
    for project in Helpers.find_dirs_with_file(ALIB_DIRECTORY, Config.get(CONFIG, 'General', 'ProjectConfFile'))
}
GROUPS = {
    group: [p for n, p in PROJECTS.items() if group in p.groups] for group in Config.get_section(CONFIG, 'Groups').keys()
}
CATEGORIES = {
    category: SmartOpen(template, directory=Helpers.path(SCRIPT_DIRECTORY, Config.get(CONFIG, 'General', 'PathToTemplates')), mode='r').read()
    for category, template in Config.get_section(CONFIG, 'CMake:Categories').items()
}


# ----------------------------------------------------------------------------------------------------------------------

def compute_packages(packages, groups, no_dependencies, no_test_dependencies, verbose):
    # filter out invalid
    for g in groups:
        if g not in GROUPS:
            print("Warning: Package group {} is not a valid group. Skipping".format(g), file=sys.stderr)

    for p in packages:
        if p not in PROJECTS:
            print("Warning: Package {} is not a valid project. Skipping".format(p), file=sys.stderr)

    packages = [p for p in packages if p in PROJECTS]
    groups = [g for g in groups if g in GROUPS]
    packages = [PROJECTS[p] for p in packages] + list(itertools.chain.from_iterable([v for k, v in GROUPS.items() if k in groups]))

    res = set()

    # no packages specified, i.e. return all
    if len(packages) == 0:
        packages = PROJECTS.values()

    # possibly compute dependencies
    for p in packages:
        if verbose:
            p_dependencies_str = map(lambda s: "'{}'".format(s), p.dependencies)
            print("Project '{}' depends on: {}".format(p, Helpers.join(p_dependencies_str, ', ')), file=sys.stderr)

            if no_test_dependencies is False:
                p_dependencies_test_str = map(lambda s: "'{}'".format(s), p.dependencies_test)
                print("Project '{}' tests depends on: {}".format(p, Helpers.join(p_dependencies_test_str, ', ')), file=sys.stderr)

        if no_dependencies:
            res.add(p)
        else:
            res |= {PROJECTS[d] for d in p.recursive_dependencies(no_test_dependencies)}
    return res


def main(dry_run, main_file, packages_requested, groups_requested, no_dependencies, no_test_dependencies, only_show_dependencies):
    packages = compute_packages(packages_requested, groups_requested, no_dependencies, no_test_dependencies, only_show_dependencies)

    print("The following packages will be generated:", file=sys.stderr)
    print(", ".join(sorted([p.name for p in packages])), file=sys.stderr)

    if only_show_dependencies:
        return 0

    packages_cnt = len(packages) + (main_file is True)

    # Generate packages
    for i, p in enumerate(packages, 1):
        try:
            p.generate(dry_run)
            print('[{}/{}] Generated {} package {}'.format(i, packages_cnt, p.category, p.name), file=sys.stderr)
        except ProjectException as e:
            print('[{}/{}] Error while generating {}: {}'.format(i, packages_cnt, p.name, e), file=sys.stderr)
            return 1

    # Generate root file
    if main_file:
        Generator.generate_root(None, dry_run, packages)
        print('[{}/{}] Generated main CMakeLists.txt'.format(packages_cnt, packages_cnt), file=sys.stderr)

    return 0

# ----------------------------------------------------------------------------------------------------------------------


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='CMakeLists generator for Algorithms Library Toolkit')
    parser.add_argument('-w', '--write', action='store_true', default=False, help="Write files")
    parser.add_argument('-m', '--main', action='store_true', default=False, help="Generate main CMakeLists.txt")
    parser.add_argument('-p', '--package', action='append', help='Specify packages to build. For multiple packages, use -p multiple times. Can be combined with -g')
    parser.add_argument('-g', '--group', action='append', help='Specify package group to build. For multiple groups, use -g multiple times. Can be combined with -p')
    parser.add_argument('--no-deps', action='store_true', default=False, help="Don't generate dependencies")
    parser.add_argument('--no-test-deps', action='store_true', default=False, help="Don't consider test dependencies as dependencies")
    parser.add_argument('--show-deps', action='store_true', default=False, help='Only show dependencies of selected packages or groups')
    parser.add_argument('-v', '--version', action='store_true', default=False, help="Show current version")

    args = parser.parse_args()

    if args.version is True:
        print("{}.{}.{}".format(Config.get(CONFIG, 'Versioning', 'major'),
            Config.get(CONFIG, 'Versioning', 'minor'),
	        Config.get(CONFIG, 'Versioning', 'patch')))
        sys.exit(0)

    sys.exit(main(not args.write,
                  args.main,
                  args.package if args.package else [],
                  args.group if args.group else [],
                  args.no_deps,
                  args.no_test_deps,
                  args.show_deps))
