find_package(Git)

# check if GIT executable present
if (NOT Git_FOUND)
	message ( WARNING "[Git] Git not found" )
	return ()
endif()

# check if GIT repository
execute_process (
	COMMAND ${GIT_EXECUTABLE} status
	WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
	OUTPUT_VARIABLE GIT_REPOSITORY
	RESULT_VARIABLE RET_CODE
	OUTPUT_STRIP_TRAILING_WHITESPACE
	OUTPUT_QUIET
	ERROR_QUIET
	)

if("${RET_CODE}" STREQUAL "0")
else()
	message ( WARNING "[Git] Not a git repository. Git version info for debug and snapshot versions not provided." )
	set ( ALIB_GIT_BRANCH   "<unknown branch>" )
	set ( ALIB_GIT_DESCRIBE "<unknown commit>" )
	return ()
endif()

# current branch
execute_process (
	COMMAND ${GIT_EXECUTABLE} rev-parse --abbrev-ref HEAD
	WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
	OUTPUT_VARIABLE ALIB_GIT_BRANCH
	RESULT_VARIABLE RET_CODE
	OUTPUT_STRIP_TRAILING_WHITESPACE
	)

if("${RET_CODE}" STREQUAL "0")
	message ( "[Git] Branch: '${ALIB_GIT_BRANCH}'" )
	SET ( ALIB_GIT_BRANCH "${ALIB_GIT_BRANCH}" )
else()
	message ( WARNING "[Git] Failed retrieving current branch (return code ${RET_CODE})" )
endif()

# describe
execute_process (
	COMMAND ${GIT_EXECUTABLE} describe --tags --match "v*" HEAD
	WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
	OUTPUT_VARIABLE ALIB_GIT_DESCRIBE
	RESULT_VARIABLE RET_CODE
	OUTPUT_STRIP_TRAILING_WHITESPACE
	)

if("${RET_CODE}" STREQUAL "0")
	message ( "[Git] Describe: '${ALIB_GIT_DESCRIBE}'" )
	SET ( ALIB_GIT_DESCRIBE "${ALIB_GIT_DESCRIBE}" )
else()
	message ( WARNING "[Git] Failed retrieving current commit (return code ${RET_CODE})" )
endif()

