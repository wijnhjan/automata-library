# install paths
# -------------

include(GNUInstallDirs)

if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    set (CMAKE_INSTALL_PREFIX /usr)
endif()

message ( "[Install] Install directory prefix is: ${CMAKE_INSTALL_PREFIX}" )
message ( "[Install] Full install paths: ${CMAKE_INSTALL_FULL_BINDIR}, ${CMAKE_INSTALL_FULL_LIBDIR}, ${CMAKE_INSTALL_FULL_INCLUDEDIR}" )

set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
