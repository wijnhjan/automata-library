find_path(Tclap_INCLUDE_DIR NAMES tclap/CmdLine.h)

# Handle the QUIETLY and REQUIRED arguments and set GRAPHVIZ_FOUND to TRUE if all listed variables are TRUE.
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Tclap
	FOUND_VAR Tclap_FOUND
	REQUIRED_VARS Tclap_INCLUDE_DIR
	)

mark_as_advanced(Tclap_INCLUDE_DIR)
