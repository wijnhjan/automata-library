#################
# ALIB Versioning

message("[Versioning] Current stable version is ${PROJECT_VERSION}")

include(git)

# git describe into version string
if(ALIB_GIT_DESCRIBE MATCHES ".*unknown*")
	set(ALIB_SNAPSHOT_VERSION ${ALIB_GIT_DESCRIBE})
	string(REGEX REPLACE "^v" "" ALIB_SNAPSHOT_VERSION ${ALIB_SNAPSHOT_VERSION})
	string(REGEX REPLACE "\([^-]*-g\)" "r\\1" ALIB_SNAPSHOT_VERSION ${ALIB_SNAPSHOT_VERSION})
	string(REPLACE "-" "." ALIB_SNAPSHOT_VERSION ${ALIB_SNAPSHOT_VERSION})
endif()

# overwrite git info, if -DVERSION provided
if(DEFINED VERSION)
	set(ALIB_SNAPSHOT_VERSION ${VERSION})
endif()


if(BUILD_TYPE STREQUAL "snapshot")
	if(DEFINED ALIB_SNAPSHOT_VERSION)
		message("[Versioning] Snapshot version: ${ALIB_SNAPSHOT_VERSION}")
	else()
		message(WARNING "[Versioning] Snapshot version not defined (could not reach GIT and -DVERSION was not provided")
		set(ALIB_SNAPSHOT_VERSION "${PROJECT_VERSION}.r<unknown>.g<unknown>")
	endif()
endif()

if(BUILD_TYPE STREQUAL "debug")
	set(ALIB_VERSION_INFO "${PROJECT_VERSION} | Debug build @ ${ALIB_GIT_BRANCH} ${ALIB_GIT_DESCRIBE}" )
elseif(BUILD_TYPE STREQUAL "release")
	set(ALIB_VERSION_INFO "${PROJECT_VERSION}")
elseif(BUILD_TYPE STREQUAL "snapshot")
	set(ALIB_VERSION_INFO "${ALIB_SNAPSHOT_VERSION}")
endif()

configure_file (
	${CONFIGURE_HEADERS_SRC_DIR}/version.hpp.in
	${CONFIGURE_HEADERS_DST_DIR}/version.hpp
	)
install (
	FILES ${CONFIGURE_HEADERS_DST_DIR}/version.hpp
	DESTINATION include/algorithms-library/
	)
