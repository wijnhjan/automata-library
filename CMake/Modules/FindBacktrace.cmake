include(CheckSymbolExists)

CHECK_SYMBOL_EXISTS ( "backtrace" "execinfo.h" BACKTRACE_EXISTS )
if ( NOT BACKTRACE_EXISTS )
	set ( BACKTRACE_EXISTS FALSE )
	message ( WARNING "Backtrace symbol not found in execinfo.h." )
else ( )
	set ( BACKTRACE_EXISTS TRUE )
endif ( )
