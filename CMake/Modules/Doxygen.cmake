find_package(Doxygen COMPONENTS dot)

if (${DOXYGEN_FOUND})
	# construct input for doxygen
	set(ALIB_DOXYGEN_INPUTS)
	foreach (module ${ALIB_MODULES})
		if (EXISTS ${CMAKE_SOURCE_DIR}/${module}/src)
    		list(APPEND ALIB_DOXYGEN_INPUTS "${module}/src")
    	endif()
	endforeach ()
	string (REPLACE ";" " " ALIB_DOXYGEN_INPUTS "${ALIB_DOXYGEN_INPUTS}")

	set(ALIB_DOXYGEN_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/apidoc")

	configure_file (
		${CMAKE_SOURCE_DIR}/docs/Doxyfile.in
		${CMAKE_BINARY_DIR}/Doxyfile
	)

	file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/doxygen)
	add_custom_target ( "doxygen"
		python3 ${CMAKE_SOURCE_DIR}/extra/scripts/doxygen.py "${CMAKE_SOURCE_DIR}" "${CMAKE_BINARY_DIR}/doxygen" "${ALIB_DOXYGEN_INPUTS}"
		COMMAND doxygen ../Doxyfile
		WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/doxygen
		COMMENT "Generating API documentation with Doxygen"
        VERBATIM )

	message ( "[Doxygen] Doxyfile generated. Run target 'doxygen' to generate Doxygen XML output" )
else()
	message ( WARNING "[Doxygen] Doxygen not found. Target 'doxygen' not created" )
endif()
