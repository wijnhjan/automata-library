# Compiler handling
# -----------------
# Flags according to Debug/Release decision
#  - CMake uses -g on Debug implicitly
#  - fPIC for libraries will be enabled explicitly

include(CheckCXXCompilerFlag)
find_package(Backtrace)

message("[Build type]: ${BUILD_TYPE} (cmake: ${CMAKE_BUILD_TYPE})")
message("[Compiler] ${CMAKE_CXX_COMPILER_ID} ${CMAKE_CXX_COMPILER_VERSION}")

if (NOT ${CMAKE_CXX_COMPILER_ID} MATCHES "(Clang|GNU)")
	message(WARNING "Compiler ${CMAKE_CXX_COMPILER_ID} is not officially supported.")
endif ()

# set cxx standard
set (CMAKE_CXX_STANDARD 17)
set (CXX_STANDARD_REQUIRED ON)

# set -fPIC
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

# add build flags
add_compile_options(-Wall -pedantic -pipe -Wextra -Werror -Wshadow -Wpointer-arith -Wcast-qual -Wdelete-non-virtual-dtor -Wredundant-decls)

# set definitions
add_definitions(-DQT_NO_FOREACH)
if(BACKTRACE_EXISTS)
	add_definitions(-DBACKTRACE)
endif()

if(BUILD_TYPE STREQUAL "debug")
	add_definitions(-DREL_DEBUG)
elseif(BUILD_TYPE STREQUAL "release")
	add_definitions(-DREL_RELEASE)
elseif(BUILD_TYPE STREQUAL "snapshot")
	add_definitions(-DREL_SNAPSHOT)
endif()

if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    add_definitions(-DDEBUG)

	CHECK_CXX_COMPILER_FLAG("-Og" COMPILER_HAS_OG)
	if(COMPILER_HAS_OG)
		add_compile_options(-Og)
	else()
		add_compile_options(-O0)
	endif()

elseif (CMAKE_BUILD_TYPE STREQUAL "Release")
    remove_definitions(-DNDEBUG)
    add_definitions(-DRELEASE)
	add_compile_options(-O3)

	# hardened in the release mode
	add_definitions("-D_FORTIFY_SOURCE=2")
	CHECK_CXX_COMPILER_FLAG("-fstack-protector-strong" COMPILER_HAS_STACKPROTECTOR)
	if ($COMPILER_HAS_STACKPROTECTOR)
		add_compile_options("-fstack-protector-strong")
	endif()
endif ()

# set linker flags
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,-no-as-needed")

# use lld if possible
CHECK_CXX_COMPILER_FLAG("-fuse-ld=lld" COMPILER_HAS_LLD)
if ($COMPILER_HAS_LLD)
	set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fuse-ld=lld")
	set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -fuse-ld=lld")
endif()
