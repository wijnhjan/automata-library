/*
 * RandomTreeFactory.h
 *
 *  Created on: 18. 3. 2015
 *	  Author: Stepan Plachy
 */

#ifndef RANDOM_TREE_FACTORY_H_
#define RANDOM_TREE_FACTORY_H_

#include <climits>

#include <tree/ranked/RankedTree.h>
#include <tree/ranked/RankedPattern.h>
#include <tree/ranked/RankedNonlinearPattern.h>
#include <tree/unranked/UnrankedTree.h>
#include <tree/unranked/UnrankedPattern.h>

namespace tree {

namespace generate {

class RandomRankedTreeFactory {
public:
	static tree::RankedTree < > generateRankedTree ( int depth, int nodesCount, int maxAlphabetSize, bool randomizedAlphabet, int maxRank = INT_MAX );
};

class RandomRankedPatternFactory {
public:
	static tree::RankedPattern < > generateRankedPattern ( int depth, int nodesCount, int maxAlphabetSize, bool randomizedAlphabet, int maxRank = INT_MAX );
};

class RandomRankedNonlinearPatternFactory {
public:
	static tree::RankedNonlinearPattern < > generateRankedNonlinearPattern ( int depth, int nodesCount, int maxAlphabetSize, bool randomizedAlphabet, bool singleNonlinearVariable, int maxRank = INT_MAX );
};

class RandomUnrankedTreeFactory {
public:
	static tree::UnrankedTree < > generateUnrankedTree ( int depth, int nodesCount, int maxAlphabetSize, bool randomizedAlphabet, int maxRank = INT_MAX );
};

class RandomUnrankedPatternFactory {
public:
	static tree::UnrankedPattern < > generateUnrankedPattern ( int depth, int nodesCount, int maxAlphabetSize, bool randomizedAlphabet, int maxRank = INT_MAX );
};

} /* namespace generate */

} /* namespace tree */

#endif /* RANDOM_TREE_FACTORY_H_ */
