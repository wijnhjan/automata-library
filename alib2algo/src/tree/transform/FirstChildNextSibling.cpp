/*
 * FirstChildNextSibling.cpp
 *
 *  Created on: 3. 4. 2020
 *      Author: Jan Travnicek
 */

#include "FirstChildNextSibling.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto FirstChildNextSiblingUnrankedTree = registration::AbstractRegister < tree::transform::FirstChildNextSibling, tree::RankedTree < >, const tree::UnrankedTree < > & > ( tree::transform::FirstChildNextSibling::transform );

} /* namespace */
