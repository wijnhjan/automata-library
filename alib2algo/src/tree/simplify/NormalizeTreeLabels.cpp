/*
 * NormalizeTreeLabels.cpp
 *
 *  Created on: 10. 5. 2017
 *      Author: Aleksandr Shatrovskii
 */

#include "NormalizeTreeLabels.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto NormalizeTreeLabelsRankedTree = registration::AbstractRegister < tree::simplify::NormalizeTreeLabels, tree::RankedTree < unsigned >, const tree::RankedTree < > & > ( tree::simplify::NormalizeTreeLabels::normalize );

} /* namespace */
