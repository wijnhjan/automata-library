/*
 * BorderArrayNaive.cpp
 *
 *  Created on: 5. 11. 2014
 *      Author: Radomir Polach, Tomas Pecka, Jan Travnicek
 */

#include "BorderArrayNaive.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BorderArrayPrefixRankedBarNonlinearPattern = registration::AbstractRegister < tree::properties::BorderArrayNaive, ext::vector < size_t >, const tree::PrefixRankedBarNonlinearPattern < > & > ( tree::properties::BorderArrayNaive::construct );
auto BorderArrayPrefixRankedBarPattern = registration::AbstractRegister < tree::properties::BorderArrayNaive, ext::vector < size_t >, const tree::PrefixRankedBarPattern < > & > ( tree::properties::BorderArrayNaive::construct );
auto BorderArrayPrefixRankedNonlinearPattern = registration::AbstractRegister < tree::properties::BorderArrayNaive, ext::vector < size_t >, const tree::PrefixRankedNonlinearPattern < > & > ( tree::properties::BorderArrayNaive::construct );
auto BorderArrayPrefixRankedPattern = registration::AbstractRegister < tree::properties::BorderArrayNaive, ext::vector < size_t >, const tree::PrefixRankedPattern < > & > ( tree::properties::BorderArrayNaive::construct );

} /* namespace */
