/*
 * BadCharacterShiftTable.cpp
 *
 *  Created on: 5. 11. 2014
 *      Author: Radomir Polach, Tomas Pecka, Jan Travnicek
 */

#include "BadCharacterShiftTable.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BadCharacterShiftTablePrefixRankedBarPattern = registration::AbstractRegister < tree::properties::BadCharacterShiftTable, ext::map < common::ranked_symbol < >, size_t >, const tree::PrefixRankedBarPattern < > & > ( tree::properties::BadCharacterShiftTable::bcs );
auto BadCharacterShiftTablePrefixRankedBarNonlinearPattern = registration::AbstractRegister < tree::properties::BadCharacterShiftTable, ext::map < common::ranked_symbol < >, size_t >, const tree::PrefixRankedBarNonlinearPattern < > & > ( tree::properties::BadCharacterShiftTable::bcs );

} /* namespace */
