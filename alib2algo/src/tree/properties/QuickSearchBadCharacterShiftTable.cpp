/*
 * BadCharacterShiftTable.cpp
 *
 *  Created on: 6. 3. 2018
 *	  Author: Michal Cvach
 */

#include "QuickSearchBadCharacterShiftTable.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto QuickSearchBadCharacterShiftTablePrefixRankedBarPattern = registration::AbstractRegister < tree::properties::QuickSearchBadCharacterShiftTable, ext::map < common::ranked_symbol < >, size_t >, const tree::PrefixRankedBarPattern < > & > ( tree::properties::QuickSearchBadCharacterShiftTable::bcs );
auto QuickSearchBadCharacterShiftTablePrefixRankedBarNonlinearPattern = registration::AbstractRegister < tree::properties::QuickSearchBadCharacterShiftTable, ext::map < common::ranked_symbol < >, size_t >, const tree::PrefixRankedBarNonlinearPattern < > & > ( tree::properties::QuickSearchBadCharacterShiftTable::bcs );

} /* namespace */
