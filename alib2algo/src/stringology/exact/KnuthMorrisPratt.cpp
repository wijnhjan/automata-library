/*
 * KnuthMorrisPratt.cpp
 *
 *  Created on: 5. 11. 2014
 *      Author: Jan Travnicek
 */

#include "KnuthMorrisPratt.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto KnuthMorrisPratt = registration::AbstractRegister < stringology::exact::KnuthMorrisPratt, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::KnuthMorrisPratt::match );

} /* namespace */
