/*
 * BoyerMooreHorspool.cpp
 *
 *  Created on: 5. 11. 2014
 *      Author: Radomir Polach, Tomas Pecka
 */

#include "BoyerMooreHorspool.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BoyerMooreHorspoolLinearStringLinearString = registration::AbstractRegister < stringology::exact::BoyerMooreHorspool, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & > ( stringology::exact::BoyerMooreHorspool::match );

} /* namespace */
