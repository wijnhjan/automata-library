//
// Created by shushiri on 25.2.19.
//

#ifndef RESTRICTED_APPROXIMATE_COVERS_COMPUTATION_H
#define RESTRICTED_APPROXIMATE_COVERS_COMPUTATION_H

#include <string/LinearString.h>
#include <stringology/cover/ApproximateCoversComputation.h>

namespace stringology::cover {

class RestrictedApproximateCoversComputation {
public:
    /**
     * Computes all restricted approximate covers of a string
     * Source: Shushkova Irina: Implementace automatových algoritmů na hledání pravidelností (2019), chapter 2.3
     *
     * @param pattern string for which the covers are computed
     * @return set of all restricted approximate covers of input pattern.
     */
    template < class SymbolType >
    static ext::set < ext::pair < string::LinearString < SymbolType >, unsigned int > > compute ( const string::LinearString < SymbolType > & pattern, unsigned int k );
};

template < class SymbolType >
ext::set < ext::pair < string::LinearString < SymbolType >, unsigned int > > RestrictedApproximateCoversComputation::compute ( const string::LinearString < SymbolType > & pattern, unsigned int k ) {
	return stringology::cover::ApproximateCoversComputation::compute ( pattern, k, true );
}

} /* namespace stringology::cover */

#endif /* RESTRICTED_APPROXIMATE_COVERS_COMPUTATION_H */
