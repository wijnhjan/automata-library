/*
 * PositionHeapFactors.cpp
 *
 *  Created on: 2. 1. 2017
 *      Author: Jan Travnicek
 */

#include "PositionHeapFactors.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto PositionHeapFactorsLinearString = registration::AbstractRegister < stringology::query::PositionHeapFactors, ext::set < unsigned >, const indexes::stringology::PositionHeap < > &, const string::LinearString < > & > ( stringology::query::PositionHeapFactors::query );

} /* namespace */
