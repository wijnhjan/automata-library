/*
 * BitParallelismFactors.cpp
 *
 *  Created on: 2. 1. 2017
 *      Author: Jan Travnicek
 */

#include "BitParallelismFactors.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BitParallelismFactorsLinearString = registration::AbstractRegister < stringology::query::BitParallelismFactors, ext::set < unsigned >, const indexes::stringology::BitParallelIndex < > &, const string::LinearString < > & > ( stringology::query::BitParallelismFactors::query );

} /* namespace */
