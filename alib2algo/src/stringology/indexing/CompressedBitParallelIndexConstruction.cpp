/*
 * CompressedBitParallelIndexConstruction.cpp
 *
 *  Created on: 6. 2. 2017
 *      Author: Jan Travnicek
 */

#include "CompressedBitParallelIndexConstruction.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto compressedCompressedBitParallelIndexConstructionLinearString = registration::AbstractRegister < stringology::indexing::CompressedBitParallelIndexConstruction, indexes::stringology::CompressedBitParallelIndex < DefaultSymbolType >, const string::LinearString < > & > ( stringology::indexing::CompressedBitParallelIndexConstruction::construct );

} /* namespace */
