/*
 * Author: Radovan Cerveny
 */

#include "ExactFactorOracleAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto FactorOracleAutomatonLinearString = registration::AbstractRegister < stringology::indexing::ExactFactorOracleAutomaton, indexes::stringology::FactorOracleAutomaton < >, const string::LinearString < > & > ( stringology::indexing::ExactFactorOracleAutomaton::construct );

} /* namespace */
