/*
 * ExactNondeterministicSubsequenceAutomaton.h
 *
 *  Created on: 7. 4. 2015
 *      Author: Jan Travnicek
 */

#ifndef _EXACT_NONDETERMINISTIC_SUBSEQUENCE_AUTOMATON__H_
#define _EXACT_NONDETERMINISTIC_SUBSEQUENCE_AUTOMATON__H_

#include <automaton/FSM/EpsilonNFA.h>
#include <string/LinearString.h>

namespace stringology {

namespace indexing {

class ExactNondeterministicSubsequenceAutomaton {
public:
	/**
	 * Performs conversion.
	 * @return left regular grammar equivalent to source automaton.
	 */
	template < class SymbolType >
	static automaton::EpsilonNFA < SymbolType, unsigned > construct ( const string::LinearString < SymbolType > & text );
};

template < class SymbolType >
automaton::EpsilonNFA < SymbolType, unsigned > ExactNondeterministicSubsequenceAutomaton::construct ( const string::LinearString < SymbolType > & text ) {
	automaton::EpsilonNFA < SymbolType, unsigned > res ( 0 );
	res.addFinalState ( 0 );
	res.setInputAlphabet ( text.getAlphabet ( ) );

	unsigned i = 1;
	for ( const SymbolType & symbol : text.getContent ( ) ) {
		res.addState ( i );
		res.addFinalState( i );

		res.addTransition ( i - 1, symbol, i );
		res.addTransition ( i - 1, i );
		i++;
	}
	return res;
}

} /* namespace indexing */

} /* namespace stringology */

#endif /* _EXACT_NONDETERMINISTIC_SUBSEQUENCE_AUTOMATON__H_ */
