/*
 * HammingBitParalelism.cpp
 *
 *  Created on: 4. 5. 2018
 *      Author: Tomas Capek
 */

#include "HammingBitParalelism.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto HammingBitParalelismLinearString = registration::AbstractRegister < stringology::simulations::HammingBitParalelism, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & , unsigned > ( stringology::simulations::HammingBitParalelism::search );

} /* namespace */
