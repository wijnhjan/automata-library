/*
 * LevenshteinDynamicProgramming.cpp
 *
 *  Created on: 4. 5. 2018
 *      Author: Tomas Capek
 */

#include "LevenshteinDynamicProgramming.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto LevenshteinDynamicProgrammingLinearString = registration::AbstractRegister < stringology::simulations::LevenshteinDynamicProgramming, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > &, unsigned > ( stringology::simulations::LevenshteinDynamicProgramming::search );

} /* namespace */
