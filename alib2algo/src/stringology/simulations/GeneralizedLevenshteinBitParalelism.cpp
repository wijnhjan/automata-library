/*
 * GeneralizedLevenshteinBitParalelism.cpp
 *
 *  Created on: 4. 5. 2018
 *      Author: Tomas Capek
 */

#include "GeneralizedLevenshteinBitParalelism.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto GeneralizedLevenshteinBitParalelismLinearString = registration::AbstractRegister < stringology::simulations::GeneralizedLevenshteinBitParalelism, ext::set < unsigned >, const string::LinearString < > &, const string::LinearString < > & , unsigned > ( stringology::simulations::GeneralizedLevenshteinBitParalelism::search );

} /* namespace */
