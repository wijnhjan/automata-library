/*
 * WideBNDMMatcherConstruction.cpp
 *
 *  Created on: 6. 2. 2017
 *      Author: Jan Travnicek
 */

#include "WideBNDMMatcherConstruction.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto WideBNDMIndexConstructionLinearString = registration::AbstractRegister < stringology::matching::WideBNDMMatcherConstruction, indexes::stringology::BitParallelIndex < DefaultSymbolType >, const string::LinearString < > & > ( stringology::matching::WideBNDMMatcherConstruction::construct );

} /* namespace */
