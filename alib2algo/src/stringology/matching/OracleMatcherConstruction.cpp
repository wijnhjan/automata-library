/*
 * Author: Radovan Cerveny
 */

#include "OracleMatcherConstruction.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto OracleMatcherConstructionLinearString = registration::AbstractRegister < stringology::matching::OracleMatcherConstruction, automaton::DFA < DefaultSymbolType, unsigned >, const string::LinearString < > & > ( stringology::matching::OracleMatcherConstruction::construct );

} /* namespace */
