/*
 * ExactMatchingAutomaton.cpp
 *
 *  Created on: 9. 2. 2014
 *      Author: Jan Travnicek
 */

#include "ExactMatchingAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactMatchingAutomatonLinearString = registration::AbstractRegister < stringology::matching::ExactMatchingAutomaton, automaton::NFA < DefaultSymbolType, unsigned >, const string::LinearString < > & > ( stringology::matching::ExactMatchingAutomaton::construct );

} /* namespace */
