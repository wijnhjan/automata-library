/*
 * LZ77Decompression.h
 *
 *  Created on: 1. 3. 2017
 *      Author: Jan Parma
 */

#ifndef _LZ77DECOMPRESS_H_
#define _LZ77DECOMPRESS_H_

#include <tuple>
#include <vector>
#include <alib/set>

#include <string/LinearString.h>

namespace stringology {

namespace compression {

class LZ77Decompression {
public:
	template < class SymbolType >
	static string::LinearString < SymbolType > decompress ( const std::vector < std::tuple < unsigned, unsigned, SymbolType > > & input );
};

// Main method that handle decompress
template < class SymbolType >
string::LinearString < SymbolType > LZ77Decompression::decompress ( const std::vector < std::tuple < unsigned, unsigned, SymbolType > > & input ) {

	string::LinearString < SymbolType > output;

	for ( unsigned i = 0; i < input.size ( ); i++ ) {
		for ( unsigned j = 0; j < std::get < 1 > ( input[i] ); j++ ) {
			output.appendSymbol ( output.getContent ( )[output.getContent ( ).size ( ) - std::get < 0 > ( input[i] )] );
		}

		output.extendAlphabet ( ext::set < SymbolType > { std::get < 2 > ( input[i] ) } );
		output.appendSymbol ( std::get < 2 > ( input[i] ) );
	}

	return output;
}

} /* namespace compression */

} /* namespace stringology */

#endif /* _LZ77DECOMPRESS_H_ */
