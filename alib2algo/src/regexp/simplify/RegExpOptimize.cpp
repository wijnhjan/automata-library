/*
 * RegExpOptimize.cpp
 *
 *  Created on: 20. 1. 2014
 *	  Author: Tomas Pecka
 */

#include "RegExpOptimize.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto RegExpOptimizeFormalRegExp = registration::AbstractRegister < regexp::simplify::RegExpOptimize, regexp::FormalRegExp < >, const regexp::FormalRegExp < > & > ( regexp::simplify::RegExpOptimize::optimize, "regexp" ).setDocumentation (
"Implements a regexp simplification algorithm that is transforming the regular expression to be smaller.\n\
\n\
@param regexp the simplified regexp\n\
@return the simlified regexp" );

auto RegExpOptimizeUnboundedRegExp = registration::AbstractRegister < regexp::simplify::RegExpOptimize, regexp::UnboundedRegExp < >, const regexp::UnboundedRegExp < > & > ( regexp::simplify::RegExpOptimize::optimize, "regexp" ).setDocumentation (
"Implements a regexp simplification algorithm that is transforming the regular expression to be smaller.\n\
\n\
@param regexp the simplified regexp\n\
@return the simlified regexp" );

} /* namespace */
