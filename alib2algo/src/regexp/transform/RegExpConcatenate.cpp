/*
 * RegExpConcatenate.cpp
 *
 *  Created on: 17. 10. 2014
 *	  Author: Jan Travnicek
 */

#include "RegExpConcatenate.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto RegExpConcatenateFormalRegExp = registration::AbstractRegister < regexp::transform::RegExpConcatenate, regexp::FormalRegExp < >, const regexp::FormalRegExp < > &, const regexp::FormalRegExp < > & > ( regexp::transform::RegExpConcatenate::concatenate, "first", "second" ).setDocumentation (
"Implements concatenation of two regular expressions.\n\
\n\
@param first the first regexp to concatenate\n\
@param second the second regexp to concatenate\n\
@return regexp describing first . second" );

auto RegExpConcatenateUnboundedRegExp = registration::AbstractRegister < regexp::transform::RegExpConcatenate, regexp::UnboundedRegExp < >, const regexp::UnboundedRegExp < > &, const regexp::UnboundedRegExp < > & > ( regexp::transform::RegExpConcatenate::concatenate, "first", "second" ).setDocumentation (
"Implements concatenation of two regular expressions.\n\
\n\
@param first the first regexp to concatenate\n\
@param second the second regexp to concatenate\n\
@return regexp describing first . second" );

} /* namespace */
