/*
 * RegExpIntegral.cpp
 *
 *  Created on: 24. 2. 2014
 *	  Author: Tomas Pecka
 */

#include "RegExpIntegral.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto RegExpIntegralFormalRegExpString = registration::AbstractRegister < regexp::transform::RegExpIntegral, regexp::FormalRegExp < >, const regexp::FormalRegExp < > &, const string::LinearString < > & > ( regexp::transform::RegExpIntegral::integral, "regexp", "string" ).setDocumentation (
"Implements integration of regular expression by a symbol. Result does not include the integration constant.\n\
\n\
@param regexp the regexp to derivate\n\
@param string the sequence of symbols to integrate by\n\
@return resulting regexp" );

auto RegExpIntegralUnboundedRegExpString = registration::AbstractRegister < regexp::transform::RegExpIntegral, regexp::UnboundedRegExp < >, const regexp::UnboundedRegExp < > &, const string::LinearString < > & > ( regexp::transform::RegExpIntegral::integral, "regexp", "string" ).setDocumentation (
"Implements integration of regular expression by a symbol. Result does not include the integration constant.\n\
\n\
@param regexp the regexp to derivate\n\
@param string the sequence of symbols to integrate by\n\
@return resulting regexp" );

auto RegExpIntegralFormalRegExpSymbol = registration::AbstractRegister < regexp::transform::RegExpIntegral, regexp::FormalRegExp < >, const regexp::FormalRegExp < > &, const DefaultSymbolType & > ( regexp::transform::RegExpIntegral::integral, "regexp", "symbol" ).setDocumentation (
"Implements integration of regular expression by a symbol. Result does not include the integration constant.\n\
\n\
@param regexp the regexp to derivate\n\
@param symbol the symbol to integrate by\n\
@return resulting regexp" );

auto RegExpIntegralUnboundedRegExpSymbol = registration::AbstractRegister < regexp::transform::RegExpIntegral, regexp::UnboundedRegExp < >, const regexp::UnboundedRegExp < > &, const DefaultSymbolType & > ( regexp::transform::RegExpIntegral::integral, "regexp", "symbol" ).setDocumentation (
"Implements integration of regular expression by a symbol. Result does not include the integration constant.\n\
\n\
@param regexp the regexp to derivate\n\
@param symbol the symbol to integrate by\n\
@return resulting regexp" );

} /* namespace */
