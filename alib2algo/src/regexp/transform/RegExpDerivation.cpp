/*
 * RegExpDerivation.cpp
 *
 *  Created on: 19. 1. 2014
 *	  Author: Tomas Pecka
 */

#include "RegExpDerivation.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto RegExpDerivationFormalRegExpString = registration::AbstractRegister < regexp::transform::RegExpDerivation, regexp::FormalRegExp < >, const regexp::FormalRegExp < > &, const string::LinearString < > & > ( regexp::transform::RegExpDerivation::derivation, "regexp", "string" ).setDocumentation (
"Implements derivation of regular expression by a symbol sequence.\n\
\n\
@param regexp the regexp to derivate\n\
@param string the sequence of symbols to derivate by\n\
@return resulting regexp");

auto RegExpDerivationUnboundedRegExpString = registration::AbstractRegister < regexp::transform::RegExpDerivation, regexp::UnboundedRegExp < >, const regexp::UnboundedRegExp < > &, const string::LinearString < > & > ( regexp::transform::RegExpDerivation::derivation, "regexp", "string" ).setDocumentation (
"Implements derivation of regular expression by a symbol sequence.\n\
\n\
@param regexp the regexp to derivate\n\
@param string the sequence of symbols to derivate by\n\
@return resulting regexp");

auto RegExpDerivationFormalRegExpSymbol = registration::AbstractRegister < regexp::transform::RegExpDerivation, regexp::FormalRegExp < >, const regexp::FormalRegExp < > &, const DefaultSymbolType & > ( regexp::transform::RegExpDerivation::derivation, "regexp", "symbol" ).setDocumentation (
"Implements derivation of regular expression by a symbol sequence.\n\
\n\
@param regexp the regexp to derivate\n\
@param symbol the symbol to derivate by\n\
@return resulting regexp");

auto RegExpDerivationUnboundedRegExpSymbol = registration::AbstractRegister < regexp::transform::RegExpDerivation, regexp::UnboundedRegExp < >, const regexp::UnboundedRegExp < > &, const DefaultSymbolType & > ( regexp::transform::RegExpDerivation::derivation, "regexp", "symbol" ).setDocumentation (
"Implements derivation of regular expression by a symbol sequence.\n\
\n\
@param regexp the regexp to derivate\n\
@param symbol the symbol to derivate by\n\
@return resulting regexp");

} /* namespace */
