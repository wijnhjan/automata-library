/*
 * ReachableSymbols.cpp
 *
 *  Created on: 22. 3. 2014
 *	  Author: Tomas Pecka
 */

#include <grammar/ContextFree/CFG.h>
#include <grammar/ContextFree/EpsilonFreeCFG.h>
#include <grammar/ContextFree/GNF.h>
#include <grammar/ContextFree/CNF.h>
#include <grammar/ContextFree/LG.h>
#include <grammar/Regular/LeftLG.h>
#include <grammar/Regular/LeftRG.h>
#include <grammar/Regular/RightLG.h>
#include <grammar/Regular/RightRG.h>

#include "ReachableSymbols.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ReachableSymbolsCFG = registration::AbstractRegister < grammar::properties::ReachableSymbols, ext::set < ext::variant < DefaultSymbolType, DefaultSymbolType > >, const grammar::CFG < > & > ( grammar::properties::ReachableSymbols::getReachableSymbols, "grammar" ).setDocumentation (
"Implements retrieval of symbols that are reachable, i.e. symbols a \\in { b : S->*\\alpha b \\beta, where \\alpha and \\beta \\in (NuT)*}\n\
\n\
@param grammar the tested grammar\n\
@return set of symbols that are reachable in the grammar" );

auto ReachableSymbolsEpsilonFreeCFG = registration::AbstractRegister < grammar::properties::ReachableSymbols, ext::set < ext::variant < DefaultSymbolType, DefaultSymbolType > >, const grammar::EpsilonFreeCFG < > & > ( grammar::properties::ReachableSymbols::getReachableSymbols, "grammar" ).setDocumentation (
"Implements retrieval of symbols that are reachable, i.e. symbols a \\in { b : S->*\\alpha b \\beta, where \\alpha and \\beta \\in (NuT)*}\n\
\n\
@param grammar the tested grammar\n\
@return set of symbols that are reachable in the grammar" );

auto ReachableSymbolsGNF = registration::AbstractRegister < grammar::properties::ReachableSymbols, ext::set < ext::variant < DefaultSymbolType, DefaultSymbolType > >, const grammar::GNF < > & > ( grammar::properties::ReachableSymbols::getReachableSymbols, "grammar" ).setDocumentation (
"Implements retrieval of symbols that are reachable, i.e. symbols a \\in { b : S->*\\alpha b \\beta, where \\alpha and \\beta \\in (NuT)*}\n\
\n\
@param grammar the tested grammar\n\
@return set of symbols that are reachable in the grammar" );

auto ReachableSymbolsCNF = registration::AbstractRegister < grammar::properties::ReachableSymbols, ext::set < ext::variant < DefaultSymbolType, DefaultSymbolType > >, const grammar::CNF < > & > ( grammar::properties::ReachableSymbols::getReachableSymbols, "grammar" ).setDocumentation (
"Implements retrieval of symbols that are reachable, i.e. symbols a \\in { b : S->*\\alpha b \\beta, where \\alpha and \\beta \\in (NuT)*}\n\
\n\
@param grammar the tested grammar\n\
@return set of symbols that are reachable in the grammar" );

auto ReachableSymbolsLG = registration::AbstractRegister < grammar::properties::ReachableSymbols, ext::set < ext::variant < DefaultSymbolType, DefaultSymbolType > >, const grammar::LG < > & > ( grammar::properties::ReachableSymbols::getReachableSymbols, "grammar" ).setDocumentation (
"Implements retrieval of symbols that are reachable, i.e. symbols a \\in { b : S->*\\alpha b \\beta, where \\alpha and \\beta \\in (NuT)*}\n\
\n\
@param grammar the tested grammar\n\
@return set of symbols that are reachable in the grammar" );

auto ReachableSymbolsLeftLG = registration::AbstractRegister < grammar::properties::ReachableSymbols, ext::set < ext::variant < DefaultSymbolType, DefaultSymbolType > >, const grammar::LeftLG < > & > ( grammar::properties::ReachableSymbols::getReachableSymbols, "grammar" ).setDocumentation (
"Implements retrieval of symbols that are reachable, i.e. symbols a \\in { b : S->*\\alpha b \\beta, where \\alpha and \\beta \\in (NuT)*}\n\
\n\
@param grammar the tested grammar\n\
@return set of symbols that are reachable in the grammar" );

auto ReachableSymbolsLeftRG = registration::AbstractRegister < grammar::properties::ReachableSymbols, ext::set < ext::variant < DefaultSymbolType, DefaultSymbolType > >, const grammar::LeftRG < > & > ( grammar::properties::ReachableSymbols::getReachableSymbols, "grammar" ).setDocumentation (
"Implements retrieval of symbols that are reachable, i.e. symbols a \\in { b : S->*\\alpha b \\beta, where \\alpha and \\beta \\in (NuT)*}\n\
\n\
@param grammar the tested grammar\n\
@return set of symbols that are reachable in the grammar" );

auto ReachableSymbolsRightLG = registration::AbstractRegister < grammar::properties::ReachableSymbols, ext::set < ext::variant < DefaultSymbolType, DefaultSymbolType > >, const grammar::RightLG < > & > ( grammar::properties::ReachableSymbols::getReachableSymbols, "grammar" ).setDocumentation (
"Implements retrieval of symbols that are reachable, i.e. symbols a \\in { b : S->*\\alpha b \\beta, where \\alpha and \\beta \\in (NuT)*}\n\
\n\
@param grammar the tested grammar\n\
@return set of symbols that are reachable in the grammar" );

auto ReachableSymbolsRightRG = registration::AbstractRegister < grammar::properties::ReachableSymbols, ext::set < ext::variant < DefaultSymbolType, DefaultSymbolType > >, const grammar::RightRG < > & > ( grammar::properties::ReachableSymbols::getReachableSymbols, "grammar" ).setDocumentation (
"Implements retrieval of symbols that are reachable, i.e. symbols a \\in { b : S->*\\alpha b \\beta, where \\alpha and \\beta \\in (NuT)*}\n\
\n\
@param grammar the tested grammar\n\
@return set of symbols that are reachable in the grammar" );

} /* namespace */
