/*
 * RecursiveNonterminal.cpp
 *
 *  Created on: 16. 11. 2015
 *	  Author: Jan Travnicek
 */

#include <grammar/ContextFree/CFG.h>
#include <grammar/ContextFree/EpsilonFreeCFG.h>
#include <grammar/ContextFree/GNF.h>
#include <grammar/ContextFree/CNF.h>
#include <grammar/ContextFree/LG.h>
#include <grammar/Regular/LeftLG.h>
#include <grammar/Regular/LeftRG.h>
#include <grammar/Regular/RightLG.h>
#include <grammar/Regular/RightRG.h>

#include "RecursiveNonterminal.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto RecursiveNonterminalCFG = registration::AbstractRegister < grammar::properties::RecursiveNonterminal, bool, const grammar::CFG < > &, const DefaultSymbolType & > ( grammar::properties::RecursiveNonterminal::isNonterminalRecursive, "grammar", "nonterminal" ).setDocumentation (
"Retrieves A \\in { B : A->^+ B \\alpha, where \\alpha \\in (NuT)* } for given grammar and nonterminal\n\
\n\
@param grammar the tested grammar\n\
@param nonterminal the tested nonterminal\n\
@return bool which denote whether the nonterminal is recursive in the grammar" );

auto RecursiveNonterminalEpsilonFreeCFG = registration::AbstractRegister < grammar::properties::RecursiveNonterminal, bool, const grammar::EpsilonFreeCFG < > &, const DefaultSymbolType & > ( grammar::properties::RecursiveNonterminal::isNonterminalRecursive, "grammar", "nonterminal" ).setDocumentation (
"Retrieves A \\in { B : A->^+ B \\alpha, where \\alpha \\in (NuT)* } for given grammar and nonterminal\n\
\n\
@param grammar the tested grammar\n\
@param nonterminal the tested nonterminal\n\
@return bool which denote whether the nonterminal is recursive in the grammar" );

auto RecursiveNonterminalGNF = registration::AbstractRegister < grammar::properties::RecursiveNonterminal, bool, const grammar::GNF < > &, const DefaultSymbolType & > ( grammar::properties::RecursiveNonterminal::isNonterminalRecursive, "grammar", "nonterminal" ).setDocumentation (
"Retrieves A \\in { B : A->^+ B \\alpha, where \\alpha \\in (NuT)* } for given grammar and nonterminal\n\
\n\
@param grammar the tested grammar\n\
@param nonterminal the tested nonterminal\n\
@return bool which denote whether the nonterminal is recursive in the grammar" );

auto RecursiveNonterminalCNF = registration::AbstractRegister < grammar::properties::RecursiveNonterminal, bool, const grammar::CNF < > &, const DefaultSymbolType & > ( grammar::properties::RecursiveNonterminal::isNonterminalRecursive, "grammar", "nonterminal" ).setDocumentation (
"Retrieves A \\in { B : A->^+ B \\alpha, where \\alpha \\in (NuT)* } for given grammar and nonterminal\n\
\n\
@param grammar the tested grammar\n\
@param nonterminal the tested nonterminal\n\
@return bool which denote whether the nonterminal is recursive in the grammar" );

auto RecursiveNonterminalLG = registration::AbstractRegister < grammar::properties::RecursiveNonterminal, bool, const grammar::LG < > &, const DefaultSymbolType & > ( grammar::properties::RecursiveNonterminal::isNonterminalRecursive, "grammar", "nonterminal" ).setDocumentation (
"Retrieves A \\in { B : A->^+ B \\alpha, where \\alpha \\in (NuT)* } for given grammar and nonterminal\n\
\n\
@param grammar the tested grammar\n\
@param nonterminal the tested nonterminal\n\
@return bool which denote whether the nonterminal is recursive in the grammar" );

auto RecursiveNonterminalLeftLG = registration::AbstractRegister < grammar::properties::RecursiveNonterminal, bool, const grammar::LeftLG < > &, const DefaultSymbolType & > ( grammar::properties::RecursiveNonterminal::isNonterminalRecursive, "grammar", "nonterminal" ).setDocumentation (
"Retrieves A \\in { B : A->^+ B \\alpha, where \\alpha \\in (NuT)* } for given grammar and nonterminal\n\
\n\
@param grammar the tested grammar\n\
@param nonterminal the tested nonterminal\n\
@return bool which denote whether the nonterminal is recursive in the grammar" );

auto RecursiveNonterminalLeftRG = registration::AbstractRegister < grammar::properties::RecursiveNonterminal, bool, const grammar::LeftRG < > &, const DefaultSymbolType & > ( grammar::properties::RecursiveNonterminal::isNonterminalRecursive, "grammar", "nonterminal" ).setDocumentation (
"Retrieves A \\in { B : A->^+ B \\alpha, where \\alpha \\in (NuT)* } for given grammar and nonterminal\n\
\n\
@param grammar the tested grammar\n\
@param nonterminal the tested nonterminal\n\
@return bool which denote whether the nonterminal is recursive in the grammar" );

auto RecursiveNonterminalRightLG = registration::AbstractRegister < grammar::properties::RecursiveNonterminal, bool, const grammar::RightLG < > &, const DefaultSymbolType & > ( grammar::properties::RecursiveNonterminal::isNonterminalRecursive, "grammar", "nonterminal" ).setDocumentation (
"Retrieves A \\in { B : A->^+ B \\alpha, where \\alpha \\in (NuT)* } for given grammar and nonterminal\n\
\n\
@param grammar the tested grammar\n\
@param nonterminal the tested nonterminal\n\
@return bool which denote whether the nonterminal is recursive in the grammar" );

auto RecursiveNonterminalRightRG = registration::AbstractRegister < grammar::properties::RecursiveNonterminal, bool, const grammar::RightRG < > &, const DefaultSymbolType & > ( grammar::properties::RecursiveNonterminal::isNonterminalRecursive, "grammar", "nonterminal" ).setDocumentation (
"Retrieves A \\in { B : A->^+ B \\alpha, where \\alpha \\in (NuT)* } for given grammar and nonterminal\n\
\n\
@param grammar the tested grammar\n\
@param nonterminal the tested nonterminal\n\
@return bool which denote whether the nonterminal is recursive in the grammar" );

} /* namespace */
