/*
 * GrammarAlternation.cpp
 *
 *  Created on: 05. 11. 2019
 *	  Author: Tomas Pecka
 */

#include "GrammarAlternation.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto GrammarAlternationCFG = registration::AbstractRegister < grammar::transform::GrammarAlternation,
	grammar::CFG < DefaultSymbolType, ext::pair < DefaultSymbolType, unsigned > >,
	const grammar::CFG < DefaultSymbolType, DefaultSymbolType > &,
	const grammar::CFG < DefaultSymbolType, DefaultSymbolType > & > ( grammar::transform::GrammarAlternation::alternation, "first", "second" ).setDocumentation (
"Alternates two context-free grammars.\n\
\n\
@param first First grammar (G1)\n\
@param second Second grammar (G2)\n\
@return context-free grammar G where L(G) = L(G1) \\cup L(G2)" );

} /* namespace */
