/*
 * Determinize.cpp
 *
 *  Created on: 16. 1. 2014
 *	  Author: Jan Vesely
 */

#include "Determinize.h"
#include <automaton/FSM/DFA.h>
#include <automaton/FSM/MultiInitialStateNFA.h>
#include <automaton/PDA/DPDA.h>
#include <automaton/PDA/NPDA.h>
#include <automaton/PDA/InputDrivenDPDA.h>
#include <automaton/PDA/InputDrivenNPDA.h>
#include <automaton/PDA/SinglePopDPDA.h>
#include <automaton/PDA/VisiblyPushdownDPDA.h>
#include <automaton/TM/OneTapeDTM.h>
#include <automaton/TA/DFTA.h>
#include <automaton/TA/UnorderedDFTA.h>
#include <automaton/PDA/RealTimeHeightDeterministicNPDA.h>
#include <registration/AlgoRegistration.hpp>

namespace {

auto DeterminizeDFA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::DFA < >, const automaton::DFA < > & > ( automaton::determinize::Determinize::determinize, "dfa" ).setDocumentation (
"Determinization of deterministic finite automata.\n\
Implemented as a no-op.\n\
\n\
@param dfa deterministic finite automaton\n\
@return deterministic finite automaton equivalent to @p dfa" );

auto DeterminizeNFA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::DFA < DefaultSymbolType, ext::set < DefaultStateType > >, const automaton::NFA < > & > ( automaton::determinize::Determinize::determinize, "nfa" ).setDocumentation (
"Implementation of subset determinization for nondeterministic finite automata.\n\
\n\
@param nfa nondeterministic finite automaton\n\
@return deterministic finite automaton equivalent to @p nfa" );

auto DeterminizeMultiInitialStateNFA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::DFA < DefaultSymbolType, ext::set < DefaultStateType > >, const automaton::MultiInitialStateNFA < > & > ( automaton::determinize::Determinize::determinize, "nfa" ).setDocumentation (
"Implementation of subset determinization for nondeterministic finite automata with multiple initial states.\n\
\n\
@param nfa nondeterministic finite automaton with multiple initial states\n\
@return deterministic finite automaton equivalent to @p nfa" );

auto DeterminizeUnorderedDFTA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::UnorderedDFTA < >, const automaton::UnorderedDFTA < > & > ( automaton::determinize::Determinize::determinize, "dfta" ).setDocumentation (
"Determinization of deterministic finite tree automata.\n\
Implemented as a no-op.\n\
\n\
@param dfta deterministic finite tree automaton\n\
@return deterministic finite tree automaton equivalent to @p dfta" );

auto DeterminizeUnorderedNFTA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::UnorderedDFTA < DefaultSymbolType, ext::set < DefaultSymbolType > >, const automaton::UnorderedNFTA < > & > ( automaton::determinize::Determinize::determinize, "nfta" ).setDocumentation (
"Implementation of subset determinization for nondeterministic finite tree automata.\n\
\n\
@param nfta nondeterministic finite tree automaton\n\
@return deterministic finite tree automaton equivalent to @p nfta" );

auto DeterminizeDFTA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::DFTA < >, const automaton::DFTA < > & > ( automaton::determinize::Determinize::determinize, "dfta" ).setDocumentation (
"Determinization of deterministic finite tree automata.\n\
Implemented as a no-op.\n\
\n\
@param dfta deterministic finite tree automaton\n\
@return deterministic finite tree automaton equivalent to @p dfta" );

auto DeterminizeNFTA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::DFTA < DefaultSymbolType, ext::set < DefaultSymbolType > >, const automaton::NFTA < > & > ( automaton::determinize::Determinize::determinize, "nfta" ).setDocumentation (
"Implementation of subset determinization for nondeterministic finite tree automata.\n\
\n\
@param nfta nondeterministic finite tree automaton\n\
@return deterministic finite tree automaton equivalent to @p nfta" );

auto DeterminizeInputDrivenDPDA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::InputDrivenDPDA < >, const automaton::InputDrivenDPDA < > & > ( automaton::determinize::Determinize::determinize, "dpda" ).setDocumentation (
"Determinization of deterministic input-driven pushdown automata.\n\
Implemented as a no-op.\n\
\n\
@param dpda deterministic input-driven pushdown automaton\n\
@return deterministic input-driven pushdown automaton equivalent to @p dpda" );

auto DeterminizeInputDrivenNPDA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::InputDrivenDPDA < DefaultSymbolType, DefaultSymbolType, ext::set < DefaultStateType > >, const automaton::InputDrivenNPDA < > & > ( automaton::determinize::Determinize::determinize, "npda" ).setDocumentation (
"Implementation of determinization for input-driven pushdown automata.\n\
\n\
@param npda nondeterministic input-driven pushdown automaton\n\
@return deterministic input-driven pushdown automaton equivalent to @p npda" );

auto DeterminizeVisiblyPushdownDPDA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::VisiblyPushdownDPDA < >, const automaton::VisiblyPushdownDPDA < > & > ( automaton::determinize::Determinize::determinize, "dpda" ).setDocumentation (
"Determinization of deterministic visibly pushdown automata.\n\
Implemented as a no-op.\n\
\n\
@param dpda deterministic visibly pushdown automaton\n\
@return deterministic pushdown automaton equivalent to @p dpda" );

auto DeterminizeVisiblyPushdownNPDA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::VisiblyPushdownDPDA < DefaultSymbolType, ext::pair < ext::set < ext::pair < DefaultStateType, DefaultStateType > >, DefaultSymbolType >, ext::set < ext::pair < DefaultStateType, DefaultStateType > > >, const automaton::VisiblyPushdownNPDA < > & > ( automaton::determinize::Determinize::determinize, "npda" ).setDocumentation (
"Determinization of nondeterministic visibly pushdown automata.\n\
\n\
@param npda nondeterministic visibly pushdown automaton\n\
@return deterministic pushdown automaton equivalent to @p npda" );

auto DeterminizeRealTimeHeightDeterministicDPDA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::RealTimeHeightDeterministicDPDA < >, const automaton::RealTimeHeightDeterministicDPDA < > & > ( automaton::determinize::Determinize::determinize, "dpda" ).setDocumentation (
"Determinization of deterministic real-time height-deterministic pushdown automata.\n\
Implemented as a no-op.\n\
\n\
@param dpda deterministic real-time height-deterministic pushdown automaton\n\
@return deterministic pushdown automaton equivalent to @p dpda" );

auto DeterminizeRealTimeHeightDeterministicNPDA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::RealTimeHeightDeterministicDPDA < DefaultSymbolType, ext::pair < ext::set < ext::pair < DefaultStateType, DefaultStateType > >, common::symbol_or_epsilon < DefaultSymbolType > >, ext::set < ext::pair < DefaultStateType, DefaultStateType > > >, const automaton::RealTimeHeightDeterministicNPDA < > & > ( automaton::determinize::Determinize::determinize, "npda" ).setDocumentation (
"Determinization of nondeterministic real-time height-deterministic pushdown automata.\n\
\n\
@param npda nondeterministic real-time height-deterministic pushdown automaton\n\
@return deterministic pushdown automaton equivalent to @p npda" );

auto DeterminizeSinglePopDPDA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::SinglePopDPDA < >, const automaton::SinglePopDPDA < > & > ( automaton::determinize::Determinize::determinize, "dpda" ).setDocumentation (
"Determinization of deterministic single pop pushdown automata.\n\
Implemented as a no-op.\n\
\n\
@param dpda nondeterministic finite automaton with multiple initial states\n\
@return deterministic single pop deterministic pushdown automaton equivalent do @p dpda" );

auto DeterminizeDPDA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::DPDA < >, const automaton::DPDA < > & > ( automaton::determinize::Determinize::determinize, "dpda" ).setDocumentation (
"Determinization of deterministic pushdown automata.\n\
Implemented as a no-op.\n\
\n\
@param dpda deterministic pushdown automaton\n\
@return deterministic pushdown automaton equivalent to @p dpda" );

auto DeterminizeNPDA = registration::AbstractRegister < automaton::determinize::Determinize, automaton::DPDA < >, const automaton::NPDA < > & > ( automaton::determinize::Determinize::determinize, "npda" ).setDocumentation (
"Determinization of nondeterministic pushdown automata is implemented as a cast of such automaton to RhPDA.\n\
\n\
@param npda nondeterministic pushdown automaton\n\
@return nondeterministic pushdown automaton equivalent to @p npda" );

auto DeterminizeOneTapeDTM = registration::AbstractRegister < automaton::determinize::Determinize, automaton::OneTapeDTM < >, const automaton::OneTapeDTM < > & > ( automaton::determinize::Determinize::determinize, "dtm" ).setDocumentation (
"Determinization of deterministic pushdown automata.\n\
Implemented as a no-op.\n\
\n\
@param dtm deterministic one-tape turing machine\n\
@return deterministic one-tape turing machine equivalent to @p dtm" );

} /* namespace */
