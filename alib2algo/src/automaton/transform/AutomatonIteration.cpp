/*
 * AutomatonIteration.cpp
 *
 *  Created on: 29. 11. 2014
 *	  Author: Tomas Pecka
 */

#include "AutomatonIteration.h"
#include <common/createUnique.hpp>
#include <registration/AlgoRegistration.hpp>

namespace {

auto AutomatonIterationDFA = registration::AbstractRegister < automaton::transform::AutomatonIteration, automaton::NFA < >, const automaton::DFA < > & > ( automaton::transform::AutomatonIteration::iteration, "automaton" ).setDocumentation (
"Iteration of a finite automaton.\n\
\n\
@param automaton automaton to iterate\n\
@return nondeterministic FA representing the intersection of @p automaton" );

auto AutomatonIterationNFA = registration::AbstractRegister < automaton::transform::AutomatonIteration, automaton::NFA < >, const automaton::NFA < > & > ( automaton::transform::AutomatonIteration::iteration, "automaton" ).setDocumentation (
"Iteration of a finite automaton.\n\
\n\
@param automaton automaton to iterate\n\
@return nondeterministic FA representing the intersection of @p automaton" );

} /* namespace */
