/*
 * AutomatonIterationEpsilonTransition.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 29. 11. 2014
 *	  Author: Tomas Pecka
 */

#ifndef AUTOMATON_ITERATION_EPSILON_TRANSITION_H_
#define AUTOMATON_ITERATION_EPSILON_TRANSITION_H_

#include <automaton/FSM/EpsilonNFA.h>

namespace automaton {

namespace transform {

/**
 * Iteration of a finite automaton using epsilon transitions.
 * For finite automaton A1, we create automaton A such that L(A) = L(A1)*
 * This method utilizes epsilon transitions in finite automata (Melichar: Jazyky a překlady, 2.84).
 */
class AutomatonIterationEpsilonTransition {
public:
	/**
	 * Iteration of a finite automaton using epsilon transitions.
	 * @tparam T Type of the finite automaton.
	 * @param automaton automaton to iterate
	 * @return epsilon nondeterministic FA representing the intersection of @p automaton
	 */
	template < class T >
	static automaton::EpsilonNFA < typename T::SymbolType, typename T::StateType > iteration ( const T & automaton );
};

template < class T >
automaton::EpsilonNFA < typename T::SymbolType, typename T::StateType > AutomatonIterationEpsilonTransition::iteration ( const T & automaton ) {
	using SymbolType = typename T::SymbolType;
	using StateType = typename T::StateType;

	automaton::EpsilonNFA < SymbolType, StateType > res(automaton);

	for(const auto&q : automaton.getFinalStates())
		res.addTransition(q, automaton.getInitialState());

	StateType newInitialState = common::createUnique(automaton.getInitialState(), automaton.getStates());
	res.addState(newInitialState);
	res.setInitialState(newInitialState);
	res.addFinalState(newInitialState);

	res.addTransition(newInitialState, automaton.getInitialState());
	return res;
}

} /* namespace transform */

} /* namespace automaton */

#endif /* AUTOMATON_ITERATION_EPSILON_H_ */
