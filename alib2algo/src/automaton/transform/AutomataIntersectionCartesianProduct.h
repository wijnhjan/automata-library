/*
 * AutomataIntersectionCartesianProduct.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 20. 11. 2014
 *	  Author: Tomas Pecka
 */

#ifndef AUTOMATA_INTERSECTION_CARTESIAN_H_
#define AUTOMATA_INTERSECTION_CARTESIAN_H_

#include <alib/foreach>

#include <automaton/FSM/EpsilonNFA.h>
#include <automaton/TA/NFTA.h>

namespace automaton {

namespace transform {

/**
 * Intersection of two finite automata.
 * For finite automata A1, A2, we create a finite automaton A such that L(A) = L(A1) \cap L(A2).
 * This method utilizes epsilon transitions in finite automata (Melichar: Jazyky a překlady, 2.75).
 */
class AutomataIntersectionCartesianProduct {
public:
	/**
	 * Intersects two finite automata.
	 * @tparam SymbolType Type for input symbols.
	 * @tparam StateType1 Type for states in the first automaton.
	 * @tparam StateType2 Type for states in the second automaton.
	 * @param first First automaton (A1)
	 * @param second Second automaton (A2)
	 * @return (non)deterministic FA representing the intersection of two automata
	 */
	template < class SymbolType, class StateType1, class StateType2 >
	static automaton::NFA < SymbolType, ext::pair < StateType1, StateType2 > > intersection(const automaton::NFA < SymbolType, StateType1 > & first, const automaton::NFA < SymbolType, StateType2 > & second);

	/**
	 * @overload
	 */
	template < class SymbolType, class StateType1, class StateType2 >
	static automaton::DFA < SymbolType, ext::pair < StateType1, StateType2 > > intersection(const automaton::DFA < SymbolType, StateType1 > & first, const automaton::DFA < SymbolType, StateType2 > & second);

	/**
	 * @overload
	 */
	template < class SymbolType, class StateType1, class StateType2 >
	static automaton::DFTA < SymbolType, ext::pair < StateType1, StateType2 > > intersection(const automaton::DFTA < SymbolType, StateType1 > & first, const automaton::DFTA < SymbolType, StateType2 > & second);
};

template < class SymbolType, class StateType1, class StateType2 >
automaton::DFA < SymbolType, ext::pair < StateType1, StateType2 > > AutomataIntersectionCartesianProduct::intersection(const automaton::DFA < SymbolType, StateType1 > & first, const automaton::DFA < SymbolType, StateType2 > & second) {
	ext::pair < StateType1, StateType2 > q0 ( first.getInitialState ( ), second.getInitialState ( ) );
	automaton::DFA < SymbolType, ext::pair < StateType1, StateType2 > > res(q0);

	for(const auto& a : first.getInputAlphabet())
		res.addInputSymbol(a);
	for(const auto& a : second.getInputAlphabet())
		res.addInputSymbol(a);

	for(const auto& p : first.getStates())
		for(const auto& q : second.getStates())
			res.addState ( ext::make_pair ( p, q ) );

	for(const auto& p : first.getFinalStates())
		for(const auto& q : second.getFinalStates())
			res.addFinalState ( ext::make_pair ( p, q ) );

	for(const ext::pair < StateType1, StateType2 > & state : res.getStates ( ) )
		for(const auto & tp : first.getTransitionsFromState ( state.first ) )
			for(const auto & tq : second.getTransitionsFromState ( state.second ) )
				if(tp.first.second == tq.first.second)
					res.addTransition ( state, tp.first.second, ext::make_pair ( tp.second, tq.second ) );

	return res;
}

template < class SymbolType, class StateType1, class StateType2 >
automaton::NFA < SymbolType, ext::pair < StateType1, StateType2 > > AutomataIntersectionCartesianProduct::intersection(const automaton::NFA < SymbolType, StateType1 > & first, const automaton::NFA < SymbolType, StateType2 > & second) {
	ext::pair < StateType1, StateType2 > q0 ( first.getInitialState ( ), second.getInitialState ( ) );
	automaton::NFA < SymbolType, ext::pair < StateType1, StateType2 > > res ( q0 );

	for(const auto& a : first.getInputAlphabet())
		res.addInputSymbol(a);
	for(const auto& a : second.getInputAlphabet())
		res.addInputSymbol(a);

	for(const auto& p : first.getStates())
		for(const auto& q : second.getStates())
			res.addState ( ext::make_pair ( p, q ) );

	for(const auto& p : first.getFinalStates())
		for(const auto& q : second.getFinalStates())
			res.addFinalState ( ext::make_pair ( p, q ) );

	for(const ext::pair < StateType1, StateType2 > & state : res.getStates ( ) )
		for(const auto & tp : first.getTransitionsFromState ( state.first ) )
			for(const auto & tq : second.getTransitionsFromState ( state.second ) )
				if(tp.first.second == tq.first.second)
					res.addTransition ( state, tp.first.second, ext::make_pair ( tp.second, tq.second ) );

	return res;
}

template < class SymbolType, class StateType1, class StateType2 >
automaton::DFTA < SymbolType, ext::pair < StateType1, StateType2 > > AutomataIntersectionCartesianProduct::intersection(const automaton::DFTA < SymbolType, StateType1 > & first, const automaton::DFTA < SymbolType, StateType2 > & second) {
	automaton::DFTA < SymbolType, ext::pair < StateType1, StateType2 > > res;

	for(const auto& a : first.getInputAlphabet())
		res.addInputSymbol(a);
	for(const auto& a : second.getInputAlphabet())
		res.addInputSymbol(a);

	for(const auto& p : first.getStates())
		for(const auto& q : second.getStates())
			res.addState ( ext::make_pair ( p, q ) );

	for(const auto& p : first.getFinalStates())
		for(const auto& q : second.getFinalStates())
			res.addFinalState ( ext::make_pair ( p, q ) );

	for(const auto & tp : first.getTransitions ( ) )
		for(const auto & tq : second.getTransitions ( ) )
			if(tp.first.first == tq.first.first) {
				ext::vector < ext::pair < StateType1, StateType2 > > source;
				for ( ext::tuple < const StateType1 &, const StateType2 & > singleSourceState : ext::make_tuple_foreach ( tp.first.second, tq.first.second ) )
					source.push_back ( ext::make_pair ( std::get < 0 > ( singleSourceState ), std::get < 1 > ( singleSourceState ) ) );

				res.addTransition ( tp.first.first, source, ext::make_pair ( tp.second, tq.second ) );
			}

	return res;
}

} /* namespace transform */

} /* namespace automaton */

#endif /* AUTOMATA_INTERSECTION_CARTESIAN_H_ */
