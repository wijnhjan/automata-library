/*
 * MinimizeVerbose.cpp
 *
 *  Created on: Dec 9, 2013
 *	  Author: honza
 */

#include "MinimizeVerbose.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto MinimizeVerboseNFA = registration::AbstractRegister < automaton::simplify::MinimizeVerbose, ext::vector < ext::map < std::pair < DefaultStateType, DefaultStateType >, ext::map < DefaultSymbolType, DefaultStateType > > >, const automaton::DFA < > & > ( automaton::simplify::MinimizeVerbose::minimize, "dfa" ).setDocumentation (
"Minimizes deterministic finite autmaton, also reports the middle steps of the computation.\n\
\n\
@param dfa deterministic finite automaton to minimize.\n\
@return trace of minimisation of the automaton" );

} /* namespace */
