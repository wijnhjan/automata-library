/*
 * UnreachableStatesRemover.cpp
 *
 *  Created on: 23. 3. 2014
 *	  Author: Tomas Pecka
 */

#include "UnreachableStatesRemover.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto UnreachableStatesRemoverEpsilonNFA = registration::AbstractRegister < automaton::simplify::UnreachableStatesRemover, automaton::EpsilonNFA < >, const automaton::EpsilonNFA < > & > ( automaton::simplify::UnreachableStatesRemover::remove, "automaton" ).setDocumentation (
"Removes unreachable states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without unreachable states" );

auto UnreachableStatesRemoverNFA = registration::AbstractRegister < automaton::simplify::UnreachableStatesRemover, automaton::NFA < >, const automaton::NFA < > & > ( automaton::simplify::UnreachableStatesRemover::remove, "automaton" ).setDocumentation (
"Removes unreachable states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without unreachable states" );

auto UnreachableStatesRemoverCompactNFA = registration::AbstractRegister < automaton::simplify::UnreachableStatesRemover, automaton::CompactNFA < >, const automaton::CompactNFA < > & > ( automaton::simplify::UnreachableStatesRemover::remove, "automaton" ).setDocumentation (
"Removes unreachable states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without unreachable states" );

auto UnreachableStatesRemoverExtendedNFA = registration::AbstractRegister < automaton::simplify::UnreachableStatesRemover, automaton::ExtendedNFA < >, const automaton::ExtendedNFA < > & > ( automaton::simplify::UnreachableStatesRemover::remove, "automaton" ).setDocumentation (
"Removes unreachable states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without unreachable states" );

auto UnreachableStatesRemoverDFA = registration::AbstractRegister < automaton::simplify::UnreachableStatesRemover, automaton::DFA < >, const automaton::DFA < > & > ( automaton::simplify::UnreachableStatesRemover::remove, "automaton" ).setDocumentation (
"Removes unreachable states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without unreachable states" );

auto UnreachableStatesRemoverMultiInitialStateNFA = registration::AbstractRegister < automaton::simplify::UnreachableStatesRemover, automaton::MultiInitialStateNFA < >, const automaton::MultiInitialStateNFA < > & > ( automaton::simplify::UnreachableStatesRemover::remove, "automaton" ).setDocumentation (
"Removes unreachable states from an automaton.\n\
\n\
@param automaton automaton to trim\n\
@return @p automaton without unreachable states" );

auto UnreachableStatesRemoverDFTA = registration::AbstractRegister < automaton::simplify::UnreachableStatesRemover, automaton::DFTA < >, const automaton::DFTA < > & > ( automaton::simplify::UnreachableStatesRemover::remove, "fta" ).setDocumentation (
"Removes unreachable states from a deterministic finite tree automaton.\n\
\n\
@param fta automaton to trim\n\
@return @p autoamton without unreachable states" );

auto UnreachableStatesRemoverNFTA = registration::AbstractRegister < automaton::simplify::UnreachableStatesRemover, automaton::NFTA < >, const automaton::NFTA < > & > ( automaton::simplify::UnreachableStatesRemover::remove, "fta" ).setDocumentation (
"Removes unreachable states from a deterministic finite tree automaton.\n\
\n\
@param fta automaton to trim\n\
@return @p autoamton without unreachable states" );

} /* namespace */
