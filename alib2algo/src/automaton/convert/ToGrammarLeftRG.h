/*
 * ToGrammarLeftRG.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *  Created on: 9. 2. 2014
 *      Author: Tomas Pecka
 */

#ifndef __TO_GRAMMAR_LEFT_RG_H__
#define __TO_GRAMMAR_LEFT_RG_H__

#include <grammar/Regular/LeftRG.h>
#include <automaton/FSM/NFA.h>
#include <automaton/FSM/DFA.h>

#include <alib/map>
#include <common/createUnique.hpp>

#include <alphabet/InitialSymbol.h>

namespace automaton {

namespace convert {

/**
 * Converts a finite automaton to a left regular grammar.
 */
class ToGrammarLeftRG {
public:
	/**
	 * Performs the conversion of the finite automaton to left regular grammar.
	 *
	 * \tparam T the converted automaton
	 *
	 * \param automaton a finite automaton to convert
	 *
	 * \return left regular grammar equivalent to the source @p automaton.
	 */
	template < class T >
	static ext::require < isDFA < T > || isNFA < T >, grammar::LeftRG < typename T::SymbolType, typename T::StateType > > convert ( const T & automaton );

};

template < class T >
ext::require < isDFA < T > || isNFA < T >, grammar::LeftRG < typename T::SymbolType, typename T::StateType > > ToGrammarLeftRG::convert ( const T & automaton ) {
	using SymbolType = typename T::SymbolType;
	using StateType = typename T::StateType;

	// step 2
	grammar::LeftRG < SymbolType, StateType > grammar(alphabet::InitialSymbol::instance < StateType > ( ) );

	// step 1
	grammar.setTerminalAlphabet ( automaton.getInputAlphabet ( ) );

	for ( const auto & state : automaton.getStates ( ) ) {
		grammar.addNonterminalSymbol ( state );
	}

	// step 3 - create set of P in G
	for ( const auto & transition : automaton.getTransitions ( ) ) {
		const StateType & from = transition.first.first;
		const SymbolType & input = transition.first.second;
		const StateType & to = transition.second;

		// 3a
		grammar.addRule ( to, ext::make_pair ( from, input ) );

		if ( automaton.getFinalStates ( ).count ( to ) > 0 )
			grammar.addRule ( grammar.getInitialSymbol ( ), ext::make_pair ( from, input ) );


		if ( automaton.getInitialState ( ) == from ) {
			grammar.addRule ( to, input );

			if ( automaton.getFinalStates ( ).count ( to ) > 0 )
				grammar.addRule ( grammar.getInitialSymbol ( ), input );
		}
	}

	if ( automaton.getFinalStates ( ).count ( automaton.getInitialState ( ) ) > 0 )
		grammar.setGeneratesEpsilon ( true );

	return grammar;
}

} /* namespace convert */

} /* namespace automaton */

#endif /* __TO_GRAMMAR_LEFT_RG_H__ */
