/*
 * ForwardBisimulation.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: 27. 3. 2019
 *	  Author: Tomas Pecka
 */

#ifndef FORWARD_BISIMULATION_H_
#define FORWARD_BISIMULATION_H_

#include <alib/set>
#include <alib/map>

#include <automaton/TA/DFTA.h>
#include <automaton/TA/NFTA.h>
#include <automaton/TA/UnorderedNFTA.h>

#include <automaton/FSM/DFA.h>
#include <automaton/FSM/NFA.h>

namespace automaton {

namespace properties {

/**
 * Find all forwardBisimulation pairs of states of DFA.
 * Implements table-filling algorithm, Hopcroft 2nd edition, 4.4.1
 */
class ForwardBisimulation {
	template < class StateType >
	static ext::set < ext::pair < StateType, StateType > > initial ( const ext::set < StateType > & states, const ext::set < StateType > & finals ) {
		ext::set < ext::pair < StateType, StateType > > init;

		for ( const StateType & a : states ) {
			for ( const StateType & b : states ) {
				if ( finals.count ( a ) == finals.count ( b ) ) {
					init.insert ( ext::make_pair ( a, b ) );
					init.insert ( ext::make_pair ( b, a ) );
				}
			}
		}

		return init;
	}

public:

	/**
	 * Computes a relation on states of the DFA satisfying the forward bisimulation definition.
	 *
	 * @param fta the examined automaton
	 *
	 * @return set of pairs of states of the @p fta that are the forward bisimulation.
	 */
	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > forwardBisimulation ( const automaton::DFA < SymbolType, StateType > & fta );

	/**
	 * Computes a relation on states of the NFA satisfying the forward bisimulation definition.
	 *
	 * @param fta the examined automaton
	 *
	 * @return set of pairs of states of the @p fta that are the forward bisimulation.
	 */
	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > forwardBisimulation ( const automaton::NFA < SymbolType, StateType > & fta );

	/**
	 * Computes a relation on states of the DFTA satisfying the forward bisimulation definition.
	 *
	 * @param fta the examined automaton
	 *
	 * @return set of pairs of states of the @p fta that are the forward bisimulation.
	 */
	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > forwardBisimulation ( const automaton::DFTA < SymbolType, StateType > & fta );

	/**
	 * Computes a relation on states of the NFTA satisfying the forward bisimulation definition.
	 *
	 * @param fta the examined automaton
	 *
	 * @return set of pairs of states of the @p fta that are the forward bisimulation.
	 */
	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > forwardBisimulation ( const automaton::NFTA < SymbolType, StateType > & fta );

	/**
	 * Computes a relation on states of the DFTA satisfying the forward bisimulation definition.
	 *
	 * @param fta the examined automaton
	 *
	 * @return set of pairs of states of the @p fta that are the forward bisimulation.
	 */
	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > forwardBisimulation ( const automaton::UnorderedDFTA < SymbolType, StateType > & fta );

	/**
	 * Computes a relation on states of the NFTA satisfying the forward bisimulation definition.
	 *
	 * @param fta the examined automaton
	 *
	 * @return set of pairs of states of the @p fta that are the forward bisimulation.
	 */
	template < class SymbolType, class StateType >
	static ext::set < ext::pair < StateType, StateType > > forwardBisimulation ( const automaton::UnorderedNFTA < SymbolType, StateType > & fta );
};

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > ForwardBisimulation::forwardBisimulation ( const automaton::DFA < SymbolType, StateType > & fta ) {
	return forwardBisimulation ( automaton::NFA < SymbolType, StateType > ( fta ) );
}

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > ForwardBisimulation::forwardBisimulation ( const automaton::NFA < SymbolType, StateType > & fta ) {
	ext::set < ext::pair < StateType, StateType > > forwardBisimulation = initial ( fta.getStates ( ), fta.getFinalStates ( ) );

	bool changed;
	do {
		changed = false;

		for ( const StateType & p : fta.getStates ( ) ) {
			for ( const StateType & q : fta.getStates ( ) ) {
				if ( ! forwardBisimulation.contains ( ext::make_pair ( p, q ) ) )
					continue;

				for ( const std::pair < const ext::pair < StateType, SymbolType >, StateType > & pTransition : fta.getTransitions ( ) ) {
					if ( pTransition.first.first == p ) {
						bool exists = false;
						for ( const std::pair < const ext::pair < StateType, SymbolType >, StateType > & qTransition : fta.getTransitions ( ).equal_range ( std::make_pair ( q, pTransition.first.second ) ) ) {
							if ( forwardBisimulation.contains ( ext::make_pair ( pTransition.second, qTransition.second ) ) ) {
								exists = true;
								break;
							}
						}

						if ( ! exists ) {
							forwardBisimulation.erase ( ext::make_pair ( p, q ) );
							forwardBisimulation.erase ( ext::make_pair ( q, p ) );
							changed = true;
						}
					}
				}
			}
		}
	} while ( changed );

	return forwardBisimulation;
}

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > ForwardBisimulation::forwardBisimulation ( const automaton::DFTA < SymbolType, StateType > & fta ) {
	return forwardBisimulation ( automaton::NFTA < SymbolType, StateType > ( fta ) );
}

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > ForwardBisimulation::forwardBisimulation ( const automaton::NFTA < SymbolType, StateType > & fta ) {
	ext::set < ext::pair < StateType, StateType > > forwardBisimulation = initial ( fta.getStates ( ), fta.getFinalStates ( ) );

	bool changed;
	do {
		changed = false;

		for ( const StateType & p : fta.getStates ( ) ) {
			for ( const StateType & q : fta.getStates ( ) ) {
				if ( ! forwardBisimulation.contains ( ext::make_pair ( p, q ) ) )
					continue;

				for ( const std::pair < const ext::pair < common::ranked_symbol < SymbolType >, ext::vector < StateType > >, StateType > & pTransition : fta.getTransitions ( ) ) {
					for ( size_t i = 0; i < pTransition.first.second.size ( ); ++ i ) {
						if ( pTransition.first.second [ i ] == p ) {
							ext::vector < StateType > copy = pTransition.first.second;
							copy [ i ] = q;

							bool exists = false;
							for ( const std::pair < const ext::pair < common::ranked_symbol < SymbolType >, ext::vector < StateType > >, StateType > & qTransition : fta.getTransitions ( ).equal_range ( std::make_pair ( pTransition.first.first, std::move ( copy ) ) ) ) {
								if ( forwardBisimulation.contains ( ext::make_pair ( pTransition.second, qTransition.second ) ) ) {
									exists = true;
									break;
								}
							}

							if ( ! exists ) {
								forwardBisimulation.erase ( ext::make_pair ( p, q ) );
								forwardBisimulation.erase ( ext::make_pair ( q, p ) );
								changed = true;
							}
						}
					}
				}
			}
		}
	} while ( changed );

	return forwardBisimulation;
}

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > ForwardBisimulation::forwardBisimulation ( const automaton::UnorderedDFTA < SymbolType, StateType > & fta ) {
	return forwardBisimulation ( automaton::UnorderedNFTA < SymbolType, StateType > ( fta ) );
}

template < class SymbolType, class StateType >
ext::set < ext::pair < StateType, StateType > > ForwardBisimulation::forwardBisimulation ( const automaton::UnorderedNFTA < SymbolType, StateType > & fta ) {
	ext::set < ext::pair < StateType, StateType > > forwardBisimulation = initial ( fta.getStates ( ), fta.getFinalStates ( ) );

	bool changed;
	do {
		changed = false;

		for ( const StateType & p : fta.getStates ( ) ) {
			for ( const StateType & q : fta.getStates ( ) ) {
				if ( ! forwardBisimulation.contains ( ext::make_pair ( p, q ) ) )
					continue;

				for ( const std::pair < const ext::pair < common::ranked_symbol < SymbolType >, ext::multiset < StateType > >, StateType > & pTransition : fta.getTransitions ( ) ) {
					for ( const StateType & state : pTransition.first.second ) {
						if ( state == p ) {
							ext::multiset < StateType > copy = pTransition.first.second;
							copy.erase ( copy.find ( state ) );
							copy.insert ( q );

							bool exists = false;
							for ( const std::pair < const ext::pair < common::ranked_symbol < SymbolType >, ext::multiset < StateType > >, StateType > & qTransition : fta.getTransitions ( ).equal_range ( std::make_pair ( pTransition.first.first, std::move ( copy ) ) ) ) {
								if ( forwardBisimulation.contains ( ext::make_pair ( pTransition.second, qTransition.second ) ) ) {
									exists = true;
									break;
								}
							}

							if ( ! exists ) {
								forwardBisimulation.erase ( ext::make_pair ( p, q ) );
								forwardBisimulation.erase ( ext::make_pair ( q, p ) );
								changed = true;
							}
						}
					}
				}
			}
		}
	} while ( changed );

	return forwardBisimulation;
}

} /* namespace properties */

} /* namespace automaton */

#endif /* FORWARD_BISIMULATION_H_ */
