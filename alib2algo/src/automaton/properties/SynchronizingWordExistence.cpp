/*
 * SynchronizingWordExistence.cpp
 *
 *  Created on: 8. 3. 2020
 *	  Author: Tomas Pecka
 */

#include "SynchronizingWordExistence.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto SynchronizingWordExistenceDFA = registration::AbstractRegister < automaton::properties::SynchronizingWordExistence, bool, const automaton::DFA < > & > ( automaton::properties::SynchronizingWordExistence::exists, "dfa" ).setDocumentation (
"Checks whether the given DFA has a synchronizing word.\n\
\n\
@param dfa automaton\n\
@return boolean indicating whether the automaton has a synchronizing word" );

} /* namespace */
