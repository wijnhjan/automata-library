/*
 * InfiniteLanguage.cpp
 *
 *  Created on: 26. 3. 2019
 *	  Author: Tomas Pecka
 */

#include "InfiniteLanguage.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto InfiniteLanguageNFA = registration::AbstractRegister < automaton::properties::InfiniteLanguage, bool, const automaton::NFA < > & > ( automaton::properties::InfiniteLanguage::infinite, "fsm" ).setDocumentation (
"Determines whether input automaton accepts infinite language.\n\
\n\
@param fsm automaton\n\
@return bool" );

auto InfiniteLanguageDFA = registration::AbstractRegister < automaton::properties::InfiniteLanguage, bool, const automaton::DFA < > & > ( automaton::properties::InfiniteLanguage::infinite, "fsm" ).setDocumentation (
"Determines whether input automaton accepts infinite language.\n\
\n\
@param fsm automaton\n\
@return bool" );

} /* namespace */
