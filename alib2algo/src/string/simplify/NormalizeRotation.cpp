/*
 * NormalizeRotation.cpp
 *
 *  Created on: 9. 2. 2014
 *      Author: Radomir Polach
 */

#include "NormalizeRotation.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto NormalizeRotationCyclicString = registration::AbstractRegister < string::simplify::NormalizeRotation, string::CyclicString < >, const string::CyclicString < > & > ( string::simplify::NormalizeRotation::normalize, "string" ).setDocumentation (
"Computes lexicographically least circular rotation,\n\
based on Kellogg S. Booth 1979,\n\
modified to working code by Ladislav Vagner\n\
\n\
@param string the rotated string\n\
@return the @p string in its least lexicographical rotation" );

} /* namespace */
