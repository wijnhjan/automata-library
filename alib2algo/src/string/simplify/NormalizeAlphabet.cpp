/*
 * NormalizeAlphabet.cpp
 *
 *  Created on: Dec 9, 2013
 *	Author: Jan Travnicek
 */

#include "NormalizeAlphabet.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto NormalizeAlphabetLinearString = registration::AbstractRegister < string::simplify::NormalizeAlphabet, string::LinearString < std::string >, const string::LinearString < > & > ( string::simplify::NormalizeAlphabet::normalize, "str" ).setDocumentation (
"Applies a homomorphism asigning characters 'a', 'b', ... to the first, second, ... occurring symbol in the string content.\n\
\n\
@param str the input string\n\
@return the normalized string" );

} /* namespace */
