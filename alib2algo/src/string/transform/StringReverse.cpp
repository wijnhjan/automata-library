/*
 * StringReverse.cpp
 *
 *  Created on: 17. 10. 2014
 *	  Author: Jan Travnicek
 */

#include "StringReverse.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto LinearStringReverse = registration::AbstractRegister < string::transform::StringReverse, string::LinearString < >, const string::LinearString < > & > ( string::transform::StringReverse::concatenate, "arg" ).setDocumentation (
"Implements the reverse of a string.\n\
\n\
@param arg the string to concatenate\n\
@return string arg ^ R" );

} /* namespace */
