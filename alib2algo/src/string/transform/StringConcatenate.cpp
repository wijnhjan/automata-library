/*
 * StringConcatenate.cpp
 *
 *  Created on: 17. 10. 2014
 *	  Author: Jan Travnicek
 */

#include "StringConcatenate.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto LinearStringConcatenate = registration::AbstractRegister < string::transform::StringConcatenate, string::LinearString < >, const string::LinearString < > &, const string::LinearString < > & > ( string::transform::StringConcatenate::concatenate, "first", "second" ).setDocumentation (
"Implements concatenation of two strings.\n\
\n\
@param first the first string to concatenate\n\
@param second the second string to concatenate\n\
@return string first . second" );

} /* namespace */
