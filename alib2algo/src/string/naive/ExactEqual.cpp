/*
 * ExactEqual.cpp
 *
 *  Created on: Oct 9, 2014
 *	  Author: Radomir Polach */

#include "ExactEqual.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactEqualLinearString = registration::AbstractRegister < string::naive::ExactEqual, bool, const string::LinearString < > &, const string::LinearString < > & > ( string::naive::ExactEqual::equals, "u", "v" ).setDocumentation (
"Implements exact equality test of string contents.\n\
\n\
@param u the first string to compare\n\
@param v the second string to compare\n\
@return true if strings are equal, false othervise" );

auto ExactEqualCyclicString = registration::AbstractRegister < string::naive::ExactEqual, bool, const string::CyclicString < > &, const string::CyclicString < > & > ( string::naive::ExactEqual::equals, "u", "v" ).setDocumentation (
"Implements exact equality test of string contens. The algorithm handles rotations of strings.\n\
\n\
@param u the first string to compare\n\
@param v the second string to compare\n\
@return true if strings are equal in some rotation, false othervise" );

} /* namespace */
