/*
 * BorderArray.cpp
 *
 *  Created on: 1. 11. 2014
 *      Author: Tomas Pecka
 */

#include "BorderArray.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto BorderArrayLinearString = registration::AbstractRegister < string::properties::BorderArray, ext::vector < size_t >, const string::LinearString < > & > ( string::properties::BorderArray::construct );

} /* namespace */
