/*
 * ReversedBadCharacterShiftTable.cpp
 *
 *  Created on: 5. 11. 2014
 *      Author: Jan Travnicek
 */

#include "ReversedBadCharacterShiftTable.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ReversedBadCharacterShiftTableLinearString = registration::AbstractRegister < string::properties::ReversedBadCharacterShiftTable, ext::map < DefaultSymbolType, size_t >, const string::LinearString < > & > ( string::properties::ReversedBadCharacterShiftTable::bcs );

} /* namespace */
