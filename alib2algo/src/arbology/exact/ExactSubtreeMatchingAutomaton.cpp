/*
 * ExactSubtreeMatchingAutomaton.cpp
 *
 *  Created on: 9. 2. 2014
 *      Author: Jan Travnicek
 */

#include "ExactSubtreeMatchingAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactSubtreeMatchingAutomatonPrefixRankedTree = registration::AbstractRegister < arbology::exact::ExactSubtreeMatchingAutomaton, automaton::InputDrivenNPDA < common::ranked_symbol < DefaultSymbolType >, char, unsigned >, const tree::PrefixRankedTree < > & > ( arbology::exact::ExactSubtreeMatchingAutomaton::construct );

auto ExactSubtreeMatchingAutomatonPrefixRankedBarTree = registration::AbstractRegister < arbology::exact::ExactSubtreeMatchingAutomaton, automaton::InputDrivenNPDA < common::ranked_symbol < DefaultSymbolType >, char, unsigned >, const tree::PrefixRankedBarTree < > & > ( arbology::exact::ExactSubtreeMatchingAutomaton::construct );

auto ExactSubtreeMatchingAutomatonRankedTree = registration::AbstractRegister < arbology::exact::ExactSubtreeMatchingAutomaton, automaton::NFTA < DefaultSymbolType, unsigned >, const tree::RankedTree < > & > ( arbology::exact::ExactSubtreeMatchingAutomaton::construct );

auto ExactSubtreeMatchingAutomatonUnorderedRankedTree = registration::AbstractRegister < arbology::exact::ExactSubtreeMatchingAutomaton, automaton::UnorderedNFTA < DefaultSymbolType, unsigned >, const tree::UnorderedRankedTree < > & > ( arbology::exact::ExactSubtreeMatchingAutomaton::construct );

} /* namespace */
