/*
 * ExactSubtreeAutomaton.cpp
 *
 *  Created on: 7. 4. 2015
 *      Author: Jan Travnicek
 */

#include "ExactSubtreeAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactSubtreeAutomatonPrefixRankedTree = registration::AbstractRegister < arbology::exact::ExactSubtreeAutomaton, automaton::InputDrivenNPDA < common::ranked_symbol < DefaultSymbolType >, char, unsigned >, const tree::PrefixRankedTree < > & > ( arbology::exact::ExactSubtreeAutomaton::construct );

} /* namespace */
