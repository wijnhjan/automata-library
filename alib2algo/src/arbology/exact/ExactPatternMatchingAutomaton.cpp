/*
 * ExactPatternMatchingAutomaton.cpp
 *
 *  Created on: 9. 2. 2014
 *      Author: Jan Travnicek
 */

#include "ExactPatternMatchingAutomaton.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto ExactPatternMatchingAutomatonPrefixRankedTree = registration::AbstractRegister < arbology::exact::ExactPatternMatchingAutomaton, automaton::InputDrivenNPDA < common::ranked_symbol < DefaultSymbolType >, char, unsigned >, const tree::PrefixRankedTree < > & > ( arbology::exact::ExactPatternMatchingAutomaton::construct );

auto ExactPatternMatchingAutomatonPrefixRankedPattern = registration::AbstractRegister < arbology::exact::ExactPatternMatchingAutomaton, automaton::NPDA < common::ranked_symbol < DefaultSymbolType >, char, unsigned >, const tree::PrefixRankedPattern < > & > ( arbology::exact::ExactPatternMatchingAutomaton::construct );

auto ExactPatternMatchingAutomatonPrefixRankedBarTree = registration::AbstractRegister < arbology::exact::ExactPatternMatchingAutomaton, automaton::InputDrivenNPDA < common::ranked_symbol < DefaultSymbolType >, char, unsigned >, const tree::PrefixRankedBarTree < > & > ( arbology::exact::ExactPatternMatchingAutomaton::construct );

auto ExactPatternMatchingAutomatonPrefixRankedBarPattern = registration::AbstractRegister < arbology::exact::ExactPatternMatchingAutomaton, automaton::VisiblyPushdownNPDA < common::ranked_symbol < DefaultSymbolType >, char, unsigned >, const tree::PrefixRankedBarPattern < > & > ( arbology::exact::ExactPatternMatchingAutomaton::construct );

auto ExactPatternMatchingAutomatonRankedTree = registration::AbstractRegister < arbology::exact::ExactPatternMatchingAutomaton, automaton::NFTA < DefaultSymbolType, unsigned >, const tree::RankedTree < > & > ( arbology::exact::ExactPatternMatchingAutomaton::construct );

auto ExactPatternMatchingAutomatonRankedPattern = registration::AbstractRegister < arbology::exact::ExactPatternMatchingAutomaton, automaton::NFTA < DefaultSymbolType, unsigned >, const tree::RankedPattern < > & > ( arbology::exact::ExactPatternMatchingAutomaton::construct );

auto ExactPatternMatchingAutomatonUnorderedRankedTree = registration::AbstractRegister < arbology::exact::ExactPatternMatchingAutomaton, automaton::UnorderedNFTA < DefaultSymbolType, unsigned >, const tree::UnorderedRankedTree < > & > ( arbology::exact::ExactPatternMatchingAutomaton::construct );

auto ExactPatternMatchingAutomatonUnorderedRankedPattern = registration::AbstractRegister < arbology::exact::ExactPatternMatchingAutomaton, automaton::UnorderedNFTA < DefaultSymbolType, unsigned >, const tree::UnorderedRankedPattern < > & > ( arbology::exact::ExactPatternMatchingAutomaton::construct );

} /* namespace */
