/*
 * NonlinearCompressedBitParallelIndexConstruction.cpp
 *
 *  Created on: 22. 8. 2017
 *      Author: Jan Travnicek
 */

#include "NonlinearCompressedBitParallelIndexConstruction.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto nonlinearcompressedBitParallelIndexConstructionPrefixRankedBarTree = registration::AbstractRegister < arbology::indexing::NonlinearCompressedBitParallelIndexConstruction, indexes::arbology::NonlinearCompressedBitParallelTreeIndex < >, const tree::PrefixRankedBarTree < > & > ( arbology::indexing::NonlinearCompressedBitParallelIndexConstruction::construct );

} /* namespace */
