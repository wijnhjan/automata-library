/*
 * FullAndLinearIndexConstruction.cpp
 *
 *  Created on: 6. 2. 2017
 *      Author: Jan Travnicek
 */

#include "FullAndLinearIndexConstruction.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto fullAndLinearIndexConstructionPrefixRankedTree = registration::AbstractRegister < arbology::indexing::FullAndLinearIndexConstruction, indexes::arbology::FullAndLinearIndex < DefaultSymbolType >, const tree::PrefixRankedTree < > & > ( arbology::indexing::FullAndLinearIndexConstruction::construct );

auto fullAndLinearIndexConstructionPrefixRankedBarTree = registration::AbstractRegister < arbology::indexing::FullAndLinearIndexConstruction, indexes::arbology::FullAndLinearIndex < DefaultSymbolType >, const tree::PrefixRankedBarTree < > & > ( arbology::indexing::FullAndLinearIndexConstruction::construct );

} /* namespace */
