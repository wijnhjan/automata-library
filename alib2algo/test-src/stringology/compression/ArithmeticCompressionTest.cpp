#include <catch2/catch.hpp>

#include <stringology/compression/ArithmeticCompression.h>
#include <stringology/compression/ArithmeticDecompression.h>

TEST_CASE ( "Arithmetic Compression", "[unit][algo][stringology][compression]" ) {
	SECTION ( "basics" ) {
		ext::string rawInput ( "abbabbabaae123456789r0 8723 babababb  ab bapobababbbabaaabbafjfjdjlvldsuiueqwpomvdhgataewpvdihviasubababbba 5475 baaabba" );
		string::LinearString < char > input ( rawInput );

		ext::vector < bool > compressed = stringology::compression::AdaptiveIntegerArithmeticCompression::compress ( input );
		INFO ( "compressed = " << compressed );
		string::LinearString < char > output = stringology::compression::AdaptiveIntegerArithmeticDecompression::decompress ( compressed, input.getAlphabet ( ) );

		INFO ( "original= " << input << " decompressed = " << output );
		INFO ( "compressed size = " << compressed.size ( ) << " original_size = " << input.getContent ( ).size ( ) * 8 );
		CHECK( input == output );
	}
}

