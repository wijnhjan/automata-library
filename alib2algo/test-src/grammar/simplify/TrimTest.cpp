#include <catch2/catch.hpp>

#include "grammar/simplify/Trim.h"
#include "grammar/Regular/RightRG.h"

TEST_CASE ( "Trim Grammar", "[unit][algo][grammar][simplify]" ) {
	SECTION ( "Test CFG" ) {

		grammar::RightRG < > rrGrammar(DefaultSymbolType(1));

		rrGrammar.addNonterminalSymbol(DefaultSymbolType(1));
		rrGrammar.addNonterminalSymbol(DefaultSymbolType(2));
		rrGrammar.addNonterminalSymbol(DefaultSymbolType(3));
		rrGrammar.addNonterminalSymbol(DefaultSymbolType(4));
		rrGrammar.addNonterminalSymbol(DefaultSymbolType(5));
		rrGrammar.addNonterminalSymbol(DefaultSymbolType(6));
		rrGrammar.addTerminalSymbol(DefaultSymbolType("a"));
		rrGrammar.addTerminalSymbol(DefaultSymbolType("b"));

		rrGrammar.addRule(DefaultSymbolType(1), ext::make_pair(DefaultSymbolType("a"), DefaultSymbolType(2)));
		rrGrammar.addRule(DefaultSymbolType(2), ext::make_pair(DefaultSymbolType("b"), DefaultSymbolType(3)));
		rrGrammar.addRule(DefaultSymbolType(3), DefaultSymbolType("a"));

		rrGrammar.addRule(DefaultSymbolType(4), ext::make_pair(DefaultSymbolType("b"), DefaultSymbolType(5)));
		rrGrammar.addRule(DefaultSymbolType(5), DefaultSymbolType("a"));
		rrGrammar.addRule(DefaultSymbolType(5), ext::make_pair(DefaultSymbolType("b"), DefaultSymbolType(2)));
		rrGrammar.addRule(DefaultSymbolType(6), ext::make_pair(DefaultSymbolType("b"), DefaultSymbolType(6)));

		grammar::RightRG < > trimed = grammar::simplify::Trim::trim(rrGrammar);

		CHECK(trimed.getNonterminalAlphabet().size() == 3);
	}
}

