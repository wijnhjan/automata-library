#include <catch2/catch.hpp>

#include <alib/list>

#include "automaton/simplify/Normalize.h"

TEST_CASE ( "Normalize", "[unit][algo][automaton][simplify]" ) {
	SECTION ( "DFA" ) {
		automaton::DFA < > automaton(DefaultStateType(0));

		automaton.addState(DefaultStateType(0));
		automaton.addState(DefaultStateType(1));
		automaton.addState(DefaultStateType(2));
		automaton.addInputSymbol(DefaultSymbolType("a"));
		automaton.addInputSymbol(DefaultSymbolType("b"));

		automaton.addTransition(DefaultStateType(0), DefaultSymbolType("a"), DefaultStateType(1));
		automaton.addTransition(DefaultStateType(1), DefaultSymbolType("b"), DefaultStateType(2));

		automaton.addFinalState(DefaultStateType(2));

		automaton::DFA < DefaultSymbolType, unsigned > normalized = automaton::simplify::Normalize::normalize(automaton);

		CHECK(normalized.getStates().size() == automaton.getStates().size());
		CHECK(normalized.getTransitions().size() == automaton.getTransitions().size());

		automaton::DFA < DefaultSymbolType, unsigned > reference(0);

		reference.addState(0);
		reference.addState(1);
		reference.addState(2);
		reference.addInputSymbol(DefaultSymbolType("a"));
		reference.addInputSymbol(DefaultSymbolType("b"));

		reference.addTransition(0, DefaultSymbolType("a"), 1);
		reference.addTransition(1, DefaultSymbolType("b"), 2);

		reference.addFinalState(2);

		CHECK ( normalized == reference );
	}
}
