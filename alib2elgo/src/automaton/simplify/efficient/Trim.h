/*
 * Trim.h
 *
 *  Created on: 23. 3. 2014
 *	  Author: Tomas Pecka
 */

#ifndef EFFICIENT_AUTOMATON_TRIM_H_
#define EFFICIENT_AUTOMATON_TRIM_H_

#include <algorithm>
#include <deque>
#include <set>

#include "UselessStatesRemover.h"
#include "UnreachableStatesRemover.h"

namespace automaton {

namespace simplify {

namespace efficient {

class Trim {
public:
	/**
	 * Removes dead states from FSM. Melichar 2.29
	 */
	template<class T>
	static T trim( const T & fsm );
};

template<class T>
T Trim::trim( const T & fsm ) {
	return UselessStatesRemover::remove ( UnreachableStatesRemover::remove( fsm ) );
}

} /* namespace efficient */

} /* namespace simplify */

} /* namespace automaton */

#endif /* EFFICIENT_AUTOMATON_TRIM_H_ */
