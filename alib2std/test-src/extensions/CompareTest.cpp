#include <catch2/catch.hpp>

#include <alib/set>
#include <alib/vector>
#include <alib/pair>
#include <alib/map>
#include <alib/tuple>

#include <alib/compare>

TEST_CASE ( "Compare Test", "[unit][std][bits]" ) {
	SECTION ( "Compare" ) {
		ext::vector<int> vector1 {1, 2, 3};
		ext::vector<int> vector2 {2, 3, 4};

		ext::compare<ext::vector<int>> comp1;
		CHECK(comp1(vector1, vector2) < 0);

		ext::set<ext::vector<int>> set1 {vector1};
		ext::set<ext::vector<int>> set2 {vector2};

		ext::compare<ext::set<ext::vector<int>>> comp2;
		CHECK(comp2(set1, set2) < 0);

		ext::pair<ext::set<ext::vector<int>>, ext::set<ext::vector<int>>> pair1 {set1, set2};
		ext::pair<ext::set<ext::vector<int>>, ext::set<ext::vector<int>>> pair2 {set2, set1};

		ext::compare<ext::pair<ext::set<ext::vector<int>>, ext::set<ext::vector<int>>>> comp3;
		CHECK(comp3(pair1, pair2) < 0);

		int two = 2;
		int three = 3;
		int four = 4;

		ext::map<int, const int*> map1 = { {1, &two}, {2, &three} };
		ext::map<int, const int*> map2 = { {2, &three}, {3, &four} };

		ext::compare<ext::map<int, const int*>> comp4;
		CHECK(comp4(map1, map2) < 0);

		auto first = ext::tie(vector1, set1, pair1, map1);
		auto second = ext::tie(vector2, set2, pair2, map2);

		ext::compare<decltype ( first ) > comp5;
		CHECK(comp5(first, second) < 0);
	}
}

TEST_CASE ( "Dry Comparison Test", "[unit][std][bits]" ) {
	SECTION ( "Dry Compare each other" ) {
		CHECK ( ext::none_of { 4, 5, 3 } <= ext::any_of ( 1, 2 ) );
	}
}
