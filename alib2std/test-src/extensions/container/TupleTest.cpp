#include <catch2/catch.hpp>
#include <alib/tuple>
#include <sstream>

TEST_CASE ( "Tuple", "[unit][std][container]" ) {
	SECTION ( "Test call on nth" ) {
		ext::tuple < int, int, int, int, int > t = ext::make_tuple ( 1, 2, 3, 4, 5 );

		std::stringstream ss;
		ss << t;
		CAPTURE ( ss.str ( ) );
		CHECK ( ss.str ( ) == "(1, 2, 3, 4, 5)" );
	}
}

