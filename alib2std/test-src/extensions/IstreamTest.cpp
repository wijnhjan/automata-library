#include <catch2/catch.hpp>

#include <alib/set>
#include <alib/vector>
#include <alib/pair>
#include <sstream>
#include <alib/istream>
#include <alib/string>

TEST_CASE ( "Istream", "[unit][std][bits]" ) {
	SECTION ( "istream" ) {
		std::stringstream ss ( "TEST" );

		CHECK ( ( ( bool ) ( ss >> ext::string ( "TEST" ) ) ) == true );

		ss.str ( "TEST" );

		CHECK ( ( ( bool ) ( ss >> ext::string ( "TESS" ) ) ) == false );
		CHECK ( ss.str ( ) == "TEST" );


		CHECK ( ( ( bool ) ( ss >> ext::string ( "TESTS" ) ) ) == false );
		CHECK ( ss.str ( ) == "TEST" );
	}
}
