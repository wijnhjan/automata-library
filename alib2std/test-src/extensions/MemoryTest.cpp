#include <catch2/catch.hpp>

#include <alib/compare>
#include <alib/memory>

TEST_CASE ( "Memory", "[unit][std][bits]" ) {
	SECTION ( "Compare Pointers" ) {
		std::shared_ptr < int > i = std::make_shared < int > ( 1 );
		std::shared_ptr < int > j = i;
		std::shared_ptr < int > k = std::make_shared < int > ( 1 );
		std::shared_ptr < int > l;

		ext::compare < std::shared_ptr < int > > comp;

		CHECK ( comp ( i, i ) == 0 );
		CHECK ( comp ( i, j ) == 0 );
		CHECK ( comp ( i, k ) == 0 );
		CHECK ( comp ( i, l ) > 0 );
		CHECK ( comp ( l, i ) < 0 );
	}
}
