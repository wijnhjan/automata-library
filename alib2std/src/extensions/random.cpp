/*
 * random.cpp
 *
 * Created on: Apr 1, 2013
 * Author: Jan Travnicek
 */

#include "random.hpp"

namespace ext {

	std::random_device & random_devices::random = getRandom ( );
	random_devices::semirandom_device & random_devices::semirandom = getSemirandom ( );

} /* namespace ext */
