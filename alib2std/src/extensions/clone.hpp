/*
 * clone.hpp
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Created on: Apr 1, 2013
 * Author: Jan Travnicek
 */

#ifndef __CLONE_HPP_
#define __CLONE_HPP_

#include "type_traits.hpp"

namespace ext {

/**
 * \brief
 * Wrapper around clone by means of using copy constructor or clone method if available.
 *
 * \param tmp universal reference to a value to be cloned.
 *
 * \tparam T the type of cloned value
 *
 * \return the cloned value dynamically allocated
 */
template < class T >
auto clone ( T && tmp ) {
	if constexpr ( ext::has_clone < T >::value && ! std::is_final_v < T > )
		return std::forward < T > ( tmp ).clone ( );
	else
		return new typename std::decay < T >::type ( std::forward < T > ( tmp ) );
}

} /* namespace ext */

#endif /* __CLONE_HPP_ */
