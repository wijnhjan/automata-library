/*
 * tuple.hpp
 *
 * Created on: Apr 1, 2013
 * Author: Jan Travnicek
 */

#ifndef __TUPLE_COMMON_HPP_
#define __TUPLE_COMMON_HPP_

#include <tuple>
#include <utility>

namespace ext {

template < class Tuple, class Callable, std::size_t ... Is >
static void foreach_fn ( Tuple && t, Callable callback, std::index_sequence < Is ... > ) {
	( callback ( std::forward < std::tuple_element_t < Is, std::remove_reference_t < Tuple > > > ( t.template get < Is > ( ) ) ), ... );
}

template < class Tuple, class Callable >
void foreach ( Tuple && t, Callable callback ) {
	return foreach_fn ( std::forward < Tuple > ( t ), callback, std::make_index_sequence < std::tuple_size_v < std::remove_reference_t < Tuple > > > ( ) );
}

template < class Tuple, std::size_t ... Is >
static inline int compare_help ( const Tuple & t1, const Tuple & t2, std::index_sequence < Is ... > ) {
	int res = 0;

	auto compare_fn = [] ( const auto & first, const auto & second ) {
		static compare < std::decay_t < decltype ( first ) > > comp;
		return comp ( first, second );
	};

	( ( res = res == 0 ? compare_fn ( t1.template get < Is > ( ), t2.template get < Is > ( ) ) : res ), ... );
	return res;
}

} /* namespace ext */

#endif /* __TUPLE_COMMON_HPP_ */
