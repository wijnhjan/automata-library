/**
 * optional.hpp
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 * https://gist.github.com/tibordp/6909880
 *
 * Created on: May 16, 2019
 * Created: Jan Travnicek
 */

#ifndef __OPTIONAL_HPP_
#define __OPTIONAL_HPP_

#include <optional>
#include <sstream>

#include <extensions/compare.hpp>

namespace ext {

template < typename T >
class optional : public std::optional < T > {
public:
	/**
	 * Inherit constructors of the standard optional
	 */
	using std::optional< T >::optional; // NOLINT(modernize-use-equals-default)

	/**
	 * Inherit operator = of the standard optional
	 */
	using std::optional< T >::operator =;
#ifndef __clang__

	/**
	 * Default constructor needed by g++ since it is not inherited
	 */
	optional ( ) = default;

	/**
	 * Copy constructor needed by g++ since it is not inherited
	 */
	optional ( const optional & other ) = default;

	/**
	 * Move constructor needed by g++ since it is not inherited
	 */
	optional ( optional && other ) = default;

	/**
	 * Copy operator = needed by g++ since it is not inherited
	 */
	optional & operator = ( optional && other ) = default;

	/**
	 * Move operator = needed by g++ since it is not inherited
	 */
	optional & operator = ( const optional & other ) = default;
#endif
};

/**
 * \brief
 * Specialisation of the compare structure implementing the three-way comparison.
 *
 * \tparam T type in the optional
 */
template < typename T >
struct compare < ext::optional < T > > {

	/**
	 * \brief
	 * Implementation of the three-way comparison.
	 *
	 * \param first the left operand of the comparison
	 * \param second the right operand of the comparison
	 *
	 * \return negative value of left < right, positive value if left > right, zero if left == right
	 */
	int operator ( ) ( const ext::optional < T > & first, const ext::optional < T > & second ) const {
		if ( ! first && ! second )
			return 0;
		if ( first && ! second )
			return 1;
		if ( ! first && second )
			return -1;

		static ext::compare < T > comp;
		return comp ( first.value ( ), second.value ( ) );
	}
};

/**
 * \brief
 * Operator to print the optional to the output stream.
 *
 * \param out the output stream
 * \param optional the optional to print
 *
 * \tparam T the type of value inside the optional
 *
 * \return the output stream from the \p out
 */
template< class T >
std::ostream & operator << ( std::ostream & out, const ext::optional < T > & optional ) {
	if ( ! optional )
		return out << "void";
	else
		return out << optional.value ( );
}


/**
 * \brief
 * Overload of to_string function.
 *
 * \param value the optional to be converted to string
 *
 * \tparam T the type of values inside the optional
 *
 * \return string representation
 */
template < class T >
std::string to_string ( const ext::optional < T > & value ) {
	std::stringstream ss;
	ss << value;
	return ss.str();
}

} /* namespace ext */

#endif /* __OPTIONAL_HPP_ */
