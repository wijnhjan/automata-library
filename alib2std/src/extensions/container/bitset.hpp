/*
 * bitset.hpp
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Created on: May 04, 2017
 * Author: Jan Travnicek
 */

#ifndef __BITSET_HPP_
#define __BITSET_HPP_

#include <bitset>
#include <ostream>
#include <sstream>
#include <string>

#include <extensions/compare.hpp>

namespace ext {

/**
 * \brief
 * Class extending the bitset class from the standard library. Original reason is to allow printing of the container with overloaded operator <<.
 *
 * The class mimics the behavior of the bitset from the standatd library.
 *
 * \tparam N the size of the bitset
 */
template < std::size_t N >
class bitset : public std::bitset < N > {
public:
	/**
	 * Inherit constructors of the standard bitset
	 */
	using std::bitset < N >::bitset; // NOLINT(modernize-use-equals-default)

	/**
	 * Inherit operator = of the standard bitset
	 */
	using std::bitset < N >::operator =;
#ifndef __clang__

	/**
	 * Default constructor needed by g++ since it is not inherited
	 */
	bitset ( ) = default;

	/**
	 * Copy constructor needed by g++ since it is not inherited
	 */
	bitset ( const bitset & other ) = default;

	/**
	 * Move constructor needed by g++ since it is not inherited
	 */
	bitset ( bitset && other ) = default;

	/**
	 * Copy operator = needed by g++ since it is not inherited
	 */
	bitset & operator = ( bitset && other ) = default;

	/**
	 * Move operator = needed by g++ since it is not inherited
	 */
	bitset & operator = ( const bitset & other ) = default;
#endif
};

/**
 * \brief
 * Operator to print the bitset to the output stream.
 *
 * \param out the output stream
 * \param bitset the bitset to print
 *
 * \tparam N the size of the bitset
 *
 * \return the output stream from the \p out
 */
template < size_t N >
std::ostream & operator << ( std::ostream & out, const ext::bitset < N > & bitset ) {
	out << "[";

	for ( size_t i = 0; i < N; ++i ) {
		if ( i != 0 ) out << ", ";
		out << bitset [ i ];
	}

	out << "]";
	return out;
}

/**
 * \brief
 * Specialisation of the compare structure implementing the three-way comparison.
 *
 * \tparam N the size of the bitset
 */
template < size_t N >
struct compare < ext::bitset < N > > {

	/**
	 * \brief
	 * Implementation of the three-way comparison.
	 *
	 * \param first the left operand of the comparison
	 * \param second the right operand of the comparison
	 *
	 * \return negative value of left < right, positive value if left > right, zero if left == right
	 */
	int operator ( ) ( const ext::bitset < N > & first, const ext::bitset < N > & second ) const {
		for ( size_t i = 0; i < N; ++i ) {
			int res = first [ i ] != second [ i ];
			if ( res != 0 )
				return res;
		}
		return 0;
	}
};

/**
 * \brief
 * Overload of to_string function.
 *
 * \param value the bitset to be converted to string
 *
 * \tparam N the size of the bitset
 *
 * \return string representation
 */
template < size_t N >
std::string to_string ( const ext::bitset < N > & value ) {
	std::stringstream ss;
	ss << value;
	return ss.str ( );
}

} /* namespace ext */

#endif /* __BITSET_HPP_ */
