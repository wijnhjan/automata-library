/*
 * typeindex.cpp
 *
 * Created on: Jul 4, 2017
 * Author: Jan Travnicek
 */

#include "typeindex.h"

#include <cstdlib>
#include <cxxabi.h>

namespace ext {

std::ostream & operator << ( std::ostream & os, const ext::type_index & type ) {
	os << ext::to_string ( type );
	return os;
}

std::string to_string ( const ext::type_index & type ) {
	int status;

	char* demangled = abi::__cxa_demangle(type.name(), nullptr, nullptr, &status);
	std::string res ( demangled );
	free ( demangled );
	return res;
}

} /* namespace ext */
