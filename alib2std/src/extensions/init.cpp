/*
 * iostream.cpp
 *
 * Created on: Feb 12, 2016
 * Author: Radovan Cerveny
 */

#include "init.h"
#include "random.hpp"

namespace ext {

ofdstream cmeasure ( CMEASURE_FD, CERR_FD );

Init::Init ( ) : clog_fdaccessor ( CLOG_FD, CERR_FD ), clog_fdstreambuf ( clog_fdaccessor.get_fd ( ) ) {
	std::clog.rdbuf ( & clog_fdstreambuf );
	std::clog.clear ( );

	ext::random_devices::getSemirandom ( ).seed ( ext::random_devices::getRandom ( ) ( ) );
}

Init::~Init ( ) {
	std::clog.flush ( );
}

Init relinkio;

} /* namespace ext */
