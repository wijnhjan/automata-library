/*
 * NonlinearVariableSymbol.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "NonlinearVariableSymbol.h"
#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::NonlinearVariableSymbol < > > ( );

} /* namespace */
