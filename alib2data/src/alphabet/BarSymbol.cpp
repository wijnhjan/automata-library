/*
 * BarSymbol.h
 *
 *  Created on: Apr 10, 2013
 *      Author: Stepan Plachy
 */

#include "BarSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

BarSymbol::BarSymbol() = default;

int BarSymbol::compare(const BarSymbol&) const {
	return 0;
}

std::ostream & operator << ( std::ostream & out, const BarSymbol & ) {
	return out << "(Bar symbol)";
}

BarSymbol::operator std::string () const {
	return BarSymbol::instance < std::string > ( );
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::BarSymbol > ( );

} /* namespace */
