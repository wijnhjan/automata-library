/*
 * InitialSymbol.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travnicek
 */

#ifndef INITIAL_SYMBOL_LABEL_H_
#define INITIAL_SYMBOL_LABEL_H_

#include <alib/compare>
#include <object/Object.h>
#include <common/ranked_symbol.hpp>

namespace alphabet {

/**
 * \brief
 * Represents initial symbol used as an initial symbol of a grammar.
 */
class InitialSymbol : public ext::CompareOperators < InitialSymbol > {
public:
	/**
	 * \brief
	 * Creates a new instance of the symbol.
	 */
	explicit InitialSymbol ( );

	/**
	 * The actual compare method
	 *
	 * \param other the other instance
	 *
	 * \returns the actual relation between two by type same symbols
	 */
	int compare ( const InitialSymbol & other ) const;

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param os ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend std::ostream & operator << ( std::ostream & out, const InitialSymbol & instance );

	/**
	 * Casts this instance to as compact as possible string representation.
	 *
	 * \returns string representation of the object
	 */
	explicit operator std::string ( ) const;

	/**
	 * \brief Factory for the symbol construction of the symbol based on given type.
	 */
	template < typename Base >
	static inline typename std::enable_if <   std::is_integral < Base >::value, Base >::type instance ( );

	/**
	 * \brief Factory for the symbol construction of the symbol based on given type.
	 */
	template < typename Base >
	static inline typename std::enable_if < ! std::is_integral < Base >::value, Base >::type instance ( );
};

template < typename Base >
inline typename std::enable_if < std::is_integral < Base >::value, Base >::type InitialSymbol::instance ( ) {
	return 0;
}

template < >
inline object::Object InitialSymbol::instance < object::Object > ( ) {
	return object::Object ( InitialSymbol ( ) );
}

template < >
inline std::string InitialSymbol::instance < std::string > ( ) {
	return std::string ( "S" );
}

template < >
inline InitialSymbol InitialSymbol::instance < InitialSymbol > ( ) {
	return InitialSymbol ( );
}

template < >
inline common::ranked_symbol < > InitialSymbol::instance < common::ranked_symbol < > > ( ) {
	return common::ranked_symbol < > ( InitialSymbol::instance < DefaultSymbolType > ( ), 0u );
}

} /* namespace alphabet */

#endif /* INITIAL_SYMBOL_LABEL_H_ */
