/*
 * EndSymbol.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Jun 19, 2014
 *      Author: Jan Travnicek
 */

#ifndef END_SYMBOL_H_
#define END_SYMBOL_H_

#include <alib/compare>
#include <object/Object.h>
#include <common/ranked_symbol.hpp>

namespace alphabet {

/**
 * \brief
 * Represents end symbol used as termation symbol of a string.
 */
class EndSymbol : public ext::CompareOperators < EndSymbol > {
public:
	/**
	 * \brief
	 * Creates a new instance of the symbol.
	 */
	explicit EndSymbol ( );

	/**
	 * The actual compare method
	 *
	 * \param other the other instance
	 *
	 * \returns the actual relation between two by type same symbols
	 */
	int compare ( const EndSymbol & other ) const;

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param os ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend std::ostream & operator << ( std::ostream & out, const EndSymbol & instance );

	/**
	 * Casts this instance to as compact as possible string representation.
	 *
	 * \returns string representation of the object
	 */
	explicit operator std::string ( ) const;

	/**
	 * \brief Factory for the symbol construction of the symbol based on given type.
	 */
	template < typename Base >
	static inline typename std::enable_if <   std::is_integral < Base >::value, Base >::type instance ( );

	/**
	 * \brief Factory for the symbol construction of the symbol based on given type.
	 */
	template < typename Base >
	static inline typename std::enable_if < ! std::is_integral < Base >::value, Base >::type instance ( );
};

template < typename Base >
inline typename std::enable_if < std::is_integral < Base >::value, Base >::type EndSymbol::instance ( ) {
	return '$';
}

template < >
inline object::Object EndSymbol::instance < object::Object > ( ) {
	return object::Object ( EndSymbol ( ) );
}

template < >
inline std::string EndSymbol::instance < std::string > ( ) {
	return std::string ( 1, EndSymbol::instance < char > ( ) );
}

template < >
inline EndSymbol EndSymbol::instance < EndSymbol > ( ) {
	return EndSymbol ( );
}

template < >
inline common::ranked_symbol < > EndSymbol::instance < common::ranked_symbol < > > ( ) {
	return common::ranked_symbol < > ( EndSymbol::instance < DefaultSymbolType > ( ), 0u );
}

} /* namespace alphabet */

#endif /* END_SYMBOL_H_ */
