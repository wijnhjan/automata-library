/*
 * EndSymbol.h
 *
 *  Created on: Jun 19, 2014
 *      Author: Jan Travnicek
 */

#ifndef _XML_END_SYMBOL_H_
#define _XML_END_SYMBOL_H_

#include <alphabet/EndSymbol.h>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < alphabet::EndSymbol > {
	static alphabet::EndSymbol parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const alphabet::EndSymbol & data );
};

} /* namespace core */

#endif /* _XML_END_SYMBOL_H_ */
