/*
 * BarSymbol.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Apr 10, 2013
 *      Author: Stepan Plachy
 */

#ifndef BAR_SYMBOL_H_
#define BAR_SYMBOL_H_

#include <alib/compare>
#include <object/Object.h>
#include <common/ranked_symbol.hpp>

namespace alphabet {

/**
 * \brief
 * Represents bar symbol used in tree linearization.
 */
class BarSymbol : public ext::CompareOperators < BarSymbol > {
public:
	/**
	 * \brief
	 * Creates a new instance of the symbol.
	 */
	explicit BarSymbol ( );

	/**
	 * The actual compare method
	 *
	 * \param other the other instance
	 *
	 * \returns the actual relation between two by type same symbols
	 */
	int compare ( const BarSymbol & other ) const;

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param os ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend std::ostream & operator << ( std::ostream & out, const BarSymbol & instnace );

	/**
	 * Casts this instance to as compact as possible string representation.
	 *
	 * \returns string representation of the object
	 */
	explicit operator std::string ( ) const;

	/**
	 * \brief Factory for the symbol construction of the symbol based on given type.
	 */
	template < typename Base >
	static inline typename std::enable_if <   std::is_integral < Base >::value, Base >::type instance ( );

	/**
	 * \brief Factory for the symbol construction of the symbol based on given type.
	 */
	template < typename Base >
	static inline typename std::enable_if < ! std::is_integral < Base >::value, Base >::type instance ( );
};

template < typename Base >
inline typename std::enable_if < std::is_integral < Base >::value, Base >::type BarSymbol::instance ( ) {
	return '|';
}

template < >
inline std::string BarSymbol::instance < std::string > ( ) {
	return std::string ( 1, BarSymbol::instance < char > ( ) );
}

template < >
inline BarSymbol BarSymbol::instance < BarSymbol > ( ) {
	return BarSymbol ( );
}

template < >
inline object::Object BarSymbol::instance < object::Object > ( ) {
	return object::Object ( BarSymbol ( ) );
}

template < >
inline common::ranked_symbol < > BarSymbol::instance < common::ranked_symbol < > > ( ) {
	return common::ranked_symbol < > ( BarSymbol::instance < DefaultSymbolType > ( ), 0u );
}

} /* namespace alphabet */

#endif /* BAR_SYMBOL_H_ */
