/*
 * StartSymbol.cpp
 *
 *  Created on: Jun 19, 2014
 *      Author: Jan Travnicek
 */

#include "StartSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

StartSymbol::StartSymbol() = default;

int StartSymbol::compare(const StartSymbol&) const {
	return 0;
}

std::ostream & operator << ( std::ostream & out, const StartSymbol & ) {
	return out << "(StartSymbol)";
}

StartSymbol::operator std::string ( ) const {
	return StartSymbol::instance < std::string > ( );
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::StartSymbol > ( );

} /* namespace */
