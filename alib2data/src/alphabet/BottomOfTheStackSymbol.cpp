/*
 * BottomOfTheStackSymbol.cpp
 *
 *  Created on: Jun 19, 2014
 *      Author: Jan Travnicek
 */

#include "BottomOfTheStackSymbol.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace alphabet {

BottomOfTheStackSymbol::BottomOfTheStackSymbol() = default;

int BottomOfTheStackSymbol::compare(const BottomOfTheStackSymbol&) const {
	return 0;
}

std::ostream & operator << ( std::ostream & out, const BottomOfTheStackSymbol & ) {
	return out << "(BottomOfTheStackSymbol)";
}

BottomOfTheStackSymbol::operator std::string ( ) const {
	return BottomOfTheStackSymbol::instance < std::string > ( );
}

} /* namespace alphabet */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < alphabet::BottomOfTheStackSymbol > ( );

} /* namespace */
