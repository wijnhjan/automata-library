/*
 * Tree.h
 *
 *  Created on: Nov 16, 2014
 *      Author: Stepan Plachy
 */

#ifndef TREE_H_
#define TREE_H_

namespace tree {

/**
 * Wrapper around tree.
 */
class Tree;

} /* namespace tree */

#endif /* TREE_H_ */
