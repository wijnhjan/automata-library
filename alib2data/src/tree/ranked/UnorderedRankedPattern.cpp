/*
 * UnorderedRankedPattern.cpp
 *
 *  Created on: Nov 30, 2019
 *      Author: Jan Travnicek
 */

#include "UnorderedRankedPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::UnorderedRankedPattern < >;

namespace {

auto components = registration::ComponentRegister < tree::UnorderedRankedPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::UnorderedRankedPattern < > > ( );

auto UnorderedRankedPatternFromRankedPattern = registration::CastRegister < tree::UnorderedRankedPattern < >, tree::RankedPattern < > > ( );

} /* namespace */
