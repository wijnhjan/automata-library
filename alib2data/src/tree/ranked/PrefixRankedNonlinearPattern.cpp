/*
 * PrefixRankedNonlinearPattern.cpp
 *
 *  Created on: Jul 20, 2016
 *      Author: Jan Travnicek
 */

#include "PrefixRankedNonlinearPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::PrefixRankedNonlinearPattern < >;

namespace {

auto components = registration::ComponentRegister < tree::PrefixRankedNonlinearPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::PrefixRankedNonlinearPattern < > > ( );

auto PrefixRankedNonlinearPatternFromRankedTree = registration::CastRegister < tree::PrefixRankedNonlinearPattern < >, tree::RankedTree < > > ( );
auto PrefixRankedNonlinearPatternFromRankedPattern = registration::CastRegister < tree::PrefixRankedNonlinearPattern < >, tree::RankedPattern < > > ( );
auto PrefixRankedNonlinearPatternFromRankedNonlinearPattern = registration::CastRegister < tree::PrefixRankedNonlinearPattern < >, tree::RankedNonlinearPattern < > > ( );
auto PrefixRankedNonlinearPatternFromPrefixRankedTree = registration::CastRegister < tree::PrefixRankedNonlinearPattern < >, tree::PrefixRankedTree < > > ( );
auto PrefixRankedNonlinearPatternFromPrefixRankedPattern = registration::CastRegister < tree::PrefixRankedNonlinearPattern < >, tree::PrefixRankedPattern < > > ( );

auto LinearStringFromPrefixRankedNonlinearPattern = registration::CastRegister < string::LinearString < common::ranked_symbol < > >, tree::PrefixRankedNonlinearPattern < > > ( );

} /* namespace */
