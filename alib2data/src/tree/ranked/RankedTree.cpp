/*
 * RankedTree.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: Stepan Plachy
 */

#include "RankedTree.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::RankedTree < >;

namespace {

auto components = registration::ComponentRegister < tree::RankedTree < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::RankedTree < > > ( );

auto RankedTreeFromUnrankedTree = registration::CastRegister < tree::RankedTree < >, tree::UnrankedTree < > > ( );
auto RankedTreeFromPostfixRankedTree = registration::CastRegister < tree::RankedTree < >, tree::PostfixRankedTree < > > ( );
auto RankedTreeFromPrefixRankedTree = registration::CastRegister < tree::RankedTree < >, tree::PrefixRankedTree < > > ( );

} /* namespace */
