/*
 * PrefixRankedPattern.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#include "PrefixRankedPattern.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>
#include <registration/ComponentRegistration.hpp>

template class tree::PrefixRankedPattern < >;

namespace {

auto components = registration::ComponentRegister < tree::PrefixRankedPattern < > > ( );

auto valuePrinter = registration::ValuePrinterRegister < tree::PrefixRankedPattern < > > ( );

auto PrefixRankedPatternFromRankedPattern = registration::CastRegister < tree::PrefixRankedPattern < >, tree::RankedPattern < > > ( );
auto PrefixRankedPatternFromPrefixRankedTree = registration::CastRegister < tree::PrefixRankedPattern < >, tree::PrefixRankedTree < > > ( );

auto LinearStringFromPrefixRankedPattern = registration::CastRegister < string::LinearString < common::ranked_symbol < > >, tree::PrefixRankedPattern < > > ( );

} /* namespace */
