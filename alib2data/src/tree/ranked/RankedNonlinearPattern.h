/*
 * RankedNonlinearPattern.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#ifndef RANKED_NONLINEAR_PATTERN_H_
#define RANKED_NONLINEAR_PATTERN_H_

#include <common/DefaultSymbolType.h>

namespace tree {

template < class SymbolType = DefaultSymbolType >
class RankedNonlinearPattern;

} /* namespace tree */

#include <sstream>

#include <alib/string>
#include <alib/set>
#include <alib/tree>
#include <alib/iostream>
#include <alib/algorithm>

#include <core/components.hpp>
#include <common/ranked_symbol.hpp>

#include <tree/TreeException.h>
#include <tree/common/TreeAuxiliary.h>

#include <core/normalize.hpp>
#include <tree/common/TreeNormalize.h>

#include "../unranked/UnrankedNonlinearPattern.h"

namespace tree {

class GeneralAlphabet;
class SubtreeWildcard;
class NonlinearAlphabet;

/**
 * \brief
 * Nonlinear tree pattern represented in its natural representation. The representation is so called ranked, therefore it consists of ranked symbols. The rank of the ranked symbol needs to be compatible with unsigned integer. Additionally the pattern contains a special wildcard symbol representing any subtree and nonlinear variables each to represent same subtree (in the particular occurrence in a tree).
 *
 * \details
 * T = (A, C, W \in ( A \minus B ), V \in ( A \minus B) ),
 * A (Alphabet) = finite set of ranked symbols,
 * C (Content) = pattern in its natural representation
 * W (Wildcard) = special symbol representing any subtree
 * V (Variables) = finite set of special symbols each representing same subtree
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template < class SymbolType >
class RankedNonlinearPattern final : public ext::CompareOperators < RankedNonlinearPattern < SymbolType > >, public core::Components < RankedNonlinearPattern < SymbolType >, ext::set < common::ranked_symbol < SymbolType > >, component::Set, std::tuple < GeneralAlphabet, NonlinearAlphabet >, common::ranked_symbol < SymbolType >, component::Value, SubtreeWildcard > {
	/**
	 * Natural representation of the pattern content.
	 */
	ext::tree < common::ranked_symbol < SymbolType > > m_content;

	/**
	 * Checks that symbols of the pattern are present in the alphabet.
	 *
	 * \throws TreeException when some symbols of the pattern representation are not present in the alphabet
	 *
	 * \param data the pattern in its natural representation
	 */
	void checkAlphabet ( const ext::tree < common::ranked_symbol < SymbolType > > & data ) const;

	/**
	 * Checks that the rank of each symbol of a pattern node corresponds to the number of child nodes of that same node.
	 *
	 * \throws TreeException when some nodes of a pattern have different number of children than the rank of their label states
	 *
	 * \param data the pattern in its natural representation
	 */
	void checkArities ( const ext::tree < common::ranked_symbol < SymbolType > > & data ) const;

public:
	/**
	 * \brief Creates a new instance of the pattern with concrete alphabet, content, wildcard, and nonlinear variables.
	 *
	 * \param subtreeWildcard the wildcard symbol
	 * \param nonlinearVariables the set of nonlinear variables
	 * \param alphabet the initial alphabet of the pattern
	 * \param pattern the initial content in it's natural representation
	 */
	explicit RankedNonlinearPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::set < common::ranked_symbol < SymbolType > > nonlinearVariables, ext::set < common::ranked_symbol < SymbolType > > alphabet, ext::tree < common::ranked_symbol < SymbolType > > pattern );

	/**
	 * \brief Creates a new instance of the pattern with concrete content, wildcard, and nonlinear variables. The alphabet is deduced from the content.
	 *
	 * \param subtreeWildcard the wildcard symbol
	 * \param nonlinearVariables the set of nonlinear variables
	 * \param pattern the initial content in it's natural representation
	 */
	explicit RankedNonlinearPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::set < common::ranked_symbol < SymbolType > > nonlinearVariables, ext::tree < common::ranked_symbol < SymbolType > > pattern );

	/**
	 * \brief Creates a new instance of the pattern with concrete content and wildcard. The alphabet is deduced from the content. Nonlinear variables are defaultly constructed to empty set.
	 *
	 * \param subtreeWildcard the wildcard symbol
	 * \param pattern the initial content in it's natural representation
	 */
	explicit RankedNonlinearPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::tree < common::ranked_symbol < SymbolType > > pattern );

	/**
	 * \brief Creates a new instance of the pattern based on UnrankedNonlinearPattern, the alphabet is created from the content of the UnrankedNonlinearPattern. The alphabet is created since ranks cannot be deduced.
	 *
	 * \param other the pattern represented as UnrankedNonlinearPattern
	 */
	explicit RankedNonlinearPattern ( const UnrankedNonlinearPattern < SymbolType > & other );

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the pattern
	 */
	const ext::set < common::ranked_symbol < SymbolType > > & getAlphabet ( ) const & {
		return this->template accessComponent < GeneralAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the pattern
	 */
	ext::set < common::ranked_symbol < SymbolType > > && getAlphabet ( ) && {
		return std::move ( this->template accessComponent < GeneralAlphabet > ( ).get ( ) );
	}

	/**
	 * Adder of an alphabet symbols.
	 *
	 * \param symbols the new symbols to be added to the alphabet
	 */
	void extendAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & symbols ) {
		this->template accessComponent < GeneralAlphabet > ( ).add ( symbols );
	}

	/**
	 * Getter of the wildcard symbol.
	 *
	 * \returns the wildcard symbol of the pattern
	 */
	const common::ranked_symbol < SymbolType > & getSubtreeWildcard ( ) const & {
		return this->template accessComponent < SubtreeWildcard > ( ).get ( );
	}

	/**
	 * Getter of the wildcard symbol.
	 *
	 * \returns the wildcard symbol of the pattern
	 */
	common::ranked_symbol < SymbolType > && getSubtreeWildcard ( ) && {
		return std::move ( this->template accessComponent < SubtreeWildcard > ( ).get ( ) );
	}

	/**
	 * Getter of the nonlinear variables.
	 *
	 * \returns the nonlinear variables of the pattern
	 */
	const ext::set < common::ranked_symbol < SymbolType > > & getNonlinearVariables ( ) const & {
		return this->template accessComponent < NonlinearAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the nonlinear variables.
	 *
	 * \returns the nonlinear variables of the pattern
	 */
	ext::set < common::ranked_symbol < SymbolType > > && getNonlinearVariables ( ) && {
		return std::move ( this->template accessComponent < NonlinearAlphabet > ( ).get ( ) );
	}

	/**
	 * Getter of the pattern representation.
	 *
	 * \return the natural representation of the pattern.
	 */
	const ext::tree < common::ranked_symbol < SymbolType > > & getContent ( ) const &;

	/**
	 * Getter of the pattern representation.
	 *
	 * \return the natural representation of the pattern.
	 */
	ext::tree < common::ranked_symbol < SymbolType > > && getContent ( ) &&;

	/**
	 * Setter of the representation of the pattern.
	 *
	 * \throws TreeException in same situations as checkAlphabet and checkArities
	 *
	 * \param pattern new representation of the pattern.
	 */
	void setTree ( ext::tree < common::ranked_symbol < SymbolType > > pattern );

	/**
	 * The actual compare method
	 *
	 * \param other the other instance
	 *
	 * \returns the actual relation between two by type same pattern instances
	 */
	int compare ( const RankedNonlinearPattern & other ) const;

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param out ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend std::ostream & operator << ( std::ostream & out, const RankedNonlinearPattern & instance ) {
		out << "(RankedNonlinearPattern ";
		out << "alphabet = " << instance.getAlphabet ( );
		out << "content = " << instance.getContent ( );
		out << "nonlinearVariables = " << instance.getNonlinearVariables ( );
		out << "subtreeWildcard = " << instance.getSubtreeWildcard ( );
		out << ")";
		return out;
	}

	/**
	 * Casts this instance to as compact as possible string representation.
	 *
	 * \returns string representation of the object
	 */
	explicit operator std::string ( ) const;

	/**
	 * Nice printer of the tree natural representation
	 *
	 * \param os the output stream to print to
	 */
	void nicePrint ( std::ostream & os ) const;
};

template < class SymbolType >
RankedNonlinearPattern < SymbolType >::RankedNonlinearPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::set < common::ranked_symbol < SymbolType > > nonlinearVariables, ext::set < common::ranked_symbol < SymbolType > > alphabet, ext::tree < common::ranked_symbol < SymbolType > > pattern ) : core::Components < RankedNonlinearPattern, ext::set < common::ranked_symbol < SymbolType > >, component::Set, std::tuple < GeneralAlphabet, NonlinearAlphabet >, common::ranked_symbol < SymbolType >, component::Value, SubtreeWildcard > ( std::move ( alphabet ), std::move ( nonlinearVariables ), std::move ( subtreeWildcard ) ), m_content ( std::move ( pattern ) ) {
	checkAlphabet ( m_content );
	checkArities ( m_content );
}

template < class SymbolType >
RankedNonlinearPattern < SymbolType >::RankedNonlinearPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::set < common::ranked_symbol < SymbolType > > nonlinearVariables, ext::tree < common::ranked_symbol < SymbolType > > pattern ) : RankedNonlinearPattern ( subtreeWildcard, nonlinearVariables, ext::set < common::ranked_symbol < SymbolType > > ( pattern.prefix_begin ( ), pattern.prefix_end ( ) ) + nonlinearVariables + ext::set < common::ranked_symbol < SymbolType > > { subtreeWildcard }, pattern ) {
}

template < class SymbolType >
RankedNonlinearPattern < SymbolType >::RankedNonlinearPattern ( common::ranked_symbol < SymbolType > subtreeWildcard, ext::tree < common::ranked_symbol < SymbolType > > pattern ) : RankedNonlinearPattern ( subtreeWildcard, ext::set < common::ranked_symbol < SymbolType > > { }, pattern ) {
}

template < class SymbolType >
RankedNonlinearPattern < SymbolType >::RankedNonlinearPattern ( const UnrankedNonlinearPattern < SymbolType > & other ) : RankedNonlinearPattern ( common::ranked_symbol < SymbolType > ( other.getSubtreeWildcard ( ), 0 ), TreeAuxiliary::unrankedToRanked < SymbolType > ( other.getContent ( ) ) ) {
}

template < class SymbolType >
const ext::tree < common::ranked_symbol < SymbolType > > & RankedNonlinearPattern < SymbolType >::getContent ( ) const & {
	return m_content;
}

template < class SymbolType >
ext::tree < common::ranked_symbol < SymbolType > > && RankedNonlinearPattern < SymbolType >::getContent ( ) && {
	return std::move ( m_content );
}

template < class SymbolType >
void RankedNonlinearPattern < SymbolType >::checkAlphabet ( const ext::tree < common::ranked_symbol < SymbolType > > & data ) const {
	ext::set < common::ranked_symbol < SymbolType > > minimalAlphabet ( data.prefix_begin ( ), data.prefix_end ( ) );
	ext::set < common::ranked_symbol < SymbolType > > unknownSymbols;
	std::set_difference ( minimalAlphabet.begin ( ), minimalAlphabet.end ( ), getAlphabet().begin ( ), getAlphabet().end ( ), std::inserter ( unknownSymbols, unknownSymbols.end ( ) ) );

	if ( !unknownSymbols.empty ( ) )
		throw exception::CommonException ( "Input symbols not in the alphabet." );
}

template < class SymbolType >
void RankedNonlinearPattern < SymbolType >::checkArities ( const ext::tree < common::ranked_symbol < SymbolType > > & data ) const {
	if ( ( size_t ) data.getData ( ).getRank ( ) != data.getChildren ( ).size ( ) )
		throw exception::CommonException ( "Invalid rank." );

	for ( const ext::tree < common::ranked_symbol < SymbolType > > & child : data )
		checkArities ( child );
}

template < class SymbolType >
void RankedNonlinearPattern < SymbolType >::setTree ( ext::tree < common::ranked_symbol < SymbolType > > pattern ) {
	checkAlphabet ( pattern );
	checkArities ( pattern );

	this->m_content = std::move ( pattern );
}

template < class SymbolType >
int RankedNonlinearPattern < SymbolType >::compare ( const RankedNonlinearPattern & other ) const {
	auto first = ext::tie ( m_content, getAlphabet(), getSubtreeWildcard(), getNonlinearVariables() );
	auto second = ext::tie ( other.m_content, other.getAlphabet(), other.getSubtreeWildcard(), getNonlinearVariables() );

	static ext::compare < decltype ( first ) > comp;

	return comp ( first, second );
}

template < class SymbolType >
void RankedNonlinearPattern < SymbolType >::nicePrint ( std::ostream & os ) const {
	m_content.nicePrint ( os );
}

template < class SymbolType >
RankedNonlinearPattern < SymbolType >::operator std::string ( ) const {
	std::stringstream ss;
	ss << * this;
	return ss.str ( );
}

} /* namespace tree */

namespace core {

/**
 * Helper class specifying constraints for the pattern's internal alphabet component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template < class SymbolType >
class SetConstraint< tree::RankedNonlinearPattern < SymbolType >, common::ranked_symbol < SymbolType >, tree::GeneralAlphabet > {
public:
	/**
	 * Returns true if the symbol is still used in the pattern.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const tree::RankedNonlinearPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol ) {
		const ext::tree < common::ranked_symbol < SymbolType > > & m_content = pattern.getContent ( );
		return std::find(m_content.prefix_begin(), m_content.prefix_end(), symbol) != m_content.prefix_end() || pattern.template accessComponent < tree::SubtreeWildcard > ( ).get ( ) == symbol || pattern.template accessComponent < tree::NonlinearAlphabet > ( ).get ( ).count ( symbol );
	}

	/**
	 * Returns true as all symbols are possibly available to be in an alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const tree::RankedNonlinearPattern < SymbolType > &, const common::ranked_symbol < SymbolType > & ) {
		return true;
	}

	/**
	 * All symbols are valid as symbols of an alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 */
	static void valid ( const tree::RankedNonlinearPattern < SymbolType > &, const common::ranked_symbol < SymbolType > & ) {
	}
};

/**
 * Helper class specifying constraints for the pattern's internal nonlinear variables component.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template < class SymbolType >
class SetConstraint< tree::RankedNonlinearPattern < SymbolType >, common::ranked_symbol < SymbolType >, tree::NonlinearAlphabet > {
public:
	/**
	 * Returns false. Nonlinear symbol is only a mark that the pattern itself does require further.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const tree::RankedNonlinearPattern < SymbolType > &, const common::ranked_symbol < SymbolType > & ) {
		return false;
	}

	/**
	 * Determines whether the symbol is available in the pattern's alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true
	 */
	static bool available ( const tree::RankedNonlinearPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol ) {
		return pattern.template accessComponent < tree::GeneralAlphabet > ( ).get ( ).count ( symbol );
}

	/**
	 * Nonlinear variable needs to have zero arity and needs to be different from subtree wildcard.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \throws TreeException if the symbol does not have zero arity
	 */
	static void valid ( const tree::RankedNonlinearPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol) {
		if( ( unsigned ) symbol.getRank() != 0 )
			throw tree::TreeException ( "Nonlinear variable symbol has nonzero arity" );

		if ( pattern.template accessComponent < tree::SubtreeWildcard > ( ).get ( ) == symbol )
			throw tree::TreeException ( "Symbol " + ext::to_string ( symbol ) + "cannot be set as nonlinear variable since it is already subtree wildcard" );
	}
};

/**
 * Helper class specifying constraints for the pattern's internal subtree wildcard element.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbols of the alphabet of the pattern.
 */
template < class SymbolType >
class ElementConstraint< tree::RankedNonlinearPattern < SymbolType >, common::ranked_symbol < SymbolType >, tree::SubtreeWildcard > {
public:
	/**
	 * Determines whether the symbol is available in the pattern's alphabet.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is already in the alphabet of the pattern
	 */
	static bool available ( const tree::RankedNonlinearPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol ) {
		return pattern.template accessComponent < tree::GeneralAlphabet > ( ).get ( ).count ( symbol );
	}

	/**
	 * Subtree wildcard needs to have zero arity and it needs to be different from any nonlinear variable of the tree.
	 *
	 * \param pattern the tested pattern
	 * \param symbol the tested symbol
	 *
	 * \throws TreeException if the symbol does not have zero arity
	 */
	static void valid ( const tree::RankedNonlinearPattern < SymbolType > & pattern, const common::ranked_symbol < SymbolType > & symbol) {
		if( ( unsigned ) symbol.getRank() != 0 )
			throw tree::TreeException ( "SubtreeWildcard symbol has nonzero arity" );

		if ( pattern.template accessComponent < tree::NonlinearAlphabet > ( ).get ( ).count ( symbol ) )
			throw tree::TreeException ( "Symbol " + ext::to_string ( symbol ) + "cannot be set as subtree wildcard since it is already nonlinear variable" );
	}
};

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols.
 *
 * \returns new instance of the tree with default template parameters or unmodified instance if the template parameters were already default ones
 */
template < class SymbolType >
struct normalize < tree::RankedNonlinearPattern < SymbolType > > {
	static tree::RankedNonlinearPattern < > eval ( tree::RankedNonlinearPattern < SymbolType > && value ) {
		common::ranked_symbol < DefaultSymbolType > wildcard = alphabet::SymbolNormalize::normalizeRankedSymbol ( std::move ( value ).getSubtreeWildcard ( ) );
		ext::set < common::ranked_symbol < DefaultSymbolType > > nonlinearAlphabet = alphabet::SymbolNormalize::normalizeRankedAlphabet ( std::move ( value ).getNonlinearVariables ( ) );
		ext::set < common::ranked_symbol < DefaultSymbolType > > alphabet = alphabet::SymbolNormalize::normalizeRankedAlphabet ( std::move ( value ).getAlphabet ( ) );
		ext::tree < common::ranked_symbol < DefaultSymbolType > > content = tree::TreeNormalize::normalizeRankedTree ( std::move ( value ).getContent ( ) );

		return tree::RankedNonlinearPattern < > ( std::move ( wildcard ), std::move ( nonlinearAlphabet ), std::move ( alphabet ), std::move ( content ) );
	}
};

} /* namespace core */

extern template class tree::RankedNonlinearPattern < >;

#endif /* RANKED_NONLINEAR_PATTERN_H_ */
