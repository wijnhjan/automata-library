/*
 * TreeException.h
 *
 *  Created on: Nov 16, 2014
 *      Author: Stepan Plachy
 */

#ifndef TREE_EXCEPTION_H_
#define TREE_EXCEPTION_H_

#include <exception/CommonException.h>

namespace tree {

/**
 * Exception thrown by an tree, tree parser or tree printer.
 */
class TreeException: public exception::CommonException {
public:
	explicit TreeException(const std::string& cause);
};

} /* namespace tree */

#endif /* TREE_EXCEPTION_H_ */
