/*
 * PrefixRankedBarNonlinearPattern.cpp
 *
 *  Created on: Jul 20, 2016
 *      Author: Jan Travnicek
 */

#include "PrefixRankedBarNonlinearPattern.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < tree::PrefixRankedBarNonlinearPattern < > > ( );
auto xmlRead = registration::XmlReaderRegister < tree::PrefixRankedBarNonlinearPattern < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, tree::PrefixRankedBarNonlinearPattern < > > ( );

} /* namespace */
