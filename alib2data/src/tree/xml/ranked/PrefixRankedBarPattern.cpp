/*
 * PrefixRankedBarPattern.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#include "PrefixRankedBarPattern.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < tree::PrefixRankedBarPattern < > > ( );
auto xmlRead = registration::XmlReaderRegister < tree::PrefixRankedBarPattern < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, tree::PrefixRankedBarPattern < > > ( );

} /* namespace */
