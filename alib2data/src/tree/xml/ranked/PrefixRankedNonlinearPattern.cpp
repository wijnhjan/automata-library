/*
 * PrefixRankedNonlinearPattern.cpp
 *
 *  Created on: Jul 20, 2016
 *      Author: Jan Travnicek
 */

#include "PrefixRankedNonlinearPattern.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < tree::PrefixRankedNonlinearPattern < > > ( );
auto xmlRead = registration::XmlReaderRegister < tree::PrefixRankedNonlinearPattern < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, tree::PrefixRankedNonlinearPattern < > > ( );

} /* namespace */
