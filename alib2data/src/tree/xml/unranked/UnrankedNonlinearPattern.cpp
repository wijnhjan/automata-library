/*
 * UnrankedNonlinearPattern.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#include "UnrankedNonlinearPattern.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < tree::UnrankedNonlinearPattern < > > ( );
auto xmlRead = registration::XmlReaderRegister < tree::UnrankedNonlinearPattern < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, tree::UnrankedNonlinearPattern < > > ( );

} /* namespace */
