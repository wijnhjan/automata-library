/*
 * FormalRTE.cpp
 *
 *  Created on: Jan 30, 2014
 *      Author: Jan Travnicek
 */

#include <rte/xml/FormalRTE.h>

#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < rte::FormalRTE < > > ( );
auto xmlRead = registration::XmlReaderRegister < rte::FormalRTE < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, rte::FormalRTE < > > ( );

} /* namespace */
