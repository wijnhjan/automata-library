/*
 * FormalRTE.cpp
 *
 *  Created on: Feb 20, 2016
 *      Author: Tomas Pecka
 */

#include "FormalRTE.h"
#include "FormalRTEElements.h"

#include <registration/ValuePrinterRegistration.hpp>

template class rte::FormalRTE < >;
template class rte::FormalRTEStructure < DefaultSymbolType >;
template class rte::FormalRTEElement < DefaultSymbolType >;
template class rte::FormalRTEAlternation < DefaultSymbolType >;
template class rte::FormalRTESubstitution < DefaultSymbolType >;
template class rte::FormalRTEIteration < DefaultSymbolType >;
template class rte::FormalRTEEmpty < DefaultSymbolType >;
template class rte::FormalRTESymbol < DefaultSymbolType >;
template class rte::FormalRTESymbolAlphabet < DefaultSymbolType >;
template class rte::FormalRTESymbolSubst < DefaultSymbolType >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < rte::FormalRTE < > > ( );

} /* namespace */
