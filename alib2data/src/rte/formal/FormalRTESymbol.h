/*
 * FormalRTESymbol.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Feb 20, 2016
 *      Author: Tomas Pecka
 */

#ifndef FORMAL_RTE_SYMBOL_H_
#define FORMAL_RTE_SYMBOL_H_

#include <common/ranked_symbol.hpp>
#include "FormalRTEElement.h"

namespace rte {

/**
 * \brief Represents the common part of SubstitutionSymbol and TerminalSymbol.
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template < class SymbolType >
class FormalRTESymbol {
protected:
	/**
	 * The symbol of the node.
	 */
	common::ranked_symbol < SymbolType > m_symbol;

public:
	/**
	 * \brief Creates a new instance of the symbol node using the actual symbol to represent.
	 *
	 * \param symbol the value of the represented symbol
	 */
	FormalRTESymbol ( common::ranked_symbol < SymbolType > symbol );

	/**
	 * Getter of the symbol.
	 *
	 * \return the symbol
	 */
	const common::ranked_symbol < SymbolType > & getSymbol ( ) const &;

	/**
	 * Getter of the symbol.
	 *
	 * \return the symbol
	 */
	common::ranked_symbol < SymbolType > && getSymbol ( ) &&;
};

template < class SymbolType >
FormalRTESymbol < SymbolType >::FormalRTESymbol ( common::ranked_symbol < SymbolType > symbol ) : m_symbol ( std::move ( symbol ) ) {
}

template < class SymbolType >
const common::ranked_symbol < SymbolType > & FormalRTESymbol < SymbolType >::getSymbol ( ) const & {
	return m_symbol;
}

template < class SymbolType >
common::ranked_symbol < SymbolType > && FormalRTESymbol < SymbolType >::getSymbol ( ) && {
	return std::move ( m_symbol );
}

} /* namespace rte */

extern template class rte::FormalRTESymbol < DefaultSymbolType >;

#endif /* FORMAL_RTE_SYMBOL_H_ */
