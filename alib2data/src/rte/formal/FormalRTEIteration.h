/*
 * FormalRTEIteration.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Feb 20, 2016
 *      Author: Tomas Pecka
 */

#ifndef FORMAL_RTE_ITERATION_H_
#define FORMAL_RTE_ITERATION_H_

#include "FormalRTEElement.h"
#include "FormalRTESymbolSubst.h"

#include <exception/CommonException.h>

#include <sstream>
#include <alib/utility>

namespace rte {

/**
 * \brief Represents the iteration operator in the regular tree expression. The node has exactly one child.
 *
 * The structure is derived from UnaryNode that provides the child node and its accessors.
 *
 * The node can be visited by the FormalRegExpElement < SymbolType >::Visitor
 *
 * \tparam SymbolType used for the symbol part of the ranked symbol
 */
template < class SymbolType >
class FormalRTEIteration : public ext::UnaryNode < FormalRTEElement < SymbolType > > {
	/**
	 * The substitution symbol of the node. The symbol will be substitued in left tree by right
	 */
	FormalRTESymbolSubst < SymbolType > m_substitutionSymbol;

	/**
	 * @copydoc regexp::FormalRTEElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename FormalRTEElement < SymbolType >::ConstVisitor & visitor ) const override {
		visitor.visit ( * this );
	}

	/**
	 * @copydoc regexp::FormalRTEElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename FormalRTEElement < SymbolType >::Visitor & visitor ) override {
		visitor.visit ( * this );
	}

public:
	/**
	 * \brief Creates a new instance of the substitution node with explicit tree to iterate and a symbol representing place of substitution
	 *
	 * \param element the tree to iterate
	 * \param substitutionSymbol the symbol representing the substitution place
	 */
	explicit FormalRTEIteration ( FormalRTEElement < SymbolType > && element, FormalRTESymbolSubst < SymbolType > substitutionSymbol );

	/**
	 * \brief Creates a new instance of the substitution node with explicit tree to iterate and a symbol representing place of substitution
	 *
	 * \param element the tree to iterate
	 * \param substitutionSymbol the symbol representing the substitution place
	 */
	explicit FormalRTEIteration ( const FormalRTEElement < SymbolType > & element, FormalRTESymbolSubst < SymbolType > substitutionSymbol );

	/**
	 * @copydoc FormalRTEElement < SymbolType >::clone ( ) const &
	 */
	FormalRTEIteration < SymbolType > * clone ( ) const & override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::clone ( ) &&
	 */
	FormalRTEIteration < SymbolType > * clone ( ) && override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::testSymbol ( const common::ranked_symbol < SymbolType > & ) const
	 */
	bool testSymbol ( const common::ranked_symbol < SymbolType > & symbol ) const override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > &, ext::set < common::ranked_symbol < SymbolType > > & ) const
	 */
	void computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > & alphabetF, ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > &, const ext::set < common::ranked_symbol < SymbolType > > & ) const
	 */
	bool checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & alphabetF, const ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const override;

	/**
	 * Getter of the iterated tree
	 *
	 * \return the iterated tree
	 */
	const FormalRTEElement < SymbolType > & getElement ( ) const;

	/**
	 * Getter of the substitution symbol
	 *
	 * \return the substitution symbol
	 */
	const FormalRTESymbolSubst < SymbolType > & getSubstitutionSymbol ( ) const;

	/**
	 * Getter of the iterated tree
	 *
	 * \return the iterated tree
	 */
	FormalRTEElement < SymbolType > & getElement ( );

	/**
	 * Getter of the substitution symbol
	 *
	 * \return the substitution symbol
	 */
	FormalRTESymbolSubst < SymbolType > & getSubstitutionSymbol ( );

	/**
	 * Setter of the iterated tree
	 *
	 * \param element the iterated tree
	 */
	void setElement ( const FormalRTEElement < SymbolType > & element );

	/**
	 * Setter of the iterated tree
	 *
	 * \param element the iterated tree
	 */
	void setElement ( FormalRTEElement < SymbolType > && element );

	/**
	 * Setter of the substitution symbol
	 *
	 * \param element the substitution symbol
	 */
	void setSubstitutionSymbol ( FormalRTESymbolSubst < SymbolType > symbol );

	/**
	 * @copydoc base::CommonBase < FormalRTEElement < SymbolType > >::compare ( const FormalRTEElement < SymbolType > & ) const
	 */
	int compare ( const FormalRTEElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return this->compare ( ( decltype ( * this ) )other );

		return ext::type_index ( typeid ( * this ) ) - ext::type_index ( typeid ( other ) );
	}

	/**
	 * The actual compare method
	 *
	 * \param other the other instance
	 *
	 * \returns the actual relation between two by type same node instances
	 */
	int compare ( const FormalRTEIteration & ) const;

	/**
	 * @copydoc base::CommonBase < FormalRTEElement < SymbolType > >::operator >> ( std::ostream & ) const
	 */
	void operator >>( std::ostream & out ) const override;

	/**
	 * @copydoc base::CommonBase < FormalRTEElement < SymbolType > >::operator std::string ( ) const
	 */
	explicit operator std::string ( ) const override;

	/**
	 * @copydoc FormalRTEElement < SymbolType >::normalize ( ) &&
	 */
	ext::smart_ptr < FormalRTEElement < DefaultSymbolType > > normalize ( ) && override {
		FormalRTESymbolSubst < DefaultSymbolType > subst ( alphabet::SymbolNormalize::normalizeRankedSymbol ( std::move ( m_substitutionSymbol ).getSymbol ( ) ) );
		return ext::smart_ptr < FormalRTEElement < DefaultSymbolType > > ( new FormalRTEIteration < DefaultSymbolType > ( std::move ( * std::move ( getElement ( ) ).normalize ( ) ), std::move ( subst ) ) );
	}
};

template < class SymbolType >
FormalRTEIteration < SymbolType >::FormalRTEIteration ( FormalRTEElement < SymbolType > && element, FormalRTESymbolSubst < SymbolType > substitutionSymbol ) : ext::UnaryNode < FormalRTEElement < SymbolType > > ( std::move ( element ) ), m_substitutionSymbol ( std::move ( substitutionSymbol ) ) {
}

template < class SymbolType >
FormalRTEIteration < SymbolType >::FormalRTEIteration ( const FormalRTEElement < SymbolType > & element, FormalRTESymbolSubst < SymbolType > substitutionSymbol ) : FormalRTEIteration ( ext::move_copy ( element ), std::move ( substitutionSymbol ) ) {
}

template < class SymbolType >
const FormalRTEElement < SymbolType > & FormalRTEIteration < SymbolType >::getElement ( ) const {
	return this->getChild ( );
}

template < class SymbolType >
FormalRTEElement < SymbolType > & FormalRTEIteration < SymbolType >::getElement ( ) {
	return this->getChild ( );
}

template < class SymbolType >
const FormalRTESymbolSubst < SymbolType > & FormalRTEIteration < SymbolType >::getSubstitutionSymbol ( ) const {
	return m_substitutionSymbol;
}

template < class SymbolType >
FormalRTESymbolSubst < SymbolType > & FormalRTEIteration < SymbolType >::getSubstitutionSymbol ( ) {
	return m_substitutionSymbol;
}

template < class SymbolType >
void FormalRTEIteration < SymbolType >::setElement ( const FormalRTEElement < SymbolType > & element ) {
	setElement ( ext::move_copy ( element ) );
}

template < class SymbolType >
void FormalRTEIteration < SymbolType >::setElement ( FormalRTEElement < SymbolType > && element ) {
	this->setChild ( std::move ( element ) );
}

template < class SymbolType >
void FormalRTEIteration < SymbolType >::setSubstitutionSymbol ( FormalRTESymbolSubst < SymbolType > symbol ) {
	m_substitutionSymbol = std::move ( symbol );
}

template < class SymbolType >
FormalRTEIteration < SymbolType > * FormalRTEIteration < SymbolType >::clone ( ) const & {
	return new FormalRTEIteration ( * this );
}

template < class SymbolType >
FormalRTEIteration < SymbolType > * FormalRTEIteration < SymbolType >::clone ( ) && {
	return new FormalRTEIteration ( std::move ( * this ) );
}

template < class SymbolType >
int FormalRTEIteration < SymbolType >::compare ( const FormalRTEIteration & other ) const {
	auto first = ext::tie ( getElement ( ), getSubstitutionSymbol ( ) );
	auto second = ext::tie ( other.getElement ( ), other.getSubstitutionSymbol ( ) );

	static ext::compare < decltype ( first ) > comp;

	return comp ( first, second );
}

template < class SymbolType >
void FormalRTEIteration < SymbolType >::operator >>( std::ostream & out ) const {
	out << "(FormalRTEIteration " << m_substitutionSymbol << " " << getElement ( ) << ")";
}

template < class SymbolType >
bool FormalRTEIteration < SymbolType >::testSymbol ( const common::ranked_symbol < SymbolType > & symbol ) const {
	return this->getElement ( ).testSymbol ( symbol );
}

template < class SymbolType >
void FormalRTEIteration < SymbolType >::computeMinimalAlphabet ( ext::set < common::ranked_symbol < SymbolType > > & alphabetF, ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const {
	alphabetK.insert ( m_substitutionSymbol.getSymbol ( ) );
	this->getElement ( ).computeMinimalAlphabet ( alphabetF, alphabetK );
}

template < class SymbolType >
bool FormalRTEIteration < SymbolType >::checkAlphabet ( const ext::set < common::ranked_symbol < SymbolType > > & alphabetF, const ext::set < common::ranked_symbol < SymbolType > > & alphabetK ) const {
	return alphabetK.count ( getSubstitutionSymbol ( ).getSymbol ( ) ) > 0 && getElement ( ).checkAlphabet ( alphabetF, alphabetK );
}

template < class SymbolType >
FormalRTEIteration < SymbolType >::operator std::string ( ) const {
	std::stringstream ss;
	ss << * this;
	return ss.str ( );
}

} /* namespace rte */

extern template class rte::FormalRTEIteration < DefaultSymbolType >;

#endif /* FORMAL_RTE_ITERATION_H_ */
