/*
 * DefaultSymbolType.h
 *
 *  Created on: Nov 29, 2016
 *      Author: Jan Travnicek
 */

#ifndef DEFAULT_SYMBOL_TYPE_H_
#define DEFAULT_SYMBOL_TYPE_H_

#include <object/Object.h>

typedef object::Object DefaultSymbolType;

#endif /* DEFAULT_SYMBOL_TYPE_H_ */
