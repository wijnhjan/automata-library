/*
 * SuffixTrie.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#include "SuffixTrie.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister < indexes::stringology::SuffixTrie < > > ( );

} /* namespace */
