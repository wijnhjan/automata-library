/*
 * CompressedBitParallelIndex.cpp
 *
 *  Created on: Jan 8, 2017
 *      Author: Jan Travnicek
 */

#include "CompressedBitParallelIndex.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister < indexes::stringology::CompressedBitParallelIndex < > > ( );

} /* namespace */
