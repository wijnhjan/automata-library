/*
 * CompressedBitParallelTreeIndex.cpp
 *
 *  Created on: Jan 8, 2017
 *      Author: Jan Travnicek
 */

#include "CompressedBitParallelTreeIndex.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister < indexes::arbology::CompressedBitParallelTreeIndex < > > ( );

} /* namespace */
