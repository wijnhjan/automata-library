/*
 * NonlinearFullAndLinearIndex.cpp
 *
 *  Created on: Apr 13, 2017
 *      Author: Jan Travnicek
 */

#include "NonlinearFullAndLinearIndex.h"

#include <registration/ValuePrinterRegistration.hpp>

namespace {

auto valuePrinter = registration::ValuePrinterRegister < indexes::arbology::NonlinearFullAndLinearIndex < > > ( );

} /* namespace */
