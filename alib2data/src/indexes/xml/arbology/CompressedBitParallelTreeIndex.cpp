/*
 * CompressedBitParallelTreeIndex.cpp
 *
 *  Created on: Jan 8, 2017
 *      Author: Jan Travnicek
 */

#include "CompressedBitParallelTreeIndex.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < indexes::arbology::CompressedBitParallelTreeIndex < > > ( );
auto xmlRead = registration::XmlReaderRegister < indexes::arbology::CompressedBitParallelTreeIndex < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, indexes::arbology::CompressedBitParallelTreeIndex < > > ( );

} /* namespace */
