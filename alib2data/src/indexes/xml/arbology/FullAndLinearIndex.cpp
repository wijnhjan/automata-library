/*
 * FullAndLinearIndex.cpp
 *
 *  Created on: Jan 8, 2017
 *      Author: Jan Travnicek
 */

#include "FullAndLinearIndex.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < indexes::arbology::FullAndLinearIndex < > > ( );
auto xmlRead = registration::XmlReaderRegister < indexes::arbology::FullAndLinearIndex < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, indexes::arbology::FullAndLinearIndex < > > ( );

} /* namespace */
