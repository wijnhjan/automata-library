/*
 * FactorOracleAutomaton.cpp
 *
 *  Created on: Jan 8, 2017
 *      Author: Jan Travnicek
 */

#include "FactorOracleAutomaton.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < indexes::stringology::FactorOracleAutomaton < > > ( );
auto xmlRead = registration::XmlReaderRegister < indexes::stringology::FactorOracleAutomaton < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, indexes::stringology::FactorOracleAutomaton < > > ( );

} /* namespace */
