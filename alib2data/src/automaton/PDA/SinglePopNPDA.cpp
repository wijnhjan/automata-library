/*
 * SinglePopNPDA.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "SinglePopNPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::SinglePopNPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::SinglePopNPDA < > > ( );

} /* namespace */
