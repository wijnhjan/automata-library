/*
 * VisiblyPushdownDPDA.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "VisiblyPushdownDPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::VisiblyPushdownDPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::VisiblyPushdownDPDA < > > ( );

} /* namespace */
