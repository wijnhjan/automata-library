/*
 * InputDrivenDPDA.cpp
 *
 *  Created on: Mar 25, 2013
 *      Author: Jan Travnicek
 */

#include "InputDrivenDPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::InputDrivenDPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::InputDrivenDPDA < > > ( );

} /* namespace */
