/*
 * InputDrivenNPDA.cpp
 *
 *  Created on: Mar 25, 2013
 *      Author: Jan Travnicek
 */

#include "InputDrivenNPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::InputDrivenNPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::InputDrivenNPDA < > > ( );

} /* namespace */
