/*
 * NPDTA.cpp
 *
 *  Created on: 10. 5. 2016
 *      Author: Jakub Doupal
 */

#include "NPDTA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::NPDTA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::NPDTA < > > ( );

} /* namespace */
