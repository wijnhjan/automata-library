/*
 * NPDA.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "NPDA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::NPDA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::NPDA < > > ( );

} /* namespace */
