/*
 * UnorderedDFTA.cpp
 *
 *  Created on: Apr 14, 2015
 *      Author: Stepan Plachy
 */

#include "UnorderedDFTA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::UnorderedDFTA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::UnorderedDFTA < > > ( );

} /* namespace */
