/*
 * NFTA.cpp
 *
 *  Created on: Mar 21, 2015
 *      Author: Stepan Plachy
 */

#include "NFTA.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class automaton::NFTA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::NFTA < > > ( );

auto NFTAFromDFTA = registration::CastRegister < automaton::NFTA < >, automaton::DFTA < > > ( );

} /* namespace */
