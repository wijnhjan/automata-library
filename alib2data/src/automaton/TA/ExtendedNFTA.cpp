/*
 * ExtendedNFTA.cpp
 *
 *  Created on: Mar 13, 2019
 *      Author: Tomas Pecka
 */

#include "ExtendedNFTA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::ExtendedNFTA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::ExtendedNFTA < > > ( );

} /* namespace */
