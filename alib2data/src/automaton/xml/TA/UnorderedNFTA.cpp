/*
 * UnorderedNFTA.cpp
 *
 *  Created on: Mar 21, 2015
 *      Author: Stepan Plachy
 */

#include "UnorderedNFTA.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWriteUnorderedNFTA = registration::XmlWriterRegister < automaton::UnorderedNFTA < > > ( );
auto xmlReadUnorderedNFTA = registration::XmlReaderRegister < automaton::UnorderedNFTA < > > ( );

auto xmlGroupUnorderedNFTA = registration::XmlRegisterTypeInGroup < object::Object, automaton::UnorderedNFTA < > > ( );

} /* namespace */
