/*
 * VisiblyPushdownNPDA.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "VisiblyPushdownNPDA.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < automaton::VisiblyPushdownNPDA < > > ( );
auto xmlRead = registration::XmlReaderRegister < automaton::VisiblyPushdownNPDA < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, automaton::VisiblyPushdownNPDA < > > ( );

} /* namespace */
