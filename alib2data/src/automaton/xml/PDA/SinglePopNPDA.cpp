/*
 * SinglePopNPDA.cpp
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "SinglePopNPDA.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < automaton::SinglePopNPDA < > > ( );
auto xmlRead = registration::XmlReaderRegister < automaton::SinglePopNPDA < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, automaton::SinglePopNPDA < > > ( );

} /* namespace */
