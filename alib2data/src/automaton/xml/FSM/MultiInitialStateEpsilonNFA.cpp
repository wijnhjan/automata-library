/*
 * MultiInitialStateEpsilonNFA.cpp
 *
 *  Created on: Mar 25, 2013
 *      Author: Jan Travnicek
 */

#include "MultiInitialStateEpsilonNFA.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < automaton::MultiInitialStateEpsilonNFA < > > ( );
auto xmlRead = registration::XmlReaderRegister < automaton::MultiInitialStateEpsilonNFA < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, automaton::MultiInitialStateEpsilonNFA < > > ( );

} /* namespace */
