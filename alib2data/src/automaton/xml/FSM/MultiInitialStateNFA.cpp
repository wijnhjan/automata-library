/*
 * MultiInitialStateNFA.cpp
 *
 *  Created on: Mar 25, 2013
 *      Author: Jan Travnicek
 */

#include "MultiInitialStateNFA.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < automaton::MultiInitialStateNFA < > > ( );
auto xmlRead = registration::XmlReaderRegister < automaton::MultiInitialStateNFA < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, automaton::MultiInitialStateNFA < > > ( );

} /* namespace */
