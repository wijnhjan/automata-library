/*
 * EpsilonNFA.cpp
 *
 *  Created on: Mar 25, 2013
 *      Author: Jan Travnicek
 */

#include "EpsilonNFA.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class automaton::EpsilonNFA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::EpsilonNFA < > > ( );

auto epsilonNFAFromDFA = registration::CastRegister < automaton::EpsilonNFA < >, automaton::DFA < > > ( );
auto epsilonNFAFromNFA = registration::CastRegister < automaton::EpsilonNFA < >, automaton::NFA < > > ( );
auto epsilonNFAFromMultiInitialStateNFA = registration::CastRegister < automaton::EpsilonNFA < >, automaton::MultiInitialStateNFA < > > ( );

} /* namespace */
