/*
 * ExtendedNFA.cpp
 *
 *  Created on: Mar 25, 2013
 *      Author: Jan Travnicek
 */

#include "ExtendedNFA.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class automaton::ExtendedNFA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::ExtendedNFA < > > ( );

auto extendedNFAFromDFA = registration::CastRegister < automaton::ExtendedNFA < >, automaton::DFA < > > ( );
auto extendedNFAFromNFA = registration::CastRegister < automaton::ExtendedNFA < >, automaton::NFA < > > ( );
auto extendedNFAFromMultiInitialStateNFA = registration::CastRegister < automaton::ExtendedNFA < >, automaton::MultiInitialStateNFA < > > ( );
auto extendedNFAFromEpsilonNFA = registration::CastRegister < automaton::ExtendedNFA < >, automaton::EpsilonNFA < > > ( );
auto extendedNFAFromMultiInitialStateEpsilonNFA = registration::CastRegister < automaton::ExtendedNFA < >, automaton::MultiInitialStateEpsilonNFA < > > ( );
auto extendedNFAFromCompactNFA = registration::CastRegister < automaton::ExtendedNFA < >, automaton::CompactNFA < > > ( );

} /* namespace */
