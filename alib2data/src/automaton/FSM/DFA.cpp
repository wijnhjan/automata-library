/*
 * DFA.cpp
 *
 *  Created on: Mar 25, 2013
 *      Author: Jan Travnicek
 */

#include "DFA.h"

#include <registration/ValuePrinterRegistration.hpp>

template class automaton::DFA < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < automaton::DFA < > > ( );

} /* namespace */
