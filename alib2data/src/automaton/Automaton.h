/*
 * Automaton.h
 *
 *  Created on: Apr 10, 2013
 *      Author: Martin Zak
 */

#ifndef AUTOMATON_H_
#define AUTOMATON_H_

#include <type_traits>

namespace automaton {

/**
 * \brief Wrapper around any automaton type.
 */
class Automaton;

} /* namespace automaton */

#endif /* AUTOMATON_H_ */
