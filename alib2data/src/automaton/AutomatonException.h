/*
 * AutomatonException.h
 *
 *  Created on: Apr 1, 2013
 *      Author: Martin Zak
 */

#ifndef AUTOMATON_EXCEPTION_H_
#define AUTOMATON_EXCEPTION_H_

#include <exception/CommonException.h>

namespace automaton {

/**
 * Exception thrown by an automaton, automaton parser or automaton printer.
 */
class AutomatonException: public exception::CommonException {
public:
	explicit AutomatonException(const std::string& cause);
};

} /* namespace automaton */

#endif /* AUTOMATON_EXCEPTION_H_ */
