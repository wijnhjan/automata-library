/*
 * AutomatonException.cpp
 *
 *  Created on: Apr 1, 2013
 *      Author: Martin Zak
 */

#include "AutomatonException.h"

namespace automaton {

AutomatonException::AutomatonException(const std::string& cause) :
		CommonException(cause) {
}

} /* namespace automaton */
