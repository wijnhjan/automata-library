/*
 * UnboundedRegExpEmpty.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Jan 30, 2014
 *      Author: Jan Travnicek
 */

#ifndef UNBOUNDED_REG_EXP_EMPTY_H_
#define UNBOUNDED_REG_EXP_EMPTY_H_

#include <sstream>

#include "UnboundedRegExpElement.h"

namespace regexp {

/**
 * \brief Represents the empty expression in the regular expression. The node can't have any children.
 *
 * The structure is derived from NullaryNode disallowing adding any child.
 *
 * The node can be visited by the UnboundedRegExpElement < SymbolType >::Visitor
 *
 * \tparam SymbolType used for the terminal alphabet
 */
template < class SymbolType >
class UnboundedRegExpEmpty : public ext::NullaryNode < UnboundedRegExpElement < SymbolType > > {
	/**
	 * @copydoc regexp::UnboundedRegExpElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename UnboundedRegExpElement < SymbolType >::Visitor & visitor ) const & override {
		visitor.visit ( * this );
	}

	/**
	 * @copydoc regexp::UnboundedRegExpElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename UnboundedRegExpElement < SymbolType >::RvalueVisitor & visitor ) && override {
		visitor.visit ( std::move ( * this ) );
	}

public:
	/**
	 * \brief Creates a new instance of the empty node.
	 */
	explicit UnboundedRegExpEmpty ( ) = default;

	/**
	 * @copydoc UnboundedRegExpElement::clone ( ) const &
	 */
	UnboundedRegExpEmpty < SymbolType > * clone ( ) const & override;

	/**
	 * @copydoc UnboundedRegExpElement::clone ( ) const &
	 */
	UnboundedRegExpEmpty < SymbolType > * clone ( ) && override;

	/**
	 * @copydoc UnboundedRegExpElement::cloneAsFormal() const
	 */
	ext::smart_ptr < FormalRegExpElement < SymbolType > > asFormal ( ) const override;

	/**
	 * @copydoc UnboundedRegExpElement::testSymbol() const
	 */
	bool testSymbol ( const SymbolType & symbol ) const override;

	/**
	 * @copydoc RegExpElement::computeMinimalAlphabet()
	 */
	void computeMinimalAlphabet ( ext::set < SymbolType > & alphabet ) const override;

	/**
	 * @copydoc UnboundedRegExpElement::checkAlphabet()
	 */
	bool checkAlphabet ( const ext::set < SymbolType > & alphabet ) const override;

	/**
	 * @copydoc base::CommonBase < UnboundedRegExpElement < SymbolType > >::compare ( const UnboundedRegExpElement < SymbolType > & )
	 */
	int compare ( const UnboundedRegExpElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return this->compare ( ( decltype ( * this ) )other );

		return ext::type_index ( typeid ( * this ) ) - ext::type_index ( typeid ( other ) );
	}

	/**
	 * The actual compare method
	 *
	 * \param other the other instance
	 *
	 * \returns the actual relation between two by type same node instances
	 */
	int compare ( const UnboundedRegExpEmpty & ) const;

	/**
	 * @copydoc base::CommonBase < UnboundedRegExpElement < SymbolType > >::operator >> ( std::ostream & )
	 */
	void operator >>( std::ostream & out ) const override;

	/**
	 * @copydoc base::CommonBase < UnboundedRegExpElement < SymbolType > >::operator std::string ( )
	 */
	explicit operator std::string ( ) const override;

	/**
	 * @copydoc regexp::UnboundedRegExpElement < SymbolType >::normalize ( ) &&
	 */
	ext::smart_ptr < UnboundedRegExpElement < DefaultSymbolType > > normalize ( ) && override {
		return ext::smart_ptr < UnboundedRegExpElement < DefaultSymbolType > > ( new UnboundedRegExpEmpty < DefaultSymbolType > ( ) );
	}
};

} /* namespace regexp */

#include "../formal/FormalRegExpEmpty.h"

namespace regexp {

template < class SymbolType >
UnboundedRegExpEmpty < SymbolType > * UnboundedRegExpEmpty < SymbolType >::clone ( ) const & {
	return new UnboundedRegExpEmpty ( * this );
}

template < class SymbolType >
UnboundedRegExpEmpty < SymbolType > * UnboundedRegExpEmpty < SymbolType >::clone ( ) && {
	return new UnboundedRegExpEmpty ( std::move ( * this ) );
}

template < class SymbolType >
ext::smart_ptr < FormalRegExpElement < SymbolType > > UnboundedRegExpEmpty < SymbolType >::asFormal ( ) const {
	return ext::smart_ptr < FormalRegExpElement < SymbolType > > ( new FormalRegExpEmpty < SymbolType > ( ) );
}

template < class SymbolType >
int UnboundedRegExpEmpty < SymbolType >::compare ( const UnboundedRegExpEmpty & ) const {
	return 0;
}

template < class SymbolType >
void UnboundedRegExpEmpty < SymbolType >::operator >>( std::ostream & out ) const {
	out << "(UnboundedRegExpEmpty)";
}

template < class SymbolType >
bool UnboundedRegExpEmpty < SymbolType >::testSymbol ( const SymbolType & ) const {
	return false;
}

template < class SymbolType >
bool UnboundedRegExpEmpty < SymbolType >::checkAlphabet ( const ext::set < SymbolType > & ) const {
	return true;
}

template < class SymbolType >
void UnboundedRegExpEmpty < SymbolType >::computeMinimalAlphabet ( ext::set < SymbolType > & ) const {
}

template < class SymbolType >
UnboundedRegExpEmpty < SymbolType >::operator std::string ( ) const {
	std::stringstream ss;
	ss << * this;
	return ss.str ( );
}

} /* namespace regexp */

extern template class regexp::UnboundedRegExpEmpty < DefaultSymbolType >;

#endif /* UNBOUNDED_REG_EXP_EMPTY_H_ */
