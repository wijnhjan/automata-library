/*
 * UnboundedRegExpIteration.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Nov 23, 2013
 *      Author: Martin Zak
 */

#ifndef UNBOUNDED_REG_EXP_ITERATION_H_
#define UNBOUNDED_REG_EXP_ITERATION_H_

#include <sstream>

#include <exception/CommonException.h>

#include "UnboundedRegExpElement.h"

namespace regexp {

/**
 * \brief Represents the iteration operator in the regular expression. The node has exactly one child.
 *
 * The structure is derived from UnaryNode that provides the child node and its accessors.
 *
 * The node can be visited by the UnboundedRegExpElement < SymbolType >::Visitor
 *
 * \tparam SymbolType used for the terminal alphabet
 */
template < class SymbolType >
class UnboundedRegExpIteration : public ext::UnaryNode < UnboundedRegExpElement < SymbolType > > {
	/**
	 * @copydoc regexp::UnboundedRegExpElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename UnboundedRegExpElement < SymbolType >::Visitor & visitor ) const & override {
		visitor.visit ( * this );
	}

	/**
	 * @copydoc regexp::UnboundedRegExpElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename UnboundedRegExpElement < SymbolType >::RvalueVisitor & visitor ) && override {
		visitor.visit ( std::move ( * this ) );
	}

public:
	/**
	 * \brief Creates a new instance of the iteration node with explicit iterated element
	 *
	 * \param element the iterated element
	 */
	explicit UnboundedRegExpIteration ( UnboundedRegExpElement < SymbolType > && element );

	/**
	 * \brief Creates a new instance of the iteration node with explicit iterated element
	 *
	 * \param element the iterated element
	 */
	explicit UnboundedRegExpIteration ( const UnboundedRegExpElement < SymbolType > & element );

	/**
	 * @copydoc UnboundedRegExpElement::clone ( ) const &
	 */
	UnboundedRegExpIteration < SymbolType > * clone ( ) const & override;

	/**
	 * @copydoc UnboundedRegExpElement::clone ( ) const &
	 */
	UnboundedRegExpIteration < SymbolType > * clone ( ) && override;

	/**
	 * @copydoc UnboundedRegExpElement::cloneAsFormal() const
	 */
	ext::smart_ptr < FormalRegExpElement < SymbolType > > asFormal ( ) const override;

	/**
	 * @copydoc UnboundedRegExpElement::testSymbol() const
	 */
	bool testSymbol ( const SymbolType & symbol ) const override;

	/**
	 * @copydoc UnboundedRegExpElement::computeMinimalAlphabet()
	 */
	void computeMinimalAlphabet ( ext::set < SymbolType > & alphabet ) const override;

	/**
	 * @copydoc UnboundedRegExpElement::checkAlphabet()
	 */
	bool checkAlphabet ( const ext::set < SymbolType > & alphabet ) const override;

	/**
	 * Getter of the iterated element
	 *
	 * \return iterated element
	 */
	const UnboundedRegExpElement < SymbolType > & getElement ( ) const;

	/**
	 * Getter of the iterated element
	 *
	 * \return iterated element
	 */
	UnboundedRegExpElement < SymbolType > & getElement ( );

	/**
	 * Setter of the iterated element
	 *
	 * \param iterated element
	 */
	void setElement ( UnboundedRegExpElement < SymbolType > && element );

	/**
	 * Setter of the iterated element
	 *
	 * \param iterated element
	 */
	void setElement ( const UnboundedRegExpElement < SymbolType > & element );

	/**
	 * @copydoc base::CommonBase < UnboundedRegExpElement < SymbolType > >::compare ( const UnboundedRegExpElement < SymbolType > & )
	 */
	int compare ( const UnboundedRegExpElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return this->compare ( ( decltype ( * this ) )other );

		return ext::type_index ( typeid ( * this ) ) - ext::type_index ( typeid ( other ) );
	}

	/**
	 * The actual compare method
	 *
	 * \param other the other instance
	 *
	 * \returns the actual relation between two by type same node instances
	 */
	int compare ( const UnboundedRegExpIteration & ) const;

	/**
	 * @copydoc base::CommonBase < UnboundedRegExpElement < SymbolType > >::operator >> ( std::ostream & )
	 */
	void operator >>( std::ostream & out ) const override;

	/**
	 * @copydoc base::CommonBase < UnboundedRegExpElement < SymbolType > >::operator std::string ( )
	 */
	explicit operator std::string ( ) const override;

	/**
	 * @copydoc regexp::UnboundedRegExpElement < SymbolType >::normalize ( ) &&
	 */
	ext::smart_ptr < UnboundedRegExpElement < DefaultSymbolType > > normalize ( ) && override {
		return ext::smart_ptr < UnboundedRegExpElement < DefaultSymbolType > > ( new UnboundedRegExpIteration < DefaultSymbolType > ( std::move ( * std::move ( getElement ( ) ).normalize ( ) ) ) );
	}
};

} /* namespace regexp */

#include "UnboundedRegExpEmpty.h"
#include "../formal/FormalRegExpIteration.h"

namespace regexp {

template < class SymbolType >
UnboundedRegExpIteration < SymbolType >::UnboundedRegExpIteration ( UnboundedRegExpElement < SymbolType > && element ) : ext::UnaryNode < UnboundedRegExpElement < SymbolType > > ( std::move ( element ) ) {
}

template < class SymbolType >
UnboundedRegExpIteration < SymbolType >::UnboundedRegExpIteration ( const UnboundedRegExpElement < SymbolType > & element ) : UnboundedRegExpIteration ( ext::move_copy ( element ) ) {
}

template < class SymbolType >
const UnboundedRegExpElement < SymbolType > & UnboundedRegExpIteration < SymbolType >::getElement ( ) const {
	return this->getChild ( );
}

template < class SymbolType >
UnboundedRegExpElement < SymbolType > & UnboundedRegExpIteration < SymbolType >::getElement ( ) {
	return this->getChild ( );
}

template < class SymbolType >
void UnboundedRegExpIteration < SymbolType >::setElement ( UnboundedRegExpElement < SymbolType > && element ) {
	this->setChild ( std::move ( element ) );
}

template < class SymbolType >
void UnboundedRegExpIteration < SymbolType >::setElement ( const UnboundedRegExpElement < SymbolType > & element ) {
	setElement ( ext::move_copy ( element ) );
}

template < class SymbolType >
UnboundedRegExpIteration < SymbolType > * UnboundedRegExpIteration < SymbolType >::clone ( ) const & {
	return new UnboundedRegExpIteration ( * this );
}

template < class SymbolType >
UnboundedRegExpIteration < SymbolType > * UnboundedRegExpIteration < SymbolType >::clone ( ) && {
	return new UnboundedRegExpIteration ( std::move ( * this ) );
}

template < class SymbolType >
ext::smart_ptr < FormalRegExpElement < SymbolType > > UnboundedRegExpIteration < SymbolType >::asFormal ( ) const {
	return ext::smart_ptr < FormalRegExpElement < SymbolType > > ( new FormalRegExpIteration < SymbolType > ( std::move ( * getElement ( ).asFormal ( ) ) ) );
}

template < class SymbolType >
int UnboundedRegExpIteration < SymbolType >::compare ( const UnboundedRegExpIteration < SymbolType > & other ) const {
	return getElement ( ).compare ( other.getElement ( ) );
}

template < class SymbolType >
void UnboundedRegExpIteration < SymbolType >::operator >>( std::ostream & out ) const {
	out << "(RegExpUnboundedRegExpIteration " << getElement ( ) << ")";
}

template < class SymbolType >
bool UnboundedRegExpIteration < SymbolType >::testSymbol ( const SymbolType & symbol ) const {
	return getElement ( ).testSymbol ( symbol );
}

template < class SymbolType >
bool UnboundedRegExpIteration < SymbolType >::checkAlphabet ( const ext::set < SymbolType > & alphabet ) const {
	return getElement ( ).checkAlphabet ( alphabet );
}

template < class SymbolType >
void UnboundedRegExpIteration < SymbolType >::computeMinimalAlphabet ( ext::set < SymbolType > & alphabet ) const {
	getElement ( ).computeMinimalAlphabet ( alphabet );
}

template < class SymbolType >
UnboundedRegExpIteration < SymbolType >::operator std::string ( ) const {
	std::stringstream ss;
	ss << * this;
	return ss.str ( );
}

} /* namespace regexp */

extern template class regexp::UnboundedRegExpIteration < DefaultSymbolType >;

#endif /* UNBOUNDED_REG_EXP_ITERATION_H_ */
