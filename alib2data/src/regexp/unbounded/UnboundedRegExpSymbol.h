/*
 * UnboundedRegExpSymbol.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Nov 23, 2013
 *      Author: Martin Zak
 */

#ifndef UNBOUNDED_REG_EXP_SYMBOL_H_
#define UNBOUNDED_REG_EXP_SYMBOL_H_

#include <sstream>

#include "UnboundedRegExpElement.h"
#include <alphabet/common/SymbolNormalize.h>

namespace regexp {

/**
 * \brief Represents the symbol in the regular expression. The can't have any children.
 *
 * The structure is derived from NullaryNode disallowing adding any child.
 *
 * The node can be visited by the UnboundedRegExpElement < SymbolType >::Visitor
 *
 * \tparam SymbolType used for the terminal alphabet
 */
template < class SymbolType >
class UnboundedRegExpSymbol : public ext::NullaryNode < UnboundedRegExpElement < SymbolType > > {
	/**
	 * The symbol of the node.
	 */
	SymbolType m_symbol;

	/**
	 * @copydoc regexp::UnboundedRegExpElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename UnboundedRegExpElement < SymbolType >::Visitor & visitor ) const & override {
		visitor.visit ( * this );
	}

	/**
	 * @copydoc regexp::UnboundedRegExpElement < SymbolType >::accept ( ) const
	 */
	void accept ( typename UnboundedRegExpElement < SymbolType >::RvalueVisitor & visitor ) && override {
		visitor.visit ( std::move ( * this ) );
	}

public:
	/**
	 * \brief Creates a new instance of the symbol node using the actual symbol to represent.
	 *
	 * \param symbol the value of the represented symbol
	 */
	explicit UnboundedRegExpSymbol ( SymbolType symbol );

	/**
	 * @copydoc UnboundedRegExpElement::clone ( ) const &
	 */
	UnboundedRegExpSymbol < SymbolType > * clone ( ) const & override;

	/**
	 * @copydoc UnboundedRegExpElement::clone ( ) const &
	 */
	UnboundedRegExpSymbol < SymbolType > * clone ( ) && override;

	/**
	 * @copydoc UnboundedRegExpElement::cloneAsFormal() const
	 */
	ext::smart_ptr < FormalRegExpElement < SymbolType > > asFormal ( ) const override;

	/**
	 * @copydoc UnboundedRegExpElement::testSymbol() const
	 */
	bool testSymbol ( const SymbolType & symbol ) const override;

	/**
	 * @copydoc UnboundedRegExpElement::computeMinimalAlphabet()
	 */
	void computeMinimalAlphabet ( ext::set < SymbolType > & alphabet ) const override;

	/**
	 * @copydoc UnboundedRegExpElement::checkAlphabet()
	 */
	bool checkAlphabet ( const ext::set < SymbolType > & alphabet ) const override;

	/**
	 * Getter of the symbol.
	 *
	 * \return the symbol
	 */
	const SymbolType & getSymbol ( ) const &;

	/**
	 * Getter of the symbol.
	 *
	 * \return the symbol
	 */
	SymbolType && getSymbol ( ) &&;

	/**
	 * @copydoc base::CommonBase < UnboundedRegExpElement < SymbolType > >::compare ( const UnboundedRegExpElement < SymbolType > & )
	 */
	int compare ( const UnboundedRegExpElement < SymbolType > & other ) const override {
		if ( ext::type_index ( typeid ( * this ) ) == ext::type_index ( typeid ( other ) ) ) return this->compare ( ( decltype ( * this ) )other );

		return ext::type_index ( typeid ( * this ) ) - ext::type_index ( typeid ( other ) );
	}

	/**
	 * The actual compare method
	 *
	 * \param other the other instance
	 *
	 * \returns the actual relation between two by type same node instances
	 */
	int compare ( const UnboundedRegExpSymbol & ) const;

	/**
	 * @copydoc base::CommonBase < UnboundedRegExpElement < SymbolType > >::operator >> ( std::ostream & )
	 */
	void operator >>( std::ostream & out ) const override;

	/**
	 * @copydoc base::CommonBase < UnboundedRegExpElement < SymbolType > >::operator std::string ( )
	 */
	explicit operator std::string ( ) const override;

	/**
	 * @copydoc regexp::UnboundedRegExpElement < SymbolType >::normalize ( ) &&
	 */
	ext::smart_ptr < UnboundedRegExpElement < DefaultSymbolType > > normalize ( ) && override {
		return ext::smart_ptr < UnboundedRegExpElement < DefaultSymbolType > > ( new UnboundedRegExpSymbol < DefaultSymbolType > ( alphabet::SymbolNormalize::normalizeSymbol ( std::move ( m_symbol ) ) ) );
	}
};

} /* namespace regexp */

#include "UnboundedRegExpSymbol.h"
#include "../formal/FormalRegExpSymbol.h"
#include <sstream>

namespace regexp {

template < class SymbolType >
UnboundedRegExpSymbol < SymbolType >::UnboundedRegExpSymbol ( SymbolType symbol ) : m_symbol ( std::move ( symbol ) ) {
}

template < class SymbolType >
UnboundedRegExpSymbol < SymbolType > * UnboundedRegExpSymbol < SymbolType >::clone ( ) const & {
	return new UnboundedRegExpSymbol ( * this );
}

template < class SymbolType >
UnboundedRegExpSymbol < SymbolType > * UnboundedRegExpSymbol < SymbolType >::clone ( ) && {
	return new UnboundedRegExpSymbol ( std::move ( * this ) );
}

template < class SymbolType >
ext::smart_ptr < FormalRegExpElement < SymbolType > > UnboundedRegExpSymbol < SymbolType >::asFormal ( ) const {
	return ext::smart_ptr < FormalRegExpElement < SymbolType > > ( new FormalRegExpSymbol < SymbolType > ( this->m_symbol ) );
}

template < class SymbolType >
int UnboundedRegExpSymbol < SymbolType >::compare ( const UnboundedRegExpSymbol & other ) const {
	static ext::compare < decltype ( m_symbol ) > comp;

	return comp ( m_symbol, other.m_symbol );
}

template < class SymbolType >
void UnboundedRegExpSymbol < SymbolType >::operator >>( std::ostream & out ) const {
	out << "(UnboundedRegExpSymbol " << m_symbol << ")";
}

template < class SymbolType >
bool UnboundedRegExpSymbol < SymbolType >::testSymbol ( const SymbolType & symbol ) const {
	return symbol == this->m_symbol;
}

template < class SymbolType >
bool UnboundedRegExpSymbol < SymbolType >::checkAlphabet ( const ext::set < SymbolType > & alphabet ) const {
	return alphabet.count ( m_symbol );
}

template < class SymbolType >
void UnboundedRegExpSymbol < SymbolType >::computeMinimalAlphabet ( ext::set < SymbolType > & alphabet ) const {
	alphabet.insert ( this->m_symbol );
}

template < class SymbolType >
const SymbolType & UnboundedRegExpSymbol < SymbolType >::getSymbol ( ) const & {
	return this->m_symbol;
}

template < class SymbolType >
SymbolType && UnboundedRegExpSymbol < SymbolType >::getSymbol ( ) && {
	return std::move ( this->m_symbol );
}

template < class SymbolType >
UnboundedRegExpSymbol < SymbolType >::operator std::string ( ) const {
	std::stringstream ss;
	ss << * this;
	return ss.str ( );
}

} /* namespace regexp */

extern template class regexp::UnboundedRegExpSymbol < DefaultSymbolType >;

#endif /* UNBOUNDED_REG_EXP_SYMBOL_H_ */
