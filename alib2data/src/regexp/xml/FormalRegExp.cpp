/*
 * FormalRegExp.cpp
 *
 *  Created on: Jan 30, 2014
 *      Author: Jan Travnicek
 */

#include <regexp/xml/FormalRegExp.h>

#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < regexp::FormalRegExp < > > ( );
auto xmlRead = registration::XmlReaderRegister < regexp::FormalRegExp < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, regexp::FormalRegExp < > > ( );

} /* namespace */
