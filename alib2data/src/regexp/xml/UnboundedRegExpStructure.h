/*
 * UnboundedRegExpStructure.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Oct 20, 2016
 *      Author: Jan Travnicek
 */

#ifndef _XML_UNBOUNDED_REG_EXP_STRUCTURE_H_
#define _XML_UNBOUNDED_REG_EXP_STRUCTURE_H_

#include <regexp/unbounded/UnboundedRegExpStructure.h>
#include <regexp/xml/common/RegExpFromXmlParser.h>
#include <regexp/xml/common/RegExpToXmlComposer.h>

namespace core {

/**
 * \brief Specialisation of the xmlApi class for unbounded regular expression
 *
 * The class provides the usual intarface of the xmlApi including parse and compose
 *
 * \tparam SymbolType used for the terminal alphabet
 */
template < class SymbolType >
struct xmlApi < regexp::UnboundedRegExpStructure < SymbolType > > {
	/**
	 * Parsing from a sequence of xml tokens helper.
	 *
	 * \params input the iterator to sequence of xml tokens to parse from
	 *
	 * \returns the new instance of the regexp
	 */
	static regexp::UnboundedRegExpStructure < SymbolType > parse ( ext::deque < sax::Token >::iterator & input );

	/**
	 * Composing to a sequence of xml tokens helper.
	 *
	 * \param output the sink for new xml tokens representing the automaton
	 * \param input the automaton to compose
	 */
	static void compose ( ext::deque < sax::Token > & output, const regexp::UnboundedRegExpStructure < SymbolType > & input );
};

template < class SymbolType >
regexp::UnboundedRegExpStructure < SymbolType > xmlApi < regexp::UnboundedRegExpStructure < SymbolType > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	return regexp::UnboundedRegExpStructure < SymbolType > ( regexp::RegExpFromXmlParser::parseUnboundedRegExpElement < SymbolType > ( input ) );
}

template < class SymbolType >
void xmlApi < regexp::UnboundedRegExpStructure < SymbolType > >::compose ( ext::deque < sax::Token > & output, const regexp::UnboundedRegExpStructure < SymbolType > & input ) {
	input.getStructure ( ).template accept < void, regexp::RegExpToXmlComposer::Unbounded > ( output );
}

} /* namespace core */

#endif /* _XML_UNBOUNDED_REG_EXP_STRUCTURE_H_ */
