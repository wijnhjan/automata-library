/*
 * FormalRegExp.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#ifndef FORMAL_REG_EXP_H_
#define FORMAL_REG_EXP_H_

#include <common/DefaultSymbolType.h>

namespace regexp {

template < class SymbolType = DefaultSymbolType >
class FormalRegExp;

} /* namespace regexp */

#include <alib/string>
#include <alib/set>
#include <alib/iostream>
#include <alib/algorithm>
#include <alib/compare>
#include <sstream>

#include <core/components.hpp>
#include <exception/CommonException.h>

#include "FormalRegExpStructure.h"

#include <core/normalize.hpp>
#include <alphabet/common/SymbolNormalize.h>

#include "../unbounded/UnboundedRegExp.h"

namespace regexp {

class GeneralAlphabet;

/**
 * \brief
 * Formal regular expression represents regular expression. It describes regular languages. The expression consists of the following nodes:
 *   - Alternation - the representation of + in the regular expression
 *   - Concatenation - the representation of &sdot; in the regular expression
 *   - Iteration - the representation of * (Kleene star)
 *   - Epsilon - the representation of empty word
 *   - Empty - the representation of empty regular expression
 *   - Symbol - the representation of single symbol
 *
 * The formal representation allows nodes of concatenation and alternation to have exactly two children.
 *
 * The structure of the regular expression is wrapped in a structure class to allow separate the alphabet from the structure for example in Extended NFA

 * \details
 * Definition is unbounded definition of regular expressions.
 * E = (T, C),
 * T (Alphabet) = finite set of terminal symbols
 * C (Content) = representation of the regular expression
 *
 * \tparam SymbolType used for the terminal alphabet
 */
template < class SymbolType >
class FormalRegExp final : public ext::CompareOperators < FormalRegExp < SymbolType > >, public core::Components < FormalRegExp < SymbolType >, ext::set < SymbolType >, component::Set, GeneralAlphabet > {
	/**
	 * The structure of the regular expression.
	 */
	FormalRegExpStructure < SymbolType > m_regExp;

public:
	/**
	 * The exposed SymbolType template parameter.
	 */
	using symbol_type = SymbolType;

	/**
	 * \brief Creates a new instance of the expression. The default constructor creates expression describing empty language.
	 */
	explicit FormalRegExp ( );

	/**
	 * \brief Creates a new instance of the expression with a concrete alphabet and initial content.
	 *
	 * \param alphabet the initial input alphabet
	 * \param regExp the initial regexp content
	 */
	explicit FormalRegExp ( ext::set < SymbolType > alphabet, FormalRegExpStructure < SymbolType > regExp );

	/**
	 * \brief Creates a new instance of the expression based on initial content. The alphabet is derived from the content.
	 *
	 * \param regExp the initial regexp content
	 */
	explicit FormalRegExp ( FormalRegExpStructure < SymbolType > regExp );

	/**
	 * \brief Created a new instance of the expression based on the UnboundedRegExp representation.
	 */
	explicit FormalRegExp ( const UnboundedRegExp < SymbolType > & other );

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the expression
	 */
	const ext::set < SymbolType > & getAlphabet ( ) const & {
		return this->template accessComponent < GeneralAlphabet > ( ).get ( );
	}

	/**
	 * Getter of the alphabet.
	 *
	 * \returns the alphabet of the expression
	 */
	ext::set < SymbolType > && getAlphabet ( ) && {
		return std::move ( this->template accessComponent < GeneralAlphabet > ( ).get ( ) );
	}

	/**
	 * Get the structure of the expression.
	 *
	 * \returns the structure of the expression.
	 */
	const FormalRegExpStructure < SymbolType > & getRegExp ( ) const &;

	/**
	 * Get the structure of the expression.
	 *
	 * \returns the structure of the expression.
	 */
	FormalRegExpStructure < SymbolType > && getRegExp ( ) &&;

	/**
	 * Set the structure of the expression.
	 *
	 * \param regExp the new structure of the expression.
	 */
	void setRegExp ( FormalRegExpStructure < SymbolType > param );

	/**
	 * The actual compare method
	 *
	 * \param other the other instance
	 *
	 * \returns the actual relation between two by type same expression instances
	 */
	int compare ( const FormalRegExp & other ) const;

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param out ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend std::ostream & operator << ( std::ostream & out, const FormalRegExp & instance ) {
		return out << "(FormalRegExp " << instance.getRegExp ( ).getStructure ( ) << ")";
	}

	/**
	 * Casts this instance to as compact as possible string representation.
	 *
	 * \returns string representation of the object
	 */
	explicit operator std::string ( ) const;
};

template < class SymbolType >
FormalRegExp < SymbolType >::FormalRegExp ( ext::set < SymbolType > alphabet, FormalRegExpStructure < SymbolType > regExp ) : core::Components < FormalRegExp, ext::set < SymbolType >, component::Set, GeneralAlphabet > ( std::move ( alphabet ) ), m_regExp ( std::move ( regExp ) ) {
	if ( !this->m_regExp.getStructure ( ).checkAlphabet ( getAlphabet ( ) ) )
		throw exception::CommonException ( "Input symbols not in the alphabet." );
}

template < class SymbolType >
FormalRegExp < SymbolType >::FormalRegExp ( ) : FormalRegExp ( ext::set < SymbolType > ( ), FormalRegExpStructure < SymbolType > ( ) ) {
}

template < class SymbolType >
FormalRegExp < SymbolType >::FormalRegExp ( FormalRegExpStructure < SymbolType > regExp ) : FormalRegExp ( regExp.getStructure ( ).computeMinimalAlphabet ( ), regExp ) {
}

template < class SymbolType >
FormalRegExp < SymbolType >::FormalRegExp ( const UnboundedRegExp < SymbolType > & other ) : FormalRegExp ( other.getAlphabet ( ), FormalRegExpStructure < SymbolType > ( other.getRegExp ( ) ) ) {
}

template < class SymbolType >
const FormalRegExpStructure < SymbolType > & FormalRegExp < SymbolType >::getRegExp ( ) const & {
	return m_regExp;
}

template < class SymbolType >
FormalRegExpStructure < SymbolType > && FormalRegExp < SymbolType >::getRegExp ( ) && {
	return std::move ( m_regExp );
}

template < class SymbolType >
void FormalRegExp < SymbolType >::setRegExp ( FormalRegExpStructure < SymbolType > param ) {
	if ( !param.getStructure ( ).checkAlphabet ( getAlphabet ( ) ) )
		throw exception::CommonException ( "Input symbols not in the alphabet." );

	this->m_regExp = std::move ( param );
}

template < class SymbolType >
int FormalRegExp < SymbolType >::compare ( const FormalRegExp & other ) const {
	auto first = ext::tie ( m_regExp.getStructure ( ), getAlphabet ( ) );
	auto second = ext::tie ( other.m_regExp.getStructure ( ), other.getAlphabet ( ) );

	static ext::compare < decltype ( first ) > comp;

	return comp ( first, second );
}

template < class SymbolType >
FormalRegExp < SymbolType >::operator std::string ( ) const {
	std::stringstream ss;
	ss << * this;
	return ss.str ( );
}

} /* namespace regexp */

namespace core {

/**
 * Helper class specifying constraints for the expression's internal alphabet component.
 *
 * \tparam SymbolType used for the terminal alphabet of the expression.
 */
template < class SymbolType >
class SetConstraint< regexp::FormalRegExp < SymbolType >, SymbolType, regexp::GeneralAlphabet > {
public:
	/**
	 * Returns true if the symbol is still used somewhere in the structure of the expression.
	 *
	 * \param regexp the tested expresion
	 * \param symbol the tested symbol
	 *
	 * \returns true if the symbol is used, false othervise
	 */
	static bool used ( const regexp::FormalRegExp < SymbolType > & regexp, const SymbolType & symbol ) {
		return regexp.getRegExp ( ).getStructure ( ).testSymbol ( symbol );
	}

	/**
	 * Returns true as all symbols are possibly available to be elements of the alphabet.
	 *
	 * \param regexp the tested expresion
	 * \param symbol the tested state
	 *
	 * \returns true
	 */
	static bool available ( const regexp::FormalRegExp < SymbolType > &, const SymbolType & ) {
		return true;
	}

	/**
	 * All symbols are valid as symbols of the alphabet.
	 *
	 * \param regexp the tested expresion
	 * \param symbol the tested state
	 */
	static void valid ( const regexp::FormalRegExp < SymbolType > &, const SymbolType & ) {
	}
};

/**
 * Helper for normalisation of types specified by templates used as internal datatypes of symbols.
 *
 * \returns new instance of the expression with default template parameters or unmodified instance if the template parameters were already the default ones
 */
template < class SymbolType >
struct normalize < regexp::FormalRegExp < SymbolType > > {
	static regexp::FormalRegExp < > eval ( regexp::FormalRegExp < SymbolType > && value ) {
		ext::set < DefaultSymbolType > alphabet = alphabet::SymbolNormalize::normalizeAlphabet ( std::move ( value ).getAlphabet ( ) );

		return regexp::FormalRegExp < > ( alphabet, std::move ( value ).getRegExp ( ).normalize ( ) );
	}
};

} /* namespace core */

extern template class regexp::FormalRegExp < >;

#endif /* FORMAL_REG_EXP_H_ */
