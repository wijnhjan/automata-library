/*
 * LinearString.cpp
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#include "LinearString.h"

#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < string::LinearString < > > ( );
auto xmlRead = registration::XmlReaderRegister < string::LinearString < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, string::LinearString < > > ( );

} /* namespace */
