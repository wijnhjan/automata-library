/*
 * FailStateLabel.cpp
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travicek
 */

#include "FailStateLabel.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace label {

FailStateLabel::FailStateLabel() = default;

int FailStateLabel::compare(const FailStateLabel&) const {
	return 0;
}

std::ostream & operator << ( std::ostream & out, const FailStateLabel & ) {
	return out << "(FailStateLabel)";
}

FailStateLabel::operator std::string ( ) const {
	return FailStateLabel::instance < std::string > ( );
}

} /* namespace label */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < label::FailStateLabel > ( );

} /* namespace */
