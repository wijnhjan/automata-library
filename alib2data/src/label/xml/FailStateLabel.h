/*
 * FailStateLabel.h
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travnicek
 */

#ifndef _XML_FAIL_STATE_LABEL_H_
#define _XML_FAIL_STATE_LABEL_H_

#include <label/FailStateLabel.h>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < label::FailStateLabel > {
	static label::FailStateLabel parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const label::FailStateLabel & data );
};

} /* namespace core */

#endif /* _XML_FAIL_STATE_LABEL_H_ */
