/*
 * FinalStateLabel.h
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travnicek
 */

#ifndef _XML_FINAL_STATE_LABEL_H_
#define _XML_FINAL_STATE_LABEL_H_

#include <label/FinalStateLabel.h>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < label::FinalStateLabel > {
	static label::FinalStateLabel parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, const label::FinalStateLabel & data );
};

} /* namespace core */

#endif /* _XML_FINAL_STATE_LABEL_H_ */
