/*
 * InitialStateLabel.h
 *
 *  Created on: Apr 10, 2013
 *      Author: Jan Travnicek
 */

#include "InitialStateLabel.h"
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace core {

label::InitialStateLabel xmlApi < label::InitialStateLabel >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return label::InitialStateLabel ( );
}

bool xmlApi < label::InitialStateLabel >::first ( const ext::deque < sax::Token >::const_iterator & input ) {
	return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
}

std::string xmlApi < label::InitialStateLabel >::xmlTagName ( ) {
	return "InitialStateLabel";
}

void xmlApi < label::InitialStateLabel >::compose ( ext::deque < sax::Token > & output, const label::InitialStateLabel & ) {
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );
	output.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

namespace {

auto xmlWrite = registration::XmlWriterRegister < label::InitialStateLabel > ( );
auto xmlRead = registration::XmlReaderRegister < label::InitialStateLabel > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, label::InitialStateLabel > ( );

} /* namespace */
