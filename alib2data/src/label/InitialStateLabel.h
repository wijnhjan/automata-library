/*
 * InitialStateLabel.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travnicek
 */

#ifndef INITIAL_STATE_LABEL_H_
#define INITIAL_STATE_LABEL_H_

#include <alib/compare>
#include <object/Object.h>

namespace label {

/**
 * \brief
 * Represents label of the final state of an automaton.
 */
class InitialStateLabel final : public ext::CompareOperators < InitialStateLabel > {
public:
	/**
	 * \brief
	 * Creates a new instance of the label.
	 */
	explicit InitialStateLabel ( );

	/**
	 * The actual compare method
	 *
	 * \param other the other instance
	 *
	 * \returns the actual relation between two by type same labels
	 */
	int compare ( const InitialStateLabel & other ) const;

	/**
	 * Print this object as raw representation to ostream.
	 *
	 * \param os ostream where to print
	 * \param instance object to print
	 *
	 * \returns modified output stream
	 */
	friend std::ostream & operator << ( std::ostream & out, const InitialStateLabel & instance );

	/**
	 * Casts this instance to as compact as possible string representation.
	 *
	 * \returns string representation of the object
	 */
	explicit operator std::string ( ) const;

	/**
	 * \brief Factory for the label construction of the label based on given type.
	 */
	template < typename Base >
	static inline typename std::enable_if <   std::is_integral < Base >::value, Base >::type instance ( );

	/**
	 * \brief Factory for the label construction of the label based on given type.
	 */
	template < typename Base >
	static inline typename std::enable_if < ! std::is_integral < Base >::value, Base >::type instance ( );
};

template < typename Base >
inline typename std::enable_if < std::is_integral < Base >::value, Base >::type InitialStateLabel::instance ( ) {
	return 0;
}

template < >
inline std::string InitialStateLabel::instance < std::string > ( ) {
	return std::string ( "initial" );
}

template < >
inline InitialStateLabel InitialStateLabel::instance < InitialStateLabel > ( ) {
	return InitialStateLabel ( );
}

template < >
inline object::Object InitialStateLabel::instance < object::Object > ( ) {
	return object::Object ( InitialStateLabel ( ) );
}

} /* namespace label */

#endif /* INITIAL_STATE_LABEL_H_ */
