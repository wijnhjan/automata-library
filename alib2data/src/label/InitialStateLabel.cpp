/*
 * InitialStateLabel.cpp
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travicek
 */

#include "InitialStateLabel.h"

#include <object/Object.h>

#include <registration/ValuePrinterRegistration.hpp>

namespace label {

InitialStateLabel::InitialStateLabel() = default;

int InitialStateLabel::compare(const InitialStateLabel&) const {
	return 0;
}

std::ostream & operator << ( std::ostream & out, const InitialStateLabel & ) {
	return out << "(InitialStateLabel)";
}

InitialStateLabel::operator std::string ( ) const {
	return InitialStateLabel::instance < std::string > ( );
}

} /* namespace label */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < label::InitialStateLabel > ( );

} /* namespace */
