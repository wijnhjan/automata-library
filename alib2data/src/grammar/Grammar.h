/*
 * Grammar.h
 *
 *  Created on: Apr 10, 2013
 *      Author: Martin Zak
 */

#ifndef GRAMMAR_H_
#define GRAMMAR_H_

#include <alib/type_traits>

namespace grammar {

/**
 * Wrapper around grammars.
 */
class Grammar;

template < class T >
using TerminalSymbolTypeOfGrammar = typename std::decay < decltype (std::declval<T>().getTerminalAlphabet()) >::type::value_type;

template < class T >
using NonterminalSymbolTypeOfGrammar = typename std::decay < decltype (std::declval<T>().getNonterminalAlphabet()) >::type::value_type;

} /* namespace grammar */

#endif /* GRAMMAR_H_ */
