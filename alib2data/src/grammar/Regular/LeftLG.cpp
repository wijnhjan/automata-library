/*
 * LeftLG.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#include "LeftLG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::LeftLG < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::LeftLG < > > ( );

} /* namespace */
