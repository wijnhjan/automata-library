/*
 * CSG.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#include "CSG.h"
#include <grammar/Grammar.h>
#include <object/Object.h>

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < grammar::CSG < > > ( );
auto xmlRead = registration::XmlReaderRegister < grammar::CSG < > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, grammar::CSG < > > ( );

} /* namespace */
