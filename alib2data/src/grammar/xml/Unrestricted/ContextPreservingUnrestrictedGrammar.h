/*
 * ContextPreservingUnrestrictedGrammar.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#ifndef _XML_CONTEXT_PRESERVING_UNRESTRICTED_GRAMMAR_H_
#define _XML_CONTEXT_PRESERVING_UNRESTRICTED_GRAMMAR_H_

#include <grammar/Unrestricted/ContextPreservingUnrestrictedGrammar.h>
#include "../common/GrammarFromXMLParser.h"
#include "../common/GrammarToXMLComposer.h"

namespace core {

template < class SymbolType >
struct xmlApi < grammar::ContextPreservingUnrestrictedGrammar < SymbolType > > {
	/**
	 * \brief The XML tag name of class.
	 *
	 * \details Intentionaly a static member function to be safe in the initialisation before the main function starts.
	 *
	 * \returns string representing the XML tag name of the class
	 */
	static std::string xmlTagName() {
		return "ContextPreservingUnrestrictedGrammar";
	}

	/**
	 * \brief Tests whether the token stream starts with this type
	 *
	 * \params input the iterator to sequence of xml tokens to test
	 *
	 * \returns true if the token stream iterator points to opening tag named with xml tag name of this type, false otherwise.
	 */
	static bool first ( const ext::deque < sax::Token >::const_iterator & input ) {
		return sax::FromXMLParserHelper::isToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );
	}

	/**
	 * Parsing from a sequence of xml tokens helper.
	 *
	 * \params input the iterator to sequence of xml tokens to parse from
	 *
	 * \returns the new instance of the grammar
	 */
	static grammar::ContextPreservingUnrestrictedGrammar < SymbolType > parse ( ext::deque < sax::Token >::iterator & input );

	/**
	 * Helper for parsing of individual rules of the grammar from a sequence of xml tokens.
	 *
	 * \params input the iterator to sequence of xml tokens to parse from
	 * \params grammar the grammar to add the rule to
	 */
	static void parseRule ( ext::deque < sax::Token >::iterator & input, grammar::ContextPreservingUnrestrictedGrammar < SymbolType > & grammar );

	/**
	 * Composing to a sequence of xml tokens helper.
	 *
	 * \param out sink for new xml tokens representing the grammar
	 * \param grammar the grammar to compose
	 */
	static void compose ( ext::deque < sax::Token > & out, const grammar::ContextPreservingUnrestrictedGrammar < SymbolType > & grammar );

	/**
	 * Helper for composing rules of the grammar to a sequence of xml tokens.
	 *
	 * \param out sink for xml tokens representing the rules of the grammar
	 * \param grammar the grammar to compose
	 */
	static void composeRules ( ext::deque < sax::Token > & out, const grammar::ContextPreservingUnrestrictedGrammar < SymbolType > & grammar );
};

template < class SymbolType >
grammar::ContextPreservingUnrestrictedGrammar < SymbolType > xmlApi < grammar::ContextPreservingUnrestrictedGrammar < SymbolType > >::parse ( ext::deque < sax::Token >::iterator & input ) {
	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::START_ELEMENT, xmlTagName ( ) );

	ext::set < SymbolType > nonterminalAlphabet = grammar::GrammarFromXMLParser::parseNonterminalAlphabet < SymbolType > ( input );
	ext::set < SymbolType > terminalAlphabet = grammar::GrammarFromXMLParser::parseTerminalAlphabet < SymbolType > ( input );
	SymbolType initialSymbol = grammar::GrammarFromXMLParser::parseInitialSymbol < SymbolType > ( input );

	grammar::ContextPreservingUnrestrictedGrammar < SymbolType > grammar ( std::move ( initialSymbol ) );

	grammar.setNonterminalAlphabet ( std::move ( nonterminalAlphabet ) );
	grammar.setTerminalAlphabet ( std::move ( terminalAlphabet ) );

	grammar::GrammarFromXMLParser::parseRules ( input, grammar );

	sax::FromXMLParserHelper::popToken ( input, sax::Token::TokenType::END_ELEMENT, xmlTagName ( ) );
	return grammar;
}

template < class SymbolType >
void xmlApi < grammar::ContextPreservingUnrestrictedGrammar < SymbolType > >::parseRule ( ext::deque < sax::Token >::iterator & input, grammar::ContextPreservingUnrestrictedGrammar < SymbolType > & grammar ) {
	ext::vector < SymbolType > lContext = grammar::GrammarFromXMLParser::parseRuleLContext < SymbolType > ( input );
	SymbolType lhs = grammar::GrammarFromXMLParser::parseRuleSingleSymbolLHS < SymbolType > ( input );
	ext::vector < SymbolType > rContext = grammar::GrammarFromXMLParser::parseRuleRContext < SymbolType > ( input );
	ext::vector < SymbolType > rhs = grammar::GrammarFromXMLParser::parseRuleRHS < SymbolType > ( input );

	grammar.addRule ( std::move ( lContext ), std::move ( lhs ), std::move ( rContext ), std::move ( rhs ) );
}

template < class SymbolType >
void xmlApi < grammar::ContextPreservingUnrestrictedGrammar < SymbolType > >::compose ( ext::deque < sax::Token > & out, const grammar::ContextPreservingUnrestrictedGrammar < SymbolType > & grammar ) {
	out.emplace_back ( xmlTagName ( ), sax::Token::TokenType::START_ELEMENT );

	grammar::GrammarToXMLComposer::composeNonterminalAlphabet ( out, grammar.getNonterminalAlphabet ( ) );
	grammar::GrammarToXMLComposer::composeTerminalAlphabet ( out, grammar.getTerminalAlphabet ( ) );
	grammar::GrammarToXMLComposer::composeInitialSymbol ( out, grammar.getInitialSymbol ( ) );
	composeRules ( out, grammar );

	out.emplace_back ( xmlTagName ( ), sax::Token::TokenType::END_ELEMENT );
}

template < class SymbolType >
void xmlApi < grammar::ContextPreservingUnrestrictedGrammar < SymbolType > >::composeRules ( ext::deque < sax::Token > & out, const grammar::ContextPreservingUnrestrictedGrammar < SymbolType > & grammar ) {
	out.emplace_back ( "rules", sax::Token::TokenType::START_ELEMENT );

	for ( const auto & rule : grammar.getRules ( ) )

		for ( const auto & rhs : rule.second ) {
			out.emplace_back ( "rule", sax::Token::TokenType::START_ELEMENT );

			grammar::GrammarToXMLComposer::composeRuleLContext ( out, std::get < 0 > ( rule.first ) );
			grammar::GrammarToXMLComposer::composeRuleSingleSymbolLHS ( out, std::get < 1 > ( rule.first ) );
			grammar::GrammarToXMLComposer::composeRuleRContext ( out, std::get < 2 > ( rule.first ) );
			grammar::GrammarToXMLComposer::composeRuleRHS ( out, rhs );

			out.emplace_back ( "rule", sax::Token::TokenType::END_ELEMENT );
		}

	out.emplace_back ( "rules", sax::Token::TokenType::END_ELEMENT );
}

} /* namespace core */

#endif /* _XML_CONTEXT_PRESERVING_UNRESTRICTED_GRAMMAR_H_ */
