/*
 * CSG.cpp
 *
 *  Created on: Nov 17, 2013
 *      Author: Jan Travnicek
 */

#include "CSG.h"

#include <registration/ValuePrinterRegistration.hpp>
#include <registration/CastRegistration.hpp>

template class grammar::CSG < >;

namespace {

auto valuePrinter = registration::ValuePrinterRegister < grammar::CSG < > > ( );

} /* namespace */
