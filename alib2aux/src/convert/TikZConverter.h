/*
 * TikZConverter.h
 *
 *  Created on: Dec 1, 2015
 *      Author: Jan Travnicek
 */

#ifndef TIKZ_CONVERTER_H_
#define TIKZ_CONVERTER_H_

#include <ostream>
#include <alib/set>
#include <alib/list>
#include <alib/vector>
#include <alib/typeinfo>
#include <alib/map>
#include <alib/utility>

#include <exception/CommonException.h>
#include <string/String.h>

#include <automaton/FSM/NFA.h>
#include <automaton/FSM/EpsilonNFA.h>
#include <automaton/FSM/MultiInitialStateNFA.h>
#include <automaton/FSM/DFA.h>
#include <automaton/FSM/ExtendedNFA.h>
#include <automaton/FSM/CompactNFA.h>
#include <automaton/TA/NFTA.h>
#include <automaton/TA/DFTA.h>
#include <automaton/PDA/NPDA.h>
#include <automaton/PDA/SinglePopNPDA.h>
#include <automaton/PDA/InputDrivenDPDA.h>
#include <automaton/PDA/InputDrivenNPDA.h>
#include <automaton/PDA/VisiblyPushdownDPDA.h>
#include <automaton/PDA/VisiblyPushdownNPDA.h>
#include <automaton/PDA/RealTimeHeightDeterministicDPDA.h>
#include <automaton/PDA/RealTimeHeightDeterministicNPDA.h>
#include <automaton/PDA/DPDA.h>
#include <automaton/PDA/SinglePopDPDA.h>
#include <automaton/TM/OneTapeDTM.h>
#include <grid/GridClasses.hpp>
#include <rte/formal/FormalRTE.h>
#include <rte/formal/FormalRTEElements.h>

#include "common/converterCommon.hpp"

#include <factory/StringDataFactory.hpp>
#include <string/string/LinearString.h>
#include <regexp/string/UnboundedRegExp.h>

namespace convert {

class TikZConverter {
	template < class SymbolType, class StateType >
	static void transitions(const automaton::DFA < SymbolType, StateType > & fsm, const ext::map < StateType, int > & states, std::ostream & out);

	template < class SymbolType, class StateType >
	static void transitions(const automaton::NFA < SymbolType, StateType > & fsm, const ext::map < StateType, int > & states, std::ostream & out);

	template < class SymbolType, class StateType >
	static void transitions(const automaton::MultiInitialStateNFA < SymbolType, StateType > & fsm, const ext::map < StateType, int > & states, std::ostream & out);

	template < class SymbolType, class StateType >
	static void transitions(const automaton::EpsilonNFA < SymbolType, StateType > & fsm, const ext::map < StateType, int > & states, std::ostream & out);

	template < class SymbolType, class StateType >
	static void transitions(const automaton::ExtendedNFA < SymbolType, StateType > & fsm, const ext::map < StateType, int > & states, std::ostream & out);

	template < class SymbolType, class StateType >
	static void transitions(const automaton::CompactNFA < SymbolType, StateType > & fsm, const ext::map < StateType, int > & states, std::ostream & out);

	template < class SymbolType, class StateType >
	static void transitions(const automaton::NFTA < SymbolType, StateType > & fta, const ext::map < StateType, int > & states, std::ostream & out);

	template < class SymbolType, class StateType >
	static void transitions(const automaton::DFTA < SymbolType, StateType > & fta, const ext::map < StateType, int > & states, std::ostream & out);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void transitions(const automaton::DPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void transitions(const automaton::NPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void transitions(const automaton::SinglePopDPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void transitions(const automaton::SinglePopNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void transitions(const automaton::InputDrivenDPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void transitions(const automaton::InputDrivenNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void transitions(const automaton::VisiblyPushdownDPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void transitions(const automaton::VisiblyPushdownNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void transitions(const automaton::RealTimeHeightDeterministicDPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void transitions(const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out);

	template < class SymbolType, class StateType >
	static void transitions(const automaton::OneTapeDTM < SymbolType, StateType > & tm, const ext::map < StateType, int > & states, std::ostream & out);

	template<typename TGrid>
	static void grid(std::ostream &out, const TGrid &a);

public:
	template < class SymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::DFA < SymbolType, StateType > & a);

	template < class SymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::NFA < SymbolType, StateType > & a);

	template < class SymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::MultiInitialStateNFA < SymbolType, StateType >& a);

	template < class SymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::EpsilonNFA < SymbolType, StateType > & a);

	template < class SymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::ExtendedNFA < SymbolType, StateType > & a);

	template < class SymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::CompactNFA < SymbolType, StateType > & a);

	template < class SymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::NFTA < SymbolType, StateType > & a);

	template < class SymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::DFTA < SymbolType, StateType > & a);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::DPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::NPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::SinglePopDPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::SinglePopNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::InputDrivenDPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::InputDrivenNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::VisiblyPushdownDPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::VisiblyPushdownNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::RealTimeHeightDeterministicDPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a);

	template < class InputSymbolType, class PushdownStoreSymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a);

	template < class SymbolType, class StateType >
	static void convert(std::ostream& out, const automaton::OneTapeDTM < SymbolType, StateType > & a);

	template<typename TCoordinate, typename TEdge>
	static void convert(std::ostream &out, const grid::SquareGrid4<TCoordinate, TEdge> &a) {
		return grid(out, a);
	}

	template<typename TCoordinate, typename TEdge>
	static void convert(std::ostream &out, const grid::SquareGrid8<TCoordinate, TEdge> &a) {
		return grid(out, a);
	}

	template<typename TCoordinate, typename TEdge>
	static void convert(std::ostream &out, const grid::WeightedSquareGrid4<TCoordinate, TEdge> &a) {
		return grid(out, a);
	}

	template<typename TCoordinate, typename TEdge>
	static void convert(std::ostream &out, const grid::WeightedSquareGrid8<TCoordinate, TEdge> &a) {
		return grid(out, a);
	}

	template < class T >
	static std::string convert ( const T & automaton ) {
		std::stringstream ss;
		convert ( ss, automaton );
		return ss.str ( );
	}

	template < class SymbolType >
	class FormalRTEVisitor {
		public:
			static void visit ( const rte::FormalRTEAlternation < SymbolType > & node, std::ostringstream & os );
			static void visit ( const rte::FormalRTESubstitution < SymbolType > & node, std::ostringstream & os );
			static void visit ( const rte::FormalRTEIteration < SymbolType > & node, std::ostringstream & os );
			static void visit ( const rte::FormalRTESymbolAlphabet < SymbolType > & node, std::ostringstream & os );
			static void visit ( const rte::FormalRTESymbolSubst < SymbolType > & node, std::ostringstream & os );
			static void visit ( const rte::FormalRTEEmpty < SymbolType > & node, std::ostringstream & os );
	};
	template < class SymbolType >
	static void convert ( std::ostream& out, const rte::FormalRTE < SymbolType > & rte );
};

template<class SymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::EpsilonNFA < SymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialState ( ) == state.first )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class SymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::MultiInitialStateNFA < SymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialStates ( ).count ( state.first ) )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class SymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::NFA < SymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialState ( ) == state.first )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class SymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::DFA < SymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialState ( ) == state.first )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class SymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::ExtendedNFA < SymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialState ( ) == state.first )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class SymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::CompactNFA < SymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialState ( ) == state.first )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class SymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::NFTA < SymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class SymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::DFTA < SymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::DPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialState ( ) == state.first )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::SinglePopDPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialState ( ) == state.first )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::InputDrivenDPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialState ( ) == state.first )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::InputDrivenNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialState ( ) == state.first )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::VisiblyPushdownDPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialState ( ) == state.first )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::VisiblyPushdownNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialStates ( ).count ( state.first ) )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::RealTimeHeightDeterministicDPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialState ( ) == state.first )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialStates ( ).count ( state.first ) )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::NPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialState ( ) == state.first )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::SinglePopNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialState ( ) == state.first )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class SymbolType, class StateType>
void TikZConverter::convert ( std::ostream & out, const automaton::OneTapeDTM < SymbolType, StateType > & a ) {
	out << "\\begin{tikzpicture}\n";
	int cnt = 1;

	 // Map states to indices
	ext::map < StateType, int > states;

	for ( const StateType & state : a.getStates ( ) )
		states.insert ( std::make_pair ( state, cnt++ ) );

	 // Print states
	for ( const auto & state : states ) {
		std::string mods;

		if ( a.getFinalStates ( ).count ( state.first ) )
			mods += ",accepting";

		if ( a.getInitialState ( ) == state.first )
			mods += ",initial";

		out << "\\node[state" + mods + "] (" << state.second << ") {" << replace ( factory::StringDataFactory::toString ( state.first ), "\"", "\\\"" ) << "}\n";
	}

	transitions ( a, states, out );
	out << "\\end{tikzpicture}";
}

template<class SymbolType, class StateType>
void TikZConverter::transitions ( const automaton::EpsilonNFA < SymbolType, StateType > & fsm, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	 // put transitions from automaton to "transitions"
	for ( const auto & transition : fsm.getTransitions ( ) ) {
		std::string symbol;

		if ( transition.first.second.is_epsilon ( ) )
			symbol = "&epsilon;";
		else
			symbol = replace ( factory::StringDataFactory::toString ( std::get < 1 > ( transition.first ).getSymbol ( ) ), "\"", "\\\"" );

		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( transition.second )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<class SymbolType, class StateType>
void TikZConverter::transitions ( const automaton::MultiInitialStateNFA < SymbolType, StateType > & fsm, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	 // put transitions from automaton to "transitions"
	for ( const auto & transition : fsm.getTransitions ( ) ) {
		std::string symbol = replace ( factory::StringDataFactory::toString ( transition.first.second ), "\"", "\\\"" );

		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( transition.second )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<class SymbolType, class StateType>
void TikZConverter::transitions ( const automaton::NFA < SymbolType, StateType > & fsm, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	 // put transitions from automaton to "transitions"
	for ( const auto & transition : fsm.getTransitions ( ) ) {
		std::string symbol = replace ( factory::StringDataFactory::toString ( transition.first.second ), "\"", "\\\"" );

		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( transition.second )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<class SymbolType, class StateType>
void TikZConverter::transitions ( const automaton::DFA < SymbolType, StateType > & fsm, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	 // put transitions from automaton to "transitions"
	for ( const auto & transition : fsm.getTransitions ( ) ) {
		std::string symbol = replace ( factory::StringDataFactory::toString ( transition.first.second ), "\"", "\\\"" );

		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( transition.second )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<class SymbolType, class StateType>
void TikZConverter::transitions ( const automaton::ExtendedNFA < SymbolType, StateType > & fsm, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	 // put transitions from automaton to "transitions"
	for ( const auto & transition : fsm.getTransitions ( ) ) {
		std::string symbol = replace ( factory::StringDataFactory::toString ( transition.first.second ), "\"", "\\\"" );

		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( transition.second )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<class SymbolType, class StateType>
void TikZConverter::transitions ( const automaton::CompactNFA < SymbolType, StateType > & fsm, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	 // put transitions from automaton to "transitions"
	for ( const auto & transition : fsm.getTransitions ( ) ) {
		std::string symbol = replace ( factory::StringDataFactory::toString ( string::stringFrom ( transition.first.second ) ), "\"", "\\\"" );

		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( transition.second )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<class SymbolType, class StateType>
void TikZConverter::transitions ( const automaton::NFTA < SymbolType, StateType > & fta, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, ext::vector < int > >, std::string > transitions;

	 // put transitions from automaton to "transitions"
	for ( const auto & transition : fta.getTransitions ( ) ) {
		std::string symbol = replace ( factory::StringDataFactory::toString ( transition.first.first.getSymbol ( ) ), "\"", "\\\"" );
		symbol += ext::to_string ( transition.first.first.getRank ( ) );

		std::pair < int, ext::vector < int > > key ( states.find ( transition.second )->second, { } );

		for ( const StateType & state : transition.first.second )
			key.second.push_back ( states.find ( state )->second );

		ext::map < std::pair < int, ext::vector < int > >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	 // Print auxilary dots
	for ( unsigned i = 1; i < transitions.size ( ); i++ )
		out << "\\node[draw=none,fill=none] (" << states.size ( ) + i << ") {}\n";

	out << "\\path[->]";

	 // print the map
	unsigned i = states.size ( ) + 1;

	for ( std::pair < const std::pair < int, ext::vector < int > >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << i << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.first << ")\n";

		unsigned j = 0;

		for ( int from : transition.first.second ) {
			out << "(" << from << ") edge [left] node [align=center] ";
			out << "{$" << j << "$}";
			out << "(" << i << ")\n";

			j++;
		}

		i++;
	}
}

template<class SymbolType, class StateType>
void TikZConverter::transitions ( const automaton::DFTA < SymbolType, StateType > & fta, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, ext::vector < int > >, std::string > transitions;

	 // put transitions from automaton to "transitions"
	for ( const auto & transition : fta.getTransitions ( ) ) {
		std::string symbol = replace ( factory::StringDataFactory::toString ( transition.first.first.getSymbol ( ) ), "\"", "\\\"" );
		symbol += ext::to_string ( transition.first.first.getRank ( ) );

		std::pair < int, ext::vector < int > > key ( states.find ( transition.second )->second, { } );

		for ( const StateType & state : transition.first.second )
			key.second.push_back ( states.find ( state )->second );

		ext::map < std::pair < int, ext::vector < int > >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	 // Print auxilary dots
	for ( unsigned i = 1; i < transitions.size ( ); i++ )
		out << "\\node[draw=none,fill=none] (" << states.size ( ) + i << ") {}\n";

	out << "\\path[->]";

	 // print the map
	unsigned i = states.size ( ) + 1;

	for ( std::pair < const std::pair < int, ext::vector < int > >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << i << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.first << ")\n";

		unsigned j = 0;

		for ( int from : transition.first.second ) {
			out << "(" << from << ") edge [left] node [align=center] ";
			out << "{$" << j << "$}";
			out << "(" << i << ")\n";

			j++;
		}

		i++;
	}
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::transitions ( const automaton::DPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	for ( const auto & transition : pda.getTransitions ( ) ) {
		std::string symbol;

		 // input symbol
		if ( std::get < 1 > ( transition.first ).is_epsilon ( ) )
			symbol = "&epsilon;";
		else
			symbol = replace ( factory::StringDataFactory::toString ( std::get < 1 > ( transition.first ).getSymbol ( ) ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		if ( std::get < 2 > ( transition.first ).empty ( ) )
			symbol += " &epsilon;";
		else
			for ( const PushdownStoreSymbolType & symb : std::get < 2 > ( transition.first ) )
				symbol += " " + replace ( factory::StringDataFactory::toString ( symb ), "\"", "\\\"" );

		symbol += " ->";

		 // Push part
		if ( transition.second.second.empty ( ) )
			symbol += " &epsilon;";
		else
			for ( const PushdownStoreSymbolType & symb : transition.second.second )
				symbol += " " + replace ( factory::StringDataFactory::toString ( symb ), "\"", "\\\"" );

		 // Insert into map
		std::pair < int, int > key ( states.find ( std::get < 0 > ( transition.first ) )->second, states.find ( transition.second.first )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::transitions ( const automaton::SinglePopDPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	for ( const auto & transition : pda.getTransitions ( ) ) {
		std::string symbol;

		 // input symbol
		if ( std::get < 1 > ( transition.first ).is_epsilon ( ) )
			symbol = "&epsilon;";
		else
			symbol = replace ( factory::StringDataFactory::toString ( std::get < 1 > ( transition.first ).getSymbol ( ) ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		symbol += " " + replace ( factory::StringDataFactory::toString ( std::get < 2 > ( transition.first ) ), "\"", "\\\"" );

		symbol += " ->";

		 // Push part
		if ( transition.second.second.empty ( ) )
			symbol += " &epsilon;";
		else
			for ( const PushdownStoreSymbolType & symb : transition.second.second )
				symbol += " " + replace ( factory::StringDataFactory::toString ( symb ), "\"", "\\\"" );

		 // Insert into map
		std::pair < int, int > key ( states.find ( std::get < 0 > ( transition.first ) )->second, states.find ( transition.second.first )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::transitions ( const automaton::InputDrivenDPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	const auto & symbolToPDSOperation = pda.getPushdownStoreOperations ( );

	for ( const auto & transition : pda.getTransitions ( ) ) {
		const auto & pop = symbolToPDSOperation.find ( transition.first.second )->second.first;
		const auto & push = symbolToPDSOperation.find ( transition.first.second )->second.second;

		 // input symbol
		std::string symbol = replace ( factory::StringDataFactory::toString ( transition.first.second ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		if ( pop.empty ( ) )
			symbol += " &epsilon;";
		else
			for ( const PushdownStoreSymbolType & symb : pop )
				symbol += " " + replace ( factory::StringDataFactory::toString ( symb ), "\"", "\\\"" );

		symbol += " ->";

		const auto & to = transition.second;

		 // Push part
		if ( push.empty ( ) )
			symbol += " &epsilon;";
		else
			for ( const PushdownStoreSymbolType & symb : push )
				symbol += " " + replace ( factory::StringDataFactory::toString ( symb ), "\"", "\\\"" );

		 // Insert into map
		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( to )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::transitions ( const automaton::InputDrivenNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	const auto & symbolToPDSOperation = pda.getPushdownStoreOperations ( );

	for ( const auto & transition : pda.getTransitions ( ) ) {
		const auto & pop = symbolToPDSOperation.find ( transition.first.second )->second.first;
		const auto & push = symbolToPDSOperation.find ( transition.first.second )->second.second;

		 // input symbol
		std::string symbol = replace ( factory::StringDataFactory::toString ( transition.first.second ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		if ( pop.empty ( ) )
			symbol += " &epsilon;";
		else
			for ( const PushdownStoreSymbolType & symb : pop )
				symbol += " " + replace ( factory::StringDataFactory::toString ( symb ), "\"", "\\\"" );

		symbol += " ->";

		 // Push part
		if ( push.empty ( ) )
			symbol += " &epsilon;";
		else
			for ( const PushdownStoreSymbolType & symb : push )
				symbol += " " + replace ( factory::StringDataFactory::toString ( symb ), "\"", "\\\"" );

		 // Insert into map
		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( transition.second )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::transitions ( const automaton::VisiblyPushdownDPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	for ( const auto & transition : pda.getCallTransitions ( ) ) {
		 // input symbol
		std::string symbol = replace ( factory::StringDataFactory::toString ( transition.first.second ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		symbol += " &epsilon;";
		symbol += " ->";

		symbol += " " + replace ( factory::StringDataFactory::toString ( transition.second.second ), "\"", "\\\"" );

		 // Insert into map
		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( transition.second.first )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	for ( const auto & transition : pda.getReturnTransitions ( ) ) {
		 // input symbol
		std::string symbol = replace ( factory::StringDataFactory::toString ( std::get < 1 > ( transition.first ) ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		symbol += " " + replace ( factory::StringDataFactory::toString ( std::get < 2 > ( transition.first ) ), "\"", "\\\"" );
		symbol += " ->";

		symbol += " &epsilon;";

		 // Insert into map
		std::pair < int, int > key ( states.find ( std::get < 0 > ( transition.first ) )->second, states.find ( transition.second )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	for ( const auto & transition : pda.getLocalTransitions ( ) ) {
		 // input symbol
		std::string symbol = replace ( factory::StringDataFactory::toString ( transition.first.second ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		symbol += " &epsilon;";
		symbol += " ->";

		symbol += " &epsilon;";

		 // Insert into map
		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( transition.second )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::transitions ( const automaton::VisiblyPushdownNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	for ( const auto & transition : pda.getCallTransitions ( ) ) {
		 // input symbol
		std::string symbol = replace ( factory::StringDataFactory::toString ( transition.first.second ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		symbol += " &epsilon;";
		symbol += " ->";

		symbol += " " + replace ( factory::StringDataFactory::toString ( transition.second.second ), "\"", "\\\"" );

		 // Insert into map
		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( transition.second.first )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	for ( const auto & transition : pda.getReturnTransitions ( ) ) {
		 // input symbol
		std::string symbol = replace ( factory::StringDataFactory::toString ( std::get < 1 > ( transition.first ) ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		symbol += " " + replace ( factory::StringDataFactory::toString ( std::get < 2 > ( transition.first ) ), "\"", "\\\"" );
		symbol += " ->";

		symbol += " &epsilon;";

		 // Insert into map
		std::pair < int, int > key ( states.find ( std::get < 0 > ( transition.first ) )->second, states.find ( transition.second )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	for ( const auto & transition : pda.getLocalTransitions ( ) ) {
		std::string symbol;

		 // input symbol
		symbol = replace ( factory::StringDataFactory::toString ( transition.first.second ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		symbol += " &epsilon;";
		symbol += " ->";

		symbol += " &epsilon;";

		 // Insert into map
		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( transition.second )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::transitions ( const automaton::RealTimeHeightDeterministicDPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	for ( const auto & transition : pda.getCallTransitions ( ) ) {
		std::string symbol;

		 // input symbol
		if ( transition.first.second.is_epsilon ( ) )
			symbol = "&epsilon";
		else
			symbol = replace ( factory::StringDataFactory::toString ( transition.first.second.getSymbol ( ) ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		symbol += " &epsilon;";
		symbol += " ->";

		symbol += " " + replace ( factory::StringDataFactory::toString ( transition.second.second ), "\"", "\\\"" );

		 // Insert into map
		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( transition.second.first )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	for ( const auto & transition : pda.getReturnTransitions ( ) ) {
		std::string symbol;

		 // input symbol
		if ( std::get < 1 > ( transition.first ).is_epsilon ( ) )
			symbol = "&epsilon";
		else
			symbol = replace ( factory::StringDataFactory::toString ( std::get < 1 > ( transition.first ).getSymbol ( ) ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		symbol += " " + replace ( factory::StringDataFactory::toString ( std::get < 2 > ( transition.first ) ), "\"", "\\\"" );
		symbol += " ->";

		symbol += " &epsilon;";

		 // Insert into map
		std::pair < int, int > key ( states.find ( std::get < 0 > ( transition.first ) )->second, states.find ( transition.second )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	for ( const auto & transition : pda.getLocalTransitions ( ) ) {
		std::string symbol;

		 // input symbol
		if ( transition.first.second.is_epsilon ( ) )
			symbol = "&epsilon";
		else
			symbol = replace ( factory::StringDataFactory::toString ( transition.first.second.getSymbol ( ) ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		symbol += " &epsilon;";
		symbol += " ->";

		symbol += " &epsilon;";

		 // Insert into map
		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( transition.second )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::transitions ( const automaton::RealTimeHeightDeterministicNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	for ( const auto & transition : pda.getCallTransitions ( ) ) {
		std::string symbol;

		 // input symbol
		if ( transition.first.second.is_epsilon ( ) )
			symbol = "&epsilon";
		else
			symbol = replace ( factory::StringDataFactory::toString ( transition.first.second.getSymbol ( ) ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		symbol += " &epsilon;";
		symbol += " ->";

		symbol += " " + replace ( factory::StringDataFactory::toString ( transition.second.second ), "\"", "\\\"" );

		 // Insert into map
		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( transition.second.first )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	for ( const auto & transition : pda.getReturnTransitions ( ) ) {
		std::string symbol;

		 // input symbol
		if ( std::get < 1 > ( transition.first ).is_epsilon ( ) )
			symbol = "&epsilon;";
		else
			symbol = replace ( factory::StringDataFactory::toString ( std::get < 1 > ( transition.first ).getSymbol ( ) ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		symbol += " " + replace ( factory::StringDataFactory::toString ( std::get < 2 > ( transition.first ) ), "\"", "\\\"" );
		symbol += " ->";

		symbol += " &epsilon;";

		 // Insert into map
		std::pair < int, int > key ( states.find ( std::get < 0 > ( transition.first ) )->second, states.find ( transition.second )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	for ( const auto & transition : pda.getLocalTransitions ( ) ) {
		std::string symbol;

		 // input symbol
		if ( transition.first.second.is_epsilon ( ) )
			symbol = "&epsilon;";
		else
			symbol = replace ( factory::StringDataFactory::toString ( transition.first.second.getSymbol ( ) ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		symbol += " &epsilon;";
		symbol += " ->";

		symbol += " &epsilon;";

		 // Insert into map
		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( transition.second )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::transitions ( const automaton::NPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	for ( const auto & transition : pda.getTransitions ( ) ) {
		std::string symbol;

		 // input symbol
		if ( std::get < 1 > ( transition.first ).is_epsilon ( ) )
			symbol = "&epsilon;";
		else
			symbol = replace ( factory::StringDataFactory::toString ( std::get < 1 > ( transition.first ).getSymbol ( ) ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		if ( std::get < 2 > ( transition.first ).empty ( ) )
			symbol += " &epsilon;";
		else
			for ( const PushdownStoreSymbolType & symb : std::get < 2 > ( transition.first ) )
				symbol += " " + replace ( factory::StringDataFactory::toString ( symb ), "\"", "\\\"" );

		symbol += " ->";

		 // Push part
		if ( transition.second.second.empty ( ) )
			symbol += " &epsilon;";
		else
			for ( const PushdownStoreSymbolType & symb : transition.second.second )
				symbol += " " + replace ( factory::StringDataFactory::toString ( symb ), "\"", "\\\"" );

		 // Insert into map
		std::pair < int, int > key ( states.find ( std::get < 0 > ( transition.first ) )->second, states.find ( transition.second.first )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<class InputSymbolType, class PushdownStoreSymbolType, class StateType>
void TikZConverter::transitions ( const automaton::SinglePopNPDA < InputSymbolType, PushdownStoreSymbolType, StateType > & pda, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	for ( const auto & transition : pda.getTransitions ( ) ) {
		std::string symbol;

		 // input symbol
		if ( std::get < 1 > ( transition.first ).is_epsilon ( ) )
			symbol = "&epsilon;";
		else
			symbol = replace ( factory::StringDataFactory::toString ( std::get < 1 > ( transition.first ).getSymbol ( ) ), "\"", "\\\"" );

		symbol += " |";

		 // Pop part
		symbol += " " + replace ( factory::StringDataFactory::toString ( std::get < 2 > ( transition.first ) ), "\"", "\\\"" );

		symbol += " ->";

		 // Push part
		if ( transition.second.second.empty ( ) )
			symbol += " &epsilon;";
		else
			for ( const PushdownStoreSymbolType & symb : transition.second.second )
				symbol += " " + replace ( factory::StringDataFactory::toString ( symb ), "\"", "\\\"" );

		 // Insert into map
		std::pair < int, int > key ( states.find ( std::get < 0 > ( transition.first ) )->second, states.find ( transition.second.first )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<class SymbolType, class StateType>
void TikZConverter::transitions ( const automaton::OneTapeDTM < SymbolType, StateType > & tm, const ext::map < StateType, int > & states, std::ostream & out ) {
	ext::map < std::pair < int, int >, std::string > transitions;

	for ( const auto & transition : tm.getTransitions ( ) ) {
		std::string symbol;

		 // input symbol
		symbol	= "(";
		symbol += replace ( factory::StringDataFactory::toString ( transition.first.second ), "\"", "\\\"" );
		symbol += ", ";
		symbol += replace ( factory::StringDataFactory::toString ( std::get < 1 > ( transition.second ) ), "\"", "\\\"" );
		symbol += " ";

		switch ( std::get < 2 > ( transition.second ) ) {
		case automaton::Shift::LEFT:
			symbol += "&larr;";
			break;

		case automaton::Shift::RIGHT:
			symbol += "&rarr;";
			break;

		case automaton::Shift::NONE:
			symbol += "&times;";
			break;

		default:
			throw exception::CommonException ( "Unexpected shift direction" );
		}

		 // Insert into map
		std::pair < int, int > key ( states.find ( transition.first.first )->second, states.find ( std::get < 0 > ( transition.second ) )->second );
		ext::map < std::pair < int, int >, std::string >::iterator mapit = transitions.find ( key );

		if ( mapit == transitions.end ( ) ) {
			transitions.insert ( std::make_pair ( key, symbol ) );
		} else {
			mapit->second += ",";

			size_t pos = mapit->second.find_last_of ( "\n" );

			if ( pos == std::string::npos ) pos = 0;

			if ( mapit->second.size ( ) - pos > 100 )
				mapit->second += "\n";
			else
				mapit->second += " ";

			mapit->second += symbol;
		}
	}

	out << "\\path[->]";

	 // print the map
	for ( std::pair < const std::pair < int, int >, std::string > & transition : transitions ) {
		replaceInplace ( transition.second, "\n", "\\n" );

		out << "(" << transition.first.first << ") edge [left] node [align=center] ";
		out << "{$" << transition.second << "$}";
		out << "(" << transition.first.second << ")\n";
	}
}

template<typename TGrid>
void TikZConverter::grid(std::ostream &out, const TGrid &a) {
	out << "\\begin{tikzpicture}[ultra thin/.style= {line width=0.1pt}]\n";
	long width = a.getWidth();
	long height = a.getHeight();

	out << "\t\\draw[step=1cm, gray,very thin] (0,0) grid (" << width << "," << height << ");\n";

	for (const auto &node : a.getObstacleList()) {
		out << "\t\\fill[draw=gray, fill=black, very thin] ("
			<< node.second << "," << height - node.first - 1
			<< ") rectangle ("
			<< node.second + 1 << "," << height - node.first << ");\n";
	}

	out << "\\end{tikzpicture}\n";
}

template < class SymbolType >
void TikZConverter::convert ( std::ostream& out, const rte::FormalRTE < SymbolType > & rte ) {
	std::ostringstream oss;
	out << "\\begin{forest}\n";

	rte.getRTE ( ).getStructure ( ).template accept < void, TikZConverter::FormalRTEVisitor < SymbolType > > ( oss );
	out << "    " << oss.str ( ) << "\n";

	out << "\\end{forest}" << std::endl;
}

template < class SymbolType >
void TikZConverter::FormalRTEVisitor < SymbolType >::visit ( const rte::FormalRTEAlternation    < SymbolType > & node, std::ostringstream & oss ) {
	oss << "[$+$" ;
	node.getLeftElement ( ).template accept < void, TikZConverter::FormalRTEVisitor < SymbolType > > ( oss );
	node.getRightElement ( ).template accept < void, TikZConverter::FormalRTEVisitor < SymbolType > > ( oss );
	oss << "]";
}

template < class SymbolType >
void TikZConverter::FormalRTEVisitor < SymbolType >::visit ( const rte::FormalRTESubstitution   < SymbolType > & node, std::ostringstream & oss ) {
	oss << "[$\\cdot\\square_" << node.getSubstitutionSymbol ( ).getSymbol ( ).getSymbol ( ) << "$ ";
	node.getLeftElement ( ).template accept < void, TikZConverter::FormalRTEVisitor < SymbolType > > ( oss );
	node.getRightElement ( ).template accept < void, TikZConverter::FormalRTEVisitor < SymbolType > > ( oss );
	oss << "]";
}

template < class SymbolType >
void TikZConverter::FormalRTEVisitor < SymbolType >::visit ( const rte::FormalRTEIteration      < SymbolType > & node, std::ostringstream & oss ) {
	oss << "[${}^{*,\\square_" << node.getSubstitutionSymbol ( ).getSymbol ( ).getSymbol ( ) << "}$ ";
	node.getElement ( ).template accept < void, TikZConverter::FormalRTEVisitor < SymbolType > > ( oss );
	oss << "]";
}

template < class SymbolType >
void TikZConverter::FormalRTEVisitor < SymbolType >::visit ( const rte::FormalRTESymbolAlphabet < SymbolType > & node, std::ostringstream & oss ) {
	oss << "[$" << node.getSymbol ( ).getSymbol ( ) << "$ ";
	for ( const auto & child : node.getChildren ( ) )
		child.template accept < void, TikZConverter::FormalRTEVisitor < SymbolType > > ( oss );
	oss << "]";
}

template < class SymbolType >
void TikZConverter::FormalRTEVisitor < SymbolType >::visit ( const rte::FormalRTESymbolSubst    < SymbolType > & node, std::ostringstream & oss ) {
	oss << "[$\\square_" << node.getSymbol ( ).getSymbol ( ) << "$]";
}

template < class SymbolType >
void TikZConverter::FormalRTEVisitor < SymbolType >::visit ( const rte::FormalRTEEmpty          < SymbolType > & /* node */, std::ostringstream & oss ) {
	oss << "[$\\emptyset$]";
}

} /* namespace convert */

#endif /* TIKZ_CONVERTER_H_ */
