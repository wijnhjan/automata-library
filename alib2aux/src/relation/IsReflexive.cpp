/*
* IsReflexive.cpp
 *
 *  Created on: 2. 10. 2019
 *	  Author: Jan Travnicek
 */

#include "IsReflexive.h"
#include <registration/AlgoRegistration.hpp>

#include <object/Object.h>

namespace {

auto IsReflexive = registration::AbstractRegister < relation::IsReflexive, bool, const ext::set < ext::pair < object::Object, object::Object > > &, const ext::set < object::Object > & > ( relation::IsReflexive::isReflexive, "relation", "universe" ).setDocumentation (
"Checks whether a relation is reflexive\n\
\n\
@param relation the tested relation\n\
@param universe the universe of items participating in the relation\n\
\n\
@return true if the relation is reflexive, false otherwise");

} /* namespace */
