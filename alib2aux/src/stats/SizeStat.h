/*
 * SizeStat.h
 *
 *  Created on: 20. 9. 2014
 *	  Author: Jan Travnicek
 */

#ifndef _SIZE_STAT_H_
#define _SIZE_STAT_H_

#include <alib/set>

namespace stats {

class SizeStat {
public:
	template < class T >
	static unsigned stat ( const ext::set < T > & object );
};

template < class T >
unsigned SizeStat::stat ( const ext::set < T > & object ) {
	return object.size ( );
}

}

#endif /* _SIZE_STAT_H_ */
