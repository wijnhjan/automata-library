/*
 * TreeCompare.h
 *
 *  Created on: Feb 9, 2019
 *      Author: Tomas Pecka
 */

#ifndef TREE_COMPARE_H_
#define TREE_COMPARE_H_

#include <tree/ranked/RankedTree.h>

namespace compare {

class TreeCompare {
public:
	template < class SymbolType >
	static bool compare ( const tree::RankedTree < SymbolType > & a, const tree::RankedTree < SymbolType > & b ) {
		return a == b;
	}
};

} /* namespace compare */

#endif /* TREE_COMPARE_H_ */
