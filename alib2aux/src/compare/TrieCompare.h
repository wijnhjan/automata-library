/*
 * TrieCompare.h
 *
 *  Created on: Jan 15, 2019
 *      Author: Tomas Pecka
 */

#ifndef TRIE_COMPARE_H_
#define TRIE_COMPARE_H_

#include <alib/trie>
#include <common/DefaultSymbolType.h>

namespace compare {

class TrieCompare {
public:
	template < class R, class T >
	static bool compare ( const ext::trie < R, T > & a, const ext::trie < R, T > & b ) {
		return a == b;
	}
};

} /* namespace compare */

#endif /* TRIE_COMPARE_H_ */
