/*
 * AutomatonDiff.cpp
 *
 *  Created on: Apr 1, 2013
 *      Author: martin
 */

#include "AutomatonDiff.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto AutomatonDiffDFA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::DFA < > &, const automaton::DFA < > & > ( compare::AutomatonDiff::diff );
auto AutomatonDiffNFA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::NFA < > & , const automaton::NFA < > & > ( compare::AutomatonDiff::diff );
auto AutomatonDiffMultiInitialStateNFA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::MultiInitialStateNFA < > &, const automaton::MultiInitialStateNFA < > & > ( compare::AutomatonDiff::diff );
auto AutomatonDiffEpsilonNFA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::EpsilonNFA < > &, const automaton::EpsilonNFA < > & > ( compare::AutomatonDiff::diff );
auto AutomatonDiffExtendedNFA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::ExtendedNFA < > &, const automaton::ExtendedNFA < > & > ( compare::AutomatonDiff::diff );
auto AutomatonDiffCompactDFA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::CompactDFA < > &, const automaton::CompactDFA < > & > ( compare::AutomatonDiff::diff );
auto AutomatonDiffCompactNFA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::CompactNFA < > &, const automaton::CompactNFA < > & > ( compare::AutomatonDiff::diff );

auto AutomatonDiffDFTA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::DFTA < > &, const automaton::DFTA < > & > ( compare::AutomatonDiff::diff );
auto AutomatonDiffNFTA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::NFTA < > &, const automaton::NFTA < > & > ( compare::AutomatonDiff::diff );

auto AutomatonDiffDPDA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::DPDA < > &, const automaton::DPDA < > & > ( compare::AutomatonDiff::diff );
auto AutomatonDiffNPDA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::NPDA < > &, const automaton::NPDA < > & > ( compare::AutomatonDiff::diff );
auto AutomatonDiffInputDrivenDPDA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::InputDrivenDPDA < > &, const automaton::InputDrivenDPDA < > & > ( compare::AutomatonDiff::diff );
auto AutomatonDiffInputDrivenNPDA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::InputDrivenNPDA < > &, const automaton::InputDrivenNPDA < > & > ( compare::AutomatonDiff::diff );
auto AutomatonDiffVisiblyPushdownDPDA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::VisiblyPushdownDPDA < > &, const automaton::VisiblyPushdownDPDA < > & > ( compare::AutomatonDiff::diff );
auto AutomatonDiffVisiblyPushdownNPDA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::VisiblyPushdownNPDA < > &, const automaton::VisiblyPushdownNPDA < > & > ( compare::AutomatonDiff::diff );
auto AutomatonDiffRealTimeHeightDeterministicDPDA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::RealTimeHeightDeterministicDPDA < > &, const automaton::RealTimeHeightDeterministicDPDA < > & > ( compare::AutomatonDiff::diff );
auto AutomatonDiffRealTimeHeightDeterministicNPDA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::RealTimeHeightDeterministicNPDA < > &, const automaton::RealTimeHeightDeterministicNPDA < > & > ( compare::AutomatonDiff::diff );
auto AutomatonDiffSinglePopDPDA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::SinglePopDPDA < > &, const automaton::SinglePopDPDA < > & > ( compare::AutomatonDiff::diff );
auto AutomatonDiffSinglePopNPDA = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::SinglePopNPDA < > &, const automaton::SinglePopNPDA < > & > ( compare::AutomatonDiff::diff );

auto AutomatonDiffOneTapeDTM = registration::AbstractRegister < compare::AutomatonDiff, std::string, const automaton::OneTapeDTM < > &, const automaton::OneTapeDTM < > & > ( compare::AutomatonDiff::diff );

} /* namespace */
