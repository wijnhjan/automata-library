/*
 * VectorCompare.cpp
 *
 *  Created on: Oct 4, 2019
 *      Author: Tomas Pecka
 */

#include "VectorCompare.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto VectorCompareULI = registration::AbstractRegister < compare::VectorCompare, bool, const ext::vector < unsigned long > &, const ext::vector < unsigned long > & > ( compare::VectorCompare::compare );

} /* namespace */
