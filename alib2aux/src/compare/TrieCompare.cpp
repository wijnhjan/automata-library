/*
 * TrieCompare.cpp
 *
 *  Created on: Jan 15, 2019
 *      Author: Tomas Pecka
 */

#include "TrieCompare.h"
#include <registration/AlgoRegistration.hpp>

namespace {

auto TrieCompareLinearString = registration::AbstractRegister < compare::TrieCompare, bool, const ext::trie < DefaultSymbolType, bool > &, const ext::trie < DefaultSymbolType, bool > & > ( compare::TrieCompare::compare );

} /* namespace */
