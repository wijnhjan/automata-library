/*
 * VectorCompare.h
 *
 *  Created on: Oct 4, 2019
 *      Author: Tomas Pecka
 */

#ifndef VECTOR_COMPARE_H_
#define VECTOR_COMPARE_H_

#include <alib/vector>

namespace compare {

class VectorCompare {
public:
	template < class T >
	static bool compare ( const ext::vector < T > & a, const ext::vector < T > & b ) {
		return a == b;
	}
};

} /* namespace compare */

#endif /* VECTOR_COMPARE_H_ */
