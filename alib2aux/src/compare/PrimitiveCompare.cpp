/*
 * PrimitiveCompare.cpp
 *
 *  Created on: Feb 4, 2019
 *      Author: Tomas Pecka
 */

#include "PrimitiveCompare.h"
#include <registration/AlgoRegistration.hpp>

#include <string/LinearString.h>

namespace {

auto PrimitiveCompareUnsignedInt  = registration::AbstractRegister < compare::PrimitiveCompare, bool, const unsigned int &, const unsigned int & > ( compare::PrimitiveCompare::compare );

} /* namespace */
