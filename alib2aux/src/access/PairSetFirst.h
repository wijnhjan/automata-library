/*
 * PairSetFirst.h
 *
 *  Created on: 20. 9. 2014
 *	  Author: Jan Travnicek
 */

#ifndef PAIR_SET_FIRST_H_
#define PAIR_SET_FIRST_H_

#include <alib/set>
#include <alib/pair>

namespace dataAccess {

class PairSetFirst {
public:
	template < class First, class Second >
	static ext::set < First > access ( const ext::set < ext::pair < First, Second > > & pairSet ) {
		ext::set < First > res;
		for ( const ext::pair < First, Second > & pair : pairSet )
			res.insert ( pair.first );

		return res;
	}

};

} /* namespace dataAccess */

#endif /* PAIR_SET_FIRST_H_ */
