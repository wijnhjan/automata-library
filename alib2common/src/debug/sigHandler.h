#ifndef SIG_HANDLER_H_
#define SIG_HANDLER_H_

namespace ext {

/**
 * \brief
 * Singleton class providing interupt or segmentation fault signal handler.
 */
class SigHandler {
	/**
	 * \brief
	 * Method called on interupt or segmentation fault.
	 */
	static void handler(int);

protected:
	/**
	 * \brief
	 * Constructor of the singleton. The constructor registers the handler method to be a callback on interupt or segmentation fault signal.
	 */
	SigHandler();

	/**
	 * \brief
	 * The singleton instance.
	 */
	static SigHandler HANDLER;

};

} /* namespace ext */

#endif /* SIG_HANDLER_H_ */
