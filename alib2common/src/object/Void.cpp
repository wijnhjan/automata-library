/*
 * Void.cpp
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travnicek
 */

#include "Void.h"
#include "Object.h"
#include <registration/ValuePrinterRegistration.hpp>

namespace object {

Void::Void() = default;

int Void::compare(const Void&) const {
	return 0;
}

std::ostream & operator << ( std::ostream & out, const Void & ) {
	return out << "(Void)";
}

Void::operator std::string () const {
	return "V";
}

Void Void::VOID = Void();

} /* namespace object */

namespace {

auto valuePrinter = registration::ValuePrinterRegister < object::Void > ( );

} /* namespace */
