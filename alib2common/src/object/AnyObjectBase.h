/*
 * AnyObjectBase.h
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: Mar 26, 2013
 *      Author: Jan Travnicek
 */

#ifndef ANY_OBJECT_BASE_H_
#define ANY_OBJECT_BASE_H_

#include <ostream>

#include <alib/memory>
#include <alib/string>

namespace object {

class AnyObjectBase {
public:
	/**
	 * \brief
	 * Default constructor. Needed because some constructor is specified.
	 */
	AnyObjectBase ( ) = default;

	/**
	 * \brief
	 * Default move constructor. Needed because of default destructor.
	 */
	AnyObjectBase ( AnyObjectBase && ) noexcept = default;

	/**
	 * \brief
	 * Default copy constructor.
	 */
	AnyObjectBase ( const AnyObjectBase & ) = default;

	/**
	 * \brief
	 * Default move operator =. Needed because of default destructor.
	 */
	AnyObjectBase & operator = ( AnyObjectBase && ) noexcept = default;

	/**
	 * \brief
	 * Default copy operator =.
	 */
	AnyObjectBase & operator = ( const AnyObjectBase & ) = default;

	/**
	 * \brief
	 * To allow destruction in type hierarchy
	 */
	virtual ~AnyObjectBase ( ) noexcept = default;

	/**
	 * \brief
	 * Virtual copy constructor.
	 *
	 * \return dynamically allocated copy constructed instance
	 */
	virtual AnyObjectBase * clone ( ) const & = 0;

	/**
	 * \brief
	 * Virtual copy constructor.
	 *
	 * \return dynamically allocated move constructed instance
	 */
	virtual AnyObjectBase * clone ( ) && = 0;

	/**
	 * \brief
	 * Virtual comparison method.
	 *
	 * \param other the other instance
	 *
	 * \return negative if this < other, zero if this == other, positive if this > other
	 */
	virtual int compare ( const AnyObjectBase & other ) const = 0;

	/**
	 * \brief
	 * Virtual print to stream
	 *
	 * \param out the stream instance
	 */
	virtual void operator >>( std::ostream & out ) const = 0;

	/**
	 * \brief
	 * Virtual to string cast operator
	 */
	virtual explicit operator std::string ( ) const = 0;

	/**
	 * \brief
	 * Increments the unique counter of the object.
	 *
	 * \param by how much to increment
	 */
	virtual void increment ( unsigned by ) = 0;

	/**
	 * Getter of unique identifier
	 *
	 * \return the unique identifier
	 */
	virtual unsigned getId ( ) const = 0;
};

} /* namespace object */

#endif /* ANY_OBJECT_BASE_H_ */
