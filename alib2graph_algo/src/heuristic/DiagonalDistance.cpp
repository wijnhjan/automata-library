// DiagonalDistance.cpp
//
//     Created on: 15. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#include "DiagonalDistance.hpp"

#include <registration/AlgoRegistration.hpp>
#include <common/DefaultTypes.hpp>

namespace {

auto DiagonalDistance = registration::AbstractRegister<graph::heuristic::DiagonalDistance,
                                                       std::function<DefaultWeightType(const ext::pair<
                                                           DefaultCoordinateType,
                                                           DefaultCoordinateType> &,
                                                                                       const ext::pair<
                                                                                           DefaultCoordinateType,
                                                                                           DefaultCoordinateType> &)> >(
    graph::heuristic::DiagonalDistance::diagonalDistanceFunction);

}
