// JarnikPrim.cpp
//
//     Created on: 19. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#include "JarnikPrim.hpp"

#include <registration/AlgoRegistration.hpp>

namespace {

// ---------------------------------------------------------------------------------------------------------------------
// uni-directional

auto JarnikPrim1 = registration::AbstractRegister<graph::spanning_tree::JarnikPrim,
                                                  graph::WeightedUndirectedGraph<>,
                                                  const graph::WeightedUndirectedGraph<> &,
                                                  const DefaultNodeType &>(graph::spanning_tree::JarnikPrim::findSpanningTree);
}
