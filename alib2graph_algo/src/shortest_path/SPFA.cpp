// SPFA.cpp
//
//     Created on: 05. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#include "SPFA.hpp"

#include <registration/AlgoRegistration.hpp>

namespace {

auto SPFA1 = registration::AbstractRegister<graph::shortest_path::SPFA,
                                            ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                            const graph::WeightedUndirectedGraph<> &,
                                            const DefaultNodeType &,
                                            const DefaultNodeType &>(graph::shortest_path::SPFA::findPathRegistration);

auto SPFA2 = registration::AbstractRegister<graph::shortest_path::SPFA,
                                            ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                            const graph::WeightedUndirectedMultiGraph<> &,
                                            const DefaultNodeType &,
                                            const DefaultNodeType &>(graph::shortest_path::SPFA::findPathRegistration);

auto SPFA3 = registration::AbstractRegister<graph::shortest_path::SPFA,
                                            ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                            const graph::WeightedDirectedGraph<> &,
                                            const DefaultNodeType &,
                                            const DefaultNodeType &>(graph::shortest_path::SPFA::findPathRegistration);

auto SPFA4 = registration::AbstractRegister<graph::shortest_path::SPFA,
                                            ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                            const graph::WeightedDirectedMultiGraph<> &,
                                            const DefaultNodeType &,
                                            const DefaultNodeType &>(graph::shortest_path::SPFA::findPathRegistration);

auto SPFA5 = registration::AbstractRegister<graph::shortest_path::SPFA,
                                            ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                            const graph::WeightedMixedGraph<> &,
                                            const DefaultNodeType &,
                                            const DefaultNodeType &>(graph::shortest_path::SPFA::findPathRegistration);

auto SPFA6 = registration::AbstractRegister<graph::shortest_path::SPFA,
                                            ext::pair<ext::vector<DefaultNodeType>, DefaultWeightType>,
                                            const graph::WeightedMixedMultiGraph<> &,
                                            const DefaultNodeType &,
                                            const DefaultNodeType &>(graph::shortest_path::SPFA::findPathRegistration);

auto SPFAGrid1 = registration::AbstractRegister<graph::shortest_path::SPFA,
                                                ext::pair<ext::vector<DefaultSquareGridNodeType>,
                                                          DefaultWeightType>,
                                                const grid::WeightedSquareGrid4<> &,
                                                const DefaultSquareGridNodeType &,
                                                const DefaultSquareGridNodeType &>(graph::shortest_path::SPFA::findPathRegistration);

auto SPFAGrid2 = registration::AbstractRegister<graph::shortest_path::SPFA,
                                                ext::pair<ext::vector<DefaultSquareGridNodeType>,
                                                          DefaultWeightType>,
                                                const grid::WeightedSquareGrid8<> &,
                                                const DefaultSquareGridNodeType &,
                                                const DefaultSquareGridNodeType &>(graph::shortest_path::SPFA::findPathRegistration);

}
