// FixedWeightedSquareGrid8.cpp
//
//     Created on: 07. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#include "FixedWeightedSquareGrid8.hpp"
#include <registration/AlgoRegistration.hpp>

// ---------------------------------------------------------------------------------------------------------------------

grid::WeightedSquareGrid8<int,
                          edge::WeightedEdge<ext::pair<int, int>,
                                             double> > graph::generate::FixedWeightedSquareGrid8::weighted_grid() {

  grid::WeightedSquareGrid8<int, edge::WeightedEdge<ext::pair<int, int>, double>> graph(11, 11);

  graph.addObstacle(2, 5);
  graph.addObstacle(3, 5);
  graph.addObstacle(4, 5);
  graph.addObstacle(5, 5);
  graph.addObstacle(6, 5);
  graph.addObstacle(7, 5);
  graph.addObstacle(5, 3);
  graph.addObstacle(5, 4);
  graph.addObstacle(5, 6);
  graph.addObstacle(5, 7);
  graph.addObstacle(5, 8);

  return graph;
}

// ---------------------------------------------------------------------------------------------------------------------

namespace {

auto FixedWeightedSquareGrid =
    registration::AbstractRegister<graph::generate::FixedWeightedSquareGrid8,
                                   grid::WeightedSquareGrid8<int,
                                                             edge::WeightedEdge<ext::pair<int, int>,
                                                                                double>>>(
        graph::generate::FixedWeightedSquareGrid8::weighted_grid);

}

// ---------------------------------------------------------------------------------------------------------------------
