#include <catch2/catch.hpp>

#include "object/Object.h"

#include "factory/StringDataFactory.hpp"

TEST_CASE ( "Label Test", "[unit][object][label]" ) {
	SECTION ( "Equal" ) {
		{
			std::string input = "1";
			object::Object label = factory::StringDataFactory::fromString (input);

			std::string output = factory::StringDataFactory::toString(label);

			CHECK( input == output );

			/*	object::Object label2 = factory::StringDataFactory::fromString (output);

				CHECK( label == label2 );*/
		}
		/*	{
			std::string input = "aaaa";
			object::Object label = factory::StringDataFactory::fromString (input);

			std::string output = factory::StringDataFactory::toString(label);

			CHECK( input == output );

			object::Object label2 = factory::StringDataFactory::fromString (output);

			CHECK( label == label2 );
			}
			{
			std::string input = "[1, 2, 3]";
			object::Object label = factory::StringDataFactory::fromString (input);

			std::string output = factory::StringDataFactory::toString(label);

			CHECK( input == output );

			object::Object label2 = factory::StringDataFactory::fromString (output);

			CHECK( label == label2 );
			}
			{
			std::string input = "<1, 2>";
			object::Object label = factory::StringDataFactory::fromString (input);

			std::string output = factory::StringDataFactory::toString(label);

			CHECK( input == output );

			object::Object label2 = factory::StringDataFactory::fromString (output);

			CHECK( label == label2 );
			}*/
	}
}
