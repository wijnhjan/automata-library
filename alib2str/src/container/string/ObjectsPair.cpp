/*
 * ObjectsPair.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "ObjectsPair.h"

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < ext::pair < object::Object, object::Object > > ( );
auto stringReader = registration::StringReaderRegister < object::Object, ext::pair < object::Object, object::Object > > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, ext::pair < object::Object, object::Object > > ( );
auto stringReaderGroup = registration::StringReaderRegisterTypeInGroup < object::Object, ext::pair < object::Object, object::Object > > ( );

} /* namespace */
