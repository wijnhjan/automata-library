/*
 * Void.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_VOID_H_
#define _STRING_VOID_H_

#include <object/Void.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < object::Void > {
	static object::Void parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const object::Void & primitive );
};

} /* namespace core */

#endif /* _STRING_VOID_H_ */
