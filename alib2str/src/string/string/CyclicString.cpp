/*
 * CyclicString.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "CyclicString.h"
#include <string/String.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < string::CyclicString < > > ( );
auto stringReader = registration::StringReaderRegister < string::String, string::CyclicString < > > ( );

} /* namespace */
