/*
 * StringToStringComposerCommon.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef STRING_TO_STRING_COMPOSER_COMMON_H_
#define STRING_TO_STRING_COMPOSER_COMMON_H_

#include <ostream>
#include <alib/vector>

#include <core/stringApi.hpp>

namespace string {

class StringToStringComposerCommon {
public:
	template < class SymbolType >
	static void composeContent ( std::ostream & output, const std::vector < SymbolType > & content );
};

template < class SymbolType >
void StringToStringComposerCommon::composeContent ( std::ostream & output, const std::vector < SymbolType > & content ) {
	bool first = true;
	for ( const SymbolType & symbol : content ) {
		if ( first )
			first = false;
		else
			output << " ";
		core::stringApi < SymbolType >::compose ( output, symbol );

	}
}

} /* namespace string */

#endif /* STRING_TO_STRING_COMPOSER_COMMON_H_ */
