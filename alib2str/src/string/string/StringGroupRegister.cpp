/*
 * StringGroupGegister.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include <string/String.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringReaderGroup = registration::StringReaderGroupRegister < string::String > ( );

} /* namespace */
