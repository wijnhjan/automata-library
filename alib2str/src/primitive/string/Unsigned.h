/*
 * Unsigned.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_UNSIGNED_H_
#define _STRING_UNSIGNED_H_

#include <core/stringApi.hpp>

#include <primitive/PrimitiveFromStringLexer.h>

namespace core {

template < >
struct stringApi < unsigned > {
	static unsigned parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, unsigned primitive );
};

} /* namespace core */

#endif /* _STRING_UNSIGNED_H_ */
