/*
 * Unsigned.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "Unsigned.h"
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

unsigned stringApi < unsigned >::parse ( std::istream & ) {
	throw exception::CommonException("parsing unsigned from string not implemented");
}

bool stringApi < unsigned >::first ( std::istream & ) {
	return false;
}

void stringApi < unsigned >::compose ( std::ostream & output, unsigned primitive ) {
	output << primitive;
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < unsigned > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, unsigned > ( );

} /* namespace */
