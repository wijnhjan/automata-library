/*
 * Character.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_CHARACTER_H_
#define _STRING_CHARACTER_H_

#include <core/stringApi.hpp>

#include <primitive/PrimitiveFromStringLexer.h>

namespace core {

template < >
struct stringApi < char > {
	static char parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, char primitive );
};

} /* namespace core */

#endif /* _STRING_CHARACTER_H_ */
