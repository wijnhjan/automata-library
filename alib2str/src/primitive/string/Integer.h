/*
 * Integer.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_INTEGER_H_
#define _STRING_INTEGER_H_

#include <core/stringApi.hpp>

#include <primitive/PrimitiveFromStringLexer.h>

namespace core {

template < >
struct stringApi < int > {
	static int parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, int primitive );
};

} /* namespace core */

#endif /* _STRING_INTEGER_H_ */
