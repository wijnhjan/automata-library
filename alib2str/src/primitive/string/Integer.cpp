/*
 * Integer.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "Integer.h"
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

int stringApi < int >::parse ( std::istream & input ) {
	primitive::PrimitiveFromStringLexer::Token token = primitive::PrimitiveFromStringLexer::next ( input );
	if(token.type != primitive::PrimitiveFromStringLexer::TokenType::INTEGER)
		throw exception::CommonException("Unrecognised INTEGER token.");

	return ext::from_string < int > ( token.value );
}

bool stringApi < int >::first ( std::istream & input ) {
	primitive::PrimitiveFromStringLexer::Token token = primitive::PrimitiveFromStringLexer::next ( input );
	bool res = token.type == primitive::PrimitiveFromStringLexer::TokenType::INTEGER;
	primitive::PrimitiveFromStringLexer::putback ( input, token );
	return res;
}

void stringApi < int >::compose ( std::ostream & output, int primitive ) {
	output << primitive;
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < int > ( );
auto stringReader = registration::StringReaderRegister < object::Object, int > ( );

auto stringReaderGroup = registration::StringReaderRegisterTypeInGroup < object::Object, int > ( );
auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, int > ( );

} /* namespace */
