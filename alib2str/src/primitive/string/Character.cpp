/*
 * Character.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "Character.h"
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

char stringApi < char >::parse ( std::istream & ) {
	throw exception::CommonException("parsing char from string not implemented");
}

bool stringApi < char >::first ( std::istream & ) {
	return false;
}

void stringApi < char >::compose ( std::ostream & output, char primitive ) {
	output << primitive;
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < char > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, char > ( );

} /* namespace */
