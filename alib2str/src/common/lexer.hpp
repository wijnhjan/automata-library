/*
 * lexer.hpp
 *
 * This file is part of Algorithms library toolkit.
 * Copyright (C) 2017 Jan Travnicek (jan.travnicek@fit.cvut.cz)

 * Algorithms library toolkit is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Algorithms library toolkit is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Algorithms library toolkit.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Created on: Mar 4, 2019
 * Author: Jan Travnicek
 */

#ifndef __LEXER_HPP_
#define __LEXER_HPP_

#include <exception>
#include <istream>

namespace ext {

class BasicLexer {
public:
	static void putback ( std::istream & input, const std::string & data );

	static bool test ( std::istream & input, const std::string & value );

	static void consume ( std::istream & input, const std::string & value );

	static bool testAndConsume ( std::istream & input, const std::string & value );

};

template < typename SuperLexer >
class Lexer : public BasicLexer {
public:
	struct Token {
		typename SuperLexer::TokenType type;
		std::string value;
		std::string raw;
	};

	static Token peek ( std::istream & input ) {
		Token token = SuperLexer::next ( input );
		putback ( input, token );
		return token;
	}

	using BasicLexer::putback;

	static void putback ( std::istream & input, const Token & token ) {
		putback ( input, token.raw );
	}

};

} /* namespace ext */

#endif /* __LEXER_HPP_ */
