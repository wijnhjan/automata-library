/*
 * RankedSymbol.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "RankedSymbol.h"
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < common::ranked_symbol < > > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, common::ranked_symbol < > > ( );

} /* namespace */
