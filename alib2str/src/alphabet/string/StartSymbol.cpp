/*
 * StartSymbol.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "StartSymbol.h"
#include <alphabet/StartSymbol.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::StartSymbol stringApi < alphabet::StartSymbol >::parse ( std::istream & ) {
	throw exception::CommonException("parsing StartSymbol from string not implemented");
}

bool stringApi < alphabet::StartSymbol >::first ( std::istream & ) {
	return false;
}

void stringApi < alphabet::StartSymbol >::compose ( std::ostream & output, const alphabet::StartSymbol & ) {
	output << "#^";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::StartSymbol > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::StartSymbol > ( );

} /* namespace */
