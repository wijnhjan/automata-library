/*
 * EndSymbol.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_END_SYMBOL_H_
#define _STRING_END_SYMBOL_H_

#include <alphabet/EndSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::EndSymbol > {
	static alphabet::EndSymbol parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const alphabet::EndSymbol & symbol );
};

} /* namespace core */

#endif /* _STRING_END_SYMBOL_H_ */
