/*
 * VariablesBarSymbol.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "VariablesBarSymbol.h"
#include <alphabet/VariablesBarSymbol.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace core {

alphabet::VariablesBarSymbol stringApi < alphabet::VariablesBarSymbol >::parse ( std::istream & ) {
	throw exception::CommonException("parsing VariablesBarSymbol from string not implemented");
}

bool stringApi < alphabet::VariablesBarSymbol >::first ( std::istream & ) {
	return false;
}

void stringApi < alphabet::VariablesBarSymbol >::compose ( std::ostream & output, const alphabet::VariablesBarSymbol & ) {
	output << "#/";
}

} /* namespace core */

namespace {

auto stringWrite = registration::StringWriterRegister < alphabet::VariablesBarSymbol > ( );

auto stringWriteGroup = registration::StringWriterRegisterTypeInGroup < object::Object, alphabet::VariablesBarSymbol > ( );

} /* namespace */
