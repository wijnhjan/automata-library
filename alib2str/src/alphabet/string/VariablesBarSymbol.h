/*
 * VariablesBarSymbol.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_VARIABLES_BAR_SYMBOL_H_
#define _STRING_VARIABLES_BAR_SYMBOL_H_

#include <alphabet/VariablesBarSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::VariablesBarSymbol > {
	static alphabet::VariablesBarSymbol parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const alphabet::VariablesBarSymbol & symbol );
};

} /* namespace core */

#endif /* _STRING_VARIABLES_BAR_SYMBOL_H_ */
