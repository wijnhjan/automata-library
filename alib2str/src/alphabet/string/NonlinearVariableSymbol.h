/*
 * NonlinearVariableSymbol.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_NONLINEAR_VARIABLE_SYMBOL_H_
#define _STRING_NONLINEAR_VARIABLE_SYMBOL_H_

#include <alphabet/NonlinearVariableSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < class SymbolType >
struct stringApi < alphabet::NonlinearVariableSymbol < SymbolType > > {
	static alphabet::NonlinearVariableSymbol < SymbolType > parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const alphabet::NonlinearVariableSymbol < SymbolType > & symbol );
};

template < class SymbolType >
alphabet::NonlinearVariableSymbol < SymbolType > stringApi < alphabet::NonlinearVariableSymbol < SymbolType > >::parse ( std::istream & ) {
	throw exception::CommonException("parsing NonlinearVariableSymbol from string not implemented");
}

template < class SymbolType >
bool stringApi < alphabet::NonlinearVariableSymbol < SymbolType > >::first ( std::istream & ) {
	return false;
}

template < class SymbolType >
void stringApi < alphabet::NonlinearVariableSymbol < SymbolType > >::compose ( std::ostream & output, const alphabet::NonlinearVariableSymbol < SymbolType > & symbol ) {
	output << "$";
	core::stringApi < SymbolType >::compose ( output, symbol.getSymbol ( ) );
}

} /* namespace core */

#endif /* _STRING_NONLINEAR_VARIABLE_SYMBOL_H_ */
