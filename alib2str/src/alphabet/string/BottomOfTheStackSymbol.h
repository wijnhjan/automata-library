/*
 * BottomOfTheStackSymbol.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_BOTTOM_OF_THE_STACK_SYMBOL_H_
#define _STRING_BOTTOM_OF_THE_STACK_SYMBOL_H_

#include <alphabet/BottomOfTheStackSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::BottomOfTheStackSymbol > {
	static alphabet::BottomOfTheStackSymbol parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const alphabet::BottomOfTheStackSymbol & symbol );
};

} /* namespace core */

#endif /* _STRING_BOTTOM_OF_THE_STACK_SYMBOL_H_ */
