/*
 * InitialSymbol.h
 *
 * Created on: Nov 21, 2017
 * Author: Tomas Pecka
 */

#ifndef _STRING_INITIAL_SYMBOL_H_
#define _STRING_INITIAL_SYMBOL_H_

#include <alphabet/InitialSymbol.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < alphabet::InitialSymbol > {
	static alphabet::InitialSymbol parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const alphabet::InitialSymbol & symbol );
};

} /* namespace core */

#endif /* _STRING_INITIAL_SYMBOL_H_ */
