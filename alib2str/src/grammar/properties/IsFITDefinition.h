/*
 * IsFITDefinition.h
 *
 *  Created on: 23. 11. 2017
 *	  Author: Tomas Pecka
 */

#ifndef IS_FIT_DEFINITION_H_
#define IS_FIT_DEFINITION_H_

namespace grammar {

namespace properties {

class IsFITDefinition {
public:
	/*
	 * Checks whether grammar satisfies rule about S->\eps from FIT definitions.
	 */
	template < class T >
	static bool isFITDefinition ( const T & grammar );
};

template < class T >
bool IsFITDefinition::isFITDefinition( const T & grammar ) {
	if ( ! grammar.getGeneratesEpsilon( ) )
		return true;

	auto rawRules = grammar::RawRules::getRawRules ( grammar );
	for ( const auto & rule : rawRules )
		for ( const auto & rhs : rule.second )
			if ( std::find ( rhs.begin ( ), rhs.end ( ), grammar.getInitialSymbol ( ) ) != rhs.end ( ) )
				return false;

	return true;
}

} /* namespace properties */

} /* namespace grammar */

#endif /* IS_FIT_DEFINITION_H_ */
