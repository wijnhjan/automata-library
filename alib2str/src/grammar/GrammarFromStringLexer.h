/*
 * GrammarFromStringLexer.h
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#ifndef GRAMMAR_FROM_STRING_LEXER_H_
#define GRAMMAR_FROM_STRING_LEXER_H_

#include <alib/string>
#include <alib/istream>
#include <common/lexer.hpp>

namespace grammar {

class GrammarFromStringLexer : public ext::Lexer < GrammarFromStringLexer > {
public:
	enum class TokenType {
		SET_BEGIN,
		SET_END,
		COMMA,
		TUPLE_BEGIN,
		TUPLE_END,
		SEPARATOR,
		EPSILON,
		MAPS_TO,
		RIGHT_RG,
		LEFT_RG,
		RIGHT_LG,
		LEFT_LG,
		LG,
		CFG,
		EPSILON_FREE_CFG,
		GNF,
		CNF,
		CSG,
		NON_CONTRACTING_GRAMMAR,
		CONTEXT_PRESERVING_UNRESTRICTED_GRAMMAR,
		UNRESTRICTED_GRAMMAR,
		TEOF,
		ERROR,
	};

	static Token next(std::istream& input);
};

} /* namepsace grammar */

#endif /* GRAMMAR_FROM_STRING_LEXER_H_ */
