/*
 * RightLG.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_RIGHT_LG_H_
#define _STRING_RIGHT_LG_H_

#include <grammar/Regular/RightLG.h>
#include <core/stringApi.hpp>

#include <grammar/GrammarFromStringLexer.h>

#include <grammar/string/common/GrammarFromStringParserCommon.h>
#include <grammar/string/common/GrammarToStringComposerCommon.h>

namespace core {

template < class TerminalSymbolType, class NonterminalSymbolType >
struct stringApi < grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > > {
	static grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > & grammar );
};

template < class TerminalSymbolType, class NonterminalSymbolType >
grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > stringApi < grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > >::parse ( std::istream & input ) {
	grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next(input);
	if(token.type != grammar::GrammarFromStringLexer::TokenType::RIGHT_LG)
		throw exception::CommonException("Unrecognised RightLG token.");

	return grammar::GrammarFromStringParserCommon::parseCFLikeGrammar < grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > > ( input );
}

template < class TerminalSymbolType, class NonterminalSymbolType >
bool stringApi < grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > >::first ( std::istream & input ) {
	grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next ( input );
	bool res = token.type == grammar::GrammarFromStringLexer::TokenType::RIGHT_LG;
	grammar::GrammarFromStringLexer::putback ( input, token );
	return res;
}

template < class TerminalSymbolType, class NonterminalSymbolType >
void stringApi < grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > >::compose ( std::ostream & output, const grammar::RightLG < TerminalSymbolType, NonterminalSymbolType > & grammar ) {
	output << "RIGHT_LG";
	grammar::GrammarToStringComposerCommon::composeCFLikeGrammar ( output, grammar );
}

} /* namespace core */

#endif /* _STRING_RIGHT_LG_H_ */
