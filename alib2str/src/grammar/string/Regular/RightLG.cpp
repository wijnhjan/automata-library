/*
 * RightLG.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "RightLG.h"
#include <grammar/Grammar.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < grammar::RightLG < > > ( );
auto stringReader = registration::StringReaderRegister < grammar::Grammar, grammar::RightLG < > > ( );

} /* namespace */
