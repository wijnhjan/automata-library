/*
 * GNF.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "GNF.h"
#include <grammar/Grammar.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < grammar::GNF < > > ( );
auto stringReader = registration::StringReaderRegister < grammar::Grammar, grammar::GNF < > > ( );

} /* namespace */
