/*
 * LG.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "LG.h"
#include <grammar/Grammar.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < grammar::LG < > > ( );
auto stringReader = registration::StringReaderRegister < grammar::Grammar, grammar::LG < > > ( );

} /* namespace */
