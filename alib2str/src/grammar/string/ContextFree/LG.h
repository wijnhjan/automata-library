/*
 * LG.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_LG_H_
#define _STRING_LG_H_

#include <grammar/ContextFree/LG.h>
#include <core/stringApi.hpp>

#include <grammar/GrammarFromStringLexer.h>

#include <grammar/string/common/GrammarFromStringParserCommon.h>
#include <grammar/string/common/GrammarToStringComposerCommon.h>

namespace core {

template < class TerminalSymbolType, class NonterminalSymbolType >
struct stringApi < grammar::LG < TerminalSymbolType, NonterminalSymbolType > > {
	static grammar::LG < TerminalSymbolType, NonterminalSymbolType > parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const grammar::LG < TerminalSymbolType, NonterminalSymbolType > & grammar );
};

template < class TerminalSymbolType, class NonterminalSymbolType >
grammar::LG < TerminalSymbolType, NonterminalSymbolType > stringApi < grammar::LG < TerminalSymbolType, NonterminalSymbolType > >::parse ( std::istream & input ) {
	grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next(input);
	if(token.type != grammar::GrammarFromStringLexer::TokenType::LG)
		throw exception::CommonException("Unrecognised LG token.");

	return grammar::GrammarFromStringParserCommon::parseCFLikeGrammar < grammar::LG < TerminalSymbolType, NonterminalSymbolType > > ( input );
}

template < class TerminalSymbolType, class NonterminalSymbolType >
bool stringApi < grammar::LG < TerminalSymbolType, NonterminalSymbolType > >::first ( std::istream & input ) {
	grammar::GrammarFromStringLexer::Token token = grammar::GrammarFromStringLexer::next ( input );
	bool res = token.type == grammar::GrammarFromStringLexer::TokenType::LG;
	grammar::GrammarFromStringLexer::putback ( input, token );
	return res;
}

template < class TerminalSymbolType, class NonterminalSymbolType >
void stringApi < grammar::LG < TerminalSymbolType, NonterminalSymbolType > >::compose ( std::ostream & output, const grammar::LG < TerminalSymbolType, NonterminalSymbolType > & grammar ) {
	output << "LG";
	grammar::GrammarToStringComposerCommon::composeCFLikeGrammar ( output, grammar );
}

} /* namespace core */

#endif /* _STRING_LG_H_ */
