/*
 * GrammarGroupGegister.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include <grammar/Grammar.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringReaderGroup = registration::StringReaderGroupRegister < grammar::Grammar > ( );

} /* namespace */
