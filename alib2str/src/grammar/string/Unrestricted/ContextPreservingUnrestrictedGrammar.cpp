/*
 * ContextPreservingUnrestrictedGrammar.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "ContextPreservingUnrestrictedGrammar.h"
#include <grammar/Grammar.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < grammar::ContextPreservingUnrestrictedGrammar < > > ( );
auto stringReader = registration::StringReaderRegister < grammar::Grammar, grammar::ContextPreservingUnrestrictedGrammar < > > ( );

} /* namespace */
