/*
 * UnrestrictedGrammar.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "UnrestrictedGrammar.h"
#include <grammar/Grammar.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < grammar::UnrestrictedGrammar < > > ( );
auto stringReader = registration::StringReaderRegister < grammar::Grammar, grammar::UnrestrictedGrammar < > > ( );

} /* namespace */
