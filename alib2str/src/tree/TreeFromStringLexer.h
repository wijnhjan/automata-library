/*
 * TreeFromStringLexer.h
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#ifndef TREE_FROM_STRING_LEXER_H_
#define TREE_FROM_STRING_LEXER_H_

#include <alib/string>
#include <alib/istream>
#include <common/lexer.hpp>

namespace tree {

class TreeFromStringLexer : public ext::Lexer < TreeFromStringLexer > {
public:
	enum class TokenType {
		RANKED_TREE,
		RANKED_PATTERN,
		RANKED_NONLINEAR_PATTERN,
		UNRANKED_TREE,
		UNRANKED_PATTERN,
		UNRANKED_NONLINEAR_PATTERN,
		BAR, RANK, SUBTREE_WILDCARD, NONLINEAR_VARIABLE, TEOF, ERROR
	};

	static Token next ( std::istream & input );
};

} /* namespace tree */

#endif /* TREE_FROM_STRING_LEXER_H_ */
