/*
 * StringReaderAbstraction.hpp
 *
 *  Created on: 11. 7. 2017
 *	  Author: Jan Travnicek
 */

#ifndef _STRING_READER_ABSTRACTION_HPP_
#define _STRING_READER_ABSTRACTION_HPP_

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

#include <factory/StringDataFactory.hpp>

namespace abstraction {

template < class ReturnType >
class StringReaderAbstraction : virtual public NaryOperationAbstraction < std::string && >, virtual public ValueOperationAbstraction < ReturnType > {
public:
	std::shared_ptr < abstraction::Value > run ( ) override {
		std::shared_ptr < abstraction::Value > & param = std::get < 0 > ( this->getParams ( ) );
		return std::make_shared < abstraction::ValueHolder < ReturnType > > ( factory::StringDataFactory::fromString ( abstraction::retrieveValue < std::string && > ( param ) ), true );
	}

};

} /* namespace abstraction */

#endif /* _STRING_READER_ABSTRACTION_HPP_ */
