/*
 * InitialStateLabel.h
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#ifndef _STRING_INITIAL_STATE_LABEL_H_
#define _STRING_INITIAL_STATE_LABEL_H_

#include <label/InitialStateLabel.h>
#include <core/stringApi.hpp>

namespace core {

template < >
struct stringApi < label::InitialStateLabel > {
	static label::InitialStateLabel parse ( std::istream & input );
	static bool first ( std::istream & input );
	static void compose ( std::ostream & output, const label::InitialStateLabel & label );
};

} /* namespace core */

#endif /* _STRING_INITIAL_STATE_LABEL_H_ */
