/*
 * FormalRegExp.cpp
 *
 * Created on: Sep 27, 2017
 * Author: Jan Travnicek
 */

#include "FormalRegExp.h"
#include <regexp/RegExp.h>
#include <object/Object.h>

#include <registration/StringRegistration.hpp>

namespace {

auto stringWrite = registration::StringWriterRegister < regexp::FormalRegExp < > > ( );
auto stringReader = registration::StringReaderRegister < regexp::RegExp, regexp::FormalRegExp < > > ( );

} /* namespace */
