/*
 * RTEFromStringLexer.h
 *
 *  Created on: Nov 23, 2013
 *      Author: Jan Travnicek
 */

#ifndef RTE_FROM_STRING_LEXER_H_
#define RTE_FROM_STRING_LEXER_H_

#include <alib/string>
#include <alib/istream>
#include <common/lexer.hpp>

namespace rte {

class RTEFromStringLexer :  public ext::Lexer < RTEFromStringLexer > {
public:
	enum class TokenType {
		LPAR,
		RPAR,
		PLUS,
		STAR,
		DOT,
		COMMA,
		EMPTY,
		RANK,
		TEOF,
		ERROR
	};

	static Token next(std::istream& input);
};

} /* namespace rte */

#endif /* RTE_FROM_STRING_LEXER_H_ */
