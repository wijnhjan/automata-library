#include <catch2/catch.hpp>
#include "example/CreateDataType.h"

TEST_CASE ( "CreateDataType test", "[unit][dummy][example]" ) {
	SECTION ( "no arg version" ) {
		CHECK ( example::CreateDataType::create ( ).getA ( ) == 0 );
	}

	SECTION ( "int version" ) {
		CHECK ( example::CreateDataType2::create ( 3 ).getA ( ) == 3 );
	}
}
