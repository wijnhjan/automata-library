// NodeBase.hpp
//
//     Created on: 04. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef ALIB2_NODEBASE_HPP
#define ALIB2_NODEBASE_HPP

#include <alib/compare>

namespace node {

/**
 * Represents node in graph.
 */
class NodeBase : public ext::CompareOperators < NodeBase > {
public:
	virtual ~NodeBase ( ) noexcept = default;

	/**
	 * \brief Comparison helper method evaluating allowing possibly deeper comparison of this with other class of the same type.
	 *
	 * \details If the other class is of different type the relative order is computer by means of type_index.
	 *
	 * \param other the other class to compare with
	 *
	 * \returns result of actual comparison if type of this class and other class is the same, result of difference of type indexes othervise.
	 */
	virtual int compare ( const NodeBase & other ) const = 0;

// ---------------------------------------------------------------------------------------------------------------------

 public:
// ---------------------------------------------------------------------------------------------------------------------
	friend std::ostream & operator << ( std::ostream & os, const NodeBase & instance ) {
		instance >> os;
		return os;
	}

	virtual void operator >> ( std::ostream & os ) const = 0;

	virtual operator std::string ( ) const = 0;
};

} // namespace node

#endif //ALIB2_NODEBASE_HPP
