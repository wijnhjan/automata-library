// Node.cpp
//
//     Created on: 18. 12. 2017
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#include "Node.hpp"
#include <registration/ValuePrinterRegistration.hpp>

namespace node {

// ---------------------------------------------------------------------------------------------------------------------

Node::Node() : m_id(-1) {

}

Node::Node(int id) : m_id(id) {

}

// ---------------------------------------------------------------------------------------------------------------------

std::string Node::name() const {
  return "Node";
}

// ---------------------------------------------------------------------------------------------------------------------

int Node::compare(const NodeBase &other) const {
  if (ext::type_index(typeid(*this)) == ext::type_index(typeid(other))) return this->compare((decltype(*this)) other);
  return ext::type_index(typeid(*this)) - ext::type_index(typeid(other));
}

int Node::compare(const Node &other) const {
  auto first = ext::tie(m_id);
  auto second = ext::tie(other.m_id);

  static ext::compare<decltype(first)> comp;

  return comp(first, second);
}

void Node::operator>>(std::ostream &ostream) const {
  ostream << "(" << name() << "(id=" << m_id << "))";

}

Node::operator std::string() const {
  std::stringstream ss;
  ss << "(" << name() << "(id=" << m_id << "))";
  return std::move(ss).str();
}

// ---------------------------------------------------------------------------------------------------------------------

int Node::getId() const {
  return m_id;
}

// ---------------------------------------------------------------------------------------------------------------------

} // namespace node

// =====================================================================================================================

namespace {

auto valuePrinter = registration::ValuePrinterRegister<node::Node>();

}
// ---------------------------------------------------------------------------------------------------------------------

