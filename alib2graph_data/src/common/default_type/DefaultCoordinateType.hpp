// DefaultCoordinateType.hpp
//
//     Created on: 04. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef ALIB2_DEFAULTCOORDINATETYPE_HPP
#define ALIB2_DEFAULTCOORDINATETYPE_HPP

typedef long DefaultCoordinateType;

#endif //ALIB2_DEFAULTCOORDINATETYPE_HPP
