// DefaultTypes.h
//
//     Created on: 04. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef ALIB2_DEFAULTTYPES_H
#define ALIB2_DEFAULTTYPES_H

#include "default_type/DefaultCoordinateType.hpp"
#include "default_type/DefaultEdgeType.hpp"
#include "default_type/DefaultNodeType.hpp"
#include "default_type/DefaultSquareGridEdgeType.hpp"
#include "default_type/DefaultSquareGridNodeType.hpp"
#include "default_type/DefaultSquareGridWeightedEdgeType.hpp"
#include "default_type/DefaultWeightedEdgeType.hpp"
#include "default_type/DefaultWeightType.hpp"
#include "default_type/DefaultCapacityType.hpp"
#include "default_type/DefaultCapacityEdgeType.hpp"

#endif //ALIB2_DEFAULTTYPES_H
