// GridFeatures.hpp
//
//     Created on: 04. 03. 2018
//         Author: Jan Uhlik
//    Modified by:
//
// Copyright (c) 2017 Czech Technical University in Prague | Faculty of Information Technology. All rights reserved.
// Git repository: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library

#ifndef ALIB2_GRIDFEATURES_HPP
#define ALIB2_GRIDFEATURES_HPP

#include <common/DefaultTypes.hpp>

namespace grid {

class GridBase;

template<typename TCoordinate, typename TEdge>
class SqaureGrid;

template<typename TCoordinate = DefaultCoordinateType, typename TEdge = DefaultSquareGridEdgeType>
class SquareGrid4;

template<typename TCoordinate = DefaultCoordinateType, typename TEdge = DefaultSquareGridEdgeType>
class SquareGrid8;

template<typename TCoordinate = DefaultCoordinateType, typename TEdge = DefaultSquareGridWeightedEdgeType>
class WeightedSquareGrid4;

template<typename TCoordinate = DefaultCoordinateType, typename TEdge = DefaultSquareGridWeightedEdgeType>
class WeightedSquareGrid8;

} // namespace grid

#endif //ALIB2_GRIDFEATURES_HPP
