#pragma once
#include <deque>

#include <abstraction/OperationAbstraction.hpp>

#include <Models/OutputModelBox.hpp>

class ParallelExecutor {
public:
    static std::shared_ptr<abstraction::Value> execute(OutputModelBox* output);
};


