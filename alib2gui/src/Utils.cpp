#include <Utils.hpp>
#include <abstraction/ValueHolder.hpp>
#include <common/EvalHelper.h>

namespace Utils
{
    QRectF pointsToRect(QPointF a, QPointF b) {
        qreal minx = std::min(a.x(), b.x());
        qreal maxx = std::max(a.x(), b.x());
        qreal miny = std::min(a.y(), b.y());
        qreal maxy = std::max(a.y(), b.y());
        return {{minx, miny}, QPointF {maxx, maxy}};
    }

    std::shared_ptr<abstraction::Value> generateRandomAutomaton() {
        ext::vector<std::shared_ptr<abstraction::Value>> params;
        params.push_back(std::make_shared<abstraction::ValueHolder<size_t>>( 5, true));
        params.push_back(std::make_shared<abstraction::ValueHolder<size_t>>( 3, true));
        params.push_back(std::make_shared<abstraction::ValueHolder<bool>>( true, true));
        params.push_back(std::make_shared<abstraction::ValueHolder<double>>( 10.0, true));

		abstraction::TemporariesHolder environment;
        return abstraction::EvalHelper::evalAlgorithm(environment, "automaton::generate::RandomAutomatonFactory",
                                                  {},
                                                  params,
                                                  abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT);
    }

    std::shared_ptr<abstraction::Value> generateRandomGrammar() {
        ext::vector<std::shared_ptr<abstraction::Value>> params;
        params.push_back(std::make_shared<abstraction::ValueHolder<size_t>>( 5, true));
        params.push_back(std::make_shared<abstraction::ValueHolder<size_t>>( 3, true));
        params.push_back(std::make_shared<abstraction::ValueHolder<bool>>( true, true));
        params.push_back(std::make_shared<abstraction::ValueHolder<double>>( 30.0, true));

		abstraction::TemporariesHolder environment;
        return abstraction::EvalHelper::evalAlgorithm(environment, "grammar::generate::RandomGrammarFactory",
                                                  {},
                                                  params,
                                                  abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT);
    }
}
