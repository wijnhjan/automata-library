#pragma once
#include <Algorithm/Algorithm.hpp>
#include <Models/ModelBox.hpp>

class AlgorithmModelBox : public ModelBox {
public:
    explicit AlgorithmModelBox(Algorithm* p_algorithm);

    std::string getName() const override;
    Algorithm* getAlgorithm() const { return this->algorithm; }

    std::shared_ptr<abstraction::Value> evaluate() override;

private:
    Algorithm* algorithm;
};


