#pragma once
#include <Models/ModelBox.hpp>
#include <utility>

class InputModelBox : public ModelBox {
public:
    explicit InputModelBox();

    void clearCachedResult() override {}

    std::shared_ptr<abstraction::Value> evaluate() override;

    std::string getName() const override;

    std::shared_ptr<abstraction::Value> getAutomaton() const { return this->result; }
    void setAutomaton(std::shared_ptr<abstraction::Value> automaton) { this->result = std::move(automaton); }
};


