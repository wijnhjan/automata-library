#include <Graphics/Connection/OutputConnectionBox.hpp>

OutputConnectionBox::OutputConnectionBox(GraphicsBox* parent)
    : ConnectionBox(parent, ConnectionBox::Type::Output)
{}

void OutputConnectionBox::addConnection(Connection* connection) {
    this->connections.insert(connection);
}

void OutputConnectionBox::on_Disconnect() {
    for (auto* connection: this->connections)
        connection->destroy();
    Q_ASSERT(this->connections.empty());
}
