#include <Graphics/InputGraphicsBox.hpp>

#include <memory>

#include <QtCore/QString>
#include <QtCore/QPointF>
#include <QGraphicsSceneContextMenuEvent>
#include <QtWidgets/QMenu>

#include <Graphics/Dialogs/InputDialog.hpp>

InputGraphicsBox::InputGraphicsBox(std::unique_ptr<InputModelBox> p_modelBox,
                                         const QPointF& p_pos)
    : GraphicsBox(std::move(p_modelBox), p_pos)
{
    this->color = Qt::red;
}

void InputGraphicsBox::on_SetInput()
{
    auto* model = dynamic_cast<InputModelBox*>(this->getModelBox());
    Q_ASSERT(model);
    auto inputDialog = std::make_unique<InputDialog>(model->getAutomaton());
    
    if (inputDialog->exec()) {
        model->setAutomaton(inputDialog->getAutomaton());
    }

    this->updateColor();
}

void InputGraphicsBox::contextMenuEvent(QGraphicsSceneContextMenuEvent* event) {
    QMenu menu;
    auto* a = menu.addAction("&Set input");
    QObject::connect(a, SIGNAL(triggered()), this, SLOT(on_SetInput()));
    menu.exec(event->screenPos());
    ungrabMouse();
    event->accept();
}

void InputGraphicsBox::updateColor() {
    auto* model = dynamic_cast<InputModelBox*>(this->getModelBox());
    Q_ASSERT(model);
    this->color = model->getAutomaton() ? Qt::blue : Qt::red;
    this->update();
}
