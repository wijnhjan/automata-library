#pragma once
#include <QGraphicsScene>
#include <QObject>
#include <QGraphicsSceneWheelEvent>

class GraphicsScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit GraphicsScene(QObject * parent);

protected:
    void wheelEvent(QGraphicsSceneWheelEvent *event) override;
};


