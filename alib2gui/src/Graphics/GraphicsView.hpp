#pragma once
#include <QtWidgets/QGraphicsView>

class GraphicsView : public QGraphicsView {
public:
    using QGraphicsView::QGraphicsView;
protected:
    void mousePressEvent(QMouseEvent* event) override;
};


