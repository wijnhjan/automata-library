#pragma once

#include <QtGui/QFont>
#include <QtWidgets/QGraphicsObject>

#include <Models/ModelBox.hpp>

class ConnectionBox;
class InputConnectionBox;
class OutputConnectionBox;

class GraphicsBox : public QGraphicsObject {
    Q_OBJECT
public:
    GraphicsBox(std::unique_ptr<ModelBox> p_modelBox, QPointF pos);
    ~GraphicsBox() override;

    QRectF boundingRect() const override;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;

    ModelBox* getModelBox() const { return this->modelBox.get(); }

    const std::vector<InputConnectionBox*>& getInputConnectionBoxes() const;
    OutputConnectionBox* getOutputConnectionBox() const;

    static const std::vector<GraphicsBox*>& getAllGraphicsBoxes() { return GraphicsBox::allGraphicsBoxes; }

protected:
    QColor color;

private:
    QRectF boundRect;
    QString text;
    QFont font;

    std::unique_ptr<ModelBox> modelBox;

    std::vector<InputConnectionBox*> inputConnectionBoxes;
    OutputConnectionBox* outputConnectionBox = nullptr;

    static std::vector<GraphicsBox*> allGraphicsBoxes;
};


