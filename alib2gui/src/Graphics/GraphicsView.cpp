#include <Graphics/GraphicsView.hpp>

#include <QMouseEvent>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QWidget>

#include <Algorithm/Registry.hpp>
#include <Graphics/GraphicsBox.hpp>
#include <Graphics/InputGraphicsBox.hpp>
#include <MainWindow.hpp>
#include <Models/AlgorithmModelBox.hpp>

void GraphicsView::mousePressEvent(QMouseEvent* event) {
    auto* w = MainWindow::getInstance();
    const auto& l_data = w->getCursorData();
    if (l_data.empty()) {
        QGraphicsView::mousePressEvent(event);
        return;
    }

    if (l_data == MainWindow::ADD_INPUT_CURSOR_DATA) {
        this->scene()->addItem(new InputGraphicsBox(std::make_unique<InputModelBox>(), this->mapToScene(event->pos())));
    }
    else {
        if (auto* algo = Registry::getAlgorithm(l_data)) {
            this->scene()->addItem(new GraphicsBox(std::make_unique<AlgorithmModelBox>(algo), this->mapToScene(event->pos())));
        }
    }

    event->accept();
    w->setCursorData({});
}

