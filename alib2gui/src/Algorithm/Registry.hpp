#pragma once
#include <map>
#include <memory>
#include <string>

#include <Algorithm/Algorithm.hpp>

class Registry {
public:
    static void initialize();
    static void deinitialize();

    static Algorithm* getAlgorithm(const std::string& name);
    static const std::map<std::string, std::unique_ptr<Algorithm>>& getAlgorithms();

private:
    Registry() = default;

    static Registry& getInstance();

    std::map<std::string, std::unique_ptr<Algorithm>> algorithms;
};
