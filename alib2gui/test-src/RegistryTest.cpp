#include <catch2/catch.hpp>
#include <Algorithm/Registry.hpp>


TEST_CASE ( "Registry", "[unit][gui][registry]" ) {
	SECTION ( "Registry test" ) {
		Registry::deinitialize();

		REQUIRE(Registry::getAlgorithm("automaton::determinize::Determinize") == nullptr);

		Registry::initialize();

		auto* algo = Registry::getAlgorithm("automaton::determinize::Determinize");
		CHECK(algo != nullptr);

		CHECK(Registry::getAlgorithm("nonexistent_algorithm") == nullptr);
	}

	SECTION ( "Algorithm Test Basic" ) {
		const std::string name = "automaton::determinize::Determinize";
		auto* algo = Registry::getAlgorithm(name);

		CHECK(algo->getFullName() == name);
		CHECK(algo->getPrettyName() == "Determinize");

		const auto& groups = algo->getGroups();
		CHECK(groups[0] == "automaton");
		CHECK(groups[1] == "determinize");

		CHECK(algo->getInputCount() == 1);
	}

	SECTION ( "Algorithms Test Input Count" ) {
		auto* algo = Registry::getAlgorithm("automaton::transform::AutomataConcatenation");

		REQUIRE(algo != nullptr);
		CHECK(algo->getInputCount() == 2);
	}

	SECTION ( "Algorithm Test Pretty Name" ) {
		auto* algo = Registry::getAlgorithm("automaton::transform::AutomataConcatenationEpsilonTransition");

		REQUIRE(algo != nullptr);
		CHECK(algo->getPrettyName() == u8"\u03B5-Concatenate");
	}
}
