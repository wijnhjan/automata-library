#include <environment/Environment.h>

#include <lexer/Lexer.h>
#include <parser/Parser.h>

#include <alib/exception>

#include <global/GlobalData.h>

namespace cli {

cli::CommandResult Environment::execute ( std::shared_ptr < cli::LineInterface > lineInterface ) {
	cli::CommandResult state = cli::CommandResult::OK;

	while ( state == cli::CommandResult::OK || state == cli::CommandResult::ERROR || state == cli::CommandResult::EXCEPTION )
		state = execute_line ( cli::CharSequence ( lineInterface ) );

	return state;
}

cli::CommandResult Environment::execute_line ( cli::CharSequence charSequence ) {
	try {
		cli::Parser parser = cli::Parser ( cli::Lexer ( std::move ( charSequence ) ) );
		cli::CommandResult res = parser.parse ( )->run ( * this );

		if ( res == CommandResult::CONTINUE || res == CommandResult::BREAK )
			throw std::logic_error ( "There is no loop to continue/break." );

		return res;
	} catch ( ... ) {
		alib::ExceptionHandler::handle ( common::Streams::err );
		return cli::CommandResult::EXCEPTION;
	}
}

} /* namespace cli */
