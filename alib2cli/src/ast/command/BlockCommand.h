#ifndef _CLI_BLOCK_COMMAND_H_
#define _CLI_BLOCK_COMMAND_H_

#include <ast/Command.h>

namespace cli {

class BlockCommand final : public Command {
	std::unique_ptr < Command > m_innerCommand;

public:
	BlockCommand ( std::unique_ptr < Command > innerCommand ) : m_innerCommand ( std::move ( innerCommand ) ) {
	}

	CommandResult run ( Environment & environment ) const override {
		Environment newEnvironment { ext::optional_ref < Environment > ( environment ) };
		CommandResult res = m_innerCommand->run ( newEnvironment );
		if ( newEnvironment.getResult ( ) )
			environment.setResult ( newEnvironment.getResult ( ) );
		return res;
	}

};

} /* namespace cli */

#endif /* _CLI_BLOCK_COMMAND_H_ */
