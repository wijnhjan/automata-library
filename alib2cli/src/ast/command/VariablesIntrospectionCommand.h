#ifndef _CLI_VARIABLES_INTROSPECTION_COMMAND_H_
#define _CLI_VARIABLES_INTROSPECTION_COMMAND_H_

#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class VariablesIntrospectionCommand : public Command {
	std::unique_ptr < cli::Arg > m_param;

public:
	VariablesIntrospectionCommand ( std::unique_ptr < cli::Arg > param ) : m_param ( std::move ( param ) ) {
	}

	CommandResult run ( Environment & environment ) const override {
		std::string param;
		if ( m_param != nullptr )
			param = m_param->eval ( environment );

		if ( param.empty ( ) )
			for ( const std::string & name : environment.getVariableNames ( ) )
				common::Streams::out << name << std::endl;
		else
			common::Streams::out << param << " " << environment.getVariable ( param )->getType ( ) << std::endl;

		return CommandResult::OK;
	}
};

} /* namespace cli */

#endif /* _CLI_VARIABLES_INTROSPECTION_COMMAND_H_ */
