#ifndef _CLI_COMMAND_LIST_H_
#define _CLI_COMMAND_LIST_H_

#include <ast/Command.h>

namespace cli {

class CommandList final : public Command {
	ext::vector < std::unique_ptr < Command > > m_commands;

public:
	CommandList ( ext::vector < std::unique_ptr < Command > > commands ) : m_commands ( std::move ( commands ) ) {
	}

	CommandResult run ( Environment & environment ) const override {
		CommandResult res = CommandResult::OK;
		for ( size_t i = 0; i < m_commands.size ( ) && res == CommandResult::OK; ++ i ) {
			res = m_commands [ i ]->run ( environment );
			environment.clearTemporaries ( );

			if ( res == CommandResult::CONTINUE || res == CommandResult::BREAK )
				return res;
		}

		return res;
	}

	void append ( std::unique_ptr < Command > command ) {
		m_commands.emplace_back ( std::move ( command ) );
	}

};

} /* namespace cli */

#endif /* _CLI_COMMAND_LIST_H_ */
