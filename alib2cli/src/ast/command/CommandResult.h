#ifndef _CLI_COMMAND_RESULT_H_
#define _CLI_COMMAND_RESULT_H_

namespace cli {

enum class CommandResult {
	OK,
	QUIT,
	RETURN,
	CONTINUE,
	BREAK,
	EXCEPTION,
	ERROR,
	EOT
};

} /* namespace cli */

#endif /* _CLI_COMMAND_RESULT_H_ */
