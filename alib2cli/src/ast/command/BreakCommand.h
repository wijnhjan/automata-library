#ifndef _CLI_BREAK_COMMAND_H_
#define _CLI_BREAK_COMMAND_H_

#include <ast/Command.h>
#include <environment/Environment.h>

namespace cli {

class BreakCommand : public Command {
public:
	CommandResult run ( Environment & /* environment */ ) const override {
		return CommandResult::BREAK;
	}
};

} /* namespace cli */

#endif /* _CLI_BREAK_COMMAND_H_ */
