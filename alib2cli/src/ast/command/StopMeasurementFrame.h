#ifndef _CLI_STOP_MEASUREMENT_FRAME_H_
#define _CLI_STOP_MEASUREMENT_FRAME_H_

#include <ast/Command.h>
#include <environment/Environment.h>

#include <alib/measure>

namespace cli {

class StopMeasurementFrame : public Command {
public:
	CommandResult run ( Environment & ) const override {
		measurements::end ( );
		return CommandResult::OK;
	}
};

} /* namespace cli */

#endif /* _CLI_STOP_MEASUREMENT_FRAME_H_ */
