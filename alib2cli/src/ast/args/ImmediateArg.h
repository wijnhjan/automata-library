#ifndef _CLI_IMMEDIATE_ARG_H__H_
#define _CLI_IMMEDIATE_ARG_H__H_

#include <ast/Arg.h>
#include <alib/string>

namespace cli {

class ImmediateArg final : public Arg {
	std::string m_value;

public:
	ImmediateArg ( std::string value ) : m_value ( std::move ( value ) ) {
	}

	std::string eval ( Environment & ) const override {
		return m_value;
	}
};

} /* namespace cli */

#endif /* _CLI_IMMEDIATE_ARG_H__H_ */
