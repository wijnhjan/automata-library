#ifndef _CLI_VALUE_OPTION_H_
#define _CLI_VALUE_OPTION_H_

#include <ast/Option.h>
#include <exception/CommonException.h>

namespace cli {

class TypeOption final : public Option {
	std::string m_type;

public:
	TypeOption ( std::string type ) : m_type ( std::move ( type ) ) {
	}

	const std::string & getType ( ) const {
		return m_type;
	}
};

} /* namespace cli */

#endif /* _CLI_VALUE_OPTION_H_ */
