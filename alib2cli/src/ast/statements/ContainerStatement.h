#ifndef _CLI_CONTAINER_STATEMENT_H_
#define _CLI_CONTAINER_STATEMENT_H_

#include <ast/Statement.h>
#include <ast/options/TypeOption.h>

namespace cli {

class ContainerStatement final : public Statement {
	std::string m_container;
	ext::vector < std::shared_ptr < Statement > > m_params;
	std::unique_ptr < TypeOption > m_type;

public:
	ContainerStatement ( std::string container, ext::vector < std::shared_ptr < Statement > > params, std::unique_ptr < TypeOption > type );

	std::shared_ptr < abstraction::Value > translateAndEval ( const std::shared_ptr < abstraction::Value > & prev, Environment & environment ) const override;

};

} /* namespace cli */

#endif /* _CLI_CONTAINER_STATEMENT_H_ */
