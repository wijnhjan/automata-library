#ifndef _CLI_SINGLE_STATEMENT_H_
#define _CLI_SINGLE_STATEMENT_H_

#include <ast/Statement.h>
#include <registry/Registry.h>
#include <ast/options/CategoryOption.h>

namespace cli {

class SingleStatement final : public Statement {
	std::unique_ptr < cli::Arg > m_name;
	ext::vector < std::unique_ptr < cli::Arg > > m_templateParams;
	ext::vector < std::shared_ptr < Statement > > m_params;
	std::unique_ptr < CategoryOption > m_category;

public:
	SingleStatement ( std::unique_ptr < cli::Arg > name, ext::vector < std::unique_ptr < cli::Arg > > templateParams, ext::vector < std::shared_ptr < Statement > > params, std::unique_ptr < CategoryOption > category );

	std::shared_ptr < abstraction::Value > translateAndEval ( const std::shared_ptr < abstraction::Value > & prev, Environment & environment ) const override;

};

} /* namespace cli */

#endif /* _CLI_SINGLE_STATEMENT_H_ */
