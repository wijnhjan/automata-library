#ifndef _CLI_FILE_STATEMENT_H_
#define _CLI_FILE_STATEMENT_H_

#include <ast/Statement.h>
#include <ast/options/TypeOption.h>

namespace cli {

class FileStatement final : public Statement {
	std::unique_ptr < cli::Arg > m_file;
	std::unique_ptr < Arg > m_fileType;
	std::unique_ptr < TypeOption > m_type;
	ext::vector < std::unique_ptr < cli::Arg > > m_templateParams;

public:
	FileStatement ( std::unique_ptr < Arg > file, std::unique_ptr < Arg > fileType, std::unique_ptr < TypeOption > type, ext::vector < std::unique_ptr < cli::Arg > > templateParams );

	std::shared_ptr < abstraction::Value > translateAndEval ( const std::shared_ptr < abstraction::Value > &, Environment & environment ) const override;

};

} /* namespace cli */

#endif /* _CLI_FILE_STATEMENT_H_ */
