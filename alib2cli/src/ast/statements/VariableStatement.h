#ifndef _CLI_VARIABLE_STATEMENT_H_
#define _CLI_VARIABLE_STATEMENT_H_

#include <alib/string>
#include <ast/Statement.h>

namespace cli {

class VariableStatement final : public Statement {
	std::unique_ptr < cli::Arg > m_name;

public:
	VariableStatement ( std::unique_ptr < cli::Arg > name ) : m_name ( std::move ( name ) ) {
	}

	std::shared_ptr < abstraction::Value > translateAndEval ( const std::shared_ptr < abstraction::Value > &, Environment & environment ) const override {
		return environment.getVariable ( m_name->eval ( environment ) );
	}

};

} /* namespace cli */

#endif /* _CLI_VARIABLE_STATEMENT_H_ */
