#include <ast/statements/FileStatement.h>
#include <ast/Statement.h>
#include <ast/Option.h>
#include <ast/Arg.h>

#include <registry/InputFileRegistry.hpp>

namespace cli {

FileStatement::FileStatement ( std::unique_ptr < Arg > file, std::unique_ptr < Arg > fileType, std::unique_ptr < TypeOption > type, ext::vector < std::unique_ptr < cli::Arg > > templateParams ) : m_file ( std::move ( file ) ), m_fileType ( std::move ( fileType ) ), m_type ( std::move ( type ) ), m_templateParams ( std::move ( templateParams ) ) {
}

std::shared_ptr < abstraction::Value > FileStatement::translateAndEval ( const std::shared_ptr < abstraction::Value > &, Environment & environment ) const {
	std::string filetype = "xml";
	if ( m_fileType )
		filetype = m_fileType->eval ( environment );

	std::string type = "";
	if ( m_type )
		type = m_type->getType ( );

	ext::vector < std::string > templateParams;
	for ( const std::unique_ptr < cli::Arg > & param : m_templateParams ) {
		templateParams.push_back ( param->eval ( environment ) );
	}

	std::shared_ptr < abstraction::OperationAbstraction > res = abstraction::InputFileRegistry::getAbstraction ( filetype, type, templateParams );

	std::shared_ptr < abstraction::ValueHolder < std::string > > file = std::make_shared < abstraction::ValueHolder < std::string > > ( m_file->eval ( environment ), true );
	res->attachInput ( file, 0 );

	return res->eval ( );
}

} /* namespace cli */
