#ifndef _CLI_CAST_STATEMENT_H_
#define _CLI_CAST_STATEMENT_H_

#include <ast/Statement.h>
#include <common/CastHelper.h>

namespace cli {

class CastStatement final : public Statement {
	std::unique_ptr < cli::Arg > m_type;
	std::shared_ptr < Statement > m_statement;

public:
	CastStatement ( std::unique_ptr < cli::Arg > type, std::shared_ptr < Statement > statement ) : m_type ( std::move ( type ) ), m_statement ( std::move ( statement ) ) {
	}

	std::shared_ptr < abstraction::Value > translateAndEval ( const std::shared_ptr < abstraction::Value > & prev, Environment & environment ) const override {
		std::string type = m_type->eval ( environment );

		std::shared_ptr < abstraction::Value > translatedStatement = m_statement->translateAndEval ( prev, environment );

		environment.holdTemporary ( translatedStatement );

		return abstraction::CastHelper::eval ( environment, translatedStatement, type );
	}

};

} /* namespace cli */

#endif /* _CLI_CAST_STATEMENT_H_ */
