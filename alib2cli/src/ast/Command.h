#ifndef _CLI_COMMAND_H_
#define _CLI_COMMAND_H_

#include <ast/command/CommandResult.h>

namespace cli {

class Environment;

class Command {
public:
	virtual ~Command ( ) noexcept = default;

	virtual CommandResult run ( Environment & environment ) const = 0;
};

} /* namespace cli */

#endif /* _CLI_COMMAND_H_ */
