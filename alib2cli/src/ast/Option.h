#ifndef _CLI_OPTION_H_
#define _CLI_OPTION_H_

namespace cli {

class Option {
public:
	virtual ~Option ( ) noexcept = default;
};

} /* namespace cli */

#endif /* _CLI_OPTION_H_ */
