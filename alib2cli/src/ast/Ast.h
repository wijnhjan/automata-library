#ifndef _CLI_AST_H_
#define _CLI_AST_H_

namespace cli {

class Option;
class Statement;
class Param;
class Arg;

} /* namespace cli */

#endif /* _CLI_AST_H_ */
