/*
 * Dot.h
 *
 *  Created on: 16. 8. 2017
 *	  Author: Jan Travnicek, Tomas Pecka
 */

#ifndef _DOT_H_
#define _DOT_H_

#include <alib/string>
#include <alib/vector>

#include <sys/wait.h>
#include <signal.h>
#include <string.h>

namespace cli {

namespace builtin {

/**
 * Dot visualisation executioner.
 *
 */
class Dot {
public:
	/**
	 * Cli builtin command for DOT format visualization
	 *
	 * \param dot a string containing dot data
	 */
	static void dot ( const std::string & data );

	/**
	 * Cli builtin command for DOT format visualization
	 *
	 * \param data a string containing dot data
	 * \param outputType the type of dot created image
	 * \param outputFile the destination file name
	 */
	static void dot ( const std::string & data, const std::string & outputType, const std::string & outputFile );

protected:
	static std::vector < std::string > allowedTypes;
	static void run ( const std::string & data, const std::string & outputType = "", const std::string & outputFile = "" );
	static void runBg ( const std::string & data );
};

class Dot2 : public Dot {
public:
	/**
	 * Cli builtin command for DOT format visualization
	 * Forks a child process with X11 visualization.
	 *
	 * \param dot a string containing dot data
	 */
	static void dot ( const std::string & data );
};

} /* namespace builtin */

} /* namespace cli */

#endif /* _DOT_H_ */
