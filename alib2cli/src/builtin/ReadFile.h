/*
 * ReadFile.h
 *
 *  Created on: 16. 8. 2017
 *	  Author: Jan Travnicek
 */

#ifndef _READ_FILE_H_
#define _READ_FILE_H_

#include <alib/string>

namespace cli {

namespace builtin {

/**
 * File reader command.
 */
class ReadFile {
public:
	/**
	 * Reads the content of a file into a string.
	 *
	 * \param filename the name of read file
	 *
	 * \return the content of the file
	 */
	static std::string read ( const std::string & filename );
};

} /* namespace builtin */

} /* namespace cli */

#endif /* _READ_FILE_H_ */
