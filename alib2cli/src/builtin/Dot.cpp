/*
 * Dot.cpp
 *
 *  Created on: 16. 8. 2017
 *	  Author: Jan Travnicek, Tomas Pecka
 */

#include "Dot.h"
#include <cstdlib>
#include <cctype>

#include <alib/algorithm>
#include <numeric>
#include <registration/AlgoRegistration.hpp>
#include <exception/CommonException.h>

#include <unistd.h>
#include <sys/wait.h>
#include <cstring>


namespace cli {

namespace builtin {

std::vector < std::string> Dot::allowedTypes { "dot", "xdot", "ps", "pdf", "svg", "svgz", "fig", "png", "gif", "jpg", "jpeg", "json", "imap", "cmapx" };

void Dot::runBg ( const std::string & data ) {
	int fd [ 2 ];
	if ( pipe ( fd ) != 0 )
		throw exception::CommonException ( "Dot: Failed to initialize communication pipe." );

	pid_t pid = fork ( );

	if ( pid < 0 ) {
		throw exception::CommonException ( "Dot: Failed to fork." );
	} else if ( pid == 0 ) {
		close ( STDIN_FILENO );
		close ( STDOUT_FILENO );
		dup2 ( fd [ 0 ], STDIN_FILENO );
		close ( fd [ 0 ] );
		close ( fd [ 1 ] );

		if ( execlp ( "dot", "dot", "-Tx11", nullptr ) == -1 )
			throw exception::CommonException ( "Dot: Failed to spawn child process." );
	} else {
		close ( fd [ 0 ] );
		if ( write ( fd [ 1 ], data.c_str ( ), data.size ( ) + 1 ) != ( ssize_t ) data.size ( ) + 1 )
			throw exception::CommonException ( "Dot: Failed to write data to dot child process." );

		close ( fd [ 1 ] );
	}
}

void Dot::run ( const std::string & data, const std::string & outputType, const std::string & outputFile ) {
	std::string cli;
	if ( outputType.empty ( ) )
		cli = "dot -Tx11 <<DOTDATA\n" + data + "\nDOTDATA";
	else
		cli = "dot -T" + outputType + " -o " + outputFile + " <<DOTDATA\n" + data + "\nDOTDATA";

	const int ret = std::system ( cli.c_str ( ) );

	if ( WIFEXITED ( ret ) && WEXITSTATUS ( ret ) != 0 ) {
		throw exception::CommonException ( "Dot: Dot exited with exit status " + ext::to_string ( WEXITSTATUS ( ret ) ) );
	} else if ( WIFSIGNALED ( ret ) ) {
		throw exception::CommonException ( std::string ( "Dot: Dot terminated by signal " ) + strsignal ( WTERMSIG ( ret ) ) );
	}
}

void Dot::dot ( const std::string & data ) {
	run ( data );
}

void Dot::dot ( const std::string & data, const std::string & outputType, const std::string & outputFile ) {
	std::string outputTypeUpper;
	std::transform ( outputType.begin( ), outputType.end(), std::back_inserter ( outputTypeUpper ), ::tolower );

	if ( std::find ( allowedTypes.begin( ), allowedTypes.end ( ), outputTypeUpper ) == allowedTypes.end ( ) ) {
		const std::string allowedTypesHelp = std::accumulate ( allowedTypes.begin ( ), allowedTypes.end ( ), std::string ( ), [ ] ( const std::string & res, const std::string & e ) { return res.empty ( ) ? e : res + ", " + e; } );
		throw exception::CommonException ( "Dot: Output type is invalid. Allowed types are: " + allowedTypesHelp );
	}

	run ( data, outputTypeUpper, outputFile );
}

void Dot2::dot ( const std::string & data ) {
	runBg ( data );
}

auto DotTx11 = registration::AbstractRegister < Dot, void, const std::string & > ( Dot::dot, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "dot" ).setDocumentation (
"Cli builtin command for DOT format visualization.\n\
\n\
@param dot a string containing dot data" );

auto DotTx11Bg = registration::AbstractRegister < Dot2, void, const std::string & > ( Dot2::dot, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "dot" ).setDocumentation (
"Cli builtin command for DOT format visualization. Runs in background.\n\
\n\
@param dot a string containing dot data" );

auto DotFile = registration::AbstractRegister < Dot, void, const std::string &, const std::string &, const std::string & > ( Dot::dot, abstraction::AlgorithmCategories::AlgorithmCategory::DEFAULT, "data", "outputType", "outputFile" ).setDocumentation (
"Cli builtin command for DOT format visualization\n\
\n\
@param data a string containing dot data\n\
@param outputType the type of dot created image\n\
@param outputFile the destination file name" );

} /* namespace builtin */

} /* namespace cli */

