#include <alib/typeinfo>

#include "OutputFileTypeRegistration.hpp"

#include <exception/CommonException.h>

#include <registry/XmlRegistry.h>

#include <abstraction/XmlTokensComposerAbstraction.hpp>
#include <abstraction/PackingAbstraction.hpp>

#include <registry/Registry.h>

namespace {

	std::shared_ptr < abstraction::OperationAbstraction > dummy ( const std::string & typehint ) {
		ext::vector < std::shared_ptr < abstraction::OperationAbstraction > > abstractions;

		abstractions.push_back ( abstraction::XmlRegistry::getXmlComposerAbstraction ( typehint ) );

		abstractions.push_back ( std::make_shared < abstraction::XmlTokensComposerAbstraction > ( ) );

		std::shared_ptr < abstraction::PackingAbstraction < 2 > > res = std::make_shared < abstraction::PackingAbstraction < 2 > > ( std::move ( abstractions ), 1 );

		res->setInnerConnection ( 0, 1, 0 );
		res->setOuterConnection ( 0, 1, 1 ); // filename
		res->setOuterConnection ( 1, 0, 0 ); // data

		return res;
	}

	std::shared_ptr < abstraction::OperationAbstraction > dummy2 ( const std::string & ) {
		ext::vector < std::string > templateParams;
		ext::vector < std::string > paramTypes { ext::to_string < std::string > ( ), ext::to_string < std::string > ( ) };
		abstraction::AlgorithmCategories::AlgorithmCategory category = abstraction::AlgorithmCategories::AlgorithmCategory::NONE;
		ext::vector < abstraction::TypeQualifiers::TypeQualifierSet > paramTypeQualifiers { abstraction::TypeQualifiers::typeQualifiers < const std::string & > ( ), abstraction::TypeQualifiers::typeQualifiers < const std::string & > ( ) };

		return abstraction::Registry::getAlgorithmAbstraction ( "cli::builtin::WriteFile", templateParams, paramTypes, paramTypeQualifiers, category );
	}

auto xmlOutputFileHandler = registration::OutputFileRegister ( "xml", dummy );
auto fileOutputFileHandler = registration::OutputFileRegister ( "file", dummy2 );

}
