#ifndef _INPUT_FILE_REGISTER_HPP_
#define _INPUT_FILE_REGISTER_HPP_

#include <registry/OutputFileRegistry.hpp>

namespace registration {

class OutputFileRegister {
	std::string m_FileType;

public:
	OutputFileRegister ( std::string fileType, std::shared_ptr < abstraction::OperationAbstraction > ( * callback ) ( const std::string & typehint ) ) : m_FileType ( std::move ( fileType ) ) {
		abstraction::OutputFileRegistry::registerOutputFileHandler ( m_FileType, callback );
	}

	~OutputFileRegister ( ) {
		abstraction::OutputFileRegistry::unregisterOutputFileHandler ( m_FileType );
	}
};

} /* namespace registration */

#endif // _INPUT_FILE_REGISTER_HPP_
