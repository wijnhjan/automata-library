#ifndef __CHAR_SEQUENCE_H_
#define __CHAR_SEQUENCE_H_

#include <string>
#include <vector>
#include <memory>

#include <readline/LineInterface.h>

namespace cli {

class CharSequence {
	std::shared_ptr < LineInterface > m_lineInterface;
	std::string putbackBuffer;

	const char * linePtr = nullptr;
	std::vector < std::string > m_lines;

	void fetch ( bool readNextLine );

	std::string getLine ( ) const;
public:
	CharSequence ( std::shared_ptr < cli::LineInterface > reader ) : m_lineInterface ( std::move ( reader ) ) {
	}

	template < class Interface >
	CharSequence ( Interface && reader ) : m_lineInterface ( std::make_shared < Interface > ( std::forward < Interface > ( reader ) ) ) {
	}

	CharSequence ( CharSequence && ) = default;

	CharSequence ( const CharSequence & ) = delete;

	CharSequence & operator = ( CharSequence && ) = delete;

	CharSequence & operator = ( const CharSequence & ) = delete;

	~CharSequence ( ) {
		if ( m_lineInterface )
			m_lineInterface->lineCallback ( getLine ( ) );
	}

	int getCharacter ( ) const;

	void advance ( bool readNextLine );

	void putback ( std::string string ) {
		putbackBuffer.insert ( putbackBuffer.end ( ), string.rbegin ( ), string.rend ( ) );
	}
};

} /* namespace cli */

#endif /* __CHAR_SEQUENCE_H_ */
