/*
 * XmlParserAbstraction.hpp
 *
 *  Created on: 11. 7. 2017
 *	  Author: Jan Travnicek
 */

#ifndef _XML_PARSER_ABSTRACTION_HPP_
#define _XML_PARSER_ABSTRACTION_HPP_

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

#include <factory/XmlDataFactory.hpp>

namespace abstraction {

template < class ReturnType >
class XmlParserAbstraction : virtual public NaryOperationAbstraction < ext::deque < sax::Token > && >, virtual public ValueOperationAbstraction < ReturnType > {
public:
	std::shared_ptr < abstraction::Value > run ( ) override {
		std::shared_ptr < abstraction::Value > & param = std::get < 0 > ( this->getParams ( ) );
		return std::make_shared < abstraction::ValueHolder < ReturnType > > ( factory::XmlDataFactory::fromTokens ( abstraction::retrieveValue < ext::deque < sax::Token > && > ( param ) ), true );
	}

};

} /* namespace abstraction */

#endif /* _XML_PARSER_ABSTRACTION_HPP_ */
