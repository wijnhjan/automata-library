/*
 * XmlTokensParserAbstraction.hpp
 *
 *  Created on: 7. 10. 2017
 *	  Author: Jan Travnicek
 */

#ifndef _XML_TOKENS_PARSER_ABSTRACTION_HPP_
#define _XML_TOKENS_PARSER_ABSTRACTION_HPP_

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

#include <sax/SaxParseInterface.h>

namespace abstraction {

class XmlTokensParserAbstraction : virtual public NaryOperationAbstraction < const std::string & >, virtual public ValueOperationAbstraction < ext::deque < sax::Token > > {
public:
	std::shared_ptr < abstraction::Value > run ( ) override {
		std::shared_ptr < abstraction::Value > & param = std::get < 0 > ( this->getParams ( ) );
		return std::make_shared < abstraction::ValueHolder < ext::deque < sax::Token > > > ( sax::SaxParseInterface::parseFile ( abstraction::retrieveValue < const std::string & > ( param ) ), true );
	}

};

} /* namespace abstraction */

#endif /* _XML_TOKENS_PARSER_ABSTRACTION_HPP_ */
