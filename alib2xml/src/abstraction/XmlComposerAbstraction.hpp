/*
 * XmlComposerAbstraction.hpp
 *
 *  Created on: 11. 7. 2017
 *	  Author: Jan Travnicek
 */

#ifndef _XML_COMPOSER_ABSTRACTION_HPP_
#define _XML_COMPOSER_ABSTRACTION_HPP_

#include <abstraction/NaryOperationAbstraction.hpp>
#include <abstraction/ValueOperationAbstraction.hpp>

#include <factory/XmlDataFactory.hpp>

namespace abstraction {

template < class ParamType >
class XmlComposerAbstraction : virtual public NaryOperationAbstraction < const ParamType & >, virtual public ValueOperationAbstraction < ext::deque < sax::Token > > {
public:
	std::shared_ptr < abstraction::Value > run ( ) override {
		std::shared_ptr < abstraction::Value > & param = std::get < 0 > ( this->getParams ( ) );
		return std::make_shared < abstraction::ValueHolder < ext::deque < sax::Token > > > ( factory::XmlDataFactory::toTokens ( abstraction::retrieveValue < const ParamType & > ( param ) ), true );
	}

};

} /* namespace abstraction */

#endif /* _XML_COMPOSER_ABSTRACTION_HPP_ */
