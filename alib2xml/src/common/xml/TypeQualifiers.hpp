#ifndef _XML_PARAM_QUALIFIERS_HPP_
#define _XML_PARAM_QUALIFIERS_HPP_

#include <common/TypeQualifiers.hpp>
#include <core/xmlApi.hpp>

namespace core {

template < >
struct xmlApi < abstraction::TypeQualifiers::TypeQualifierSet > {
	static abstraction::TypeQualifiers::TypeQualifierSet parse ( ext::deque < sax::Token >::iterator & input );
	static bool first ( const ext::deque < sax::Token >::const_iterator & input );
	static std::string xmlTagName ( );
	static void compose ( ext::deque < sax::Token > & output, abstraction::TypeQualifiers::TypeQualifierSet typeQualifiers );
};

} /* namespace core */

#endif /* _PARAM_QUALIFIERS_HPP_ */
