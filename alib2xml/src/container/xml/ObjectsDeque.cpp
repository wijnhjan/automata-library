/*
 * Deque.cpp
 *
 * Created on: Apr 1, 2013
 * Author: Jan Travnicek
 */

#include "ObjectsDeque.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < ext::deque < object::Object > > ( );
auto xmlReader = registration::XmlReaderRegister < ext::deque < object::Object > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, ext::deque < object::Object > > ( );

} /* namespace */
