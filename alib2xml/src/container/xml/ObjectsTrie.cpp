/*
 * ObjectsTrie.cpp
 *
 * Created on: Apr 1, 2013
 * Author: Jan Travnicek
 */

#include "ObjectsTrie.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < ext::trie < object::Object, object::Object > > ( );
auto xmlRead = registration::XmlReaderRegister < ext::trie < object::Object, object::Object > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, ext::trie < object::Object, object::Object > > ( );

} /* namespace */
