/*
 * Set.cpp
 *
 * Created on: Apr 1, 2013
 * Author: Jan Travnicek
 */

#include "ObjectsSet.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < ext::set < object::Object > > ( );
auto xmlRead = registration::XmlReaderRegister < ext::set < object::Object > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, ext::set < object::Object > > ( );

} /* namespace */
