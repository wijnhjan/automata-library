/*
 * List.cpp
 *
 * Created on: Apr 1, 2013
 * Author: Jan Travnicek
 */

#include "ObjectsList.h"

#include <registration/XmlRegistration.hpp>

namespace {

auto xmlWrite = registration::XmlWriterRegister < ext::list < object::Object > > ( );
auto xmlRead = registration::XmlReaderRegister < ext::list < object::Object > > ( );

auto xmlGroup = registration::XmlRegisterTypeInGroup < object::Object, ext::list < object::Object > > ( );

} /* namespace */
