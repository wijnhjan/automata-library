/*
 * ParserException.cpp
 *
 *  Created on: Apr 16, 2013
 *      Author: Martin Zak
 */

#include "ParserException.h"

namespace sax {

ParserException::ParserException(const Token& expected, const Token& read) : CommonException("Parser Exception: Expected: " + expected.getData() + " Read: " + read.getData()), m_expected(expected), m_read(read) {
}

} /* namespace sax */

