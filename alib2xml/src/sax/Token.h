/*
 * ParseToken.h
 *
 *  Created on: 9.8.2012
 *      Author: Martin Zak
 */

#ifndef TOKEN_H_
#define TOKEN_H_

#include <alib/string>
#include <ostream>

namespace sax {

/**
 * Represents part of parsed XML. Can be start of tag, end of tag,
 * tag attribute or array of characters.
 */
class Token {
public:

	enum class TokenType {
		START_ELEMENT, END_ELEMENT, START_ATTRIBUTE, END_ATTRIBUTE, CHARACTER
	};

private:
	std::string data;
	TokenType type;

public:
	Token ( std::string, TokenType );

	/**
	 * @return name of the tag or characters read
	 */
	const std::string & getData ( ) const &;

	/**
	 * @return name of the tag or characters read
	 */
	std::string && getData ( ) &&;

	/**
	 * @return type of the token - star of the tag, end of the tag, attribute
	 * of the tag or characters
	 */
	TokenType getType() const;

	bool operator==(const Token& other) const;

	friend std::ostream& operator<<(std::ostream& os, const Token& token);

	explicit operator std::string () const;
};

} /* namespace sax */

#endif /* TOKEN_H_ */
