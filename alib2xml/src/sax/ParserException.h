/*
 * ParserException.h
 *
 *  Created on: Apr 16, 2013
 *      Author: Martin Zak
 */

#ifndef PARSER_EXCEPTION_H_
#define PARSER_EXCEPTION_H_

#include <exception/CommonException.h>
#include "Token.h"

namespace sax {

/**
 * Exception thrown by XML parser when is expected different tag than the one which is read.
 */
class ParserException: public exception::CommonException {
	Token m_expected;
	Token m_read;
public:
	ParserException(const Token& expected, const Token& read);
};

} /* namespace sax */

#endif /* PARSER_EXCEPTION_H_ */

