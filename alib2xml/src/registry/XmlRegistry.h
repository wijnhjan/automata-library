/*
 * XmlRegistry.h
 *
 *  Created on: 10. 7. 2017
 *	  Author: Jan Travnicek
 */

#ifndef _XML_REGISTRY_H_
#define _XML_REGISTRY_H_

#include <abstraction/OperationAbstraction.hpp>

#include <alib/set>
#include <string>

namespace abstraction {

class XmlRegistry {
public:
	static ext::set < std::string > listDataTypes ( );
	static ext::set < std::string > listDataTypeGroup ( const std::string & group );

	static std::shared_ptr < abstraction::OperationAbstraction > getXmlComposerAbstraction ( const std::string & param );
	static std::shared_ptr < abstraction::OperationAbstraction > getXmlParserAbstraction ( const std::string & type );

	static std::shared_ptr < abstraction::OperationAbstraction > getXmlContainerParserAbstraction ( const std::string & container, const std::string & type );
};

} /* namespace abstraction */

#endif /* _XML_REGISTRY_H_ */

